# SCAMP ePortal #

Web application developed by [IRCDA](http://www.ircda.org).

Intended to be an application which collects and manages clinical data for patients and their encounters. 

## Related projects ##

[Scamps](https://bitbucket.org/jcksalem/scamps) - Library used for ePortal's templating system, database interactions, as well as various helper functions.

[ScampSDFs](https://bitbucket.org/jonathanchartrand/scamp-sdfs) - A collection of SCAMP SDF form definitions and supplementary files for supporting an SDF

[ScampsDB](https://bitbucket.org/jonathanchartrand/scampdb) - Another Library which leverages Entity Framework for database interactions. Defines model classes which are intended to reflect a strongly-typed representation of the database schema utilized by the application.