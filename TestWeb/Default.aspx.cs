﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Scamps
{
    public partial class Default : Scamps.Site
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            //Scamps.Site site = new Scamps.Site();
            New();
            string thepage = PageBuilder();
            pagehead.Text = pageheader;
            pagebody.Text = thepage;
        }
    }
}
