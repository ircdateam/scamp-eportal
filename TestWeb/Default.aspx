﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Scamps.Default" %>
<!DOCTYPE html>
<html>
<head id="documenthead" runat="server">
    <asp:Literal ID="pagehead" runat="server"></asp:Literal>
</head>
<body id="documentbody" runat="server">
    <asp:Literal ID="pagebody" runat="server"></asp:Literal>
</body>
</html>