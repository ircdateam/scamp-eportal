﻿--SQL statements stored for external access
--read using Scamps.Tools.ReadBlock(full path to this file, block name
--blocks are identified in this file using "--[blockname]--" on a new line
--

--[search.encounters.base]--
with enc as
	(
		 select 
       e.id,
       e.mrn, 
       p.lname as lastname,
       p.fname as firstname,
       p.sex as gender,
       p.dob,
       to_char((trunc(sysdate) - trunc(p.dob))/365.25) patientage,
       p.comments as patient_comments,       
       e.appt_id, 
       e.har,
       e.clinic_no,
       loc.description_txt as location_desc,
       loc.value_txt as location_val,
       null as sub_id, 
       null as subunit_seq_val, 
       en.encounter_name, 
       en.form_name, 
       en.encounter_name_short, 
       e.encounter_type, 
       coalesce(e.clinic_appt,e.event_dt_tm) as appt_date,
       e.inpt_disch,
       en.scamp_id,
       s.scamp_name,
       s.scamp_cd,
       'O' as visit_type,
       e.encounter_status,
       aes.status_text as status,
       aes.status_type,
       e.scamps_prov_id,
       e.encounter_user,
       e.update_dtm,
       trunc(sysdate - e.update_dtm) as editage,
       ap.first_name || ' ' || ap.last_name as providername,
       ap.email_addr as provider_email,
       apdc.first_name || ' ' || apdc.last_name as username,
       apdc.email_addr as user_email,
       0 as iteration,
       e.encounter_prereview,
       e.encounter_email,
       e.encounter_postreview,
       e.appt_notes,
       e.encounter_notes,
       0 as seq_val,
       e.mrn || s.scamp_cd mrsc
		 from pt_encounters e 
			  join admin_encounters en on en.encounter_id = e.encounter_type
        join pt_master p on p.mrn = e.mrn
        join scamp s on s.scamp_id = en.scamp_id
        left join admin_encounter_status aes on aes.status_id = e.encounter_status
        left join admin_provider ap on ap.scamps_prov_id = e.scamps_prov_id
        left join admin_provider apdc on apdc.scamps_prov_id = e.encounter_user
        left join admin_lookup loc on loc.id = e.clinic_no
     where encounter_cd != 'I' 
		 union all 
       select 
       v.id,
       e.mrn,
       p.lname as lastname,
       p.fname as firstname,
       p.sex as gender,
       p.dob,
	   to_char((trunc(sysdate) - trunc(p.dob))/365.25) patientage,
       p.comments as patient_comments,
       e.appt_id, 
       e.har,
       v.clinic_no,
       loc.description_txt as location_desc,
       loc.value_txt as location_val,
       v.id as sub_id, 
       v.seq_val as subunit_seq_val, 
       en.encounter_name, 
       en.form_name,        
       en.encounter_name_short,
       v.encounter_type, 
       v.date_dt as appt_date,
       e.inpt_disch,
       en.scamp_id, 
       s.scamp_name,
       s.scamp_cd,
       'I' as visit_type,
       v.encounter_status,
       aes.status_text as status,
       aes.status_type,       
       v.scamps_prov_id,
       v.encounter_user,
       v.update_dtm,
       trunc(sysdate - v.update_dtm) as editage,
       ap.first_name || ' ' || ap.last_name as providername,
       ap.email_addr as provider_email,
       apdc.first_name || ' ' || apdc.last_name as username,
       apdc.email_addr as user_email,      
       v.seq_val as iteration,
       v.encounter_prereview,
       v.encounter_email,
       v.encounter_postreview,
       v.appt_notes,
       v.encounter_notes,
       v.seq_val,
       e.mrn || s.scamp_cd mrsc
		 from pt_encounters e 
		 left join visits_inpt_subunit v on v.har = e.har 
     join pt_master p on p.mrn = e.mrn
		 join admin_encounters en on en.encounter_id = v.encounter_type 
     join scamp s on s.scamp_id = en.scamp_id
     left join admin_encounter_status aes on aes.status_id = v.encounter_status 
     left join admin_provider ap on ap.scamps_prov_id = v.scamps_prov_id
     left join admin_provider apdc on apdc.scamps_prov_id = v.encounter_user
     left join admin_lookup loc on loc.id = v.clinic_no
		 where e.encounter_cd = 'I'
	)
--[search.patients.base]--
select '[criteria]' as cat,p.* from pt_master p WHERE 

--[listing of a user's encounters that are ON_HOLD
--[dashboard.encounters.list]--
select v.appt_id, v.clinic_appt as date_tm, null as subunit_id,'Outpatient' as visit, 
initcap(p.lname) as last_name, initcap(p.fname) as first_name,v.mrn, s.scamp_name, en.encounter_name, aes.status_text, v.attending
	from pt_encounters v
  join pt_master p on v.mrn = p.mrn
  join admin_encounters en on v.encounter_type = en.encounter_id
  join scamp s on en.scamp_id = s.scamp_id
	join admin_encounter_status aes on v.encounter_status = aes.status_id
  where
	upper(v.encounter_cd) = 'O' and aes.STATUS_TYPE='ON_HOLD' [criteria]
  union select
	enc.appt_id, v.date_dt as date_tm, concat('?subunit=',v.id) as subunit_id, 'Inpatient' as visit, 
	initcap(p.lname) as last_name, initcap(p.fname) as first_name, enc.mrn, s.scamp_name, en.encounter_name, aes.status_text, v.attending
	from visits_inpt_subunit v
	join pt_encounters enc on enc.har = v.har and upper(enc.encounter_cd) = 'I'
	join pt_master p on enc.mrn = p.mrn
	join admin_encounters en on v.encounter_type = en.encounter_id
	join scamp s on en.scamp_id = s.scamp_id
	join admin_encounter_status aes on v.encounter_status = aes.status_id
	where
	aes.STATUS_TYPE='ON_HOLD'  [criteria]  

--[dashboard.encounters.lastviewed]--
select e.*,s.status_id,s.status_text,s.status_type,p.lname,p.fname,p.dob,p.sex,ae.form_name,ae.scamp_id,scamp.scamp_CD, scamp.scamp_name
	from pt_master p left join pt_encounters e on p.mrn = e.mrn 
	join admin_encounter_status s on e.encounter_status = s.STATUS_ID 
	join admin_encounters ae on ae.encounter_id = e.encounter_type
	join scamp on scamp.scamp_id = ae.scamp_id
	where [criteria]
	order by scamp.scamp_cd,e.update_dtm desc

--[dashboard.patients.lastviewed]--
select pat.* from pt_master pat
	where [criteria]
	order by update_dtm desc

--[dashboard.encounters.emailencounters]--
select v.appt_id, v.clinic_appt as date_tm, null as subunit_id, 'Outpatient' as visit, v.mrn, initcap(p.lname) as last_name, initcap(p.fname) as first_name, s.scamp_name, en.encounter_name, (case when v.clinic_appt >= trunc(sysdate+1) then 'Upcoming' when trunc(v.clinic_appt) = trunc(sysdate) then 'Today' when aes.status_type = 'IN_PROGRESS_NO_DATA' then 'Missing' else 'Incomplete' end) as status_type
	from pt_encounters v
	join pt_master p on v.mrn = p.mrn
	join admin_encounters en on v.encounter_type = en.encounter_id
	join scamp s on en.scamp_id = s.scamp_id
	join admin_encounter_status aes on v.encounter_status = aes.status_id
	where
	upper(v.encounter_cd) = 'O' and v.scamps_prov_id = [criteria]
	and exists(select id from pt_scamp_status where mrn = v.mrn and status_val in (0,1,-2)) 
	and aes.status_type in ('IN_PROGRESS_INCOMPLETE','IN_PROGRESS_INCOMPLETE_SCANNED','IN_PROGRESS_NO_DATA')
union select
	enc.appt_id, v.date_dt as date_tm, concat('?subunit=',v.id) as subunit_id, 'Inpatient' as visit, enc.mrn, initcap(p.lname) as last_name, initcap(p.fname) as first_name, s.scamp_name, en.encounter_name, (case when v.date_dt >= trunc(sysdate+1) then 'Upcoming' when trunc(v.date_dt) = trunc(sysdate) then 'Today' when aes.status_type = 'IN_PROGRESS_NO_DATA' then 'Missing' else 'Incomplete' end) as status_type
	from visits_inpt_subunit v
	join pt_encounters enc on enc.har = v.har and upper(enc.encounter_cd) = 'I'
	join pt_master p on enc.mrn = p.mrn
	join admin_encounters en on v.encounter_type = en.encounter_id
	join scamp s on en.scamp_id = s.scamp_id
	join admin_encounter_status aes on v.encounter_status = aes.status_id
	where
	v.scamps_prov_id = [criteria]
	and exists(select id from pt_scamp_status where mrn = enc.mrn and status_val in (0,1,-2))
	and aes.status_type in ('IN_PROGRESS_INCOMPLETE','IN_PROGRESS_INCOMPLETE_SCANNED','IN_PROGRESS_NO_DATA')
order by status_type, date_tm desc

-- Dashboard most recently updated encounters --
--[dashboard.encounters.recent]--
  with enc as
	(
		 select e.mrn, e.appt_id, null as sub_id, null as subunit_seq_val, 
		 en.encounter_name_short, e.encounter_type, coalesce(e.clinic_appt,e.event_dt_tm) as clinic_appt, 
		 en.scamp_id,e.encounter_status,e.scamps_prov_id, e.attending, e.encounter_user,e.update_dtm
		 from pt_encounters e 
			  join admin_encounters en on en.encounter_id = e.encounter_type where encounter_cd != 'I' 
		 union all 
		 select e.mrn, e.appt_id, v.id as sub_id, v.seq_val as subunit_seq_val, en.encounter_name_short,v.encounter_type, 
		 v.date_dt as clinic_appt, en.scamp_id, v.encounter_status,v.scamps_prov_id,v.attending, v.encounter_user,e.update_dtm
		 from pt_encounters e 
		 left join visits_inpt_subunit v on v.har = e.har 
		 join admin_encounters en on en.encounter_id = v.encounter_type 
		 where e.encounter_cd = 'I' 
	)
  select enc.*,s.status_id,s.status_text,s.status_type,p.lname,p.fname,p.dob,p.sex,ae.form_name,ae.scamp_id,scamp.scamp_CD
	from pt_master p left join  enc on p.mrn = enc.mrn 
	join admin_encounter_status s on enc.encounter_status = s.STATUS_ID 
	join admin_encounters ae on ae.encounter_id = enc.encounter_type
	join scamp on scamp.scamp_id = ae.scamp_id 
	where rownum<201 and enc.encounter_type is not null [criteria]
  and status_type not in ('COMPLETED','NOT_SCAMP_VISIT')order by enc.update_dtm desc

--[dashboard.encounters.today]--
  with enc as
	(
		 select e.mrn, e.appt_id, null as sub_id, null as subunit_seq_val, 
		 en.encounter_name_short, e.encounter_type, coalesce(e.clinic_appt,e.event_dt_tm) as appt_date, 
		 en.scamp_id,e.encounter_status,e.scamps_prov_id,e.encounter_user,e.update_dtm
		 from pt_encounters e 
			  join admin_encounters en on en.encounter_id = e.encounter_type where encounter_cd != 'I' 
		 union all 
		 select e.mrn, e.appt_id, v.id as sub_id, v.seq_val as subunit_seq_val, en.encounter_name_short,v.encounter_type, 
		 v.date_dt as appt_date, en.scamp_id, v.encounter_status,v.scamps_prov_id,v.encounter_user,e.update_dtm
		 from pt_encounters e 
		 left join visits_inpt_subunit v on v.har = e.har 
		 join admin_encounters en on en.encounter_id = v.encounter_type 
		 where e.encounter_cd = 'I' 
	)
select e.*,s.status_id,s.status_text,s.status_type,p.lname,p.fname,p.dob,p.sex,ae.form_name,ae.scamp_id,scamp.scamp_CD
	from pt_master p left join enc e on p.mrn = e.mrn 
	left join admin_encounter_status s on e.encounter_status = s.STATUS_ID 
	left join admin_encounters ae on ae.encounter_id = e.encounter_type
	join scamp on scamp.scamp_id = ae.scamp_id
	where rownum<201 and e.encounter_type is not null  
	and (appt_date between  to_date(trunc(sysdate)) and  to_date(trunc(sysdate) + 1))
	order by e.appt_date desc

--[dashboard.encounters.upcoming]--
  with enc as
	(
		 select e.mrn, e.appt_id, null as sub_id, null as subunit_seq_val, 
		 en.encounter_name_short, e.encounter_type, coalesce(e.clinic_appt,e.event_dt_tm) as appt_date, 
		 en.scamp_id,e.encounter_status,e.scamps_prov_id,e.encounter_user,e.update_dtm
		 from pt_encounters e 
			  join admin_encounters en on en.encounter_id = e.encounter_type where encounter_cd != 'I' 
		 union all 
		 select e.mrn, e.appt_id, v.id as sub_id, v.seq_val as subunit_seq_val, en.encounter_name_short,v.encounter_type, 
		 v.date_dt as appt_date, en.scamp_id, v.encounter_status,v.scamps_prov_id,v.encounter_user,e.update_dtm
		 from pt_encounters e 
		 left join visits_inpt_subunit v on v.har = e.har 
		 join admin_encounters en on en.encounter_id = v.encounter_type 
		 where e.encounter_cd = 'I' 
	)
select e.*,s.status_id,s.status_text,s.status_type,p.lname,p.fname,p.dob,p.sex,ae.form_name,ae.scamp_id,scamp.scamp_CD
	from pt_master p left join enc e on p.mrn = e.mrn 
	left join admin_encounter_status s on e.encounter_status = s.STATUS_ID 
	left join admin_encounters ae on ae.encounter_id = e.encounter_type
	join scamp on scamp.scamp_id = ae.scamp_id
	where rownum<201 and e.encounter_type is not null  
	and appt_date >= to_date(trunc(sysdate) + 1) and appt_date < to_date(trunc(sysdate) + 14)
	order by e.appt_date asc

--donut chart on the dashboard page, might want to do this by user, but not enough data on the test DB to test it
--[dashboard.encounter.chart]--
Select count(e.appt_id) c, s.status_text from pt_encounters e left join admin_encounter_status s on e.encounter_status = s.status_id
    where s.status_id is not null and clinic_appt>sysdate-120  and e.encounter_type is not null 
	group by s.STATUS_TEXT 
	order by c DESC

--select the scamp enrollment status for MRN
--[patient.scamp.enrollments]--
with enroll as (
	select s.scamp_id, s.scamp_name, ps.mrn as tmrn, ps.status_val
	from scamp s 
	left join pt_scamp_status ps on s.scamp_id = ps.scamp_id and upper(ps.mrn) = '[mrn:toupper]'
)
select e.*, 
	   case when status_val is null then -99 else status_val end as enrollstatus,
	   case when tmrn is null then '[mrn:toupper]' else tmrn end as mrn
from  enroll e 
order by scamp_name

--[admin.list.users]--
select * from admin_provider where user_name is not null and active_status='Y'  order by last_name

--[admin.list.providers]--
select * from admin_provider order by last_name

--[admin.list.locations]--
select * from admin_lookup where lookup_cd in('LOCTN_OUTPT','LOCTN_INPT') order by lookup_cd,description_txt

--[external.patient.mrn]--
select m.mrn, UPPER(p.FIRST_NAME) as fname, UPPER(p.LAST_NAME) as lname, 
p.DOB, p.SEX_CD as sex from adt.mpi m join adt.pat_demograph p on p.pi = m.pi

--[linked.encounters]--
SELECT en.APPT_ID,
  en.ENCOUNTER_CD,
  en.MRN,
  en.HAR,
  en.CLINIC_APPT,
  en.CLINIC_NO,
  en.CLINIC_NAME,
  en.ATTENDING,
  en.APPT_NOTES,
  en.APPT_STATUS,
  en.ENCOUNTER_TYPE,
  en.ENCOUNTER_NOTES,
  en.ENCOUNTER_STATUS,
  en.ENCOUNTER_PREREVIEW,
  en.UPDATE_DTM,
  ENCOUNTER_LINK.SOURCE_ID,
  ENCOUNTER_LINK.SOURCE_CD
FROM PT_ENCOUNTERS en
INNER JOIN ENCOUNTER_LINK
ON en.APPT_ID = ENCOUNTER_LINK.TARGET_ID
where ENCOUNTER_LINK.SOURCE_ID = [encounter_id]

--[sdf.pageheader.encounter]--
select p.lname,p.fname,p.dob,p.mrn, e.id,e.appt_id,e.clinic_name,coalesce(e.clinic_appt,e.event_dt_tm) as clinic_appt,to_char((trunc(coalesce(e.clinic_appt,e.event_dt_tm)) - trunc(p.dob))/365.25) age_at_appointment, 
	e.attending,e.har,e.encounter_cd,e.encounter_status,e.update_dtm,ap.full_name as dcname,app.full_name as providername
	from pt_master p 
	left join pt_encounters e on p.mrn = e.mrn 
	left join admin_provider ap on e.encounter_user = ap.scamps_prov_id  
	left join admin_provider app on app.scamps_prov_id = e.scamps_prov_id 
	where appt_id=[appt_id]

--[sdf.pageheader.subunit]--
select p.lname,p.fname,p.dob,p.mrn, sub.id,sub.id subid, e.appt_id,sub.clinic_name,sub.date_dt AS clinic_appt,to_char((trunc(sub.date_dt) - trunc(p.dob))/365.25) age_at_appointment, 
	sub.attending,sub.har,e.encounter_cd,sub.encounter_status,sub.update_dtm,ap.full_name as dcname,app.full_name as providername
	from pt_master p left join pt_encounters e on p.mrn = e.mrn 
	left join VISITS_INPT_SUBUNIT sub on e.HAR = sub.HAR 
	left join admin_provider ap on sub.encounter_user = ap.scamps_prov_id 
	left join admin_provider app on app.scamps_prov_id = sub.scamps_prov_id
	where appt_id=[appt_id]  and sub.ID= [sub_id]

--Unfortunately, batch processes get the provider NAME wrong
--every so often.  This corrects it.  The name and ID are both
--in the encounter table, which boggles the mind really.  ORACLE ONLY
--[admin.info.sync]--
UPDATE pt_encounters en SET
	en.attending = (SELECT full_name FROM admin_provider pr WHERE en.scamps_prov_id = pr.scamps_prov_id)
	WHERE en.scamps_prov_id IN (SELECT pr.scamps_prov_id FROM admin_provider pr WHERE pr.scamps_prov_id = en.scamps_prov_id)

-- I M P O R T   D A T A --
--[import.getallmrns]--
select mrn from pt_master

--[import.checkprovider]--
select id from admin_provider where lower(trim(BOTH from full_name)) = '[criteria]'

--[import.checklocation]--
select id from admin_lookup where lookup_cd in ('LOCTN_OUTPT') and UPPER(trim(BOTH from value_txt)) = '[criteria]'

--[import.checkapptid]--
select appt_id from pt_encounters where appt_id = [criteria]
 --[new.appt.id]--
 select to_char(pt_encounters_appt_id_seq.nextval) as appt_id from dual
 --[new.appt.har]--
 select to_char(pt_encounters_har_seq.nextval) as har from dual
 --[scamp.forms]--
 select s.scamp_id,s.scamp_cd,s.scamp_name,s.visible,
f.encounter_id, f.encounter_name,f.form_name,f.encounter_name_short,f.visible_flag,f.seq_num
from admin_encounters f left join scamp s
on f.scamp_id = s.scamp_id
--[scamp.patient.status]--
select s.scamp_name,s.scamp_cd,s.scamp_id,p.id,p.mrn,p.lname,p.fname,p.dob,p.sex,stat.status_val
from scamp s left join pt_scamp_status stat on s.scamp_id = stat.scamp_id
left join pt_master p on stat.mrn = p.mrn