﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Scamps.ErrorLog;

namespace Scamps
{
    public class Global : System.Web.HttpApplication
    {
        public static IErrorLog logger;
        
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            try
            {
                logger = new TextLogger();
                logger.init(this);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error during application initialization: {0}", ex.StackTrace);
            }

        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs
            var lastEx = this.Server.GetLastError();
            if (lastEx != null)
            {
                if (logger != null && Context != null)
                    logger.LogError(lastEx, this.Context, sender);
            }
            /*
			//if context or response is non-existant, handling the error
            bool noresponse = (this.Context == null || Response == null);
            bool ismissing = false;
            //get last error
            var lastEx = this.Server.GetLastError();
            this.Server.ClearError();

            //handle any HttpExceptions which may be 4xx
            if (lastEx is HttpException)
            {
                var httpE = lastEx as HttpException;
                var httpcode = httpE.GetHttpCode();
                
                if (noresponse)
                    return;
                ismissing = (httpcode >= 400 && httpcode < 500);
            }

            ///TODO : log error here

            if (noresponse)
                return;
            if (ismissing)
            {
                Context.Response.Redirect("~/404");
                return;
            }

            Response.StatusCode = 500;
            var lastpath = "";
            if (this.Context.Items.Contains("path"))
                lastpath = string.Concat("?ReturnUrl=", HttpUtility.UrlEncode(this.Context.Items["path"].ToString()));
            Context.Response.Redirect(string.Concat("~/500.html", lastpath));
            */
        }

        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started

        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }

        void Application_BeginRequest()
        {
            string url = HttpContext.Current.Request.Url.PathAndQuery;
            Uri originalUrl = null;
            string[] urlInfo404 = null;

            if (url.StartsWith("/404.aspx"))
            {

                urlInfo404 = Request.Url.Query.ToString().Split(';');
                if (urlInfo404.Length > 1 && Uri.TryCreate(urlInfo404[1], UriKind.RelativeOrAbsolute, out originalUrl))
                {
                    string[] urlParts = originalUrl.PathAndQuery.Split('?');
                    string requestedFile = urlParts[0];
                    string queryString = string.Empty;
                    if (urlParts.Length > 1)
                        queryString = urlParts[1];

                    if (requestedFile.IndexOf(".") > 0)
                    {
                        Context.Items["PageUrl"] = requestedFile;
                        // There's some extension, so this is not an extensionless URL.
                        // Don't handle such URL because these are really missing files
                        //not needed because if i dont rewrite the url then it will pick up the 404 page
                        HttpContext.Current.RewritePath("Four04.aspx", true);
                    }
                    else
                    {
                        // Extensionless URL. Use your URL rewriting logic to handle such URL
                        // I will just add .aspx extension to the extension less URL.
                        requestedFile += ".aspx";
                        if (queryString.Length > 0)
                        {
                            requestedFile += "?" + queryString;
                        }
                        HttpContext.Current.RewritePath(requestedFile,true);
                    }
                }

            }
            else if (url.IndexOf(".") == -1)
            {
                string[] urlParts = url.Split('?');
                string requestedFile = urlParts[0];
                string queryString = string.Empty;
                if (urlParts.Length > 1)
                {
                    queryString = Request.QueryString.ToString();
                }
                requestedFile += ".aspx";
                if (queryString.Length > 0)
                {
                    requestedFile += "?" + queryString;
                }
                HttpContext.Current.RewritePath(requestedFile, true);
            }
        }


    }
}
