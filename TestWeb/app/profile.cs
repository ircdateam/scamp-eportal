﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Profile;
using System.Configuration;
using ScampsDB.Provider;

namespace TestWeb.app
{
    public static class ProviderProfile
    {
        public static Dictionary<string,string> getProfile(string key, string value, string[] columns){
            return ProfileMethods.getProfile(key, value, columns);
        }
    }
}