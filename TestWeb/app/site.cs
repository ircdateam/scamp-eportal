﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Data;
using System.IO;
using System.Reflection;
using ExtensionMethods;

namespace Scamps
{
    public class Site : System.Web.UI.Page
    {

        public bool authoring_active = true;

        public string Language = "";
        public string pagecontents = "";
        public string pageheader = "";
        public string pagetitle = "";
        public long appointment = 0;
        public string identifier = "";
        public int id = 0;
        public string pagetemplate = "";
        public string innertemplate = "";
        public string action = "";
        public List<string> path = new List<string>();
        
        public Dictionary<string, string> PageData = new Dictionary<string, string>();
        public Dictionary<string, string> Parameters = new Dictionary<string, string>();
        public Dictionary<string, string> QueryString = new Dictionary<string, string>();
        public Dictionary<string, string> Session = new Dictionary<string, string>();
        public Dictionary<string, string> FormData = new Dictionary<string, string>();
        
        //externally called methods can populate this dictionary with method results
        public Dictionary<string, string> Results = new Dictionary<string, string>();

        public HttpContext con = HttpContext.Current;
        private string _connectionstring = "";
        private string _provider = "";
        private string userrole = "";
        private int userkeys = 0;

        //private int RED = 100;
        //private int GREEN = 160;
        //private int BLUE = 255;

        public static string Version
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        public string ConnectionString
        {
            get {
                if (_connectionstring.IsNullOrEmpty())
                {
                    _connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["SCAMPs"].ToString();
                }
                return _connectionstring;
            }
            set {
                _connectionstring = value;
            }
        }

        public string Provider
        {
            get
            {
                if (_provider.IsNullOrEmpty())
                {
                    _provider = System.Configuration.ConfigurationManager.ConnectionStrings["SCAMPs"].ToString();
                }
                return _provider;
            }
            set
            {
                _provider = value;
            }
        }

        protected void Base_PreInit(object sender, System.EventArgs e)
        {
            EnableViewState = false;
        }


        public void New()
        {
            init();
        }

        public void init()
        {
            var csettings = System.Configuration.ConfigurationManager.ConnectionStrings["SCAMPs"];
            ConnectionString = csettings.ConnectionString;
            Provider = csettings.ProviderName;

            if (con.Items.Contains("path"))
            {
                string[] aS = con.Items["path"].ToString().Split('/');
                for (int i = 0; i < aS.Length; i++)
                {
                    path.Add(aS[i]);
                }
            }
            QueryString.ParseQueryString(con.Request.QueryString.ToString());
            //ZAPHOD - alternate method to parse a query string as a dictionary extension (above)
            //Scamps.Tools.QSParse(ref QueryString, con.Request.QueryString.ToString());
        }
        protected void Base_Init(object sender, System.EventArgs e)
        {

        }


        protected void Base_LoadComplete(object sender, System.EventArgs e)
        {

        }

        protected void Base_PreRender(object sender, System.EventArgs e)
        {

        }

        protected void Page_Unload(object sender, System.EventArgs e)
        {
            try
            {
                Parameters.Clear();

            }
            catch (Exception ex)
            {

            }

        }

        public bool ConnectionExists(string Name)
        {
            bool ok = false;
            try
            {
                if (!System.Configuration.ConfigurationManager.ConnectionStrings[Name].ToString().IsNullOrEmpty())
                {
                    ok = true;
                }
            }
            catch (Exception ex)
            {
                //log this?  Probably not
            }
            return ok;
        }
        public void SetPageData(string zone,string id)
        {
            string sql = "";

            //if ID=0, a "new" item, nothing to get...
            if (Tools.AsInteger(id) == 0)
            {
                if (!FormData.ContainsKey("_isnew")) { FormData.Add("_isnew", "true"); }
            }
            else
            {
                switch(zone)
                {
                    case "users":
                    case "user":
                    case "provider":
                    case "providers":
                        sql = "select * from admin_provider where scamps_prov_id=" + Scamps.Tools.AsLong(id).ToString();
                        break;
                    case "locations":
                        sql = "select * from admin_lookup where id=" + Scamps.Tools.AsLong(id).ToString();
                        break;
                }
                Scamps.DataTools db = new Scamps.DataTools();
                db.Provider=Provider;
                if(sql.Length>0)
                {
                    if(db.OpenConnection(ConnectionString))
                    {
                        db.GetResultSet(sql);
                        FormData = db.GetRowStrings();
                    }
                }
                db.Dispose();
            }
            if (FormData.ContainsKey("_recordid"))
            {
                FormData["_recordid"] = id;
            }
            else
            {
                FormData.Add("_recordid", id);
            }
        }

        public string PageBuilder()
        {

            if (Tools.FileExists(con.Server.MapPath("/resources/initial.dbt"))) { First_Run(); }


            string results = "";
            string pagetype = "";
            string zone = "";
            string target = "";
            string name = "";

            GetSiteConfig();

            if (ConnectionExists("ExternalEntities")) { Tools.AddOrReplace("externalconnection", "true", ref Parameters); }
            string domainlist = GetConfigKey("LDAPdomain");
            if (!domainlist.IsNullOrEmpty()) { 
                Tools.AddOrReplace("domainlogin", "yes", ref Parameters);
                Tools.AddOrReplace("domainlist", domainlist, ref Parameters);
                Tools.AddOrReplace("defaultdomain", GetConfigKey("LDAPauthgroupdomain"), ref Parameters); 
            }

            Scamps.Templating st = new Scamps.Templating();
            st.BasePath = HttpContext.Current.Server.MapPath("/");
            st.Language = Language;
            st.UserKeys = userkeys;
            st.UserRole = userrole;

            string qs = HttpContext.Current.Request.QueryString.ToString();

            pagetemplate = con.Items["pagetemplate"].ToString();
            innertemplate = con.Items["innertemplate"].ToString();

            if (con.Items.Contains("zone")) { zone = con.Items["zone"].ToString(); }
            if (con.Items.Contains("target")) { target = con.Items["target"].ToString(); } 
            if(con.Items.Contains("appointment")) { appointment = Scamps.Tools.AsLong(con.Items["appointment"]); }
            if(con.Items.Contains("identifier")) { identifier = con.Items["identifier"].ToString(); }
            if(con.Items.Contains("action")) { action = con.Items["action"].ToString(); }
            if (con.Items.Contains("pagetype")) { pagetype = con.Items["pagetype"].ToString(); }
            if (con.Items.Contains("name")) { name = con.Items["name"].ToString(); }
            Parameters.Add("page.zone", zone);
            Parameters.Add("page.target", target);
            Parameters.Add("page.appointment", appointment.ToString());
            Parameters.Add("page.identifier", identifier);
            Parameters.Add("page.action", action);
            Parameters.Add("page.type", pagetype);
            Parameters.Add("page.name", name);


            st.Provider = "oracle";
            st.ConnectionString = ConnectionString;

            // testing 1,2,3
            if (pagetemplate == "page\\scampsdf")
            {
                pagetemplate = st.ResolveTemplate(pagetemplate);
                if (pagetemplate.IndexOf("[#function@ScampForm/]") > 0)
                {
                    innertemplate = ScampForm();
                    pagetemplate = pagetemplate.Replace("[#function@ScampForm/]", innertemplate);
                }
            }

            //do we need to fetch data?
            if (pagetype == "edit")
            {
                SetPageData(target, identifier);
            }

            if (zone.ToLower() == "find")   // !! zone.ToLower() == "search"
            {
                results = SearchResults(QueryString.Get("search"));
                PageData.AddOrReplace("results", results);
            }

            // == pass in all page data ===
            st.data = PageData;
            st.session = Session;
            st.qs = QueryString;
            st.parameters = Parameters;
            st.formdata = FormData;

            if (pagetype == "listing")
            {
                innertemplate = st.Results(innertemplate,PageData);
            }
            

            results = st.WebPage(pagetemplate,innertemplate);
            results = Functions(results);

            if(pagetitle.Length==0 && Parameters.ContainsKey("page.title")) { pagetitle = Parameters["page.title"]; }
            if (pagetitle.Length == 0 && PageData.ContainsKey("pagetitle")) { pagetitle = PageData["pagetitle"]; }
            if (pagetitle.Length == 0) { pagetitle = "SCAMP ePortal"; }
            Page.Title = pagetitle;
            

            results = st.Format(results, Parameters);
            results = st.Format(results, PageData);
            results = st.Format(results, Session);
            results = st.Format(results, QueryString);

            pagecontents = results;

            pageheader = st.Page.Head;
            pageheader = st.Format(pageheader, Parameters);
            pageheader = st.Format(pageheader, PageData);

            st.Dispose();

            return results;
        }

        public struct searchterms
        {
            public  bool like;
            public  string term;
            public  string join;
            public  string hint;
            public bool stopword;

        }

        /// <summary>
        /// Based on search string, return matching patients and/or encounters
        /// </summary>
        /// <param name="searchstring">the raw search string, which may contain wildcards</param>
        /// <returns>String: fully formatted HTML results</returns>
        public string SearchResults(string searchstring = "")
        {
            string results = "";
            if (searchstring.IsNullOrEmpty()) { searchstring = QueryString.Get("search"); }

            searchstring = Server.UrlDecode(searchstring);

            searchstring = searchstring.Replace("+", " ");
            searchstring = searchstring.Replace(":", " :");  // hints should be padded

            // process the search terms into an array of terms + an array of term types
            searchstring = Tools.NormalString(searchstring);
            PageData.AddOrReplace("searchterms", searchstring);

            // deal with quoted terms...
            string[] aZ = new string[1];
            searchstring = Tools.CollapseString(searchstring, ref aZ);

            string[] terms = searchstring.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < terms.Length; i++)
            {
                terms[i] = Tools.ExpandString(terms[i], aZ, true);
            }

            int c = terms.Length;

            searchterms[] searchitems = new searchterms[c];
            Dictionary<string, object> queryparams = new Dictionary<string, object>();

            //string[] modifiers = new string[c];

            // clean up the search terms
            for (int i = 0; i < terms.Length; i++)
            {
                
                terms[i] = terms[i].Trim();
                searchitems[i].like = false;
                // protect wildcard escaped literals...
                // doing it this way to be more in line with SQL Server
                terms[i] = terms[i].Replace("[*]", "-[~any~]-");
                terms[i] = terms[i].Replace("[?]", "-[~one~]-");

                //wildcards...
                terms[i] = terms[i].Replace("*", "%");
                terms[i] = terms[i].Replace("?", "_");

                // reset literals
                terms[i] = terms[i].Replace("-[~any~]-", "*");
                terms[i] = terms[i].Replace("-[~one~]-", "?");

                if (terms[i].IndexOf("%") != -1 || terms[i].IndexOf("_") != -1)
                {
                    searchitems[i].like = true;
                }
                searchitems[i].hint = "word";
                searchitems[i].term = terms[i].ToUpper();
                searchitems[i].stopword = false;
                if (terms[i].StartsWith(":"))
                {
                    // this is a search hint
                    // we only understand a few hints
                    // so let's make sure this is one of them
                    terms[i] = Tools.NotStartWith(terms[i], ":");
                    searchitems[i].term = terms[i].ToUpper();
                    searchitems[i].join = "NONE";
                    searchitems[i].stopword = true;
                    switch (searchitems[i].term)
                    {
                        case "P":
                        case "PAT":
                        case "PATIENT":
                        case "PATIENTS":
                            searchitems[i].hint = "patient";

                            break;

                        case "E":
                        case "APP":
                        case "APPT":
                        case "APPOINTMENT":
                        case "A":
                        case "ENC":
                        case "ENCOUNTER":

                            searchitems[i].hint = "appt";
                            break;

                        case "S":
                        case "SCAMP":

                            searchitems[i].hint = "scamp";
                            break;

                        case "F":
                        case "FORM":

                            searchitems[i].hint = "form";
                            break;

                        default:
                            // this is not a hint we know about
                            // let it be a regular search term
                            break;
                    }
                }
                else

                if (searchitems[i].hint != "hint")
                {
                    if (searchitems[i].term == "OR")
                    {
                        // modifier to specify SQL OR condition
                        searchitems[i].hint = "OR";
                        searchitems[i].stopword = true;
                    }
                    if (searchitems[i].term == "AND")
                    {
                        // modifier to specify SQL AND condition
                        searchitems[i].hint = "AND";
                        searchitems[i].stopword = true;
                    }
                    if (terms[i].HasDigits() && (!terms[i].hasPunctuation() || searchitems[i].like))
                    {
                        // either number or alphanumeric identifier
                        if (terms[i].hasLetters())
                        {
                            searchitems[i].hint = "alphanumeric";
                        }
                        else
                        {
                            searchitems[i].hint = "number";
                        }
                    }
                    if (terms[i].IsOnly("0123456789-/%_ ")  && searchitems[i].hint != "number")
                    {
                        searchitems[i].hint = "datepart";
                    }

                    if (searchitems[i].hint != "datepart" && Tools.IsDate(terms[i]))
                    {
                        // dates.  I hate dates when dealing with ORACLE
                        searchitems[i].hint = "date";
                    }
                }
                if(searchitems[i].join != "NONE" && !searchitems[i].stopword)
                {
                    
                    switch (searchitems[i].hint)
                    {
                        case "date":
                            queryparams.AddOrReplace("p" + i.ToString(), Tools.AsDateDate(searchitems[i].term));
                            break;

                        case "number":

                            if(Tools.AsInteger(searchitems[i].term) == 0)
                            {
                                // appt ids are LONGS, because that is how Dave rolls...
                                // actually, they all need to be stirngs because of oracle
                                queryparams.AddOrReplace("p" + i.ToString(), Tools.AsLong(searchitems[i].term).ToString());
                            }
                            else
                            {
                                queryparams.AddOrReplace("p" + i.ToString(), Tools.AsInteger(searchitems[i].term).ToString());
                            }
                            break;

                        default:
                            queryparams.AddOrReplace("p" + i.ToString(), searchitems[i].term);
                            break;
                    }
                }
            }

            // assuming no errors, let's assemble our working query parts
            // then add in each result group
            // start with patients, then encounters
            string sql = "";
            string select = "";
            string tmp = "";
            string pat_sql = Tools.ReadBlock(con.Server.MapPath("/resources/data/scampsweb.sql"), "search.patients.base") + " ";
            string enc_sql = Tools.ReadBlock(con.Server.MapPath("/resources/data/scampsweb.sql"), "search.encounters.base");
            string enc_where = "select '[criteria]' as zone, enc.* from enc where ";

            Scamps.DataTools db = new Scamps.DataTools();
            db.Provider = Provider;
            db.ConnectionString = ConnectionString;
            db.OpenConnection();

            Scamps.Templating t = new Scamps.Templating();
            t.Provider = Provider;
            t.Language = Language;
            t.ConnectionString = ConnectionString;
            t.BasePath = con.Server.MapPath("/");
            t.qs = QueryString;
            t.parameters = Parameters;

            // first use of hints, dirty but proof of concept...
            if (searchstring.ToLower().IndexOf(":p") != -1  || searchstring.ToLower().IndexOf(":e") == -1)
            {

                // ===  patient MRN criteria  ===
                // - only match beginning and end of MRN, min length 3 characters
                select = pat_sql.Replace("[criteria]", "MRN");
                tmp = "";
                for (int i = 0; i < terms.Length; i++)
                {
                    if (!searchitems[i].stopword && searchitems[i].term.Length > 2 && (searchitems[i].hint == "word" || searchitems[i].hint == "alphanumeric" || searchitems[i].hint == "number"))
                    {
                        if (searchitems[i].like)
                        {
                            tmp += "UPPER(p.mrn) like @p" + i.ToString() + " OR ";
                        }
                        else
                        {
                            tmp += "UPPER(p.mrn) = @p" + i.ToString() + " OR ";
                        }
                    }
                }
                tmp = tmp.Trim();
                tmp = Tools.NotEndWith(tmp, "OR");
                if (!tmp.IsNullOrEmpty())
                {
                    tmp = "\t(" + tmp.Trim() + ")" + Environment.NewLine + "UNION ALL ";
                    sql = sql + select + Environment.NewLine + tmp + Environment.NewLine;
                }


                // === patient first and last name criteria ===
                // name has to begin with the search criteria
                select = pat_sql.Replace("[criteria]", "Name");
                tmp = "";

                for (int i = 0; i < terms.Length; i++)
                {
                    if (!searchitems[i].stopword && searchitems[i].hint == "word")
                    {
                        if (searchitems[i].term.IndexOf(" ") == -1)
                        {
                            if (searchitems[i].like)
                            {
                                tmp += "(UPPER(p.lname) like @p" + i.ToString() + " OR UPPER(p.fname) like @p" + i.ToString() + ") OR " + Environment.NewLine;
                            }
                            else
                            {
                                tmp += "(UPPER(p.lname) =  @p" + i.ToString() + " OR UPPER(p.fname) = @p" + i.ToString() + ") OR " + Environment.NewLine;
                            }
                        }
                        else
                        {
                            if (searchitems[i].like)
                            {
                                tmp += "UPPER(p.lname) like @p" + i.ToString() + " OR " + Environment.NewLine;
                                tmp += "UPPER(p.fname) like @p" + i.ToString() + " OR " + Environment.NewLine;
                                tmp += "UPPER(p.fname) || ' ' || UPPER(p.lname) like @p" + i.ToString() + " OR " + Environment.NewLine;
                            }
                            else
                            {
                                tmp += "UPPER(p.lname) =  @p" + i.ToString() + " OR " + Environment.NewLine;
                                tmp += "UPPER(p.fname) =  @p" + i.ToString() + " OR " + Environment.NewLine;
                                tmp += "UPPER(p.fname) || ' ' || UPPER(p.lname) =  @p" + i.ToString() + " OR " + Environment.NewLine;
                            }
                        }
                    }
                }
                tmp = tmp.Trim();
                tmp = Tools.NotEndWith(tmp, "OR");
                if (!tmp.IsNullOrEmpty())
                {
                    tmp = "\t(" + tmp.Trim() + ")" + Environment.NewLine + "UNION ALL ";
                    sql = sql + select + Environment.NewLine + tmp + Environment.NewLine;
                }

                // patient notes criteria
                // searching notes requires that a search term be a wild card
                select = pat_sql.Replace("[criteria]", "Comments");
                tmp = "";
                for (int i = 0; i < terms.Length; i++)
                {
                    if (!searchitems[i].stopword && searchitems[i].like && searchitems[i].term.Length > 3 && searchitems[i].hint == "word")
                    {
                        tmp += "UPPER(p.comments) like @p" + i.ToString() + " OR " + Environment.NewLine;
                    }
                }

                tmp = tmp.Trim();
                if (!tmp.IsNullOrEmpty())
                {
                    tmp = Tools.NotEndWith(tmp, "OR");
                    tmp = "\t(" + tmp + ")" + Environment.NewLine + "UNION ALL ";
                    sql = sql + select + Environment.NewLine + tmp + Environment.NewLine;
                }

                // patient DOB criteria
                // TODO: multiple dates need to be handled together...
                select = select = pat_sql.Replace("[criteria]", "DOB");
                tmp = "";
                for (int i = 0; i < terms.Length; i++)
                {
                    if (!searchitems[i].stopword && searchitems[i].hint == "date")
                    {
                        tmp += "dob = TO_DATE('" + Tools.AsDate(searchitems[i].term) + "') OR ";
                    }
                }

                tmp = tmp.Trim();
                if (!tmp.IsNullOrEmpty())
                {
                    tmp = Tools.NotEndWith(tmp, "OR");
                    tmp = "\t(" + tmp + ")" + Environment.NewLine + "UNION ALL ";
                    sql = sql + select + Environment.NewLine + tmp + Environment.NewLine;
                }

                // patient DOB by date parts criteria
                select = pat_sql.Replace("[criteria]", "Partial DOB");
                tmp = "";
                for (int i = 0; i < terms.Length; i++)
                {
                    if (!searchitems[i].stopword && (searchitems[i].hint == "number" || searchitems[i].hint == "datepart"))
                    {
                        tmp += "TO_CHAR(p.dob,'yyyy-mm-dd') like '%" + searchitems[i].term + "%' OR " + Environment.NewLine;
                    }
                }

                tmp = tmp.Trim();
                if (!tmp.IsNullOrEmpty())
                {
                    tmp = Tools.NotEndWith(tmp, "OR");
                    tmp = "\t(" + tmp + ")" + Environment.NewLine + "UNION ALL ";
                    sql = sql + select + Environment.NewLine + tmp + Environment.NewLine;
                }

                sql = sql.Trim();

                sql = Tools.NotEndWith(sql, "UNION ALL");
                pat_sql = sql;


                // run the resulting search(s)
                db.GetResultSet(pat_sql, queryparams);
                if (!pat_sql.IsNullOrEmpty())
                {

                    tmp = t.Results(db.ResultSet, t.TemplatePath + "search-patients.dbt");
                }
                results = Environment.NewLine + tmp;

            }
            // =============  E N C O U N T E R   S E A R C H ===============

            if (searchstring.ToLower().IndexOf(":e") != -1  || searchstring.ToLower().IndexOf(":p") == -1)
            {
                tmp = "";
                sql = "";

                // ===  encounter MRN criteria  ===
                // - only match beginning and end of MRN, min length 3 characters
                select = "";
                tmp = "";
                for (int i = 0; i < terms.Length; i++)
                {
                    if (!searchitems[i].stopword && searchitems[i].term.Length > 2 && (searchitems[i].hint == "word" || searchitems[i].hint == "alphanumeric" || searchitems[i].hint == "number"))
                    {
                        if (searchitems[i].like)
                        {
                            tmp += "UPPER(enc.mrn) like @p" + i.ToString() + " OR ";
                        }
                        else
                        {
                            tmp += "UPPER(enc.mrn) = @p" + i.ToString() + " OR ";
                        }
                    }

                }
                tmp = tmp.Trim();
                tmp = Tools.NotEndWith(tmp, "OR");
                if (!tmp.IsNullOrEmpty())
                {
                    //tmp = "\t(" + tmp.Trim() + ")" + Environment.NewLine + "UNION ALL ";
                    //sql = sql + select + Environment.NewLine + tmp + Environment.NewLine;

                    tmp = "\t(" + tmp.Trim() + ")" + Environment.NewLine;
                    sql = sql + select + Environment.NewLine + tmp;

                }


                // === encounter first and last name criteria ===
                // name has to begin with the search criteria
                if (sql.IsNullOrEmpty())
                { select = ""; }
                else { select = "OR"; }

                tmp = "";

                for (int i = 0; i < terms.Length; i++)
                {
                    if (!searchitems[i].stopword &&(searchitems[i].hint == "word"))
                    {
                        if (searchitems[i].term.IndexOf(" ") == -1)
                        {
                            if (searchitems[i].like)
                            {
                                tmp += "(UPPER(enc.lastname) like @p" + i.ToString() + " OR UPPER(enc.firstname) like @p" + i.ToString() + ") OR " + Environment.NewLine;
                            }
                            else
                            {
                                tmp += "(UPPER(enc.lastname) =  @p" + i.ToString() + " OR UPPER(enc.firstname) = @p" + i.ToString() + ") OR " + Environment.NewLine;
                            }
                        }
                        else
                        {
                            if (searchitems[i].like)
                            {
                                tmp += "UPPER(enc.firstname) || ' ' || UPPER(enc.lastname) like @p" + i.ToString() + " OR " + Environment.NewLine;
                            }
                            else
                            {
                                tmp += "UPPER(enc.firstname) || ' ' || UPPER(enc.lastname) =  @p" + i.ToString() + " OR " + Environment.NewLine;
                            }
                        }
                    }
                }
                tmp = tmp.Trim();
                tmp = Tools.NotEndWith(tmp, "OR");
                if (!tmp.IsNullOrEmpty())
                {
                    tmp = "\t(" + tmp.Trim() + ")" + Environment.NewLine;
                    sql = sql + select + Environment.NewLine + tmp;
                }

                // ===  encounter SCAMP criteria  ===
                // - at least 2 character search
                if (sql.IsNullOrEmpty())
                { select = ""; }
                else { select = "OR"; }

                tmp = "";
                for (int i = 0; i < terms.Length; i++)
                {
                    if (!searchitems[i].stopword && searchitems[i].term.Length > 1 && (searchitems[i].hint == "word" || searchitems[i].hint == "alphanumeric"))
                    {
                        if (searchitems[i].like)
                        {
                            tmp += "UPPER(enc.scamp_name) like @p" + i.ToString() + " OR UPPER(enc.scamp_cd) like @p" + i.ToString() + " OR ";
                        }
                        else
                        {
                            tmp += "UPPER(enc.scamp_name) = @p" + i.ToString() + " OR UPPER(enc.scamp_cd) = @p" + i.ToString() + " OR ";
                        }
                    }
                }
                tmp = tmp.Trim();
                tmp = Tools.NotEndWith(tmp, "OR");
                if (!tmp.IsNullOrEmpty())
                {
                    tmp = "\t(" + tmp.Trim() + ")" + Environment.NewLine;
                    sql = sql + select + Environment.NewLine + tmp;
                }

                // ===  encounter SDF Name criteria  ===
                // - at least 2 character search
                if (sql.IsNullOrEmpty())
                { select = ""; }
                else { select = "OR"; }

                tmp = "";
                for (int i = 0; i < terms.Length; i++)
                {
                    if (!searchitems[i].stopword && searchitems[i].term.Length > 1 && (searchitems[i].hint == "word" || searchitems[i].hint == "alphanumeric"))
                    {
                        if (searchitems[i].like)
                        {
                            tmp += "UPPER(enc.encounter_name_short) like @p" + i.ToString() +  " OR ";
                        }
                        else
                        {
                            tmp += "UPPER(enc.encounter_name_short) = @p" + i.ToString() + " OR ";
                        }
                    }
                }
                tmp = tmp.Trim();
                tmp = Tools.NotEndWith(tmp, "OR");
                if (!tmp.IsNullOrEmpty())
                {
                    tmp = "\t(" + tmp.Trim() + ")" + Environment.NewLine;
                    sql = sql + select + Environment.NewLine + tmp;
                }

                // ===  HAR criteria  ===
                // - at least 2 character search
                if (sql.IsNullOrEmpty())
                { select = ""; }
                else { select = "OR"; }

                tmp = "";
                for (int i = 0; i < terms.Length; i++)
                {
                    if (!searchitems[i].stopword && searchitems[i].term.Length > 1 && (searchitems[i].hint == "word" || searchitems[i].hint == "alphanumeric"))
                    {
                        if (searchitems[i].like)
                        {
                            tmp += "UPPER(enc.har) like @p" + i.ToString() + " OR ";
                        }
                        else
                        {
                            tmp += "UPPER(enc.har) = @p" + i.ToString() + " OR ";
                        }
                    }
                }
                tmp = tmp.Trim();
                tmp = Tools.NotEndWith(tmp, "OR");
                if (!tmp.IsNullOrEmpty())
                {
                    tmp = "\t(" + tmp.Trim() + ")" + Environment.NewLine;
                    sql = sql + select + Environment.NewLine + tmp;
                }

                // ===  Appt ID criteria  ===
                // must be a number
                if (sql.IsNullOrEmpty())
                { select = ""; }
                else { select = "OR"; }

                tmp = "";
                for (int i = 0; i < terms.Length; i++)
                {
                    if (!searchitems[i].stopword && searchitems[i].term.Length > 1 && (searchitems[i].hint == "number"))
                    {
                        if (searchitems[i].like)
                        {
                            tmp += "to_char(enc.appt_id) like @p" + i.ToString() + " OR ";
                        }
                        else
                        {
                            tmp += "to_char(enc.appt_id) = @p" + i.ToString() + " OR ";
                        }
                    }
                }
                tmp = tmp.Trim();
                tmp = Tools.NotEndWith(tmp, "OR");
                if (!tmp.IsNullOrEmpty())
                {
                    tmp = "\t(" + tmp.Trim() + ")" + Environment.NewLine;
                    sql = sql + select + Environment.NewLine + tmp;
                }


                // === Provider or DC Criteria ===
                // name has to begin with the search criteria
                if (sql.IsNullOrEmpty())
                { select = ""; }
                else { select = "OR"; }

                tmp = "";

                for (int i = 0; i < terms.Length; i++)
                {
                    if (!searchitems[i].stopword && searchitems[i].hint == "word")
                    {
                        if (searchitems[i].like)
                        {
                            tmp += "(UPPER(enc.providername) like @p" + i.ToString() + " OR UPPER(enc.username) like @p" + i.ToString() + ") OR " + Environment.NewLine;
                        }
                        else
                        {
                            tmp += "(UPPER(enc.providername) =  @p" + i.ToString() + " OR UPPER(enc.username) = @p" + i.ToString() + ") OR " + Environment.NewLine;
                        }
                    }
                }
                tmp = tmp.Trim();
                tmp = Tools.NotEndWith(tmp, "OR");
                if (!tmp.IsNullOrEmpty())
                {
                    tmp = "\t(" + tmp.Trim() + ")" + Environment.NewLine;
                    sql = sql + select + Environment.NewLine + tmp;
                }


                // finall assembly
                sql = sql.Trim();

                sql = Tools.NotEndWith(sql, "UNION ALL");
                sql = enc_where.Replace("[criteria]", "ENCOUNTER") + Environment.NewLine + sql;
                enc_sql = enc_sql + Environment.NewLine + sql;
                enc_sql += Environment.NewLine + "order by lastname,firstname,mrn,scamp_name,appt_date,har,appt_id";

                // execute search
                db.GetResultSet(enc_sql, queryparams);



                if (!enc_sql.IsNullOrEmpty())
                {
                    tmp = t.Results(db.ResultSet, t.TemplatePath + "search-encounters.dbt");
                }
                results += Environment.NewLine + tmp;
            }


            t.Dispose();
            db.Dispose();

            return results;
        }
        public void SetConfig(string attribute, string value)
        {
            attribute = attribute.ToUpper();
            Scamps.DataTools db = new Scamps.DataTools();
            db.Provider = "oracle";
            db.ConnectionString = ConnectionString;
            Dictionary<string, string> hd = new Dictionary<string, string>();
            hd.Set("ATTRIB", attribute);
            hd.Set("ATTRIB_VAL", value);

            string sql = "select id from admin_config where attrib = '" + attribute + "'";
            if (Tools.AsInteger(db.GetValue(sql)) == 0)
            {
                //new attribute...
                db.Insert(hd, "admin_config");
            }
            else
            {
                //existing update
                db.Update(hd, "admin_config","ATTRIB = '" + attribute + "'");
            }
            if (db.haserrors)
            {
                //log error here...
                
            }
            db.Clear();
            db.Dispose();
            hd.Clear();

        }
        public string Functions(string source)
        {
            //resolve functon tags and modify source appropriately...
            string results = source;
            string b = "";

            //functions
            b = Tools.StringPart(results, "[#function@", "/]");
            while (!(b.Length == 0))
            {
                results = results.Replace("[#function@" + b + "/]", Function(b));
                b = Tools.StringPart(results, "[#function@", "/]");
            }

            b = Tools.StringPart(results, "[#form@", "/]");
            while (!(b.Length == 0))
            {
                results = results.Replace("[#form@" + b + "/]", Form(b));
                b = Tools.StringPart(results, "[#form@", "/]");
            }

            return results;

        }
        public string Function(string source)
        {
            string results = "";
            string method = "";
            string options = "";
            string sql = "";
            string criteria = "";

            Scamps.Templating t = new Scamps.Templating();
            t.Provider = Provider;
            t.Language = Language;
            t.ConnectionString = ConnectionString;
            t.BasePath = con.Server.MapPath("/");
            t.qs = QueryString;
            t.parameters = Parameters;

            string provider_id = UserID();
            string provider_type = userRole();

                //"100478";  //smith, elizabeth

            Scamps.Tools.QSSplit(ref source,ref method,ref options);
            Scamps.Tools.QSParse(ref Parameters, options);

            switch (method.ToLower())
            {

                // --- any special handlers not requiring a full method can go here --- 

                case "testforms":

                    results = TestForms();
                    break;


                case "buttonnavigation":
                    results = buttonNavigation();
                    break;
                case "emailuserencounters":

                    if (provider_type != "PROVIDER")
                        break;
                    sql = Scamps.Tools.ReadBlock(con.Server.MapPath("/resources/data/scampsweb.sql"), "dashboard.encounters.emailencounters");
                    criteria = provider_id;
                    sql = sql.ReplaceToken("criteria", criteria);
                    results = t.Results(sql, "user_emailencounters");
                    break;

                case "userencounters":
                    //SDF's on Hold (v1.3 replacing 45 days)
                    sql = Scamps.Tools.ReadBlock(con.Server.MapPath("/resources/data/scampsweb.sql"),"dashboard.encounters.list");
                    criteria = " and (v.scamps_prov_id = " + provider_id + " OR v.encounter_user = " + provider_id + ")";
                    sql = sql.Replace("[criteria]", criteria);

                    results = t.Results(sql,"user_encounter_list");
                    break;

                case "providerencounterstoday":
                    
                    //if (provider_type != "PROVIDER")
                    //    break;
                    sql = Scamps.Tools.ReadBlock(con.Server.MapPath("/resources/data/scampsweb.sql"), "dashboard.encounters.today");
                    criteria = " and (scamps_prov_id = " + provider_id + "OR encounter_user = " + provider_id + ")";
                    sql = sql.Replace("[criteria]", criteria);
                    results = t.Results(sql, "user_encounter_now");
                    
                    break;

                case "providerencountersupcoming":
                    
                   // if (provider_type == "PROVIDER")
                   // {
                        sql = Scamps.Tools.ReadBlock(con.Server.MapPath("/resources/data/scampsweb.sql"), "dashboard.encounters.upcoming");
                        criteria = " and (scamps_prov_id = " + provider_id + " OR encounter_user = " + provider_id + ")";
                        sql = sql.Replace("[criteria]", criteria);
                        results = t.Results(sql, "user_encounter_upcoming");
                   // }
                    break;
                 case "recentencounters":
                    sql = Scamps.Tools.ReadBlock(con.Server.MapPath("/resources/data/scampsweb.sql"),"dashboard.encounters.recent");

                    //set the view context - if the user is a DC or anyone else...
                    if (provider_type == "DC")
                    {
                        criteria = " and encounter_user = " + provider_id;
                    }
                    else
                    {
                        criteria = " and scamps_prov_id = " + provider_id;
                    }
                    sql = sql.Replace("[criteria]", criteria);
                    results = t.Results(sql,"user_encounter_recent");

                    break;

                case "sdfactions":
                    results = sdfActionButtons();
                    break;

                case "todayencounters":

                    break;

                case "upcomingencounters":

                    break;

                case "scampform":
                    results = ScampForm();
                    break;
                case "sdfviewmode":
                    results = SDFViewMode();
                    break;
                    // === debug info ===
                case "environmentdata":
                    Parameters.Clear();
                    GetSiteConfig();
                    results = debugTable(Parameters);
                    break;
                case "databasedata":
                    bool dbconnected = false;
                    if (Parameters.ContainsKey("SITENAME"))
                    {
                        dbconnected = true;
                    }
                    Parameters.Clear();
                    results = Tools.ReplaceRange(ConnectionString, "Password", ";", "Password=************;");
                    results = Tools.ReplaceRange(results, "PASSWORD", ";", "PASSWORD=************");
                    Parameters.Add("DB Connection", results);
                    Parameters.Add("Connected", dbconnected.ToString());
                    results = debugTable(Parameters);
                    break;
                case "sessiondata":
                    results = debugTable(Session);
                    break;

                case "scampreports":
                    results = ScampReports();
                    break;
                case "report":
                    results = Report();
                    break;
                case "enrollmentbuttons":
                    results = enrollmentButtons(); // eg "TX00000024"
                    break;
                case "listpatientencounters":
                    results = listPatientEncounters();
                    break;
                default:
                    Type type = this.GetType();
                    MethodInfo info = type.GetMethod(method);
                    object instance = Activator.CreateInstance(type, null);
                    results = Scamps.Tools.AsText(info.Invoke(instance, new object[] { }));

                    break;
            }
            t.Dispose();
            return results;
        }
        private string debugTable(Dictionary<string,string> data)
        {
            string results = "";
            foreach (KeyValuePair<string, string> item in data)
            {
                results += "<tr><td>" + item.Key + "</td><td class=\"data\">" + item.Value + "</td></tr>" + System.Environment.NewLine;
            }
            results = "<table class=\"debug\">" + results + "</table>";

            return results;
        }

        public string Form(string source)
        {
            string results = "";
            string method = "";
            string options = "";
            Scamps.Tools.QSSplit(ref source, ref method, ref options);
            Scamps.Tools.QSParse(ref Parameters, options);



            return results;
        }
        // ==========  Custom Methods ===============

        public string ScampReports()
        {

            string results = "";
            string scamp = Tools.GetValue("page.identifier",Parameters).ToUpper();
            Dictionary<string,string> columns = new Dictionary<string, string>();
            columns.AddOrReplace("scamp", "");
            columns.AddOrReplace("category", "");
            columns.AddOrReplace("display", "");
            DataTable listing = Tools.Files(con.Server.MapPath("scamps"),columns, "*.report");
            
            DataView dv = listing.DefaultView;
            dv.Sort = "category, title";
            if(!scamp.IsNullOrEmpty())
            {
                dv.RowFilter = "display <> 'false' and (scamp='ALL' or scamp = '" + scamp + "')";
            }
            else
            {
                dv.RowFilter = "display <> 'false' and (scamp='')";
            }
            
            DataTable resultsDT = dv.ToTable();
            
            Scamps.Templating tmp = new Scamps.Templating();
            tmp.Language = Language;
            tmp.qs = QueryString;
            tmp.parameters = Parameters;

            results = tmp.Results(resultsDT,con.Server.MapPath("resources\\templates\\report_page_listing.dbt"));

            tmp.Dispose();
            return results;
        }

        public string TestForms()
        {
            string results = "";
            bool ok = false;
            Dictionary<string,string> finfo = new Dictionary<string,string>();

            Scamps.Form form = new Scamps.Form();
            Scamps.DataTools db = new Scamps.DataTools();
            db.Provider = "oracle";
            db.ConnectionString = ConnectionString;
            string sql = "select form_name from admin_encounters where visible_flag='Y' order by scamp_id, seq_num";
            db.OpenConnection();
            db.GetResultSet(sql);
            while (!db.EOF)
            {

                ok = form.CheckForm(con.Server.MapPath("/scamps/" + db.Get("form_name")) + ".form",ref finfo);

                Tools.AppendLine(ref results,"Form: " + finfo.getValueOrDefault("formname"));
                Tools.AppendLine(ref results, "Lists: " + finfo.getValueOrDefault("lists"));
                Tools.AppendLine(ref results, "Images: " + finfo.getValueOrDefault("images"));
                Tools.AppendLine(ref results, finfo.getValueOrDefault("results"));
                Tools.AppendLine(ref results, "Total Warnings: " + finfo.getValueOrDefault("warningcount"));
                Tools.AppendLine(ref results, "Total Errors: " + finfo.getValueOrDefault("errorcount"));
                Tools.AppendLine(ref results, "================================================================================");
                Tools.AppendLine(ref results, "");
                db.NextRow();

            }
            results = Tools.ToHTMLList(results, true);

            db.CloseConnection();
            db.Dispose();
            form.Clear();

            return results;
        }

        /// <summary>
        /// process a report file
        /// </summary>
        /// <returns>String</returns>
        public string Report()
        {

            string results = "";

            string target = Tools.GetValue("page.target", Parameters); 
            string scamp = Tools.GetValue("page.identifier", Parameters);
            string report = Tools.GetValue("page.name", Parameters);

            Scamps.Templating t = new Scamps.Templating();
            t.Provider = Provider;
            t.Language = Language;
            t.ConnectionString = ConnectionString;
            t.BasePath = con.Server.MapPath("/");
            // == pass in all page data ===
            t.data = PageData;
            t.session = Session;
            t.qs = QueryString;
            t.parameters = Parameters;
            t.formdata = FormData;

            switch (target)
            {
                case "scamps":
                    if (!report.IsNullOrEmpty())
                    {
                        report = "/scamps/" + report + ".report";
                    }
                    else
                    {
                            report = "/scamps/" + scamp + ".report";
                    }
                    break;
                case "s":
                    report = "/scamps/" + scamp + ".report";
                    break;

                case "r":

                    break;

                default:

                    break;
            }

            if (report.IsNullOrEmpty())
            {
                results = t.Report(con.Server.MapPath("/scamps/" + scamp + ".report"));
            }
            else
            {
                results = t.Report(con.Server.MapPath(report));
            }

            if(!t.Page.Head.IsNullOrEmpty())
            {
                Page.Header.Controls.Add(new System.Web.UI.LiteralControl(t.Page.Head));
            }

            t.Dispose();

            return results;

        }

        public string recentlyviewedpatients(string listofpatients)
        {
		///TODO - use a different template for patients, also probably use a different base query ...
            string results = "";
            string sql = "";
            string criteria = "";
            sql = Scamps.Tools.ReadBlock(con.Server.MapPath("/resources/data/scampsweb.sql"), "dashboard.patients.lastviewed");

            Scamps.Templating t = new Scamps.Templating();
            t.Provider = Provider;
            t.Language = Language;
            t.ConnectionString = ConnectionString;
            t.BasePath = con.Server.MapPath("/");

            criteria = " upper(mrn) in (" + listofpatients + ")";
            sql = sql.Replace("[criteria]", criteria);
            results = t.Results(sql, "userdashboard_patients");

            t.Dispose();
            return results;
        }
        public string recentlyviewedencounters(string listofencounters)
        {
            string results = "";
            string sql = "";
            string criteria = "";
            sql = Scamps.Tools.ReadBlock(con.Server.MapPath("/resources/data/scampsweb.sql"), "dashboard.encounters.lastviewed");

            Scamps.Templating t = new Scamps.Templating();
            t.Provider = Provider;
            t.Language = Language;
            t.ConnectionString = ConnectionString;
            t.BasePath = con.Server.MapPath("/");

            criteria = " appt_id in (" + listofencounters + ")";
            sql = sql.Replace("[criteria]",criteria);
            results = t.Results(sql, "userdashboard_encounters_viewed");

            t.Dispose();
            return results;
        }

        public string sdfAllowSave()
        {
            int status = Tools.AsInteger(Tools.GetValue("encounter_status", PageData));
            if ((status == 3 || status == 23 || status == 98) || (userRole() == "PROVIDER" && (status == 3 || status == 51 || status == 7 || status == 23 || status == 98)))
            {
                return "false";
            }
            else
            {
                return "true";
            }
        }

        public string sdfActionButtons()
        {
            string results = "";
            string tmplate = "";
            Tools.AppendLine(ref tmplate,"<li>");
            Tools.AppendLine(ref tmplate,"<input class=\"button{0}\" type=\"submit\"{1} value=\"{2}\"/>");
            Tools.AppendLine(ref tmplate,"</li>");
            int status = Tools.AsInteger(Tools.GetValue("encounter_status", PageData));

            if (userRole() == "PROVIDER")
            {
                if (status == 3 || status == 51 || status == 7 || status == 23 || status == 98)
                {
                    //disable the save and save & close buttons
                    results = string.Format(tmplate, " save", " disabled=\"disabled\"", "Save");
                    results += string.Format(tmplate, " saveclose", " disabled=\"disabled\"", "Save &amp; Close");

                    //already signed or completed...
                    results += string.Format(tmplate, " savesign", " disabled=\"disabled\"", "Sign Form");  
                }
                else
                {
                    //save and save & close buttons
                    results = string.Format(tmplate, " save", "", "Save");
                    results += string.Format(tmplate, " saveclose", "", "Save &amp; Close");

                    results += string.Format(tmplate, " savesign", "", "Sign Form");
                }
            }
            else
            {
                if (status == 3 || status == 23 || status == 98)
                {

                    //disable the save and save & close buttons
                    results = string.Format(tmplate, " save", " disabled=\"disabled\"", "Save");
                    results += string.Format(tmplate, " saveclose", " disabled=\"disabled\"", "Save &amp; Close");
                    
                    results += string.Format(tmplate, " savecomplete", " disabled=\"disabled\"", "Complete Form");
                }
                else
                {
                    results = string.Format(tmplate, " save", "", "Save");
                    results += string.Format(tmplate, " saveclose", "", "Save &amp; Close");

                    results += string.Format(tmplate, " savecomplete", "", "Complete Form");
                }
            }
            return results;
        }

        public string SDFViewMode()
        {

            Scamps.Element e = new Scamps.Element();
            e.listselector = "--view mode--";
            string results = "";
            string selected = QueryString.Get("viewmode"); 

            // ZAPHOD - done on the line above.  Leaving this here in case I missed something and it does need to be done on two lines...
            //if (QueryString.ContainsKey("viewmode")) { selected = QueryString["viewmode"]; }

            results = e.OptionsFromList("0:Normal,1:Scratch Pad "/*,2:Errors Only"*/, selected);
            
            return results;
        }

        /// <summary>
        /// Creates SCAMP enrollment pill buttons from list of scamps
        /// styled to show enrollment status 
        /// </summary>
        /// <returns>html segment (of SCAMPS)</returns>
        public string enrollmentButtons()
        { 
            string results = "";
            //string mrn = con.Items["target"].ToString().ToUpper(); // "TX00000024"; 
            string mrn = Parameters.Get("page.target");
           // string defaultTemplate = Tools.ReadFile(con.Server.MapPath("/resources/templates/enrollmentstatus.dbt"));
            string sql = Scamps.Tools.ReadBlock(con.Server.MapPath("/resources/data/scampsweb.sql"), "patient.scamp.enrollments");
            Scamps.Templating t = new Scamps.Templating();
            sql = t.MergeField(sql, "mrn", mrn);
            //sql = sql.Replace("[mrn]", "'" + mrn + "'");
            t.BasePath = con.Server.MapPath("/");
            //var csettings = System.Configuration.ConfigurationManager.ConnectionStrings["SCAMPs"];
            t.Provider = Provider;
            t.Language = Language;
            t.ConnectionString = ConnectionString;
            t.qs = QueryString;
            t.parameters = Parameters;

            results = t.Results(sql, "enrollmentstatus");

            t.Dispose();
            return results;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string listPatientEncounters()
        {
            string results = "";
            string mrn = Parameters.Get("page.target").ToUpper().Trim();
            mrn = Tools.URLString(mrn);
            string sql = Tools.ReadBlock(con.Server.MapPath("/resources/data/scampsweb.sql"), "search.encounters.base");
            Templating t = new Scamps.Templating();
            sql = t.MergeField(sql, "mrn", mrn);
                t.Provider = Provider;
            t.Language = Language;
            t.ConnectionString = ConnectionString;
            t.BasePath = con.Server.MapPath("/");
            t.qs = QueryString;
            t.parameters = Parameters;
            sql += "select * from enc where mrn = '" + mrn + "'";
            sql += Environment.NewLine + "order by lastname,firstname,mrn,scamp_name,appt_date,har,appt_id";
            results = t.Results(sql, "list-patient-encounters");

            t.Dispose();

            return results;
        }


        public string buttonNavigation()
        {
            string results = "";

            if (isAuthenticated())
            {
                
                results = Tools.ReadFile(con.Server.MapPath("/resources/templates/usernav.dbt"));

                // remove Rahul's screen mask...
                //results += "<i class=\"icon-th-1\" title=\"Screen Mask\"></i>";

                //if user has admin permissions...
                //Roles.IsUserInRole(
                if ((UserKeys() & 1) == 1)
                {
                    results += "<a href=\"/admin\"><i class=\"icon-cog\" title=\"Utilities\"></i></a>";
                }
                
                results += "<a href=\"/reports\"><i class=\"icon-chart-bar\" title=\"Reports\"></i></a>";
                results += "<a href=\"/search\"><i class=\"icon-search\" title=\"Search\"></i></a>";
                results += "<a href=\"/\"><i class=\"icon-user-add newpatient\" title=\"Add new patient\"></i></a>";
                results += "<a href=\"/dashboard\"><i class=\"icon-home\" title=\"Home/Dashboard\"></i></a>";
            }
            // that CRAZY guy Prem DEMANDED that this work on an inner
            // template page, so this is what needed to be done.  I know,
            // I know, OCD, but hey, as long as he is happy!   - ZAPHOD 2015.07.22
            Scamps.Templating tp = new Scamps.Templating();
            tp.Language = Language;
            tp.qs = QueryString;
            tp.parameters = Parameters;

            results = tp.Format(results, Parameters);
            tp.Dispose();
            return results;
        }

        public string GetConfigKey(string keyname)
        {
            string results = "";
            try
            {
                if (!System.Configuration.ConfigurationManager.AppSettings[keyname].ToString().IsNullOrEmpty())
                {
                    results = System.Configuration.ConfigurationManager.AppSettings[keyname].ToString();
                }
            }
            catch (Exception e)
            {
                //when we have logging, hey, we can actually do something about this...
            }

            return results;
        }

        public void GetSiteConfig()
        {
            userrole = userRole();
            userkeys = UserKeys();

            Parameters.AddOrReplace("userrole", userrole);
            Parameters.AddOrReplace("userkeys", userkeys.ToString());

            Scamps.DataTools db = new Scamps.DataTools();
            db.Provider = "oracle";
            db.ConnectionString = ConnectionString;

            //populate user info through profile method
            var uname = "";
            if (HttpContext.Current.User.Identity.IsAuthenticated){
                var profKeys = new string[] { "first_name", "last_name" };
                var Dprof = ScampsDB.Provider.ProfileMethods.getProfile("user_name",HttpContext.Current.User.Identity.Name,profKeys);
                uname = Dprof["first_name"] + " " + Dprof["last_name"];
            }
            Tools.AddOrReplace("fullname",uname,ref Parameters);
            
            //get db name from connection string
            var dbSrc = "";
            const string hostStr = "(HOST=";
            var idx = 0;
            if((idx = ConnectionString.IndexOf(hostStr)) == -1)
                dbSrc = new string(ConnectionString.Skip(ConnectionString.IndexOf("Data Source=") + 12).TakeWhile(ch => !ch.Equals(';')).ToArray());
            else
                dbSrc = new string(ConnectionString.Skip(ConnectionString.IndexOf(hostStr) + hostStr.Length).TakeWhile(ch => !ch.Equals(')')).ToArray());

            Tools.AddOrReplace("dbname", dbSrc, ref Parameters);

            Parameters.AddOrReplace("network user", Environment.UserName);
            Parameters.AddOrReplace("network domain", Environment.UserDomainName);
            Parameters.AddOrReplace("identity", System.Security.Principal.WindowsIdentity.GetCurrent().Name);

            //Parameters.AddOrReplace("network user", HttpContext.Current.User.Identity.Name);
            //Parameters.AddOrReplace("2networkdomain", HttpContext.Current.User.Identity.AuthenticationType);
            
            Parameters.AddOrReplace("version", Version);
            Parameters.AddEdit("authoring", authoring_active.ToString());

            //populate info from admin config
            if (db.OpenConnection())
            {
                // permissions hook - the system user always has all permissions
                db.ExecuteNonQuery("update admin_provider set password_answer = '2047' where user_name = 'system'");
                db.GetResultSet("select * from admin_config");
                while(!db.EOF)
                {
                    if(!Parameters.ContainsKey(db.Get("attrib")))
                    {
                        Tools.AddOrReplace(db.Get("attrib").ToLower(), db.Get("attrib_val"),ref Parameters);
                    }
                    db.NextRow();
                }
                Language = Tools.GetValue("language_code", Parameters);
            }

            db.CloseConnection();
            db.Dispose();
        }

        public string auth()
        {
            Handlers.authProcess.ProcessRequest(HttpContext.Current);
            return "";
        }
        public string search()
        {
            Handlers.sSearchHandler.ProcessRequest(HttpContext.Current);
            return "";
        }
        public string datadictionary()
        {
            Handlers.DictionaryProcess.HandleDictionaryQuery(HttpContext.Current);
            return "";
        }
        public string patient()
        {
            Handlers.PatientProcess.HandlePatientData(HttpContext.Current);
            return "";
        }

        public string encounter()
        {
            Scamps.Handlers.Ajax.HandleEncounterData(HttpContext.Current);
            return "";
        }

        public string getPivotData()
        {
            var idval = HttpContext.Current.Request.Form.Get("formid");
            Handlers.MeasurementPivotProcess.getFormData(HttpContext.Current, "encounter_id", idval);
            return "";
        }

        public string encounterstatus()
        {
            string results = "";
            string sql = Scamps.Tools.ReadBlock(con.Server.MapPath("/resources/data/scampsweb.sql"),"dashboard.encounter.chart");
            //and encounter_user=139049
            Scamps.Templating t = new Scamps.Templating();
            var csettings = System.Configuration.ConfigurationManager.ConnectionStrings["SCAMPs"];
            t.Provider = csettings.ProviderName;
            t.Language = Language;
            t.ConnectionString = csettings.ConnectionString;
            t.qs = QueryString;
            t.parameters = Parameters;

            string template = "[\"[status_text]\", [c]],";
            results = t.Results(sql, template);
            if (results.EndsWith(","))
            {
                results = results.Substring(0, results.Length - 1);
            }
            t.Dispose();
            return "[" + results + "]";
        }
        public string ScampForm()
        {
            if(appointment>0)
            {
                return _scampform(appointment,"");
            }
            else
            {
                if(QueryString.ContainsKey("form"))
                {
                    return _scampform(0,Scamps.Tools.AsText(QueryString["form"]));
                }
            }
            return "";
        }

        public string formstate()
        {
            string results = "";
            string message = "";
            int status = Tools.AsInteger(Tools.GetValue("encounter_status", PageData));
            if (status == 3 || status == 98) // Completed!!
            {
                message = "Form completed by " + Tools.GetValue("dcname",PageData,"(unknown)") + " on " + Tools.GetValue("update_dtm",PageData);
            }
            if (status == 51 || status == 7) // Signed!!
            {
                message = "Form signed by " + Tools.GetValue("providername", PageData, "(unknown)") + " on " + Tools.GetValue("update_dtm", PageData);
            }
            if (message.Length > 0)
            {
                results = "<div class=\"encstate\">" + message + "</div>";
            }
            return results;
        }

        public string linkedencounters(string mrn, string encounter_id)
        {
            string results = "";
            string sql = "";
            //TODO: are there validation rules for encounter and MRN?

            Scamps.Templating st = new Scamps.Templating();
            st.Provider = Provider;
            st.Language = Language;
            st.ConnectionString = ConnectionString;
            st.BasePath = con.Server.MapPath("/");
            sql = Tools.ReadBlock(con.Server.MapPath("/resources/data/scampsweb.sql"), "linked.encounters");
            sql = sql.Replace("[encounter_id]", encounter_id);

            //linked encounters
            results = st.Results(sql, "sdflinkedencounters");
            results = results.Replace("[appt_id]", encounter_id);
            results = results.Replace("[mrn]", mrn);
            st.Dispose();

            return results;

        }
        private string _scampform(long appt_id=0,string formname = "")
        {
            string results = "";
            string forminfo = "";
            Scamps.Form form = new Scamps.Form();
            form.BasePath = Server.MapPath("/");
            form.FormPath = Server.MapPath("/scamps/");
            form.ConnectionString = ConnectionString;
            form.Provider = Provider;
            form.ImagePath = "/scamps/images/";
            form.Entity = "measurements";

            form.Options.InnerFormOnly = true;
            form.Options.UseHTML5Markup = false;
            form.Options.PanelMarkup = true;
            form.Options.VersionWithHiddenElements = true;

            string sqlsource = Tools.ReadFile(con.Server.MapPath("//resources//data//") + "scampsweb.sql");

            //providers see slightly different lists and forms
            //TODO: refactor this for the new auth and profile system
            //      after the data schema changes.
            if (userRole() == "PROVIDER") { 
                    form.FilteredLists = true; 
                }
                else {
                    form.Version = "dc";
                }

            long? subunit = null;
            if(!HttpContext.Current.Request.QueryString["subunit"].IsNullOrEmpty()){
                long tmp = 0;
                if (long.TryParse(HttpContext.Current.Request.QueryString["subunit"], out tmp))
                    subunit = tmp;
            }
            
            //either load an existing appointment or view a form
            if (formname.Length>0)
            {
                //formpath is location of forms in filesystem
                string formpath = string.Concat(Server.MapPath("/"), "/scamps/");
                //check if form is in path, if not, attempt to export the form from the database THEN load the form from filesystem
                if (File.Exists(string.Concat(formpath, formname, ".form")))
                    form.Load(formname);
                else if (LegacyExport.exportSCAMPFormDBtoXML(ConnectionString, "Oracle.DataAccess.Client", formname, formpath, out formname))
                    form.Load(formname);
                else
                {

                    //TODO:can we fix this so it actually gives the user meaningful/actionable feedback?
                    //Throwing an error is not especially helpful and not de rigueur in modern times.
                    throw new HttpException(404, "Requested form could not be found");
                }
            }
            /* load a form if given an encounter appointment id
                note: this if branch does NOT attempt to export the form if it doesn't find it in filesystem
             */
            else if(appt_id>0)
            {
                int sub = 0;


                // this was originally not thought out especially well.  We will need basic information about the patient and 
                // the encounter for the page build itself.  So this is a little additional overhead.
                string sql = null;
                if (!int.TryParse(Request.QueryString["subunit"], out sub) || sub < 1) //If we don't have a subunit.
                {
                    //sql = "select p.lname,p.fname,p.dob,p.mrn, e.appt_id,e.clinic_name,e.clinic_appt,e.attending,e.har,e.encounter_status,e.update_dtm,ap.full_name as dcname,app.full_name as providername";
                    //sql += " from pt_master p left join pt_encounters e on p.mrn = e.mrn left join admin_provider ap on e.encounter_user = ap.scamps_prov_id left join admin_provider app on app.scamps_prov_id = e.scamps_prov_id";
                    //sql += " where appt_id=" + appt_id.ToString();

                    sql = Tools.GetBlock(sqlsource,"sdf.pageheader.encounter");
                    sql = sql.Replace("[appt_id]", appt_id.ToString());

                }
                else
                {
                    //sql = "select p.lname,p.fname,p.dob,p.mrn, e.appt_id,sub.clinic_name,sub.date_dt AS clinic_appt,sub.attending,sub.har,sub.encounter_status,sub.update_dtm,ap.full_name as dcname,app.full_name as providername";
                    //sql += " from pt_master p left join pt_encounters e on p.mrn = e.mrn left join VISITS_INPT_SUBUNIT sub on e.HAR = sub.HAR left join admin_provider ap on sub.encounter_user = ap.scamps_prov_id left join admin_provider app on app.scamps_prov_id = sub.scamps_prov_id";
                    //sql += " where appt_id=" + appt_id.ToString() + " and sub.ID=" + sub.ToString();

                    sql = Tools.GetBlock(sqlsource, "sdf.pageheader.subunit");
                    sql = sql.Replace("[appt_id]", appt_id.ToString());
                    sql = sql.Replace("[sub_id]", sub.ToString());
                }

                
                Scamps.DataTools db = new Scamps.DataTools();
                db.Provider = Provider;
                db.ConnectionString = ConnectionString;
                if (db.OpenConnection())
                {
                    db.GetResultSet(sql);
                    if (!db.EOF)
                    {
                        PageData = db.GetRowStrings();
                    }
                }

                PageData.Add("encounterstate", formstate());
                PageData.Add("linkedencounters", linkedencounters(Tools.GetValue("mrn",PageData), appt_id.ToString()));
                PageData.Add("sdfactions", sdfActionButtons());
                PageData.Add("allowsave", sdfAllowSave());

                //appointment id must be a parsable long, and the form name associated with the appointment must already be exported
                if (form.LoadAppointmentForm(appt_id, subunit))
                {
                    //the form should be loaded so the form name should be available to us
                    //we need to look up the scamp name because, for some reason, scamp ID is in the stub but not name
                    string scampname = Tools.AsText(db.GetValue("select scamp_name from scamp where scamp_id=" + Tools.AsInteger(Tools.GetValue("scamp_id",form.FormStub))));
                    PageData.Add("scamp", scampname);
                    PageData.Add("formname", form.Name);
                    Dictionary<string, string> finfo = new Dictionary<string, string>();


                    if(authoring_active)
                    {
                        bool ok = form.CheckForm(form.FormPath + form.Name + "." + form.MajorVersion + "." + form.MinorVersion + ".form", ref finfo);
                        Tools.AppendLine(ref forminfo, "Form: " + finfo.getValueOrDefault("formname"));
                        Tools.AppendLine(ref forminfo, "Lists: " + finfo.getValueOrDefault("lists"));
                        Tools.AppendLine(ref forminfo, "Images: " + finfo.getValueOrDefault("images"));
                        Tools.AppendLine(ref forminfo, finfo.getValueOrDefault("results"));
                        Tools.AppendLine(ref forminfo, "Total Warnings: " + finfo.getValueOrDefault("warningcount"));
                        Tools.AppendLine(ref forminfo, "Total Errors: " + finfo.getValueOrDefault("errorcount"));
                        Tools.AppendLine(ref forminfo, "");
                        PageData.Add("forminfo", Tools.ToHTMLList(forminfo));
                    }


                    if (Tools.FileExists(con.Server.MapPath(ExternalData.GetExternalFormRelativePath(form.Name))))
                    {
                        PageData.Add("externaldatabutton", "<input class='button externaldata' type='button' value='External Data' data-scamps-form='" + form.Name 
                            + "' data-scamps-apptid='" + appt_id 
                            + "' data-scamps-mrn='" + Tools.GetValue("mrn", PageData) 
                            + "' data-scamps-har='" + Tools.GetValue("har", PageData) +"' />");
                    }
                    else
                    {
                        PageData.Add("externaldatabutton", "");
                    }

                    if (form.HasFlaggedFields) { PageData.Add("hasflaggedfields", "true"); }
                    //set the measurements for the form if they exist
                    form.setMeasurements();
                }
                else
                {
                    //throw new HttpException(404, string.Format("failed to load form for appointment {0}", formname));
                    Response.Redirect("/no_sdf");
                }

                db.CloseConnection();
                db.Dispose();
            }
            else
            {
                throw new HttpException(404, "No form or appointment specified");
            }

            results = form.Render();
            pagetitle = form.Title;

            return results;
        }
        //==== ajax friendly ====
        public string Testing()
        {
            return "TESTING";
        }

        public string loginmodal()
        {
            string results = "";
            Scamps.Form form = new Scamps.Form();
            form.Provider = Provider;
            form.BasePath = con.Server.MapPath("/");
            form.ConnectionString = ConnectionString;
            if (form.ConnectionString.IsNullOrEmpty()) { form.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SCAMPs"].ToString(); }
            if (ConnectionExists("ExternalEntities")) { Tools.AddOrReplace("externalconnection", "true", ref Parameters); }
            string domainlist = GetConfigKey("LDAPdomain");
            if (!domainlist.IsNullOrEmpty())
            {
                form.Version = "modal domain";
                form.Load("login");
                form.theform["main"].dictionary["d"].list = domainlist;
                form.FormData.Add("d", GetConfigKey("LDAPauthgroupdomain"));
            }
            else
            {
                form.Version = "modal";
                form.Load("login");
            }

            results = form.Render();
            return results;
        }

        public string encounterlist()
        {
            string results = linkedencounters(Tools.GetValue("pid", Parameters), Tools.GetValue("encounter_id", Parameters));
            return results;
        }
        public string linkedencounterform()
        {
            string results = "";
            Scamps.Form form = new Scamps.Form();
            var csettings = System.Configuration.ConfigurationManager.ConnectionStrings["SCAMPs"];
            form.Provider = csettings.ProviderName;
            form.BasePath = con.Server.MapPath("/");
            form.ConnectionString = ConnectionString;
            if (form.ConnectionString.IsNullOrEmpty()) { form.ConnectionString = csettings.ConnectionString; }

            Scamps.Templating db = new Scamps.Templating();
            db.Provider = form.Provider;
            db.Language = Language;
            db.ConnectionString = form.ConnectionString;

            string formdoc = Tools.ReadFile(con.Server.MapPath("/resources/forms/linkedencounters.form"));
            formdoc = formdoc.Replace("[mrn]",Tools.GetValue("pid",Parameters));
            formdoc = formdoc.Replace("[appt_id]", Tools.GetValue("encounter_id", Parameters));

            results = db.Results("select target_id from encounter_link where source_id = " + Tools.GetValue("encounter_id", Parameters), "[target_id],");
            results = Tools.NotEndWith(results, ",");
            Tools.AddOrReplace("appointments", results,ref Parameters);

            form.FormData = Parameters;
            form.Load(formdoc);
            results = form.Render();
            db.Dispose();
            return results;
        }
        public string patientform()
        {
            string results = "";
            Scamps.Form form = new Scamps.Form();
            form.Provider = Provider;
            form.Options.UseHTML5Markup = false;
            form.BasePath = con.Server.MapPath("/");
            form.ConnectionString = ConnectionString;
            if (form.ConnectionString.IsNullOrEmpty()) { form.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SCAMPs"].ToString(); }
            form.Version = "new";
            form.Load("patient");
            results = form.Render();
            return results;
        }
        public string editpatient()
        {
            string results = "";
            Scamps.Form form = new Scamps.Form();
            form.Provider = Provider;
            form.BasePath = con.Server.MapPath("/");
            form.ConnectionString = ConnectionString;
            form.Load("patient");
            foreach (KeyValuePair<string,Panel> panel in form.theform)
            {
                foreach(KeyValuePair<string,Element> item in panel.Value.dictionary)
                {
                    item.Value.disabled = false;
                }
            }
            results = form.Render();
            return results;
        }
        public string checkmrn()
        {
            string results = "";
            string checkmrn = Tools.GetValue("mrn",Parameters);
            if (Tools.isLettersOrDigits(checkmrn))
            {
                Scamps.DataTools db = new Scamps.DataTools();
                db.Provider = Provider;
                db.ConnectionString = ConnectionString;
                if (db.OpenConnection())
                {
                    string mrn = Tools.AsText(db.GetValue("select mrn from pt_master where mrn='" + checkmrn + "'"));
                    if (!mrn.IsNullOrEmpty())
                    {
                        results = "EXISTS";
                    }
                    else
                    {

                        string external = "";
                        if(ConnectionExists("external"))
                        {
                            external = System.Configuration.ConfigurationManager.ConnectionStrings["external"].ToString();
                        }
                        
                        if (!external.IsNullOrEmpty())
                        {
                            Dictionary<string, string> patient = new Dictionary<string, string>();
                            db.ConnectionString = external;
                            if (db.OpenConnection())
                            {
                                string sql = Tools.ReadBlock(con.Server.MapPath("/resources/data/scampsweb.sql"), "external.patient.mrn");
                                sql += " where mrn=" + checkmrn;

                                db.GetResultSet(sql);
                                if (!db.EOF)
                                {
                                    patient = db.GetRowStrings();
                                    patient.Add("status", "IMPORT");
                                    results = Newtonsoft.Json.JsonConvert.SerializeObject(patient);
                                }
                                else
                                {
                                    results = "NEW";
                                }
                            }
                            else
                            {
                                //bad connection
                            }

                        }
                        else
                        {
                            results = "NEW";
                        }
                    }
                }
                db.Dispose();
            }
            else
            {
                results = "BADMRN";
            }
            return results;
        }
        //role provider stuff
        public bool isAuthenticated()
        {
            return con.User.Identity.IsAuthenticated;
        }

        public string UserID()
        {
            string id = "";
            var profKeys = new string[] { "scamps_prov_id" };
            var prov_id = ScampsDB.Provider.ProfileMethods.getProfile("user_name", HttpContext.Current.User.Identity.Name, profKeys);
            if (prov_id.ContainsKey("scamps_prov_id")) { id = prov_id["scamps_prov_id"]; }
            return id;
        }
        /// <summary>
        /// Return the logged in user's keys, which is right now being stored in password_answer as a string.  Ugh.
        /// </summary>
        /// <returns>Integer - bitwise keys assigned the user.</returns>
        public int UserKeys()
        {
            int keys = 0;
            var profKeys = new string[] { "password_answer" };
            var prov_id = ScampsDB.Provider.ProfileMethods.getProfile("user_name", HttpContext.Current.User.Identity.Name, profKeys);
            if (prov_id.ContainsKey("password_answer")) { keys = Tools.AsInteger(prov_id["password_answer"]); }
            return keys;
        }
        public string userRole()
        {
            string[] roles = Roles.GetRolesForUser();
            if (roles.Length > 0)
            {
                return roles[0].ToUpper();
            }
            return "";
        }

        /// <summary>
        /// Runs first time after a fresh install...
        /// </summary>
        public void First_Run()
        {
            string code_version = "1.3";
            string schema_version = "1.09";



            Scamps.DataTools db = new Scamps.DataTools();
            db.Provider = Provider;
            db.ConnectionString = ConnectionString;
            db.OpenConnection();

            Dictionary<string, string> hd = new Dictionary<string, string>();

            // V1.3 - Initially set all locations to be visible...
            db.Execute("update admin_lookup set visible_abbr='Y' where lookup_cd in('LOCTN_OUTPT','LOCTN_INPT')");
            db.Execute("commit");

            // check if admin config has all the needed values
            if (Tools.AsText(db.GetValue("select attrib_val from admin_config where attrib = 'SITECODE'")).Length == 0)
            {
                // add the variable
                hd.Clear();
                hd.AddEdit("attrib", "SITECODE");
                hd.AddEdit("attrib_val", "SITECODE");
                db.Insert(hd, "admin_config");
            }
            if (Tools.AsText(db.GetValue("select attrib_val from admin_config where attrib = 'SITENAME'")).Length == 0)
            {
                // add the variable
                hd.Clear();
                hd.AddEdit("attrib", "SITENAME");
                hd.AddEdit("attrib_val", "SITENAME");
                db.Insert(hd, "admin_config");
            }

            hd.Clear();
            hd.AddEdit("attrib_val", code_version);
            db.Update(hd, "admin_config", "attrib = 'CODE_VERSION'");

            hd.AddEdit("attrib_val", schema_version);
            db.Update(hd, "admin_config", "attrib = 'SCHEMA_VERSION'");



            // Encounter Status Updates...

            hd.Clear();
            hd.AddEdit("status_id", "0");
            hd.AddEdit("status_text", "New Encounter");
            hd.AddEdit("seq_num", "0");
            hd.AddEdit("status_type", "IN_PROGRESS_NO_DATA");
            if (db.Exists("select * from admin_encounter_status where status_id=0"))
            {
                db.Update(hd, "admin_encounter_status", "status_id = 0");
            }
            else
            {
                db.Insert(hd, "admin_encounter_status");
            }


            hd.Clear();
            hd.AddEdit("status_id", "1");
            hd.AddEdit("status_text", "Packet delivered, form to be completed");
            hd.AddEdit("status_type", "IN_PROGRESS_NO_DATA");
            hd.AddEdit("seq_num", "41");
            if (db.Exists("select * from admin_encounter_status where status_id=1"))
            {
                db.Update(hd, "admin_encounter_status", "status_id = 1");
            }
            else
            {
                db.Insert(hd, "admin_encounter_status");
            }

            hd.Clear();
            hd.AddEdit("status_id", "2");
            hd.AddEdit("status_text", "Data Entry In Progress");
            hd.AddEdit("status_type", "IN_PROGRESS_INCOMPLETE");
            hd.AddEdit("seq_num", "1");
            if (db.Exists("select * from admin_encounter_status where status_id=2"))
            {
                db.Update(hd, "admin_encounter_status", "status_id = 2");
            }
            else
            {
                db.Insert(hd, "admin_encounter_status");
            }

            hd.Clear();
            hd.AddEdit("status_id", "3");
            hd.AddEdit("status_text", "SDF Completed");
            hd.AddEdit("status_type", "COMPLETED");
            hd.AddEdit("seq_num", "5");
            if (db.Exists("select * from admin_encounter_status where status_id=3"))
            {
                db.Update(hd, "admin_encounter_status", "status_id = 3");
            }
            else
            {
                db.Insert(hd, "admin_encounter_status");
            }

            hd.Clear();
            hd.AddEdit("status_id", "4");
            hd.AddEdit("status_text", "Waiting for Test Results");
            hd.AddEdit("status_type", "ON_HOLD");
            hd.AddEdit("seq_num", "2");
            if (db.Exists("select * from admin_encounter_status where status_id=4"))
            {
                db.Update(hd, "admin_encounter_status", "status_id = 4");
            }
            else
            {
                db.Insert(hd, "admin_encounter_status");
            }

            hd.Clear();
            hd.AddEdit("status_id", "5");
            hd.AddEdit("status_text", "Waiting for Additional Data");
            hd.AddEdit("status_type", "ON_HOLD");
            hd.AddEdit("seq_num", "3");
            if (db.Exists("select * from admin_encounter_status where status_id=5"))
            {
                db.Update(hd, "admin_encounter_status", "status_id = 5");
            }
            else
            {
                db.Insert(hd, "admin_encounter_status");
            }


            hd.Clear();
            hd.AddEdit("status_id", "23");
            hd.AddEdit("status_text", "Abandoned, Data Valid");
            hd.AddEdit("status_type", "ABANDONED_VALID");
            hd.AddEdit("seq_num", "10");
            if (db.Exists("select * from admin_encounter_status where status_id=23"))
            {
                db.Update(hd, "admin_encounter_status", "status_id = 23");
            }
            else
            {
                db.Insert(hd, "admin_encounter_status");
            }


            hd.Clear();
            hd.AddEdit("status_id", "25");
            hd.AddEdit("status_text", "Abandoned, Data Invalid");
            hd.AddEdit("status_type", "ABANDONED_INVALID");
            hd.AddEdit("seq_num", "11");
            if (db.Exists("select * from admin_encounter_status where status_id=25"))
            {
                db.Update(hd, "admin_encounter_status", "status_id = 25");
            }
            else
            {
                db.Insert(hd, "admin_encounter_status");
            }

            hd.Clear();
            hd.AddEdit("status_id", "51");
            hd.AddEdit("status_text", "Signed by Provider");
            hd.AddEdit("status_type", "IN_PROGRESS_FOR_REVIEW");
            hd.AddEdit("seq_num", "4");
            if (db.Exists("select * from admin_encounter_status where status_id=51"))
            {
                db.Update(hd, "admin_encounter_status", "status_id = 51");
            }
            else
            {
                db.Insert(hd, "admin_encounter_status");
            }

            // now, change seq values in bulk for some of remaining
            db.Execute("update admin_encounter_status set seq_num = seq_num + 40 where status_id in (8,10,11,12,13,14,15,16,17,18,19,20,21,22)");
            db.Execute("commit");

            db.Execute("update admin_encounter_status set seq_num = seq_num + 30 where status_id in (7,50,52,98,99)");
            db.Execute("commit");

            db.CloseConnection();
            db.Dispose();
            hd.Clear();

            // delete the trigger file...
            Tools.DeleteFile(con.Server.MapPath("/resources/initial.dbt"));

        }
    }

}