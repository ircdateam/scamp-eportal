﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;
using ScampsDB.SearchModels;
using ScampsDB.Query;
using System.Globalization;
using System.Web.Configuration;

namespace Scamps.Handlers
{
    /*
    public class sSearchHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get { return false; }
        }
    */
    public class sSearchHandler{
        #region SearchMethods
        private static void EncounterSearch(HttpContext _context)
        {
            //encounterSearch encapsulates all the parameters and their respective type
            var en = new EncounterSearch();

            //attempt to cast all the parameters to the object
            
            en.HAR = _context.Request.Form["har"];
            en.MRN = _context.Request.Form["mrn"];
            en.firstname = _context.Request.Form["fname"];
            en.lastname = _context.Request.Form["lname"];
            en.gender = _context.Request.Form["gender"];
            en.exactSearch = (_context.Request.Form["exactsearch"] == null ? false : true);
            en.providerID = (!string.IsNullOrEmpty(_context.Request.Form["prov_id"]) ? int.Parse(_context.Request.Form["prov_id"]) : (int?)null);
            en.SCAMP = (!string.IsNullOrEmpty(_context.Request.Form["scamp_id"]) ? int.Parse(_context.Request.Form["scamp_id"]) : (int?)null);
            en.location = _context.Request.Form["location"];
            en.statusTypeFlag = (!string.IsNullOrEmpty(_context.Request.Form["sflag"]) ? int.Parse(_context.Request.Form["sflag"]) : (int?)null);
            en.disposition = (!string.IsNullOrEmpty(_context.Request.Form["disp_id"]) ? int.Parse(_context.Request.Form["disp_id"]) : 0);
            en.visitType = _context.Request.Form["vtype"];
            en.maxresults = (!string.IsNullOrEmpty(WebConfigurationManager.AppSettings["EncounterSearchLimit"]) ? int.Parse(WebConfigurationManager.AppSettings["EncounterSearchLimit"]) : 0);

            var cstyle = CultureInfo.CreateSpecificCulture("en-US");

            DateTime tmp;
            int ltmp;
            //get all dates here
            if (_context.Request.Form["dob"] != null && (DateTime.TryParse(_context.Request.Form["dob"], cstyle, DateTimeStyles.None, out tmp)))
                en.DOB = tmp;
            if (_context.Request.Form["sdate"] != null && (DateTime.TryParse(_context.Request.Form["sdate"], cstyle, DateTimeStyles.None, out tmp)))
                en.start = tmp;
            if (_context.Request.Form["edate"] != null && (DateTime.TryParse(_context.Request.Form["edate"], cstyle, DateTimeStyles.None, out tmp)))
                en.end = tmp;
            if (_context.Request.Form["pid"] != null && (int.TryParse(_context.Request.Form["pid"], out ltmp)))
                en.PID = ltmp;

            //get list of patient encounters based on search parameters
            var datalist = Encounters.getList(en);

            _context.Response.ContentType = "application/json";
            bool first = true;
            _context.Response.Write("{\"data\":[");
            foreach (var item in datalist)
            {
                if (first)
                    first = false;
                else
                    _context.Response.Write(",");

                _context.Response.Write(item.ToJSON());
            }
            _context.Response.Write("]}");
        }

        private static void PatientSearch(HttpContext _context)
        {
            var ps = new PatientSearch()
            {
                MRN = _context.Request.Form["mrn"],
                firstname = _context.Request.Form["fname"],
                lastname = _context.Request.Form["lname"],
                gender = _context.Request.Form["gender"],
                exactSearch = (_context.Request.Form["exactsearch"] == null ? false : true),
                scampDBonly = !(_context.Request.Form["localsearch"] == null && WebConfigurationManager.ConnectionStrings["ExternalEntities"] != null),
                maxresults = (WebConfigurationManager.AppSettings["PatientSearchLimit"] != null ? int.Parse(WebConfigurationManager.AppSettings["PatientSearchLimit"]) : 0)
            };

            //parse datetime
            var cstyle = CultureInfo.CreateSpecificCulture("en-US");

            DateTime tmp;
            int itmp;
            if (_context.Request.Form["DOB"] != null && (DateTime.TryParse(_context.Request.Form["dob"], cstyle, DateTimeStyles.None, out tmp)))
                ps.DOB = tmp;
            if (_context.Request.Form["pid"] != null && (int.TryParse(_context.Request.Form["pid"], out itmp)))
                ps.PID = itmp;

            var datalist = (ps.scampDBonly ? Patients.getList(ps) : Patients.getList(ps).Union(Patients.getExternalList(ps)));

            _context.Response.ContentType = "application/json";

            bool first = true;
            _context.Response.Write("{\"data\":[");
            foreach (var item in datalist)
            {
                if (first)
                    first = false;
                else
                    _context.Response.Write(",");

                _context.Response.Write(item.ToJSON());
            }
            _context.Response.Write("]}");
        }

        private static void EMRproviderSearch(HttpContext _context)
        {
            var exact = _context.Request.Form["exact"] != null;
            _context.Response.ContentType = "application/json";
            
            var searchterm = _context.Request.Form["s"];
            if(string.IsNullOrEmpty(searchterm)){
                _context.Response.Write("{\"data\":[]}");
                return;
            }

            IEnumerable<ScampsDB.ViewModels.EMRProviderModel> res = null;

            try
            {
                res = EMRProviders.searchProviders(searchterm, exact);
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error when searching EMR for providers: ", e.ToString());
            }

            if (res == null || res.Count() == 0)
            {
                _context.Response.Write("{\"data\":[]}");
                return;
            }
            _context.Response.Write("{\"data\":[");
            var first = true;

            foreach (var p in res)
            {
                if(first)
                    first = false;
                else
                    _context.Response.Write(',');
                _context.Response.Write(p.ToJSON());
            }
            _context.Response.Write("]}");
            
        }

        private static void currProviderEncounterSearch(HttpContext _context)
        {

            var en = new EncounterSearch()
            {
                providerID = Providers.getProviderIDbyUserName(_context.User.Identity.Name)
            };

            var datalist = Encounters.getList(en);
            _context.Response.ContentType = "application/json";

            bool first = true;
            _context.Response.Write("{\"data\":[");
            foreach (var item in datalist)
            {
                if (first)
                    first = false;
                else
                    _context.Response.Write(",");

                _context.Response.Write(item.ToJSON());
            }
            _context.Response.Write("]}");

        }
        #endregion
        private static void getSearchOpts(HttpContext _context)
        {
            var olist = new List<string>();
            foreach (var o in _context.Request.Form["opt"].Split(','))
            {
                if (o.All(ch => Char.IsNumber(ch)))
                    olist.Add(o);
            }

            _context.Response.Write("[");
            if (olist.Any())
            {
                var res = FormFields.getOptList(olist.ToArray());
                if (res.Any())
                {

                    bool first = true;
                    foreach (var o in res)
                    {
                        if (first)
                            first = false;
                        else
                            _context.Response.Write(",");
                        _context.Response.Write("{\"label\":\"" + o.name + "\",\"value\":\"" + o.val + "\",\"c\":" + o.cat + "}");
                    }
                }

            }
            _context.Response.Write("]");
        }

        public static void ProcessRequest(HttpContext _context)
        {
            if (!string.IsNullOrEmpty(_context.Request.Form["opt"]))
            {
                getSearchOpts(_context);
                return;
            }

            //if initial search being made, return results where current user is the patient encounter provider
            if (!string.IsNullOrEmpty(_context.Request.Form["init"]) && _context.Request.Form["init"].Equals("1"))
            {
                currProviderEncounterSearch(_context);
                return;
            }

            //determine search request, then delegate
            string searchtype = _context.Request.Form["searchtype"];

            switch (searchtype)
            {
                case "2":
                    if (WebConfigurationManager.ConnectionStrings["EMREntities"] == null)
                    {
                        _context.Response.StatusCode = 400; //HTTP_STATUS_BAD_REQUEST
                    }
                    else
                    {
                        EMRproviderSearch(_context);
                    }
                    break;
                case "1":
                    //search encounter
                    EncounterSearch(_context);
                    break;
                case "0":
                    //search patient
                    PatientSearch(_context);
                    break;
                default:
                    _context.Response.StatusCode = 400; //HTTP_STATUS_BAD_REQUEST
                    break;
            }

        }
    }
}