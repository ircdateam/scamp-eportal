﻿using System;
using System.Collections.Generic;
using System.Configuration.Provider;
using System.Web.Security;
using System.Linq;
using System.Configuration;
using ScampsDB.Provider;

namespace Scamps.Handlers
{

    public class LegacyRoleProvider : RoleProvider
    {
        public override bool IsUserInRole(string userName, string roleName)
        {
            return RoleMethods.IsUserInRole(userName, roleName);
        }

        public override string[] GetRolesForUser(string userName)
        {
            return RoleMethods.GetRolesForUser(userName);
        }

        public override string[] GetAllRoles()
        {
            return RoleMethods.GetAllRoles();
        }

        public override bool RoleExists(string roleName)
        {
            return RoleMethods.RoleExists(roleName);
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            return RoleMethods.GetUsersInRole(roleName);
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string Name
        {
            get { return GetType().Name; }
        }

        public override string ApplicationName
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

    }
}