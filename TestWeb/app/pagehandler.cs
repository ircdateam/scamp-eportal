﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.SessionState;
using System.Web.Handlers;
using System.Linq;
using ExtensionMethods;

namespace Scamps.Handlers
{
    public class PageHandler : IHttpHandler, IRequiresSessionState
    {

        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(System.Web.HttpContext con)
        {
            string page = "~/Default.aspx";
            string zone = "";           //always first unit
            string target = "";         //always second unit
            string identifier = "";     //depends on the zone/target above
            string name = "";
            string tmp = "";
            //custom path resolution...
            
            string sql = string.Empty;
            string PagePath = string.Empty;
            string directurl = string.Empty;
            
            //Get the current url minus the extension and beginning junk
            string currentURL = "";
            if (con.Request.Path == "/404.aspx" && con.Items.Contains("PageUrl") && Scamps.Tools.AsText(con.Items["PageUrl"].ToString()).Length > 0)
            {
                currentURL = Scamps.Tools.AsText(con.Items["PageUrl"].ToString());
            }
            else
            {
                currentURL = Scamps.Tools.AsText(con.Request.Path);
            }

            int period = currentURL.LastIndexOf(".");

            if (period > 0)
            {
                PagePath = Scamps.Tools.AsText(currentURL.Substring(0, period).Trim().Trim('/')).ToString().ToLower();
            }
            else
            {
                PagePath = Scamps.Tools.AsText(currentURL.Trim().Trim('/')).ToString().ToLower();
            }

            if ((!con.User.Identity.IsAuthenticated || con.User.IsInRole("NO_ACCESS")) && PagePath != "login")
            {
                directurl = con.Request.QueryString.ToString();
                if (directurl.IsNullOrEmpty())
                {
                    directurl = PagePath;
                }
                else
                {
                    //hacky hacky to handle bug in auth methods.  Hacky but fun!
                    directurl = PagePath + "?" +  directurl;
                }
                con.Response.Redirect("/login?url=" + HttpUtility.UrlEncode(directurl));
            }
            //else if (con.User.Identity.IsAuthenticated == true && PagePath == "login")
            //{
            //    PagePath = "/dashboard";
            //    directurl = PagePath;
            //}
            string[] path = PagePath.Split('/');
            con.Items["path"] = PagePath;
            con.Items["pagetemplate"] = "";
            con.Items["innertemplate"] = "";

            //check to see if its the homepage
            if (PagePath.Length == 0)
                PagePath = "default";

            if (!(PagePath == "default") && File.Exists(con.Server.MapPath(con.Request.AppRelativeCurrentExecutionFilePath)))
            {
                //grab the requested page if it exists
                page = con.Request.AppRelativeCurrentExecutionFilePath;
            }
            //else if (PagePath == "default")
            //{
            //    page = "/dyna.aspx";
            //}
            else
            {
                //initial a couple of context items...
                con.Items["pagetemplate"] = "";
                con.Items["innertemplate"] = "";
                zone = path[0];
                if (path.Length > 1) { target = path[1].ToLower(); }
                if (path.Length > 2) { identifier = path[2].ToLower(); }
                if (path.Length > 3) { name = path[3].ToLower(); }
                con.Items["zone"] = zone;
                con.Items["target"] = target;
                con.Items["identifier"] = identifier;
                con.Items["name"] = name;
                switch (zone)
                {
                    case "search":
                        con.Items["pagetemplate"] = "page\\search";
                        break;
                    case "find":
                        con.Items["pagetemplate"] = "page\\default";
                        con.Items["innertemplate"] = "search";
                        break;
                    case "login":
                        con.Items["pagetemplate"] = "page\\default";
                        con.Items["innertemplate"] = "login";
                        break;
                    case "default":
                    case "dashboard":
                        con.Items["pagetemplate"] = "page\\dashboard";
                        break;
                    case "patient":
                        //check if patient exists, if not page doesn't exist
                        tmp = Tools.NotEndWith(path[1], "/");
                        if (path.Length > 1 && ScampsDB.Query.Patients.DoesPatientExist(tmp))
                        {
                            con.Items["pagetemplate"] = "page\\patient";
                        }
                        else
                        {
                            con.Items["pagetemplate"] = "page\\404";
                        }
                        break;
                    case "patients":

                        con.Items["pagetemplate"] = "page\\default";
                        {
                            //this is either /patients/patientID or /patients/encounter/encounterID
                            if (target == "encounter")
                            {
                                //this is a scamps form for display.  The next item is the id
                                if (path.Length > 2)
                                {
                                    con.Items["identifier"] = path[2];
                                    con.Items["appointment"] = path[2];
                                    con.Items["action"] = "scampsform";
                                    con.Items["pagetemplate"] = "page\\scampsdf";

									//lookup the encounter / subunit encounter record. If they don't exist or 
                                    bool found = true;
                                    var sub = HttpContext.Current.Request.QueryString["subunit"];
                                    try{
                                        if (sub.IsNullOrEmpty())
                                        {
                                            var enc = ScampsDB.Query.Encounters.getEncounter(path[2]);
                                            found = !(enc == null || !enc.encounter_type.HasValue);
                                        }
                                        else{
											var subenc = ScampsDB.Query.Encounters.getSubUnitEncounter(sub);
                                            found = !(subenc == null || !subenc.encounter_type.HasValue);
                                        }
                                    }
                                    catch(Exception e){
                                        Console.Error.WriteLine("Error looking up encounter information: ",e.ToString());
                                        found = false;
                                    }

                                    //if subunit or encounter is not found, redirect to 404
                                    if (found)
                                    {
                                        con.Items["identifier"] = path[2];
                                        con.Items["appointment"] = path[2];
                                        con.Items["action"] = "scampsform";
                                        con.Items["pagetemplate"] = "page\\scampsdf";
                                    }
                                    else
                                    {
                                        con.Items["pagetemplate"] = "page\\404";
                                    }
                                    
                                }

                            }
                            else
                            {
                                //We're in the patients directory, but not on a valid subtemplate.
                                con.Items["pagetemplate"] = "page\\404";
                            }
                        }
                        break;

                    case "404":
                    case "no_sdf":

                        con.Items["pagetemplate"] = "page\\default";
                        con.Items["innertemplate"] = "sdf_404";

                        break;



                    //case "scamps":

                    //    con.Items["pagetemplate"] = "page\\default";

                    //    // if path.length = 1, i.e. /scamps/  should be a listing of all scamps on the site.
                    //    if (path.Length == 1)
                    //    {
                    //        con.Items["innertemplate"] = "listscamps.dbt";
                    //    }

                    //    // if path.length = 2, i.e. /scamps/scampname this should be a listing of all scamp forms in that scamp
                    //    if (path.Length == 2)
                    //    {
                    //        con.Items["target"] = target;
                    //        con.Items["innertemplate"] = "listforms.dbt";
                    //    }
                    //    if (path.Length == 4)
                    //    {
                    //        ///loading a specific scamp form for a specific encounter...
                    //        con.Items["target"] = target;
                    //        con.Items["form"] = path[2];
                    //        con.Items["identifier"] = path[3];
                    //        con.Items["scamp"] = path[3];
                    //        con.Items["innertemplate"] = "scamp";
                    //    }
                    //    break;

                    case "admin":
                        con.Items["pagetemplate"] = "page\\admin";
                        //there are sub pages in the admin, so lets look at that
                        if (path.Length == 1)
                        {
                            con.Items["innertemplate"] = "adminlanding";
                        }
                        else
                        {
                            switch (target)
                            {
                                //case "searchprovider":
                                //    con.Items["pagetemplate"] = "page\\searchprovider";
                                //    break;
                                case "users":
                                    if (path.Length > 2)
                                    {
                                        con.Items["innertemplate"] = "edituser";
                                        con.Items["identifier"] = path[2];
                                        con.Items["pagetype"] = "edit";
                                    }
                                    else
                                    {
                                        con.Items["innertemplate"] = "listusers";
                                        con.Items["pagetype"] = "listing";
                                    }
                                    break;
                                case "providers":
                                    if (path.Length > 2)
                                    {
                                        con.Items["innertemplate"] = "editprovider";
                                        con.Items["identifier"] = path[2];
                                        con.Items["pagetype"] = "edit";
                                        con.Items["formversion"] = "provider";
                                    }
                                    else
                                    {
                                        con.Items["innertemplate"] = "listproviders";
                                        con.Items["pagetype"] = "listing";
                                    }
                                    break;
                                case "locations":
                                    if (path.Length > 2)
                                    {
                                        con.Items["innertemplate"] = "editlocation";
                                        con.Items["identifier"] = path[2];
                                        con.Items["pagetype"] = "edit";
                                    }
                                    else
                                    {
                                        con.Items["innertemplate"] = "listlocations";
                                        con.Items["pagetype"] = "listing";
                                    }
                                    break;
                                case "scamps":
                                    con.Items["innertemplate"] = "formlist";
                                    break;
                                case "info":
                                    con.Items["pagetemplate"] = "page\\debug";
                                    break;
                                default:
                                    con.Items["pagetemplate"] = "page\\404";
                                    break;
                            }
                        }
                        break;

                    case "reports":
                        con.Items["pagetemplate"] = "page\\default";

                        if (path.Length == 1)
                        {
                            con.Items["innertemplate"] = "reportlanding";
                        }
                        else
                        {
                            switch (target)
                            {
                                case "patients":
                                    con.Items["innertemplate"] = "patientlist";
                                    break;
                                case "scamps":
                                    if (path.Length > 3)
                                    {
                                        con.Items["innertemplate"] = "report";
                                        con.Items["name"] = name;
                                    }
                                    else
                                    {
                                        con.Items["innertemplate"] = "formlist";
                                    }
                                    break;
                                case "info":
                                    con.Items["pagetemplate"] = "page\\debug";
                                    break;
                                default:
                                    con.Items["innertemplate"] = "report";
                                    break;
                            }
                        }
                        break;

                    case "api":
                        con.Items["pagetemplate"] = "page\\default";
                        break;

                    default:

                        if (zone.IsNullOrEmpty())
                        {
                            con.Items["pagetemplate"] = "page\\dashboard";
                        }
                        else
                        {
                            con.Items["pagetemplate"] = "page\\404";
                        }
                        break;
                }


                try
                {

                    //set the context items for our base page class to use... do it here so we can have them if/when we get to the 404
                    con.Items["QueryString"] = con.Request.QueryString.ToString();

                }
                catch (Exception ex)
                {
                    //TODO:show 404
                    //con.Items("PageUrl") = db.URLDecode(PageUrl)
                    page = "~/Four04.aspx";
                }

                con.RewritePath(page, true);

            }

            Page hand = (Page)PageParser.GetCompiledPageInstance(page, con.Server.MapPath(page), con);
            hand.ProcessRequest(con);

        }

        private string DefaultWhere(bool prefixwithtablename = false)
        {
            string result = "";
            result += " ({0}active=1) ";
            result += "and (({0}activedate is null or {0}activedate<='" + DateTime.Now + "') and ({0}expiredate is null or {0}expiredate>='" + DateTime.Now + "')) ";

            string prefix = "";
            if (prefixwithtablename)
                prefix = "CMS_ContentANDTypes.";
            return string.Format(result, prefix);
        }

    }
}
