﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;
using System.Web.Configuration;
using System.Web.Security;
using System.Security;
//for AD auth
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
//end
using ScampsDB.Query;
using Newtonsoft.Json;
using System.Security.Cryptography;
//below are for LDAPHelper
using System.DirectoryServices.Protocols;
using System.Net;
using System.Security.Cryptography.X509Certificates;

namespace Scamps.Handlers
{
    
    
    public static class authProcess {
        private static readonly string _queryseed = "select attrib_val from admin_config where attrib = 'CSEED'";
        // TODO find an appropriate place/class to put AD authentication
        //NOTE* should be deprecated
        private class ADAuthenticator
        {
            const string ADloginFormat = "{0}\\{1}";
            private string authDomain;
            private string authorizedGroup;
            private string ldapURL;
            private List<string> domains;

            public UserPrincipal currentADUser { get; internal set; }

            public ADAuthenticator()
            {
                
                //Active Directory Authentication is configurable via web.config
                authDomain = WebConfigurationManager.AppSettings.Get("LDAPauthgroupdomain");
                authorizedGroup = WebConfigurationManager.AppSettings.Get("LDAPauthgroup");
                ldapURL = WebConfigurationManager.AppSettings.Get("LDAPurl");
                domains = WebConfigurationManager.AppSettings.Get("LDAPdomain").Split(',').ToList();
            }

            /// <summary>
            /// Authenticate the user by username and password and domain using Active Directory
            /// </summary>
            /// <param name="username"></param>
            /// <param name="password"></param>
            /// <param name="domain"></param>
            /// <returns></returns>
            public int authUser(string username, string password, string domain)
            {
                if (!domains.Any(d => d.ToUpper() == domain.ToUpper()))
                    return 1;

                var ADlogin = string.Format(ADloginFormat, domain, username);

                //below is code provided by MSDN for authorizing against active directory ... it is quirky but they have justification for the implementation
                var entry = new DirectoryEntry(this.ldapURL, ADlogin, password);
                try
                {
                    var objnative = entry.NativeObject;
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine("Error connecting to active directory: " + e.ToString());
                    return 3;
                }
                //thanks Bill Gates

                var context = new PrincipalContext(ContextType.Domain, domain);
                //var context = new PrincipalContext(ContextType.Domain, domain, username, password);
                if (context.ValidateCredentials(username, password))
                {
                    currentADUser = UserPrincipal.FindByIdentity(context, username);
                    if (isUserInGroup(username, password))
                        return 0;
                    else
                        return 2;
                }
                return 3;
            }
            private bool isUserInGroup(string username, string password)
            {
                if (string.IsNullOrEmpty(this.authDomain) || string.IsNullOrEmpty(this.authorizedGroup) || this.currentADUser == null)
                    return false;

                bool foundUser = false;
                using (var context = new PrincipalContext(ContextType.Domain, this.authDomain, username, password))
                {
                    using (var group = GroupPrincipal.FindByIdentity(context, this.authorizedGroup))
                    {
                        if (group != null)
                        {
                            // GetMembers(true) is recursive (groups-within-groups)
                            // this is awful as it is a linear traversal but checking if user's membership with group membership doesn't work as intended for whatever reason
                            foreach (var member in group.GetMembers(true))
                            {
                                foundUser = member.SamAccountName.Equals(username, StringComparison.OrdinalIgnoreCase);
                                if (foundUser)
                                    break;
                            }
                        }
                    }
                }
                return foundUser;
            }
        }

        /// TO-DO: find an appropriate place to put LDAP helper
        public class LDAPHelper : IDisposable
        {
            LdapConnection connection;
            //stores the path context used for the LDAP queries (.e.g. MY.DOMAIN.COM is usually "DN=MY,DN=DOMAIN,DN=COM" )
            public string currentDN { get; set; }
            private static readonly string _qgroupCN = "(&(objectcategory=group)(CN={0}))";
            private static readonly string _qCN = "({0})";
            private static readonly string _qADUser = "(&(objectclass=user)(sAMAccountName={0}))";
            public string[] userinfoAttribs = new string[]{
                "givenname","sn","mail"
            };

            private LDAPHelper(LdapConnection connection, string domainDN = "")
            {
                this.connection = connection;
                //if domain DN is not given, one is assumed based on the given domain specified in the provided connection
                if (string.IsNullOrEmpty(domainDN))
                {
                    currentDN = connection.SessionOptions.DomainName.Split('.').Select(str => "DC=" + str.ToUpper()).Aggregate((a, b) => a + "," + b);
                }
                else
                    currentDN = domainDN;
            }

            /// <summary>
            /// create ad helper connect
            /// </summary>
            /// <param name="domain">LDAP domain</param>
            /// <param name="user">username</param>
            /// <param name="password">password</param>
            /// <param name="isSSl">whether to use SSL to connect</param>
            /// <param name="port">port to connect to domain on</param>
            /// <returns>true on successful authentication</returns>
            /// <returns>AD helper object instantiated with given connection information</returns>
            public static LDAPHelper Create(string domain, string user, string password, bool isSSl = false, int port = 389)
            {
                var ldapid = new LdapDirectoryIdentifier(domain, port);
                var credential = new NetworkCredential(user, password);
                var connection = new LdapConnection(ldapid, credential);
                connection.AuthType = AuthType.Negotiate;
                if (isSSl)
                {
                    connection.SessionOptions.SecureSocketLayer = true;
                    connection.SessionOptions.VerifyServerCertificate = (LdapConnection lc, X509Certificate c) => { return true; };
                }
                connection.Bind();
                return new LDAPHelper(connection);
            }
        
            /// <summary>
            /// given LDAP connection info, check if a user can be authenticated with the given credentials
            /// </summary>
            /// <param name="domain">LDAP domain</param>
            /// <param name="user">username</param>
            /// <param name="password">password</param>
            /// <param name="isSSl">whether to use SSL to connect</param>
            /// <param name="port">port to connect to domain on</param>
            /// <returns>true on successful authentication</returns>
            public static bool CheckAuth(string domain, string user, string password, bool isSSl = false, int port = 389)
            {
                try
                {
                    var ldapid = new LdapDirectoryIdentifier(domain,port);
                    var credential = new NetworkCredential(user,password);
                    using (var connection = new LdapConnection(ldapid, credential))
                    {
                        connection.AuthType = AuthType.Negotiate;
                        if (isSSl)
                        {
                            connection.SessionOptions.SecureSocketLayer = true;
                            connection.SessionOptions.VerifyServerCertificate = (LdapConnection lc, X509Certificate c) => { return true; };
                        }
                        connection.Bind();
                    }
                    return true;
                }
                catch (Exception e)
                {
                    if (e is LdapException)
                    {
                        Console.Error.WriteLine("LDAP exception: ", e.Message);
                    }
                    else
                    {
                        Console.Error.WriteLine("error with AD auth: ", e.Message);
                    }
                }
                return false;
            }
        

            /// <summary>
            /// given a group name (typically corresponds to the canonical name (CN) in LDAP, returns the distinguished name (DN) of the requested group
            /// </summary>
            /// <param name="groupname">name of group to find</param>
            /// <param name="groupDN">'primary key' for group in AD</param>
            /// <returns>true on group found</returns>
            public bool TryFindGroup(string groupname, out string groupDN)
            {
                groupDN = "";
                var query = string.Format(_qgroupCN, groupname);
                var search = new SearchRequest(currentDN, query, System.DirectoryServices.Protocols.SearchScope.Subtree);
                var response = connection.SendRequest(search) as SearchResponse;
                if (response.Entries.Count == 0)
                    return false;
                SearchResultEntry entry = response.Entries[0];
                groupDN = entry.DistinguishedName;
                return true;
            }

            /// <summary>
            /// tries to find a user identifier (DN) given a user name. assumes that user is an active directory user and can be looked up via the attribute 'SAMAccountName'
            /// </summary>
            /// <param name="username">username to search by</param>
            /// <param name="userDN">DN of found user</param>
            /// <returns>true on user found</returns>
            public bool TryFindUser(string username, out string userDN)
            {
                userDN = "";
                //query by SAMAccountName
                var query = string.Format(_qADUser, username);
                //search for the current domain provided/determined (e.g. if domain is WEB.SITE.COM, domainDN is assumed to be DN=WEB,DN=SITE,DN=COM )
                var search = new SearchRequest(this.currentDN, query, System.DirectoryServices.Protocols.SearchScope.Subtree);
                //send request
                var response = connection.SendRequest(search) as SearchResponse;
                //if no entries returned, user not found
                if (response.Entries.Count == 0)
                    return false;
                //with search results, only return the first entry
                SearchResultEntry entry = response.Entries[0];
                userDN = entry.DistinguishedName;
                return true;
            }

            /// <summary>
            /// Given a DN, checks if object is classified as a group by LDAP
            /// </summary>
            /// <param name="groupDN">DN string to check</param>
            /// <returns>if object specified by DN is a object within the group class on LDAP</returns>
            public bool IsGroup(string groupDN)
            {
                var search = new SearchRequest(groupDN, "(objectclass=group)", System.DirectoryServices.Protocols.SearchScope.Subtree);
                var resp = connection.SendRequest(search) as SearchResponse;
                return (resp.Entries.Count > 0);
            }

            /// <summary>
            /// Given a CN string, return a value set associated with the requested attribute
            /// </summary>
            /// <param name="itemCN">cn of entry in AD</param>
            /// <param name="attribute">name of attribute</param>
            /// <returns>attribute values</returns>
            public string[] getCNAttributeVal(string itemCN, string attribute)
            {
                var query = string.Format(_qCN, itemCN);
                var search = new SearchRequest(this.currentDN, query, System.DirectoryServices.Protocols.SearchScope.Subtree);
                var resp = connection.SendRequest(search) as SearchResponse;

                var hasAttrib = false;
                var oattrib = "";
                SearchResultEntry entry = resp.Entries[0];
                foreach (string attr in entry.Attributes.AttributeNames)
                {
                    if (attr.Equals(attribute, StringComparison.OrdinalIgnoreCase))
                    {
                        hasAttrib = true;
                        oattrib = attr;
                        break;
                    }
                }
                if (!hasAttrib)
                    return new string[0];
                return entry.Attributes[oattrib].GetValues(typeof(string)).Select(x => x.ToString()).ToArray();
            }

            /// <summary>
            /// Given a DN string, return a value set associated with the requested attribute
            /// </summary>
            /// <param name="itemDN">DN an AD entry</param>
            /// <param name="attribute">name of attribute</param>
            /// <returns>attribute values</returns>
            public string[] getDNAttributeVal(string itemDN,string attribute,string filter="(objectclass=*)")
            {
                var search = new SearchRequest(itemDN, filter, System.DirectoryServices.Protocols.SearchScope.Subtree);
                var resp = connection.SendRequest(search) as SearchResponse;

                var hasAttrib = false;
                var oattrib = "";
                SearchResultEntry entry = resp.Entries[0];
                foreach(string attr in entry.Attributes.AttributeNames){
                    if (attr.Equals(attribute, StringComparison.OrdinalIgnoreCase)){
                        hasAttrib = true;
                        oattrib = attr;
                        break;
                    }
                }
                if(!hasAttrib)
                    return new string[0];
                        //gets attribute object by key, retrieves the set of values associated with it as string, casts it to string, maps collection to array
                return entry.Attributes[oattrib].GetValues(typeof(string)).Select(x => x.ToString()).ToArray();
            }

            /// <summary>
            /// given a DN and a set of attribute names, provide a dictionary with attributes that are found and their values
            /// </summary>
            /// <param name="itemDN">DN string</param>
            /// <param name="attributenames">collection of strings to put into dictionary</param>
            /// <param name="filter"></param>
            /// <returns>dictionary with key->values</returns>
            public Dictionary<string, string[]> getDNAttributeSet(string itemDN, IEnumerable<string> attributenames, string filter="(objectclass=*)")
            {
                var attribhash = new HashSet<string>(attributenames);

                var attribSet = new Dictionary<string, string[]>();

                var search = new SearchRequest(itemDN, filter, System.DirectoryServices.Protocols.SearchScope.Subtree);
                var resp = connection.SendRequest(search) as SearchResponse;

                SearchResultEntry entry = resp.Entries[0];

                foreach (string attr in entry.Attributes.AttributeNames)
                {
                    var lattr = attr.ToLower();
                    if (attribhash.Contains(lattr) && !attribSet.ContainsKey(lattr))
                        attribSet.Add(lattr, entry.Attributes[lattr].GetValues(typeof(string)).Select(x => x.ToString()).ToArray());
                }

                return attribSet;
            }

            /// <summary>
            /// given a DN for a user and a DN for a group, checks to see if the user has membership to any groups that may be nested inside the given group
            /// </summary>
            /// <param name="userDN">user DN</param>
            /// <param name="rootgroupDN">group DN to recursively check</param>
            /// <returns>if user has membership among one of the nested groups</returns>
            public bool recursiveMembershipCheck(string userDN, string rootgroupDN)
            {
                //collection of groups a user has membership to
                var usermembership = new HashSet<string>();
                //collection of nested entries visited on recursion
                var checkedgroups = new HashSet<string>();

                //retrieve groups that user is a member of
                foreach (var usergroupDN in this.getDNAttributeVal(userDN, "memberof"))
                {
                    usermembership.Add(usergroupDN);
                }

                //create a stack which manages the recursive traversal of groups
                var nextgroups = new Stack<string>();
                nextgroups.Push(rootgroupDN);
            
                //continue to check groups until stack is either empty or escaped
                while (nextgroups.Count > 0)
                {
                    //with next group to check
                    var currGroup = nextgroups.Pop();
                    //check is user is part of the group/nested group, if so then user is a member
                    if (usermembership.Contains(currGroup))
                        return true;
                    //for current group, get membership list.. this can contain both group and user DNs
                    foreach (var gcandidate in this.getDNAttributeVal(currGroup, "member"))
                    {
                        //if we've viewed this DN before, skip
                        if (!checkedgroups.Add(gcandidate))
                            continue;
                        //check if DN is a group, and if it is, add to list of groups that still need to be checked
                        if (this.IsGroup(gcandidate))
                        {
                            nextgroups.Push(gcandidate);
                        }
                    }
                }
                //if all groups are checked and user is not a member of any them, they do not have membership recursively
                return false;
            }

            public static int LDAPSelfAuth(string domain, string username, string password, string authgroup, bool isSSL, out Dictionary<string, string> userinfo, int port = 389)
            {
                userinfo = new Dictionary<string, string>();

                try
                {
                    //create AD helper object
                    using (var adhelp = LDAPHelper.Create(domain, username, password, isSSL, port))
                    {

                        //if above line passed, connection/auth successful
                        var userDN = "";
                        var groupDN = "";

                        //find user record
                        if (!adhelp.TryFindUser(username, out userDN))
                        {
                            return 1;
                        }

                        //can't find auth group
                        if (!adhelp.TryFindGroup(authgroup, out groupDN))
                        {
                            return 3;
                        }

                        if (adhelp.recursiveMembershipCheck(userDN, groupDN))
                        {
                            //user is in auth group, get user info

                            var uinfo = adhelp.getDNAttributeSet(userDN, adhelp.userinfoAttribs);
                            foreach (var attrib in adhelp.userinfoAttribs)
                            {
                                if (userinfo.ContainsKey(attrib))
                                    continue;

                                if (uinfo.ContainsKey(attrib))
                                    userinfo.Add(attrib, uinfo[attrib].FirstOrDefault());
                                else
                                    userinfo.Add(attrib, "");
                            }

                            return 0;
                        }
                        else
                        {
                            Console.WriteLine("User is not in auth group");
                        }
                    }
                }
                catch (Exception e)
                {
                    if (e is LdapException)
                    {
                        var errcode = (e as LdapException).ErrorCode;
                        ///TO-DO: find list of ldapexception error codes
                        //invalid credentials
                        if (errcode == 49)
                        {
                            return 1;
                        }
                        //LDAP not found at given domain
                        else if (errcode == 81)
                        {
                            return 2;
                        }
                    }
                }

                return 1;
            }

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            protected void Dispose(bool disposing)
            {
                if (connection != null)
                    try
                    {
                        connection.Dispose();
                    }
                    finally
                    {
                        connection = null;
                    }
            }

            ~LDAPHelper()
            {
                Dispose(false);
            }
        }

        private static void LDAPauth(HttpContext _context, string username, string password, string domain)
        {
            Dictionary<string, string> userinfo = null;

            var authgroup = WebConfigurationManager.AppSettings.Get("LDAPauthgroup");
            var useSSL = WebConfigurationManager.AppSettings.Get("LDAPssl").Equals("true",StringComparison.OrdinalIgnoreCase);
            int port = 389;
            if (useSSL)
                port = 636;
            var portStr = WebConfigurationManager.AppSettings.Get("LDAPport");
            if(!string.IsNullOrEmpty(portStr))
                int.TryParse(portStr, out port);

            var ldapcode = LDAPHelper.LDAPSelfAuth(domain, username, password, authgroup, useSSL, out userinfo,port);

            switch (ldapcode)
            {
                case 0:
                    if (Users.userExists(username))
                    {
                        Users.updatePassword(username, password, 12);
                    }
                    else
                    {
                        //get user info from dictionary
                        var fname = userinfo["givenname"];
                        var lname = userinfo["sn"];
                        var email = userinfo["mail"];
                        Users.createUserProfile(username, password, 12, "PROVIDER", fname, lname, email, 1);
                    }

                    createAuthTicket(_context,username);
                    _context.Response.Write("{\"error\":false,\"msg\":\"Login Successful\"}");
                    return;
                case 2:
                    _context.Response.Write("{\"error\":true,\"msg\":\"Invalid LDAP server or server unavailable\",\"AD\":true}");
                    return;
                case 3:
                    _context.Response.Write("{\"error\":true,\"msg\":\"authorization group does not exist\",\"AD\":true}");
                    return;
                case 1:
                default:
                    _context.Response.Write("{\"error\":true,\"msg\":\"Invalid username or password\",\"AD\":true}");
                    return;

            }

        }
        //NOTE* should be deprecated
        private static void ADauth(HttpContext _context, string username, string password, string domain)
        {
            var ADauth = new ADAuthenticator();

            var adcode = ADauth.authUser(username, password, domain);

            if (adcode == 0)
            {
                if (Users.userExists(username))
                {
                    Users.updatePassword(username, password, 12);
                }
                else
                    Users.createUserProfile(username, password, 12, "PROVIDER", ADauth.currentADUser.GivenName, ADauth.currentADUser.Surname, ADauth.currentADUser.EmailAddress, 1);

                createAuthTicket(_context,username);

                _context.Response.Write("{\"error\":false,\"msg\":\"Login Successful\"}");
                return;
            }
            else if (adcode == 1)
            {
                //_context.Response.Write("{\"error\":true,\"msg\":\"Invalid domain specified\",\"AD\":true}");
                _context.Response.Write("{\"error\":true,\"msg\":\"Invalid username or password\",\"AD\":true}");
                return;
            }
            else if (adcode == 2)
            {
                _context.Response.Write("{\"error\":true,\"msg\":\"Invalid LDAP server or server unavailable\",\"AD\":true}");
                return;
            }
            else if (adcode == 3)
            {
                _context.Response.Write("{\"error\":true,\"msg\":\"authorization group does not exist\",\"AD\":true}");
            }
            else
            {
                _context.Response.Write("{\"error\":true,\"msg\":\"Invalid username or password\",\"AD\":true}");
                return;
            }
        }

        private static void localDBauth(HttpContext _context)
        {
            //if LDAP url is missing in web config, active directory authentication is disabled
            //var hasADauth = !System.Configuration.ConfigurationManager.AppSettings["LDAPurl"].IsNullOrEmpty();
            var hasADauth = !System.Configuration.ConfigurationManager.AppSettings["LDAPdomain"].IsNullOrEmpty();

            string u = "", p = "", d = "", token = "";
            token = _context.Request.Form["token"];

            var jsWrite = new JsonTextWriter(_context.Response.Output);

            //check for token to process
            if (token != null)
            {
                if (!decodeToken(token, ref u, ref p, ref d))
                {
                    jsWrite.WrtStartObject()
                    .WrtPropertyValue("error", true).WrtPropertyValue("msg", "Invalid Token")
                .WrtEndObject();
                    return;
                }
            }
            else
            {
                u = _context.Request.Form["u"];
                d = _context.Request.Form["d"];
                p = _context.Request.Form["p"];
            }

            var requestedToken = string.IsNullOrEmpty(token) && _context.Request.Form["gettoken"] != null && _context.Request.Form["gettoken"] == "1";

            //can't have empty username or password, escape
            if (string.IsNullOrEmpty(u) || string.IsNullOrEmpty(p))
            {
                jsWrite.WrtStartObject()
                    .WrtPropertyValue("error", true).WrtPropertyValue("msg", "Username and password can not be empty")
                .WrtEndObject();
                return;
            }

            int res = 0;
            int authmode = 0;
            bool passedAuth = false;
            try
            {
                if (requestedToken)
                {
                    var formstr = string.Concat("u=",_context.Request.Form["u"],"&p=",_context.Request.Form["p"],"&d=",_context.Request.Form["d"]);
                    token = getToken(formstr);
                }
                //authenticate user against local database and try to retrieve their authentication mode code
                // codes: 
                //  -1 - uninitialized or undefined authentication profile
                //   0 - active directory user
                //   1 - local database user

                res = Users.authenticateUser(u, p, out authmode);

                //handle login response
                //result code of 0 means that authentication was successful
                if (res == 0)
                {

                    jsWrite.WrtStartObject()
                        .WrtPropertyValue("error", false).WrtPropertyValue("msg", "Login Successful");
                    if (!string.IsNullOrEmpty(token))
                        jsWrite.WrtPropertyValue("token", token);
                    jsWrite.WrtEndObject();
                    createAuthTicket(_context, u);
                }
                else if(res == 4){
                    jsWrite.WrtStartObject()
                        .WrtPropertyValue("error", true).WrtPropertyValue("msg", "Account is disabled")
                    .WrtEndObject();
                }
                //ad auth configuration must exist, user must have authmode set for AD, domain string must not be empty
                else if (hasADauth && (authmode == -1 || authmode == 0) && !string.IsNullOrEmpty(d))
                {
                    //ADauth(_context, u, p, d);
                    LDAPauth(_context, u, p, d);
                }
                else
                {
                    jsWrite.WrtStartObject()
                        .WrtPropertyValue("error", true).WrtPropertyValue("msg", "Invalid username or password")
                    .WrtEndObject();
                }
                //attempt to log auth attempt
                passedAuth = true;
                var hostname = _context.Request.UserHostName;
                //result of auth based on res
                //0 - authenticated, 1 - user doesnt exist, 2 - user info populated but no data, 3 - invalid password, 4 - user is set to inactive/no_access
                var accesstype = new string[]{
                    "Successful login","Invalid User","AD init","Bad Password","Inactive User"
                };
                if (res >= 0 && res < accesstype.Length)
                {
                    Users.pushAuthAudit(u, hostname, "LOGIN", accesstype[res]);
                }

            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error authenticating user: ", e.ToString());
                if (!passedAuth)
                {
                    jsWrite.WrtStartObject()
                            .WrtPropertyValue("error", true).WrtPropertyValue("msg", "Invalid username or password")
                        .WrtEndObject();
                }
            }
        }

        private static void createAuthTicket(HttpContext _context, string username)
        {
            _context.Response.Cookies.Add(FormsAuthentication.GetAuthCookie(username, true));
        }

        private static void logout(HttpContext _context)
        {
            if (_context.User.Identity.IsAuthenticated)
            {
                //overwrite current auth cookie with a blank one
                var cookie = new HttpCookie(FormsAuthentication.FormsCookieName);
                _context.Response.Cookies.Clear();
                _context.Response.Cookies.Add(cookie);
            }
        }

        private static byte[] EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV){
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");
            byte[] encrypted;
            using (var aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
                // Create the streams used for encryption. 
                using (var msEncrypt = new System.IO.MemoryStream())
                using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (var swEncrypt = new System.IO.StreamWriter(csEncrypt))
                    {
                        //Write all data to the stream.
                        swEncrypt.Write(plainText);

                    }
                    encrypted = msEncrypt.ToArray();
                }
            }
            return encrypted;
        }

        static string DecryptStringFromBytes_Aes(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            // Declare the string used to hold 
            // the decrypted text. 
            string plaintext = null;

            // Create an Aes object 
            // with the specified key and IV. 
            using (var aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption. 
                using (var msDecrypt = new System.IO.MemoryStream(cipherText))
                using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                using (var srDecrypt = new System.IO.StreamReader(csDecrypt))
                    // Read the decrypted bytes from the decrypting stream and place them in a string.
                    plaintext = srDecrypt.ReadToEnd();
            }

            return plaintext;
        }

        private static byte[] unwrapBase64String(string str)
        {
            var mbyte = Convert.FromBase64String(str);
            var step = str.Length;

            if (step < mbyte.Length)
                step += mbyte.Length;

            if ((step & 1) == 0)
                step++;

            int currstep = 0;
            var payload = new byte[48];
            for (var i = 0; i < payload.Length; i++)
            {
                currstep += step;
                while (currstep >= mbyte.Length)
                    currstep -= mbyte.Length;

                payload[i] = mbyte[currstep];
            }

            return payload;
        }

        private static bool decodeToken(string token, ref string u, ref string p, ref string d)
        {
            try
            {
                var cseed = "";
                using (var db = new Scamps.DataTools(System.Configuration.ConfigurationManager.ConnectionStrings["SCAMPs"]))
                    cseed = db.GetValue(_queryseed).ToString();

                var seedbytes = unwrapBase64String(cseed);
                var res = DecryptStringFromBytes_Aes(Convert.FromBase64String(token), seedbytes.Take(32).ToArray(), seedbytes.Skip(32).Take(16).ToArray());

                //bind parameters from decrypted token
                foreach (var str in res.Split('&').Where(ss => !string.IsNullOrEmpty(ss)))
                {
                    var idx = str.IndexOf('=');
                    if (idx == -1)
                        continue;
                    var param = str.Substring(0,idx);
                    idx++;
                    switch(param){
                        case "u":
                            u = str.Substring(idx, str.Length - idx);
                            break;
                        case "p":
                            p = str.Substring(idx, str.Length - idx);
                            break;
                        case "d":
                            d = str.Substring(idx, str.Length - idx);
                            break;
                        default:
                            break;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                //could not decode
            }
            return false;
        }
        private static string getToken(string message)
        {
            
            var res = "";
            try
            {
                var cseed = "";
                using (var db = new Scamps.DataTools(System.Configuration.ConfigurationManager.ConnectionStrings["SCAMPs"]))
                    cseed = db.GetValue(_queryseed).ToString();

                if (string.IsNullOrEmpty(cseed))
                    return res;

                var seedbytes = unwrapBase64String(cseed);
                res = Convert.ToBase64String(EncryptStringToBytes_Aes(message, seedbytes.Take(32).ToArray(), seedbytes.Skip(32).Take(16).ToArray()));
            }
            catch (Exception e)
            {
                //could not generate token
            }

            return res;
        }

        public static void ProcessRequest(HttpContext _context)
        {
            var method = string.Empty;

            if ((method = _context.Request.RequestType) != "POST")
            {
                _context.Response.StatusCode = 400;
                return;
            }

            if (_context.Request.Form["logout"] != null)
            {
                logout(_context);
            }
            else
                localDBauth(_context);
        }

    }
}