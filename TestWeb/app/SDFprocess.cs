﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Scamps;
using System.Threading.Tasks;
using ScampsDB.ViewModels;

namespace Scamps
{
    public class SDFResponse
    {
        public class SDFErrorList
        {
            public string name {get;set;}
            public string error { get; set; }
            public SDFErrorList() { }
            public SDFErrorList(string name, string error) { this.name = name; this.error = error; }
        }

        public bool error { get; set; }
        public string msg { get; set; }
        public List<SDFErrorList> errlist {get; set;}

        public SDFResponse() { 
            errlist = new List<SDFErrorList>();
        }
    }
    public static class SDF
    {
        const string _queryMeasurementDataByAppt = "select to_char(id) as id, lower(element_cd) as element, element_val as val, seq_num from measurements where appt_id = :01 order by element_cd, seq_num";
        const string _queryMeasurementDataByApptSub = "select to_char(id) as id, lower(element_cd) as element, element_val as val, seq_num from measurements where appt_id = :01 and subunit_seq_val = :02 order by element_cd, seq_num";
        public const string EmptyMeasurementVal = "{empty}";

        /// <summary>
        /// given two dictionaries of measurements (new and current), returns a dictionary of measurements which will require new database entries (e.g. they need to be inserted)
        /// </summary>
        /// <param name="newMeasurements">new measurements to be inserted</param>
        /// <param name="currMeasurements">collection of current measurements</param>
        /// <returns>collection of new measurements that need to be inserted instead of updated in-place</returns>
        private static Dictionary<string, MeasurementViewModel[]> mergeMeasurements(Dictionary<string, string[]> newMeasurements, Dictionary<string, MeasurementViewModel[]> currMeasurements)
        {
            var overflowDict = new Dictionary<string, MeasurementViewModel[]>();
            //for each element code for a measurement ... 
            foreach (var kn in newMeasurements)
            {
                //check if measurement with same element code exists
                MeasurementViewModel[] cm = null;
                currMeasurements.TryGetValue(kn.Key, out cm);
                //if measurements for element code do not exist, they are new measurements and put into the overflow dictionary
                if (cm == null)
                {
                    overflowDict[kn.Key] = kn.Value.Select((mm, ii) => new MeasurementViewModel(null, kn.Key, mm, ii + 1)).ToArray();
                    continue;
                }
                //if the incoming measurements for an element code are null or empty, the currently existing measurements with the same element code need to be set to null or empty
                if (kn.Value == null)
                {
                    cm.AsParallel().ForAll(ci => ci.seq = -1);
                    continue;
                }

                //use a hashset for new and current measurements to determine where measurement values overlap
                var hn = new HashSet<string>(kn.Value);
                var hc = new HashSet<string>(cm.Select(ci => ci.val));

                int i = 0;
                int j = 0;
                int seq = 1;

                bool nchecked = false;
                bool ncontains = false;
                bool cchecked = false;
                bool ccontains = false;

              //  looping through all the current measurements in the database, measurements that are no longer relevant to the incoming data
              //  are modified in-place, and extra measurements that 'overflow' from these existing records are added to the overflow dictionary
                while (j < cm.Count())
                {
                    //for both arrays, duplicate measurement values are checked by checking the other collections value hashlist

                    //if the current array pointer for the new measurement collection reaches the end, do not check the hash list
                    if (i < kn.Value.Count())
                    {
                        if (!nchecked)
                        {
                            ncontains = hc.Contains(kn.Value.ElementAt(i));
                            nchecked = true;
                        }
                    }

                    if (!cchecked)
                    {
                        ccontains = hn.Contains(cm[j].val);
                        cm[j].seq = -1;
                        if(ccontains){
                            cm[j].seq = seq;
                            seq++;
                        }
                        cchecked = true;
                    }

                    //if new measurement value does not exist and current measurement is no longer relevant, new measurement takes old measurement's spot
                    if (!ncontains && !ccontains)
                    {
                        if (i < kn.Value.Count())
                        {
                            cm[j].val = kn.Value.ElementAt(i);
                            cm[j].seq = seq;
                            seq++;
                        }
                        i++;
                        j++;
                        nchecked = false;
                        cchecked = false;
                    }
                    //increment array pointers if they are found in the others hash list
                    if (ncontains)
                    {
                        j++;
                        cchecked = false;
                    }
                    if (ccontains)
                    {
                        if (i < kn.Value.Count())
                            i++;
                        else
                            j++;
                        cchecked = false;
                    }
                }

                //if the new measurements array pointer doesnt reach the end, new measurements need to be inserted. These are added to the overflow collection
                if (i < kn.Value.Count())
                    overflowDict[kn.Key] = kn.Value.Skip(i).Select((mm, ii) => new MeasurementViewModel(null, kn.Key, mm, seq + ii + 1)).ToArray();
            }

            return overflowDict;
        }

        /// <summary>
        /// populates a dictionary with measurement data for an encounter
        /// </summary>
        /// <param name="refDict">dictionary to populate</param>
        /// <param name="appt_id">measurements for a particular sdf/form/encounter</param>
        /// <param name="sub_seq_val">specific subunit for a measurement</param>
        private static void getExistingMeasurements(ref Dictionary<string, MeasurementViewModel[]> refDict, string appt_id, string sub_seq_val)
        {
            var currMeasurements = new Dictionary<string, List<MeasurementViewModel>>();

            //using (var db = new Scamps.DBadapter(System.Configuration.ConfigurationManager.ConnectionStrings["SCAMPs"]))
            using (var db = new Scamps.DataTools(System.Configuration.ConfigurationManager.ConnectionStrings["SCAMPs"]))
            {
                if (string.IsNullOrEmpty(sub_seq_val))
                    db.GetResultSet(_queryMeasurementDataByAppt, appt_id);
                else
                    db.GetResultSet(_queryMeasurementDataByApptSub, appt_id, sub_seq_val);

                foreach (DataRow r in db.ResultSet.Rows)
                {
                    var id = r.Field<string>(0);
                    var ele = r.Field<string>(1);
                    var val = r.Field<string>(2);
                    int seq = 1;
                    var type = db.ResultSet.Columns[3].DataType;

                    if (!r.IsNull(3))
                        seq = (int)r.Field<Int16>(3);


                    if (val.IsNullOrEmpty() || val.Equals(EmptyMeasurementVal, StringComparison.OrdinalIgnoreCase))
                        val = "";

                    if (!currMeasurements.ContainsKey(ele))
                    {
                        currMeasurements[ele] = new List<MeasurementViewModel>();
                    }

                    currMeasurements[ele].Add(new MeasurementViewModel(id, ele, val, seq));
                }
            }

            refDict = currMeasurements.ToDictionary(kv => kv.Key, kv => kv.Value.ToArray());
        }

        /// <summary>
        /// given a collection of measurements that already exist in the database, updates their value and sequence number based on their primary key
        /// </summary>
        /// <param name="measurements">collection of measurements to update</param>
        private static void updateMeasurements(Dictionary<string, MeasurementViewModel[]> measurements)
        {
            if (measurements == null || measurements.Count == 0)
                return;

            const string updateMeasurementQuery = "update measurements set element_val = :01, seq_num = :02 where id = :03";

            
            var objArr = measurements
                .Where(kv => kv.Value != null)
                .SelectMany(kv => kv.Value)
                .Where(mm => mm != null)
                .Select(mm => new object[3] { mm.val, mm.seq, mm.id }).ToArray();

            using (var db = new Scamps.DataTools(System.Configuration.ConfigurationManager.ConnectionStrings["SCAMPs"]))
            {
                db.BulkQuery(updateMeasurementQuery, objArr);
            }
        }

        /// <summary>
        /// updates an encounter's status based on related SDF information (e.g. was form completed? signed? and by what user in which type of role?)
        /// </summary>
        /// <param name="stub">dictionary of SDF information. Should contain appt_id, sub_id (if applicable), as well as the curr_prov_id (scamps_prov_id) associated with the form</param>
        /// <param name="currUserRoles">the roles associated with the currently logged-in user</param>
        /// <param name="hasErrors">whether or not the form was saved with errrors present</param>
        private static void updateEncounterStatus(Dictionary<string, string> stub, string[] currUserRoles, bool hasErrors)
        {
            const string queryApptUpdateStatus = "update pt_encounters set {0} = :01, encounter_status = :02, complete_status = :03 where appt_id = :04";
            const string querySubUpdateStatus = "update visits_inpt_subunit set {0} = :01, encounter_status = :02, complete_status = :03 where id = :04";

            const string queryUpdateEncTimestamp = "update {0} set update_dtm = :01, updt_dt_tm = :01 where {1} = :02";

            const string _qScampProv = "scamps_prov_id";
            const string _qEncUser = "encounter_user";

            var queryParam = new object[4];
            bool encounterUser = false;
            bool isSubUnit = string.IsNullOrEmpty(stub["sub_id"]);

            queryParam[0] = stub["current_prov_id"];
            int en_status = 0;

            if (stub.ContainsKey("encounter_status") && int.TryParse(stub["encounter_status"], out en_status))
                queryParam[1] = en_status;

            queryParam[3] = isSubUnit ? stub["appt_id"] : stub["sub_id"];

            //retrieve information about the current user's role

            bool updateEncounter = false;

            var adminroles = new string[] { "super_user", "admin_user", "dc" };

            //a user's role will determine how an encounter update occurs (i.e. assigned to scamps_prov_id or encounter_user)
            if (currUserRoles.Any(ur => adminroles.Contains(ur.ToLower())))
            {
                if (stub["faction"] == "complete" && !hasErrors)
                {
                    //set encounter_status and complete_status respectively in queryParam
                    queryParam[1] = 3;
                    queryParam[2] = -1;
                }
                updateEncounter = true;
                encounterUser = true;
            }
            else if (currUserRoles.Any(ur => ur.Equals("provider", StringComparison.OrdinalIgnoreCase)))
            {
                if (stub["faction"] == "sign" && !hasErrors && stub.ContainsKey("encounter_status") && stub["encounter_status"] != "51")
                {
                    queryParam[1] = 51;
                    updateEncounter = true;
                }
            }

            if (updateEncounter)
            {
                using (var db = new Scamps.DataTools(System.Configuration.ConfigurationManager.ConnectionStrings["SCAMPs"]))
                {
                    db.ExecuteNonQuery(string.Format((isSubUnit ? queryApptUpdateStatus : querySubUpdateStatus), (encounterUser ? _qEncUser : _qScampProv)), queryParam);
                }
            }
            //at the very minimum, trigger appointment update
            else
            {

                var statement = string.Format(queryUpdateEncTimestamp, isSubUnit ? "visits_inpt_subunit" : "pt_encounters", isSubUnit ? "id" : "appt_id");

                var dtparams = new object[2];
                dtparams[0] = DateTime.UtcNow;
                dtparams[1] = queryParam[3];

                using (var db = new Scamps.DataTools(System.Configuration.ConfigurationManager.ConnectionStrings["SCAMPs"]))
                {
                    db.ExecuteNonQuery(statement, dtparams);
                }
            }
        }

        /// <summary>
        /// inserts new measurements for an SDF based on the form stub provided
        /// </summary>
        /// <param name="measurements">measurements to insert</param>
        /// <param name="stub">SDF form stub with information about the SDF (appt_id, har, scamp_id, visit_type subunit_seq_val)</param>
        private static void insertMeasurements(Dictionary<string, MeasurementViewModel[]> measurements, Dictionary<string, string> stub)
        {
            if (measurements == null || measurements.Count == 0)
                return;

            var isSubunit = stub.ContainsKey("subunit_seq_val") && !string.IsNullOrEmpty(stub["subunit_seq_val"]);
            
            const string insertApptMeasurementQuery = "insert into measurements (scamp_id,appt_id, har, visit_type, element_cd,element_val,seq_num) values(:01, :02, :03, :04, :05, :06, :07)";
            const string insertSubMeasurementQuery = "insert into measurements (scamp_id,appt_id, har, visit_type, element_cd,element_val,seq_num,subunit_seq_val) values(:01, :02, :03, :04, :05, :06, :07, :08)";

            var scamp_id = stub["scamp_id"];
            var appt_id = stub["appt_id"];
            var har = stub["har"];
            var visit_type = stub["visit_type"];
            var subunit_seq_val = stub["subunit_seq_val"];

            object[][] objArr = null;

            if (isSubunit)
            {
                objArr = measurements
                .Where(kv => kv.Value != null)
                .SelectMany(kv => kv.Value)
                .Where(mm => mm != null)
                .Select(mm => new object[8] { scamp_id, appt_id, har, visit_type, mm.ele_cd, mm.val, mm.seq, subunit_seq_val }).ToArray();
            }
            else
            {
                objArr = measurements
                .Where(kv => kv.Value != null)
                .SelectMany(kv => kv.Value)
                .Where(mm => mm != null)
                .Select(mm => new object[7] { scamp_id, appt_id, har, visit_type, mm.ele_cd, mm.val, mm.seq }).ToArray();
            }

            using (var db = new Scamps.DataTools(System.Configuration.ConfigurationManager.ConnectionStrings["SCAMPs"]))
            {
                if(isSubunit)
                    db.BulkQuery(insertSubMeasurementQuery, objArr);
                else
                    db.BulkQuery(insertApptMeasurementQuery, objArr);
            }

        }

        //public static void processMeasurements(HttpContext con, Form form, Dictionary<string,string> formcontext)
        public static SDFResponse processMeasurements(IDictionary<string, string[]> measurementform, string username, Form form, Dictionary<string, string> formcontext)
        {
            var currMeasurements = new Dictionary<string, MeasurementViewModel[]>();
            var ElementErrors = new Dictionary<string, string>();
            var newMeasurements = new Dictionary<string,string[]>();
            var ElementDict = new Dictionary<string, Element>(StringComparer.OrdinalIgnoreCase);

            var ignoreTypes = new string[] { "image", "section", "panel", "html", "infobox", "label", "submit", "button", "recommendation" };

            //decrypt form data

            //assume form is already loaded
            var faction = "";
            var enstatus = "";
            var appt_id = "";
            var sub_seq_val = "";
            var actionStr = "";
            form.FormData.TryGetValue("faction", out faction);
            formcontext.TryGetValue("encounter_status", out enstatus);
            formcontext.TryGetValue("appt_id", out appt_id);
            formcontext.TryGetValue("subunit_seq_val", out sub_seq_val);
            formcontext["faction"] = faction;

            var getCurrMeasurementsTask = new Task( () => getExistingMeasurements(ref currMeasurements, appt_id, sub_seq_val) );
            getCurrMeasurementsTask.Start();

            var currUserRole = Roles.GetRolesForUser();

            //before continuing with updating/inserting measurements, determine an encounter's eligibility for editing
            if (currUserRole.Any(role => role.Equals("provider", StringComparison.OrdinalIgnoreCase)))
            {
                if (enstatus == "3" || enstatus == "98" || enstatus == "51" )
                {
                    //con.Response.Write("{\"error\":true,\"msg\":\"Form is marked as complete and can not be modified\"}");
                    //return;
                    return new SDFResponse()
                    {
                        error = true,
                        msg = "Form is marked as complete and can not be modified"
                    };
                }
            }

            var currProvID = "";

            //retrieve scamps_prov_id for current user
            //ScampsDB.Provider.ProfileMethods.getProfile("user_name", con.User.Identity.Name, new string[] { "scamps_prov_id" })
            ScampsDB.Provider.ProfileMethods.getProfile("user_name", username, new string[] { "scamps_prov_id" })
                .TryGetValue("scamps_prov_id", out currProvID);

            formcontext["current_prov_id"] = currProvID;

            //get a flattened list of elements
            var eleList = form.theform
                .SelectMany(panel =>
                    panel.Value
                        .getElementList()
                        .Where(element => !ignoreTypes.Contains(element.type.ToLower()))
            );

            {
                var formEleList = measurementform.Where(
                    kvp => { 
                        return kvp.Key.StartsWith("form_", StringComparison.InvariantCultureIgnoreCase) && 
                            (eleList.Count(ele => { return ele.name.ToLower() == kvp.Key.ToLower(); }) == 0);
                    }).Select(kvp => { return new Element() { name = kvp.Key, elementcodes = new string[] { kvp.Key } }; }).ToList();

                eleList = eleList.Concat(formEleList);
            }

            //populate dictionary of field names which map to an element (ElementDict)
            foreach (var ele in eleList)
            {
                if(ele.elementcodes != null)
                    foreach (var ele_cd in ele.elementcodes)
                    {
                        if(!ElementDict.ContainsKey(ele_cd))
                            ElementDict[ele_cd] = ele;
                    }
            }

            //add form_ fields from postback where they haven't been defined yet
            foreach (var formel in measurementform
                    .Where(kvp => kvp.Key.StartsWith("form_", StringComparison.OrdinalIgnoreCase) && !ElementDict.ContainsKey(kvp.Key.ToLower()))
                    .Select(kvp => new Element() { name = kvp.Key, type="hidden", elementcodes = new string[] { kvp.Key } })
                )
            {
                ElementDict.Add(formel.name, formel);
            }

            //using the keys in ElementDict, whitelist form field names and their values and run validation on each value
            foreach (var kp in ElementDict)
            {
                var errorStr = "";
                var currError = "";
                string[] fieldValues = null;

                //fieldValues = con.Request.Form.GetValues(kp.Key);
                if (!measurementform.TryGetValue(kp.Key, out fieldValues))
                    fieldValues = new string[] { "" };
                
                //if (fieldValues == null)
                  //  fieldValues = new string[] { "" };

                foreach (var val in fieldValues)
                {
                    //We no longer pass EmptyMeasurementVal / {empty} in to validateString. 
                    //If it's EmptyMeasurementVal, pass in an empty string
                    //Only say the field was not visible when val is null/empty.
                    if (!kp.Value.validateString((val == EmptyMeasurementVal) ? "" : val, out currError, (val.IsNullOrEmpty()) ? false : true, kp.Key))
                        errorStr += currError + ';';
                }
                if (!string.IsNullOrEmpty(errorStr))
                    ElementErrors[kp.Key] = errorStr;

                newMeasurements[kp.Key] = fieldValues;
            }

            //wait for current measurements 
            getCurrMeasurementsTask.Wait();

            actionStr = currMeasurements.Any() ? "updated" : "created";

            //in-place merge the existing measurements in the database with incoming measurements, also getting a list of new records to insert
            var insertDict = mergeMeasurements(newMeasurements, currMeasurements);
            
            //perform 'delta' update on currently existing measurements
            updateMeasurements(currMeasurements);

            //insert new measurement records into database
            insertMeasurements(insertDict, formcontext);

            //update encounter status of the form
            // commented out because the status of the encounter does not need to 
            // change when a form is saved.  ZAPHOD
            if (formcontext.getValueOrDefault("faction") == "complete" || formcontext.getValueOrDefault("faction") == "sign")
            {
                updateEncounterStatus(formcontext, currUserRole, ElementErrors.Any());
            }

            var resp = new SDFResponse();
            //craft a response to send back to the client including errors if applicable
            if ((resp.error = ElementErrors.Any()) )
            {
                if (faction == "complete")
                {
                    resp.msg = "SDF saved but could not be completed due to errors";
                    //con.Response.Write("{\"error\":true,\"msg\":\"form saved but could not be completed due to errors\"");
                }
                else if (faction == "sign")
                {
                    resp.msg = "SDF saved but could not be signed due to errors";
                    //con.Response.Write("{\"error\":true,\"msg\":\"form saved but could not be signed due to errors\"");
                }
                else
                {
                    resp.msg = "SDF saved but is incomplete due to errors";
                    //con.Response.Write("{\"error\":true,\"msg\":\"form saved, but is incomplete due to errors\"");
                }

                //con.Response.Write(",\"errlist\":[");
                foreach (var kv in ElementErrors)
                    resp.errlist.Add(new SDFResponse.SDFErrorList(kv.Key, kv.Value));
                return resp;
                /*
                bool first = true;
                foreach (var kv in ElementErrors)
                {
                    if (!first)
                        con.Response.Write(',');
                    con.Response.Write("{\"name\":\"");
                    con.Response.Write(kv.Key);
                    con.Response.Write("\",\"error\":\"");
                    con.Response.Write(kv.Value);
                    con.Response.Write("\"}");

                    first = false;
                }
                con.Response.Write("]}");
                con.Response.End();
                return;
                 */
            }

            if (faction == "complete")
            {
                resp.msg = "SDF completed and " + actionStr + " successfully";
                //con.Response.Write("{\"error\":false,\"msg\":\"form completed and " + actionStr + " successfully\"}");
            }
            else if (faction == "sign")
            {
                resp.msg = "SDF signed and " + actionStr + " successfully";
                //con.Response.Write("{\"error\":false,\"msg\":\"form signed and " + actionStr + " successfully\"}");
            }
            else
            {
                resp.msg = "SDF " + actionStr + " successfully";
                //con.Response.Write("{\"error\":false,\"msg\":\"form " + actionStr + " successfully\"}");
            }
            return resp;
            //con.Response.End();

        }
    
    }
}