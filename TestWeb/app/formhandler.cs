﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.Handlers;
using System.Web.SessionState;
using System.Web.Security;
using System.Linq;
using Scamps;
using System.Configuration;
using Ionic.Zip;
using ExtensionMethods;

namespace Scamps.Handlers
{
    public static class HttpResponseUtil
    {
        public static HttpResponse rspWrite(this HttpResponse r, char c)
        {
            r.Write(c);
            return r;
        }
        public static HttpResponse rspWrite(this HttpResponse r, string s)
        {
            r.Write(s);
            return r;
        }
        public static HttpResponse fWrite(this HttpResponse r, string fmt, params object[] args)
        {
            r.Output.Write(fmt, args);
            return r;
        }
        public static HttpResponse IFrspWrite(this HttpResponse r, bool statement, string s)
        {
            if (statement)
                return rspWrite(r, s);
            return r;
        }
        public static HttpResponse IfELSErspWrite(this HttpResponse r, bool statement, string iftrue, string iffalse){
            if(statement)
                return rspWrite(r,iftrue);
            return rspWrite(r,iffalse);
        }
    }

    public class FormHandler : IHttpHandler, IRequiresSessionState
    {
        public enum status : int
        {
            OK = 0,
            HASERRORS = 1,
            ERR = 2
        }

        public enum SuccessActions : int
        {
            ReturnBlankForm = 0,
            SuccessMessage = 1,
            SuccessRedirect = 2,
            SuccessDatabase = 3,
            SuccessStrings = 4,
            SuccessTemplate = 5,
            SuccessForm = 6,
            SuccessLogin = 7,
            SuccessRefresh = 8,
            SuccessBack = 9,
            SuccessClose = 10,
            SuccessCustom = 20
        }

        public bool IsReusable
        {
            get { return false; }
        }

        private status currentstatus = status.OK;
        private string entity = "";                         //the name of the form or the form 'concept', could simply be the target table name.
        private HttpContext con = null;
        private bool haserrors = false;
        
        public void ProcessRequest(HttpContext context)
        {
            string results = "";
            int ii = 0;

            // default database submission to true
            bool Save = false;           //default state is to save form data.
            string tmp = "";            //temporary work space for string assembly
            string tmp2 = "";
            string formname = "";       //form name;
            string path = "";           //path to the form;
            string target = "";         //what table does this data go into
            string version = "";         //the form's original version to re-render the correct elements
            string id_criteria = "";    //what is the criteria to update a record
            string action = "";         //what ya gonna do about it...
            string successdata = "";    //url, identifier, another form name... what does the success action need to know?
            int ID = 0;
            int n = 0;
            string mrn = "";
            bool adhoc = false;

            string disablecontrols = "";    //list of controls to DISABLE upon return
            string hidecontrols = "";       //list of controls to HIDE upon return

            // Set context to local
            this.con = context;

            string externalSQL = Tools.ReadFile(con.Server.MapPath("\\resources\\data\\scampsweb.sql"));

            //Scamps.Tools.AppendFile(con.Server.MapPath("/debug.log"), "enter form handler," + System.Environment.NewLine);

            //form dictionary
            Dictionary<string, string> hd = new Dictionary<string, string>();
            //response dictionary
            Dictionary<string, string> response = new Dictionary<string, string>();

            // Roll the form into the dictionary
            foreach (string name in con.Request.Form)
            {
                if (name.Length > 0)
                {
                    hd.Add(name,con.Request.Form[name].ToString().Trim());
                }
            }

            //fetch the record ID from the form...
            if (hd.ContainsKey("_ID"))
            {
                ID = Scamps.Tools.AsInteger(hd["_ID"]);
            }

            Scamps.DataTools db = new Scamps.DataTools();
            var csettings = System.Configuration.ConfigurationManager.ConnectionStrings["SCAMPs"];
            db.Provider = csettings.ProviderName;
            db.ConnectionString = csettings.ConnectionString;
            db.OpenConnection();

            Scamps.Form form = new Scamps.Form();

            // pull the form context from the reference embedded in the form itself
            // --------------------------------------------------------------------
            Dictionary<string, string> formcontext = new Dictionary<string, string>();
            if (hd.ContainsKey("_formreference"))
            {
                formcontext = form.FormContext(hd["_formreference"]);

                entity = formcontext.Get("entity");
                formname = formcontext.Get("name");
                path = formcontext.Get("form");
                version = formcontext.Get("version");

                // additional context specific form elements to add here...
                // like from the form headers, or based on user context?
                // anything with form_ is good to go.
                try
                {
                    hd.AddOrReplace("form_encounter_status", Tools.GetValue("encounter_status", formcontext));
                    hd.AddOrReplace("form_rendered_version", version);
                    TimeSpan ts = DateTime.Now - Convert.ToDateTime(Tools.GetValue("initialized", formcontext));
                    hd.AddOrReplace("form_time", ts.TotalSeconds.ToString());
                }
                catch(Exception tex)
                {
                    hd.AddOrReplace("form_time", "0");
                    hd.AddOrReplace("form_time_error", "Invalid time conversion seconds");
                }
            }
            else
            {
                // this form was not generated by the form object
                entity = hd.Get("entity");
                adhoc = true;
            }

            // special switch out for patient form
            if(entity.ToLower() == "patient")
            {
                hd.AddOrReplace("mrn", hd.getValueOrDefault("testedmrn"));
            }

            form.BasePath = con.Server.MapPath("/");
            if (entity == "measurements") { form.Options.VersionWithHiddenElements = true; }
            form.FormData = hd;
            //set the form version pulled from the form reference header.
            form.Version = version;
            form.ConnectionString = csettings.ConnectionString;
            form.Provider = csettings.ProviderName;
            
            if (path.Length==0)
            {
                path = con.Server.MapPath("/resources/forms/") + formname;
                if(!path.EndsWith(".form")) { path += ".form";}
            }

            string[] roles = Roles.GetRolesForUser();
            string userRole = "";
            if (roles.Length > 0)
            {
                userRole = roles[0].ToUpper();
            }

            // add another form_ element to track the user type...
            hd.AddOrReplace("form_user_role", userRole);
            //If the form version has been set by the formcontext, don't override it, add to it.
            if (entity != "measurements" && userRole != "PROVIDER") { 
                if(form.Version.IsNullOrEmpty())
                    form.Version = "dc"; 
                else
                    form.Version += " dc"; 
            }

            form.Options.UseHTML5Markup = false;
            form.UserKeys = UserKeys();
            form.UserRole = userRole;

            // --- Load the Form is managed by the form object ---
            if (!adhoc)
            {
                form.Load(path);
                // --- form loaded, data in place
                // --- special form element context handling
                foreach (KeyValuePair<string, Panel> panel in form.theform)
                {
                    Dictionary<string, Element> d = panel.Value.dictionary;
                    foreach (KeyValuePair<string, Element> e in d)
                    {
                        // bitwise types - multiple bit flags stored as a single integer
                        // leave the data inside the form object alone, as validation
                        // works on the value list
                        if(e.Value.type == "bitwise" || e.Value.type == "bitwiselist")
                        {
                            tmp = "bitwise_" + e.Value.name;

                            n = 0;

                            string[] an = hd.Get(tmp).Split(',');
                            for (int nn = 0; nn < an.Length; nn++)
                            {
                                n = n + Tools.AsInteger(an[nn]);
                            }
                            hd.AddOrReplace(e.Value.name, n.ToString());
                        }
                    }
                }
            }

            
            action = form.Success_Action;
            successdata = form.Success_Identifier;

            if (entity.Length > 0)
            {
                //Scamps.Tools.AppendFile(con.Server.MapPath("/debug.log"), "form handler entity:" + entity + System.Environment.NewLine);
                //measurements is special case, processed differently until refactored
                if (entity == "measurements")
                {
                    //SDF.processMeasurements(con, form, formcontext);
                    var mdict = new Dictionary<string,string[]>();
                    foreach(var key in con.Request.Form.AllKeys)
                        mdict.AddOrReplace(key,con.Request.Form.GetValues(key));

                    // Because Jon is an idiot and had to do this differently than all other forms
                    // We have to redo some of the stuff we did above because, you know, Jon
                    // Introducing a new search hash tag so we can identify these oops places and fix them eventually: #jondidit

                    if (hd.ContainsKey("form_encounter_status")) { mdict.AddOrReplace("form_encounter_status", new[] { hd.getValueOrDefault("form_encounter_status") }); }
                    if (hd.ContainsKey("form_rendered_version")) { mdict.AddOrReplace("form_rendered_version", new[] { hd.getValueOrDefault("form_rendered_version") }); }
                    if (hd.ContainsKey("form_time")) { mdict.AddOrReplace("form_time", new[] { hd.getValueOrDefault("form_time") }); }
                    if (hd.ContainsKey("form_time_error")) { mdict.AddOrReplace("form_time_error", new[] { hd.getValueOrDefault("form_time_error") }); }

                    var res = SDF.processMeasurements(mdict, con.User.Identity.Name, form, formcontext);
                    using(var jw = new Newtonsoft.Json.JsonTextWriter( con.Response.Output )){
                        var ss = Newtonsoft.Json.JsonSerializer.Create();
                            ss.Serialize(jw, res);
                    }

                    // zaphod, update status if the status currently is unstarted...
                    if (hd.Get("form_encounter_status").AsInteger() < 2)
                    {
                        if (formcontext.Get("visit_type") == "I")
                        {
                            db.Execute("update visits_inpt_subunit set encounter_status = 2 where encounter_status < 2 and id = " + formcontext.Get("sub_id"));
                            db.Execute("commit");
                        }
                        else
                        {
                            db.Execute("update pt_encounters set encounter_status = 2 where where encounter_status < 2 and appt_id = " + formcontext.Get("appt_id"));
                            db.Execute("commit");
                        }
                    }
                    return;
                }

                //form validation here
                form.Validate();
                hd = form.FormData;
                if (form.IsValid)
                {
                    Save = false;
                    currentstatus = status.OK;
                    switch (entity.ToLower())
                    {
                        //special handling for specific form instances

                        case "siteconfiguration":

                            int configid =0;
                            Dictionary<string, string> configdata = new Dictionary<string, string>();
                            foreach (KeyValuePair<string, string> field in hd)
                            {
                                if (field.Key.ToLower() != "entity" && field.Key.ToLower() != "submit")
                                {
                                    configid=Tools.AsInteger(db.GetValue("select id from admin_config where attrib='" + field.Key.ToUpper() + "'"));
                                    if(configid > 0)
                                    {
                                        configdata.Clear();
                                        configdata.Set("attrib", field.Key.ToUpper());
                                        configdata.Set("attrib_val", field.Value);
                                        db.Update(configdata, "admin_config", "id=" + configid.ToString());
                                    }
                                }
                            }
                            action = "COMPLETE";
                            break;

                        case "hashlookup":
                            mrn = Tools.TrimTo(Tools.URLString(Tools.GetValue("hash", hd)),20);

                            if (mrn.IsNullOrEmpty()) { mrn = "empty"; }
                            results = "<li>No match found for <strong><i>" + mrn + "</i></strong></li>";
                            successdata = "#hashlookup";
                            action = "update";
                            if(mrn != "empty")
                            {
                                db.GetResultSet("select mrn, fname || ' ' || lname as name from pt_master where (id = '" + mrn + "' OR ORA_HASH(mrn) = '" + mrn + "')");
                                if(!db.EOF)
                                {
                                    mrn = db.Get("mrn");
                                    results = "<li><a href=\"/patient/" + mrn + "\"><i class=\"icon-patLocal icon-user-3\"></i>" + mrn + " " + db.Get("name") + "</a></li>";
                                }
                            }
                            break;

                        case "importfile":

                            // quick file upload and import to cover Sandy when she said importing would take zero time to implement
                            if (con.Request.Files.Count > 0 || con.Request.InputStream.CanRead)
                            {
                                string filename = "";
                                string filetype = Tools.GetValue("filetype", hd);       // what type of file is this?  According to the user.
                                string tempdirectory = "";

                                string output = "";
                                n = 0;
                                int header_offset = 0;
                                int rec_count = 0;
                                int err_count = 0;
                                int inner_count = 0;
                                bool has_errors = false;
                                string uploadedfile = UploadFile(ref filename, ref tempdirectory);
                                string sql = "";
                                string mrnlist = "";
                                int encounter_type = 0;
                                int scamp_id = 0;
                                int provider_id = 0;
                                int location_id = 0;
                                bool mrnok = false;

                                // check file length?  Other checks before reading it? ZAPHOD
                                // the file size limit can be managed in web config
                                uploadedfile = Tools.ReadFile(uploadedfile);

                                Tools.AppendLine(ref output, "---" + DateTime.Now.ToString() + "---");
                                Tools.AppendLine(ref output, "LOGGED IN USER: " + HttpContext.Current.User.Identity.Name);
                                
                                // split the file into lines, retain empty lines so that line
                                // numbers can be referenced in error reporting

                                // basic validation, split the file
                                string[] lines = uploadedfile.Trim().Split(new string[] {Environment.NewLine},StringSplitOptions.None);
                                if (lines.Length > 0)
                                {
                                    //there is content in the file
                                    // check if the first line is a column name listing
                                    // if so, set the header_offset to 1
                                    // this will also confirm that the file type is, indeed 
                                    // the anticipated file type, but in many ways we don't care

                                    if (lines[0].ToLower().Trim().StartsWith("mrn,fname,lname,"))
                                    {
                                        //this is a patient layout
                                        header_offset = 1;
                                        if (filetype != "patient")
                                        {
                                            has_errors = true;
                                            Tools.AppendLine(ref output, "Uploaded file layout is not consistent with specified file type 'patient'.");
                                        }
                                    }
                                    else if (lines[0].ToLower().Trim().StartsWith("form_name,encounter_appt,appt_id,"))
                                    {
                                        //this is an encounter layout
                                        header_offset = 1;
                                        if (filetype != "encounter")
                                        {
                                            has_errors = true;
                                            Tools.AppendLine(ref output, "Uploaded file layout is not consistent with specified file type 'encounter'.");
                                        }
                                    }

                                    if (!has_errors)
                                    {
                                        // make a database connection (if all is well at this point)
                                        var data = new DataTools();
                                        var connstring = ConfigurationManager.ConnectionStrings["SCAMPs"];
                                        data.Provider = connstring.ProviderName;
                                        data.ConnectionString = connstring.ConnectionString;
                                        data.OpenConnection();
                                        // create the templating object as well
                                        var template = new Templating();
                                        template.Provider = data.Provider;
                                        template.ConnectionString = data.ConnectionString;
                                        
                                        // get an in-memory list of MRNs to check against
                                        // string size limit is 2^31.  At 8 character MRN size
                                        // this is about 220K patients, nowhere near our typical
                                        // patient count, and a minor memory hit all things considered
                                        sql = Tools.GetBlock(externalSQL, "import.getallmrns");
                                        mrnlist = template.Results(sql, "-[mrn]-");

                                        //check for connection errors and what not 

                                        if (template.haserrors)
                                        {
                                            // error with connection or otherwise
                                            Tools.AppendLine(ref output, "Error with initial data connection: " + template.errors);
                                            has_errors = true;
                                        }
                                        else
                                        {
                                            // continue...
                                            // create local line array
                                            string[] data_array = null;
                                            Dictionary<string, string> d = new Dictionary<string, string>();
                                            // loop through the file, checking each row, inserting as we go.

                                            Tools.AppendLine(ref output, "Importing " + filetype + " records.  File contains " + lines.Length + " rows.");

                                            for (n = header_offset; n < lines.Length; n++)
                                            {

                                                // how is this going?  Not well?  GTFO
                                                if (err_count > Tools.Smaller(50,Tools.AsInteger((lines.Length/4)+5)) && rec_count == 0)
                                                {
                                                    if (inner_count < Tools.Smaller(15,Tools.AsInteger(lines.Length/8)))
                                                    {
                                                        output = "Too many errors to continue. Please verify the validity of the data." + System.Environment.NewLine;
                                                    }
                                                    else
                                                    {
                                                        Tools.AppendLine(ref output, "Too many errors to continue.  Correct your file layout and try again.");
                                                    }
                                                    break;
                                                }

                                                switch (filetype)
                                                {
                                                    // Import of Users/Providers/Locations - stubs
                                                    case "location":


                                                        // LOCATIONS - both the Decription and the Value must be unique to import
                                                        // this restriction does not exist in the database or the application,
                                                        // but there should be some way to distinguish two different selections (by description)
                                                        // or find a specific location (by value), therefore they need to be unique.

                                                        // Locations in general need to be rethought to be actually useful to a site
                                                        // instead of a burden.  They are of no particular use to IRCDA.
                                                        data_array = Location(lines[n], n, ref has_errors, ref output);
                                                        if (has_errors)
                                                        {
                                                            err_count++;
                                                            has_errors = false;
                                                        }
                                                        else
                                                        {
                                                            // first, the value has to be unique?
                                                            sql = "select id from admin_lookup where upper(trim(BOTH from value_txt)) = '" + data_array[2].ToUpper() + "'";
                                                            if (Tools.AsInteger(data.GetValue(sql)) != 0)
                                                            {
                                                                Tools.AppendLine(ref output, "Line " + (n + 1).ToString() + " - Location value " + data_array[2] + " already in use.  Row skipped.");
                                                            }
                                                            else
                                                            {

                                                                // now, check that description is unique
                                                                sql = "select id from admin_lookup where upper(trim(BOTH from description_txt)) = '" + data_array[1].ToUpper() + "'";
                                                                if (Tools.AsInteger(data.GetValue(sql)) != 0)
                                                                {
                                                                    Tools.AppendLine(ref output, "Line " + (n + 1).ToString() + " - Location description " + data_array[2] + " already in use.  Row skipped.");
                                                                }
                                                                else
                                                                {
                                                                    d.Clear();
                                                                    switch (data_array[0])
                                                                    {
                                                                        case "I":
                                                                            Tools.AddOrReplace("lookup_cd", "LOCTN_INPT", ref d);
                                                                            break;
                                                                        case "O":
                                                                            Tools.AddOrReplace("lookup_cd", "LOCTN_OUTPT", ref d);
                                                                            break;
                                                                        case "E":
                                                                            Tools.AddOrReplace("lookup_cd", "LOCTN_EVENT", ref d);
                                                                            break;

                                                                    }
                                                                    Tools.AddOrReplace("description_txt", data_array[1], ref d);
                                                                    Tools.AddOrReplace("value_txt", data_array[2], ref d);
                                                                    Tools.AddOrReplace("visible_abbr", "Y", ref d);

                                                                    data.Insert(d, "admin_lookup");
                                                                    if (data.haserrors)
                                                                    {
                                                                        err_count++;
                                                                        Tools.AppendLine(ref output, "Line " + (n + 1).ToString() + " - DATA error: " + data.errors);
                                                                        data.haserrors = false;
                                                                    }
                                                                    else
                                                                    {
                                                                        rec_count++;
                                                                        Tools.AppendLine(ref output, "Line " + (n + 1).ToString() + " - <strong>Imported location " + data_array[2] + " successfully.</strong>");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        break;

                                                    case "provider":
                                                    case "user":

                                                        break;

                                                    // CORE required imports, supported for Florida
                                                    case "patient":

                                                        data_array = Patient(lines[n], n, ref has_errors, ref output);
                                                        if (has_errors)
                                                        {
                                                            err_count++;
                                                            has_errors = false;
                                                        }
                                                        else
                                                        {
                                                            inner_count++;
                                                            // Check that MRN is NOT part of the MRN list
                                                            if (mrnlist.IndexOf("-" + data_array[0] + "-") == -1)
                                                            {
                                                                d.Clear();
                                                                d.Set("mrn", data_array[0]);
                                                                d.Set("fname", data_array[1]);
                                                                d.Set("lname", data_array[2]);
                                                                d.Set("dob", data_array[3]);
                                                                d.Set("sex", data_array[4]);

                                                                data.Insert(d, "pt_master");

                                                                if (data.haserrors)
                                                                {
                                                                    err_count++;
                                                                    Tools.AppendLine(ref output, "Line " + (n + 1).ToString() + " - DATA error: " + data.errors);
                                                                    data.haserrors = false;
                                                                }
                                                                else
                                                                {
                                                                    rec_count++;
                                                                    Tools.AppendLine(ref output, "Line " + (n + 1).ToString() + " - <strong>Imported patient record successfully.</strong>");
                                                                }
                                                            }
                                                            else
                                                            {
                                                                // the MRN was found to currently exist, ignore row.
                                                                err_count++;
                                                                Tools.AppendLine(ref output, "Line " + (n + 1).ToString() + " - MRN already exists.  Row Ignored.");
                                                            }
                                                        }

                                                        break;

                                                    case "encounter":

                                                        data_array = Encounter(lines[n], n, ref has_errors, ref output);
                                                        if (has_errors)
                                                        {
                                                            err_count++;
                                                            has_errors = false;
                                                        }
                                                        else
                                                        {
                                                            mrnok = false;
                                                            // have to look up the form name to resolve the encounter type.

                                                            sql = "select encounter_id,scamp_id from admin_encounters where UPPER(encounter_name_short) = '" + data_array[0] + "'";

                                                            data.GetResultSet(sql);
                                                            if (data.EOF)
                                                            {
                                                                // - that's a bummer, dude.
                                                                err_count++;
                                                                Tools.AppendLine(ref output, "Line " + (n + 1).ToString() + " - Not a recognized/installed form reference.  Row Ignored.");
                                                                break;
                                                            }
                                                            else
                                                            {

                                                                // - got a record, the form is installed and recognized

                                                                encounter_type = Tools.AsInteger(data.Get("encounter_id"));
                                                                scamp_id = Tools.AsInteger(data.Get("scamp_id"));           // this will be used below to register the patient

                                                                // do we need the scamp ID?  We could resolve the scamp for reporting/confirmation


                                                                // check that the MRN IS part of the MRN block, i.e. this is an MRN in the system
                                                                if (mrnlist.IndexOf("-" + data_array[4] + "-") != -1) { mrnok = true; }

                                                                // resolve both location and attending
                                                                provider_id = 0;
                                                                location_id = 0;

                                                                // attending...
                                                                if (data_array[6].IsNullOrEmpty())
                                                                {
                                                                    Tools.AppendLine(ref output, "Line " + (n + 1).ToString() + " - WARNING No attending provider set.");
                                                                }
                                                                else
                                                                {
                                                                    sql = Tools.GetBlock(externalSQL, "import.checkprovider");
                                                                    sql = sql.ReplaceToken("criteria", data_array[6].ToLower());
                                                                    provider_id = Tools.AsInteger(data.GetValue(sql));
                                                                    if (provider_id == 0)
                                                                    {
                                                                        Tools.AppendLine(ref output, "Line " + (n + 1).ToString() + " - WARNING Attending provider '" + data_array[6] + "' failed to resolve.");
                                                                        data_array[6] = "";
                                                                    }
                                                                }

                                                                // location...
                                                                if (data_array[5].IsNullOrEmpty())
                                                                {
                                                                    Tools.AppendLine(ref output, "Line " + (n + 1).ToString() + " - WARNING No location set.");
                                                                }
                                                                else
                                                                {
                                                                    sql = Tools.GetBlock(externalSQL, "import.checklocation");
                                                                    sql = sql.ReplaceToken("criteria", data_array[5]);

                                                                    location_id = Tools.AsInteger(data.GetValue(sql));
                                                                    if (location_id == 0)
                                                                    {
                                                                        Tools.AppendLine(ref output, "Line " + (n + 1).ToString() + " - WARNING Location '" + data_array[5] + "' failed to resolve.");
                                                                        data_array[5] = "";
                                                                    }
                                                                }

                                                                // finally, check that the appointment ID is unique...
                                                                sql = Tools.GetBlock(externalSQL, "import.checkapptid");
                                                                sql = sql.ReplaceToken("criteria", data_array[2]);
                                                                if (Tools.AsInteger(data.GetValue(sql)) != 0)
                                                                {
                                                                    Tools.AppendLine(ref output, "Line " + (n + 1).ToString() + " - Appointment ID " + data_array[2] + " already exists.  Row ignored.");
                                                                }
                                                                else
                                                                {
                                                                    inner_count++;
                                                                    if (mrnok)
                                                                    {
                                                                        data.haserrors = false;
                                                                        d.Clear();
                                                                        d.Set("encounter_cd", "O");
                                                                        d.Set("encounter_status", "1");
                                                                        d.Set("clinic_appt", data_array[1]);
                                                                        d.Set("appt_id", data_array[2]);
                                                                        d.Set("har", data_array[3]);
                                                                        d.Set("mrn", data_array[4]);
                                                                        d.Set("clinic_name", data_array[5]);
                                                                        if (location_id != 0)
                                                                        {
                                                                            d.Set("clinic_no", location_id.ToString());
                                                                        }
                                                                        d.Set("attending", data_array[6]);

                                                                        if (provider_id != 0)
                                                                        {
                                                                            d.Set("scamps_prov_id", provider_id.ToString());
                                                                        }
                                                                        d.Set("encounter_type", encounter_type.ToString());
                                                                        d.Set("source_txt", "CSV IMPORT");

                                                                        data.Insert(d, "pt_encounters");
                                                                        if (data.haserrors)
                                                                        {
                                                                            err_count++;
                                                                            Tools.AppendLine(ref output, "Line " + (n + 1).ToString() + " - DATA error: " + data.errors);
                                                                            data.haserrors = false;
                                                                        }
                                                                        else
                                                                        {
                                                                            rec_count++;
                                                                            Tools.AppendLine(ref output, "Line " + (n + 1).ToString() + " - <strong>Imported encounter successfully.</strong>");

                                                                            // now register the user in the scamp
                                                                            // we need the scamp ID, the MRN, and the status val of 1
                                                                            // is the patient already enrolled in the SCMAP?

                                                                            d.Clear();
                                                                            d.Set("mrn", data_array[4]);
                                                                            d.Set("scamp_id", scamp_id.ToString());
                                                                            d.Set("status_val", "1");

                                                                            sql = "select id from pt_scamp_status where mrn = '" + data_array[4] + "' and scamp_id = " + scamp_id.ToString();
                                                                            if (Tools.AsInteger(data.GetValue(sql)) == 0)
                                                                            {
                                                                                // new enrollment
                                                                                data.Insert(d, "pt_scamp_status");
                                                                                if (data.haserrors)
                                                                                {
                                                                                    err_count++;
                                                                                    Tools.AppendLine(ref output, "Line " + (n + 1).ToString() + " - DATA error (enroll patient): " + data.errors);
                                                                                    data.haserrors = false;
                                                                                }
                                                                                else
                                                                                {
                                                                                    Tools.AppendLine(ref output, "Line " + (n + 1).ToString() + " - <strong>Enrolled patient in SCAMP.</strong>");
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        err_count++;
                                                                        Tools.AppendLine(ref output, "Line " + (n + 1).ToString() + " - MRN " + data_array[4] + " does not exist.  Row Ignored.");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        break;
                                                }
                                            }
                                        }

                                        // housekeeping...
                                        data.Dispose();

                                    }   // end not has_errors

                                    Tools.AppendLine(ref output, "Import process completed - " + DateTime.Now.ToString());
                                }
                                else  // lines.length == 0
                                {
                                    Tools.AppendLine(ref output, "The file uploaded is empty.");
                                    has_errors = true;
                                }

                                //write log file from log info
                                Tools.AppendFile(con.Server.MapPath("\\assets") + "\\import.log", output + System.Environment.NewLine);

                                //delete all files...
                                Directory.Delete(con.Server.MapPath("\\assets\\") + tempdirectory, true);

                                action = "update";
                                results = Tools.ToHTMLList(output);
                                successdata = "#importresults";
                                disablecontrols = ".installbutton";
                            }
                            break;

                        case "fileupload":
                            // move this to its own method once tested...

                            //yo yo yo, handle a file upload.  Going to have to break this into a few 
                            //different types of uploads, but for now, we stick with a scamp update...

                            //do we have a file to read?
                            if( con.Request.Files.Count > 0 || con.Request.InputStream.CanRead)
                            {
                                //figure a temp directory to put the file in...
                                string tempdirectory = Tools.RandomSeed(16);
                                string filename = "";
                                string loginfo = "";
                                bool hasErrors = false;
                                bool deletefiles = false;
                                Stream filehandle = null;

                                Tools.AppendLine(ref loginfo, "");
                                Tools.AppendLine(ref loginfo, "--- " + DateTime.Now.ToString() + " ---");
                                Tools.AppendLine(ref loginfo, "LOGGED IN USER: " + HttpContext.Current.User.Identity.Name);

                                int b = 0;
                                byte[] ba = new byte[8 * 1024];

                                //does our root file path exist?  If not, create it...
                                string filepath = con.Server.MapPath("/assets/");
                                if (!Directory.Exists(filepath))
                                {
                                    Directory.CreateDirectory(filepath);
                                    Tools.AppendLine(ref loginfo, "Created root assets path");
                                }

                                //does this directory exist?  If so, kill it
                                if(Directory.Exists(filepath + tempdirectory))
                                {
                                    Tools.DeleteFiles(filepath + tempdirectory);
                                    Directory.Delete(filepath + tempdirectory);
                                    Tools.AppendLine(ref loginfo, "Remove pre-existing temporary directory ");
                                }
                                // create it anew...
                                Directory.CreateDirectory(filepath + tempdirectory);
                                Tools.AppendLine(ref loginfo, "Created temporary directory ");

                                if(con.Request.Files.Count>0)
                                {
                                    filehandle = con.Request.Files[0].InputStream;
                                    filename = Path.GetFileName(con.Request.Files[0].FileName);
                                }
                                else if (con.Request.InputStream != null && con.Request.InputStream.Length>0)
                                {
                                    filehandle = con.Request.InputStream;
                                    filename = Tools.AsText(con.Request.QueryString["filename"]);
                                    if(filename.IsNullOrEmpty())
                                    {
                                        filename = "scamp.zip";
                                    }
                                }

                                filename = filename.ToLower();

                                if (filename.EndsWith(".xlsx") || filename.EndsWith(".xls"))
                                {
                                    Tools.AppendLine(ref loginfo, "<span class=\"error\">Install Failed: Uploaded file is not a SCAMP file.</span>");
                                    hasErrors = true;
                                    deletefiles = true;
                                }
                                else
                                {
                                    //save the zip file into the temp directory
                                    Stream writefile = new FileStream(filepath + tempdirectory + "\\" + filename, FileMode.Create, FileAccess.Write);

                                    //Stream writefile = File.Create(filepath + tempdirectory + "\\" + filename,(int)filehandle.Length);
                                    while ((b = filehandle.Read(ba, 0, ba.Length)) > 0)
                                    {
                                        writefile.Write(ba, 0, b);
                                    }

                                    writefile.Close();
                                    filehandle.Close();
                                    writefile.Dispose();
                                    filehandle.Dispose();

                                    Tools.AppendLine(ref loginfo, "Upload user file " + filename);

                                    //unzip the file
                                    try
                                    {
                                        using (ZipFile zip = ZipFile.Read(filepath + tempdirectory + "\\" + filename))
                                        {
                                            foreach (ZipEntry entry in zip)
                                            {
                                                entry.Extract(filepath + tempdirectory, ExtractExistingFileAction.OverwriteSilently);
                                            }
                                        }
                                        Tools.AppendLine(ref loginfo, "Extracted archive");
                                    }
                                    catch (Exception ex)
                                    {
                                        Tools.AppendLine(ref loginfo, "<span class=\"error\">File extraction failed:Uploaded file is not a SCAMP file</span>");
                                        hasErrors = true;
                                    }

                                    //create /scamps and /scamps/images directories if not already there.
                                    if (!Directory.Exists(con.Server.MapPath("/scamps")))
                                    {
                                        Directory.CreateDirectory(con.Server.MapPath("/scamps"));
                                        Tools.AppendLine(ref loginfo, "Created SCAMPS folder");
                                    }
                                    if (!Directory.Exists(con.Server.MapPath("/scamps/images")))
                                    {
                                        Directory.CreateDirectory(con.Server.MapPath("/scamps/images"));
                                        Tools.AppendLine(ref loginfo, "Created SCAMPS image asset folder");
                                    }
                                }
                                if (!hasErrors)
                                {

                                    // move all the images into /scamps/images

                                    bool filesmoved = MoveScampImages(filepath + tempdirectory,ref loginfo);
                                    if (!filesmoved)
                                    {
                                        //todo: log this error - images files could not be moved.
                                        hasErrors = true;
                                    }

                                    // update / install any .list files.
                                    bool listsimported = ImportLists(filepath + tempdirectory, ref loginfo);
                                    if (!listsimported)
                                    {
                                        hasErrors = true;
                                    }

                                    // look inside each file and validate that we know what it is...
                                    bool formsimported = ImportForms(filepath + tempdirectory, ref loginfo);
                                    if (!formsimported)
                                    {
                                        hasErrors = true;
                                    }

                                    // import reports.
                                    bool reportsimported = ImportReports(filepath + tempdirectory, ref loginfo);
                                    if (!reportsimported)
                                    {
                                        hasErrors = true;
                                    }

                                    // import other application files
                                    bool appfiles = ImportAppFiles(filepath + tempdirectory, ref loginfo);
                                    if (!appfiles)
                                    {
                                        hasErrors = true;
                                    }

                                }  //!hasErrors

                                //write log file from log info
                                Tools.AppendFile(filepath + "\\import.log", loginfo + System.Environment.NewLine);

                                //delete all files...
                                Directory.Delete(filepath + tempdirectory,true);

                                action = "update";
                                results = Tools.ToHTMLList(loginfo);
                                successdata = "#uploadresults";
                                disablecontrols = ".installbutton";

                            }


                            break;

                        case "linked":
                            string app = Tools.GetValue("appointments", hd).Trim();
                            string[] appts = new string[0];

                            if (!app.IsNullOrEmpty())
                            {
                                appts = Tools.GetValue("appointments", hd).Split(',');
                            }
                            string encounter_id = Tools.GetValue("encounter_id",hd);
                            try
                            {
                                ScampsDB.Query.Encounters.linkEncounters(encounter_id, appts);
                                action = "close";
                            }
                            catch(Exception ex)
                            {
                                //TODO: log this error and/or provide user feedback.  When we have a proper database.
                            }
                            break;
                        case "location":

                            target = "admin_lookup";
                            id_criteria = "id=" + ID.ToString();
                            string value = hd.Get("value_txt").ToUpper();
                            string desc = hd.Get("description_txt").ToUpper();
                            hd.Set("value_txt", value);
                            
                            Save = true;
                            // does this location already exist - either the description OR the value
                            // yes, yes, silly, but it is what is is until we go the schema over.
                            n = Tools.AsInteger(db.GetValue("select id from admin_lookup where lookup_cd in('LOCTN_OUTPT,LOCTN_INPT') and UPPER(description_txt) = '" + desc + "'"));
                            if(n > 0 && n != ID)
                            {
                                // description already exists.  
                                Save = false;
                                currentstatus = status.HASERRORS;
                                form.SetError("description_txt","This location description already exists");
                            }

                            n = Tools.AsInteger(db.GetValue("select id from admin_lookup where lookup_cd = '" + hd.Get("lookup_cd").ToUpper() + "' and value_txt = '" + value + "'"));
                            if (n > 0 && n != ID)
                            {
                                // description already exists.  
                                Save = false;
                                currentstatus = status.HASERRORS;
                                form.SetError("value_txt", "This location value already exists");
                            }
                            break;
                        // patient records...
                        case "patient":
                            //ID = 0;
                            target = "pt_master";
                            string localmrn = "";
                            mrn = Tools.URLString(Tools.GetValue("testedmrn", hd)).ToUpper();
                            if(mrn.IsNullOrEmpty()) {  mrn = Tools.GetValue("mrn", hd).ToUpper(); }

                            // clean up some data because apparently we really need to.
                            hd.AddEdit("lname", Tools.UnQ(hd.Get("lname").Trim()));
                            hd.AddEdit("fname", Tools.UnQ(hd.Get("fname").Trim()));

                            if (ID == 0)   // new record
                            {
                                if (Tools.isLettersOrDigits(mrn))
                                {
                                    //check that the MRN is unique...
                                    localmrn = Scamps.Tools.AsText(db.GetValue("select mrn from pt_master where mrn = '" + mrn + "'"));
                                    if (!localmrn.IsNullOrEmpty())
                                    {
                                        //error, patient exists...
                                        form.SetError("mrn", "Patient already exists");
                                    }
                                }
                                else
                                {
                                    form.SetError("mrn", "Invalid MRN");
                                }

                                db.Insert(hd, "pt_master");
                                ID = Tools.AsInteger(db.GetValue("select max(id) from pt_master"));
                                
                                // navigate to patient page after new patient is added.
                                addAction(ref action,"REDIRECT");
                                response.Add("url", "/patient/" + mrn);
                            }
                            else
                            {
                                id_criteria = "id = " + ID.ToString();
                                db.Update(hd, "pt_master",id_criteria);
                            }

                            if (form.IsValid)
                            {
                                
                                Tools.AddOrReplace("mrn", mrn, ref hd);
                            }

                            response.AddOrReplace("highlightid", "P" + ID.ToString());
                            Save = false;
                            response.AddEdit("id", mrn);
                            addAction(ref action, "SETLSPATIENT");
                            addAction(ref action,"REDIRECT");
                            response.AddOrReplace("url", "/patient/" + mrn);
                            break;

                        // enroll or change enrollment status

                        case "pt_scamp_status":

                            mrn = hd.Get("mrn").ToUpper().TrimTo(20);
                            mrn = Tools.URLString(mrn);
                            int i = hd.Get("scamp_id").AsInteger();
                            // add or update pt_scamp_status
                            if (Tools.AsInteger(db.GetValue("select id from pt_scamp_status where upper(mrn) ='" + mrn + "' and scamp_id = " + i.ToString()))==0)
                            {
                                db.Insert(hd, "pt_scamp_status");
                            }
                            else
                            {
                                db.Update(hd, "pt_scamp_status", "upper(mrn) = '" + mrn + "' and scamp_id = " + i.ToString());
                            }

                            hd.AddEdit("user_id", System.Security.Principal.WindowsIdentity.GetCurrent().Name);
                            hd.AddEdit("new_val", hd.Get("status_val"));
                            hd.AddEdit("audit_dt_tm", DateTime.Now.ToString());
                            db.Insert(hd, "admin_screening");
                            break;


                        // add or update an encounter
                        // this is a very tricky prospect, since
                        // it could be in one of two tables, maybe
                        // both, and openly editable fields can 
                        // changes where the data is located.
                        // inpatient: in visits_inpt_subunit
                        // outpatient: in pt_encounters
                        // if inpatient changes to outpatient, data already in subunits needs to be deleted
                        // and instead added into pt_encounters, or vis versa?  That is simply nuts!

                        case "encounter":

                            // NEW inpatient encounters create both a pt_encounter
                            // record, AND a visits_inpt_subunits record.

                            string visit_type = hd.Get("visit_type");
                            if(visit_type != "I" && visit_type != "O")
                            {
                                visit_type = "O";
                                hd.AddOrReplace("visit_type", visit_type);
                            }

                            hd.AddOrReplace("encounter_cd", visit_type);

                            int form_type = 0;

                            bool harupdate = false;         // is the HAR being modified?
                            bool newhar = false;            // is this a new HAR to the system?
                            bool inserthar = false;         // based on a HAR edit, do we need to insert a 
                                                            // new PT_ENCOUNTER record?

                            hd.AddEdit("har", hd.Get("har").ToUpper());
                            if(hd.Get("har") != hd.Get("original_har"))
                            {
                                harupdate = true;
                            }

                            // if the button action was delete, we should do something about it
                            string submit_action = hd.Get("form_submit_action");

                            // resolve attending name, location name
                            // because the relational model is mysterious and scary 
                            // TODO: get rid of all this when we finally get rid of the batch centric
                            //       code that makes us all crazy.

                            if (hd.Get("clinic_no").AsInteger() > 0)
                            {
                                // resolve the clinic name....
                                hd.AddOrReplace("clinic_name", Tools.AsText(db.GetValue("select description_txt || ' (' || value_txt || ')' as clinic_name from admin_lookup where id = " + hd.Get("clinic_no"))));
                            }
                            else
                            {
                                // wipe out the clinic name if clinic_no has been reset
                                hd.AddOrReplace("clinic_name", "");
                            }
                            if (hd.Get("scamps_prov_id").AsInteger() > 0)
                            {
                                // resolve the provider name....
                                hd.AddOrReplace("attending", Tools.AsText(db.GetValue("select last_name || ', ' || first_name  as attending from admin_provider where scamps_prov_id = " + hd.Get("scamps_prov_id"))));
                            }
                            else
                            {
                                hd.AddOrReplace("attending", "");
                            }
                            

                            // deal with combobox elements that allow "add new"
                            // this is not an ideal point to do this, since all
                            // form validation is not yet complete, but it is
                            // really the only place to do it under the circumstances

                            // location - trim to max length
                            tmp = hd.Get("clinic_no").TrimTo(90);
                            if (!tmp.IsNullOrEmpty() && !Tools.isDigits(tmp) && (tmp.AsInteger() == 0))
                            {
                                hd.AddOrReplace("clinic_name", tmp);
                                hd.AddOrReplace("clinic_no", setLocation(ref db, tmp,visit_type).ToString());
                            }

                            // provider
                            tmp = hd.Get("scamps_prov_id").TrimTo(90);
                            if (!tmp.IsNullOrEmpty() && !Tools.isDigits(tmp) && (tmp.AsInteger() == 0))
                            {
                                tmp = Tools.ProperCase(tmp);
                                hd.AddOrReplace("attending", tmp);
                                hd.AddOrReplace("scamps_prov_id", setProvider(ref db, tmp).ToString());
                            }

                            // DC or "encounter user"
                            tmp = hd.Get("encounter_user").TrimTo(90);
                            if (!tmp.IsNullOrEmpty() && !Tools.isDigits(tmp) && (tmp.AsInteger() == 0))
                            {
                                tmp = Tools.ProperCase(tmp);
                                hd.AddOrReplace("encounter_user", setProvider(ref db, tmp,true).ToString());
                            }

                            // ===========  N E W    E N C O U N T E R  ==========
                            if (ID==0)
                            {
                                // housekeeping, not used in ePortal but to keep data in sync with old systems
                                hd.AddEdit("data_entered", "N");

                                // if appointment HAR is not set, pull one from dual
                                // remember, this is a NEW encounter, so there is no HAR editing going on here.
                                // if a NEW HAR is entered beginning with "XX", this is not allowed unless it already exists for this patient
                                if (hd.Get("har").ToUpper().StartsWith("XX"))
                                {
                                    if (Tools.AsText(db.GetValue("select har from pt_encounters where UPPER(mrn) = '" + hd.Get("mrn").ToUpper() + "' and upper(har) = '" + hd.Get("har") + "'")).Length == 0)
                                    {
                                        // already exists
                                        haserrors = true;
                                        form.SetError("har", "Any HAR beginning with 'XX' must already exist for this patient");
                                        break;
                                    }
                                }

                                hd.AddEdit("har", hd.Get("har").ToUpper());

                                if (hd.Get("har") == "")
                                {
                                    hd.AddOrReplace("har", "XX" + Tools.AsText(db.GetValue(Tools.GetBlock(externalSQL, "new.appt.har"))));
                                }
                                else
                                {
                                    // check if the HAR already exists
                                    // this may require some rejiggering
                                    // or simply rejecting of the HAR if it 
                                    // does already exist.

                                    // first, trim the HAR in case some joe tried to game the system
                                    // TrimTo cuts a string to the specified length without an index error
                                    // if the string is already shorter than the target length
                                    hd.AddOrReplace("har", hd.Get("har").TrimTo(10).ToUpper());

                                    // NEW appointment only - just needs to be
                                    // unique if it is an inpatient encounter, even within the
                                    // same patient.
                                    
                                    if(visit_type == "I")

                                    {

                                        // do we have an existing inpatient encounter for this patient with the HAR?
                                        // resolve appt id here -- hd.Get("appt_id") == ""

                                        string sql = "";
                                        sql = "select har from pt_encounters where upper(har) = '" + hd.Get("har") + "' "
                                              + " and mrn != '" + hd.Get("mrn").ToUpper() + "' and encounter_cd = 'I'";
                                        if (Tools.AsText(db.GetValue(sql)).Length >0 )
                                        {
                                            // already exists
                                            haserrors = true;
                                            form.SetError("har", "HAR is not unique");
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        // across all encounters, a HAR must only be related to a single patient
                                        if (Tools.AsText(db.GetValue("select har from pt_encounters where mrn != '" + hd.Get("mrn").ToUpper() + "' and Upper(har) = '" + hd.Get("har") + "'")).Length > 0)
                                        {
                                            // already exists
                                            haserrors = true;
                                            form.SetError("har", "HAR is assigned to another patient");
                                            break;
                                        }
                                    }

                                }
                                // set a default encounter status if one is not set in the form
                                Tools.UpdateEmpty(ref hd,"encounter_status", "0");

                                form_type = Tools.AsInteger(hd.Get("encounter_type"));
                                // another crazy architecture construct...
                                // if this is an INPATIENT encounter, the stub CANNOT have an encounter type
                                // only the subunit can.  Exhausing, really
                                if (visit_type == "I") { hd.Remove("encounter_type"); }

                                // transpose the appt date to the pt_encounter "clinic_appt"
                                hd.AddOrReplace("clinic_appt", hd.Get("appt_date"));

                                if (visit_type == "I")
                                {
                                    // is there already an inpatient encounter stub for this HAR and this SCAMP
                                    // and this patient (of course) that this inpatient subunit can/should be 
                                    // attached to?
                                    // 

                                    string sql = "";
                                    int seq_val = 1;

                                    sql = "select count(*) from visits_inpt_subunit where "
                                        + "har = '" + hd.Get("har") + "'";       // ZAPHOD - PATIENT??? 

                                    seq_val = Tools.AsInteger(db.GetValue(sql));
                                    if(seq_val>0)  // there is at least 1 subunit with the same har and scamp
                                    {
                                        // do this again to get the max assigned seq val
                                        sql = "select max(seq_val) from visits_inpt_subunit where "
                                            + "har = '" + hd.Get("har") + "'";  
                                        seq_val = Tools.AsInteger(db.GetValue(sql));

                                        // increment it...
                                        seq_val++;

                                        // we do not need to add a new pt_encounter stub
                                        // just "relate" a subunit in the silly way they 
                                        // are related.  BUT, do we already have a PT_ENCOUNTER record?

                                        // ZAPHOD - get this by appointment ID, if we have an appt_id
                                        // use it.  Otherwise, let the catchall assign one below.  

                                        long nn = 0;
                                        sql = "select appt_id from pt_encounters where encounter_cd = 'I' and upper(har) = '" + hd.Get("har").ToUpper() + "'";
                                        nn = Tools.AsLong(db.GetValue(sql));

                                        // no existing attp_id / inpatient stub (omg, this whole thing is beyond silly)
                                        // since no stub, we need to add one.
                                        if(nn==0)
                                        {
                                            // there is no encounter stub for this inpatient encounter
                                            // so we need to add on.  This is starting to get shady...
                                            if (hd.Get("appt_id") == "")
                                            {
                                                hd.AddOrReplace("appt_id", Tools.AsText(db.GetValue(Tools.GetBlock(externalSQL, "new.appt.id"))));
                                            }
                                            db.Insert(hd, "pt_encounters");
                                        }
                                        else
                                        {
                                            // well, the other option is to do nothing, really
                                            // but should we edit the stub if only to 'touch it' for the date stamp?
                                            // not necessary, since the BIG JOIN uses the sub unit date 
                                        }
                                    }
                                    else   // this is an outpatient encounter
                                    {
                                        // we need a new stub...
                                        // if appointment ID is not set, pull next one from dual 
                                        if (hd.Get("appt_id") == "")
                                        {
                                            hd.AddOrReplace("appt_id", Tools.AsText(db.GetValue(Tools.GetBlock(externalSQL, "new.appt.id"))));
                                        }
                                        db.Insert(hd, "pt_encounters");
                                        seq_val = 1;
                                    }

                                    // set the seq_val for the sub unit, derived above
                                    hd.AddOrReplace("seq_val", seq_val.ToString());

                                    // add back in the encounter type.  
                                    hd.AddOrReplace("encounter_type", form_type.ToString());

                                    // even though the table has a column for encounter date
                                    // it usese a different column instead, because "reasons" I guess
                                    hd.AddOrReplace("date_dt", hd.Get("clinic_appt"));
                                    db.Insert(hd, "visits_inpt_subunit");

                                    // figure out the ID of the row I just inserted...
                                    // Oracle is not our friend here.
                                    ID = Tools.AsInteger(db.GetValue("select max(id) from visits_inpt_subunit"));
                                    response.AddOrReplace("highlightid", "I" + ID.ToString());
                                } 
                                else
                                {
                                    // if appointment ID is not set, pull next one from dual 
                                    if (hd.Get("appt_id") == "")
                                    {
                                        hd.AddOrReplace("appt_id", Tools.AsText(db.GetValue(Tools.GetBlock(externalSQL, "new.appt.id"))));
                                    }
                                    db.Insert(hd, "pt_encounters");
                                    ID = Tools.AsInteger(db.GetValue("select max(id) from pt_encounters"));
                                    response.AddOrReplace("highlightid", "O" + ID.ToString());
                                }

                                // now handle the pt_scamp status table
                                // this is yet another odd convention that 
                                // was settled on for unknown reasons.  
                                // This only happens with NEW encounters - enroll the patient, log the encrollment change

                                // step 1: is the current enrollment status anything but enrolled (1)?
                                db.GetResultSet("select status_val from pt_scamp_status where upper(mrn) = '" + hd.Get("mrn") + "' and scamp_id = " + hd.Get("scamp_id"));
                                if (db.EOF)
                                {
                                    n = -99;
                                }
                                else
                                {
                                    n = Tools.AsInteger(db.Get("status_val"));
                                }

                                // step 2: drop ID from the dictionary
                                hd.Remove("id");

                                // step 3: set our log status values
                                // yes, yes, all kinds of different column names for the same values.  Are you new?  Schema blows.
                                if (n != 1)
                                {
                                    hd.AddEdit("status_val", "1");
                                    hd.AddEdit("new_val", "1");
                                    hd.AddEdit("old_val", n.ToString());
                                    hd.AddEdit("user_id", System.Security.Principal.WindowsIdentity.GetCurrent().Name);
                                    hd.AddEdit("cmnt_txt", "Auto enrolled via new encounter");
                                    hd.AddEdit("audit_dt_tm", DateTime.Now.ToString());

                                    if (n == -99)
                                    {
                                        // record not found, insert it
                                        db.Insert(hd, "pt_scamp_status");
                                    }
                                    else
                                    {
                                        db.Update(hd, "pt_scamp_status", "upper(mrn) = '" + hd.Get("mrn") + "' and scamp_id = " + hd.Get("scamp_id"));
                                    }

                                    db.Insert(hd, "admin_screening");

                                }

                                addAction(ref action, "RELOAD");

                            }

                            // ===========  E X I S T I N G    E N C O U N T E R  ==========
                            else
                            {
                                hd.Remove("id");
                                string appt_id = hd.Get("appt_id");
                                if(appt_id.IsNullOrEmpty())
                                {
                                    // we really can't do much
                                    // error message and exit out of here
                                    // TODO
                                }
                                // need to account for HAR edits + possible APPT_ID mods
                                if(submit_action == "delete")
                                {
                                    // we can delete encounters if
                                    // 1. We confirm the action with the user prior
                                    // 2. It is not an inpatient stub with subunits
                                    // 3. We delete all measurements related as well
                                    // DO WE HAVE A CONFIRMATION??
                                    if (Tools.AsBoolean(hd.Get("confirm-delete-form")))
                                    {
                                        // this is the confirm form, but did they confirm?
                                        if (Tools.AsBoolean(hd.Get("confirm-delete")))
                                        {
                                            // Delete this encounter and all measurements

                                            DeleteMeasurements(ref db, hd.Get("appt_id"), hd.Get("visit_type"), hd.Get("scamp_id"));
                                            if (visit_type == "I")
                                            {
                                                DeleteInpatient(ref db, ID, hd.Get("appt_id"), hd.Get("har"));
                                            }
                                            else
                                            {
                                                DeleteOutpatient(ref db, ID);
                                            }

                                            addAction(ref action, "RELOAD");
                                        }
                                        else
                                        {
                                            // they did not check CONFIRM
                                            // just dismiss the modal and get on with it.

                                            addAction(ref action, "RELOAD");
                                        }
                                    }

                                    else
                                    {
                                        // No, OK, before we do ANY of that, we need to load 
                                        // a dialog to confirm the delete action by the user.
                                        string encounter_info = "";
                                        Scamps.Form fm = new Scamps.Form();
                                        fm.ConnectionString = form.ConnectionString;
                                        fm.Provider = form.Provider;
                                        fm.FormData = hd;
                                        form.Options.UseHTML5Markup = false;
                                        form.Options.InnerFormOnly = false;
                                        fm.Load(con.Server.MapPath("/resources/forms/encounter-delete-confirm.form"));
                                        results = fm.Render();
                                        hd.AddOrReplace("scamp_name", ScampName(ref db, hd.Get("scamp_id").AsInteger()));
                                        results = Tools.Merge(results, hd);
                                        response.AddOrReplace("modal_title", "Delete Encounter Confirmation");
                                        addAction(ref action, "SUCCESSMODALFORM");
                                    }
                                }
                                else   // update the encounter record
                                {
                                    // set a default encounter status if one is not set in the form
                                    Tools.UpdateEmpty(ref hd, "encounter_status", "0");

                                    int sub_id = hd.Get("sub_id").AsInteger();

                                    form_type = Tools.AsInteger(hd.Get("encounter_type"));
                                    if(form_type == 0 ) { form_type = Tools.AsInteger(hd.Get("set_encounter_type")); }
                                    // another crazy architecture construct...
                                    // if this is an INPATIENT encounter, the stub CANNOT have an encounter type
                                    // only the subunit can.  Exhausing, really
                                    if (visit_type == "I") { hd.Remove("encounter_type"); }

                                    // transpose the appt date to the pt_encounter "clinic_appt"
                                    hd.AddOrReplace("clinic_appt", hd.Get("appt_date"));

                                    hd.AddOrReplace("original_seq_val", hd.Get("seq_val"));

                                    // editing the HAR?
                                    if (harupdate)
                                    {
                                        // can't have a HAR that is already in use with a different patient
                                        if (Tools.AsText(db.GetValue("select har from pt_encounters where UPPER(mrn) != '" + hd.Get("mrn").ToUpper() + "' and upper(har) = '" + hd.Get("har") + "'")).Length > 0)
                                        {
                                            // this har exists, and it is associated with another patient
                                            haserrors = true;
                                            form.SetError("har", "Already exists and is associated with another patient");
                                            break;
                                        }

                                        // is this a new HAR entirely???  Do we not have a stub encounter for inpatient
                                        if (Tools.AsText(db.GetValue("select har from pt_encounters where upper(har) = '" + hd.Get("har") + "'")).Length == 0)
                                        {
                                            // this HAR does not exist
                                            newhar = true;
                                        }
                                        if(visit_type == "I")
                                        {
                                            if (Tools.AsText(db.GetValue("select har from pt_encounters where encounter_cd = 'I' and upper(har) = '" + hd.Get("har") + "'")).Length == 0)
                                            {
                                                // this record does not have a encounter stub
                                                inserthar = true;
                                            }
                                            // check if we need a new seq_val because of a merge of HARs
                                            // if a subunit already exists with the same har and subunit, we need to select a new subunit
                                            if (Tools.AsText(db.GetValue("select har from visits_inpt_subunit where upper(har) = '" + hd.Get("har") + "' and seq_val = " + hd.Get("seq_val"))).Length != 0)
                                            {
                                                // we have a existing inpatient record with the same har and seq_val - this is a MERGE
                                                // we need a new seq val...
                                                tmp = "select count(seq_val) from visits_inpt_subunit where "
                                                    + "har = '" + hd.Get("har") + "'";  // and scamp_id=" + hd.Get("scamp_id");
                                                hd.AddEdit("seq_val", (Tools.AsInteger(db.GetValue(tmp)) + 1).ToString());
                                            }
                                        }
                                    }

                                    if (visit_type == "I")
                                    {
                                        // actually, we don't need to update the encounter stub
                                        // the stub contains no information useful to update
                                        // but if we don't have a stub, we need to create one!

                                        if (harupdate && inserthar)
                                        {
                                            hd.AddOrReplace("appt_id", Tools.AsText(db.GetValue(Tools.GetBlock(externalSQL, "new.appt.id"))));
                                            db.Insert(hd, "pt_encounters");
                                        }
                                        else
                                        {
                                            if(harupdate && newhar)
                                            {
                                                db.Update(hd, "pt_encounters","appt_id =" + hd.Get("appt_id"));
                                            }
                                        }

                                        // add back in the encounter type.  
                                        hd.AddOrReplace("encounter_type", form_type.ToString());

                                        // even though the table has a column for encounter date
                                        // it usese a different column instead, because "reasons" I guess
                                        hd.AddOrReplace("date_dt", hd.Get("clinic_appt"));

                                        // must have a sub_id to save this record
                                        if (sub_id > 0)
                                        {
                                            db.Update(hd, "visits_inpt_subunit","id = " + sub_id.ToString());
                                        }

                                        response.AddOrReplace("highlightid", "I" + sub_id.ToString());
                                    }
                                    else
                                    {
                                        // Outpatient Visit
                                        db.Update(hd, "pt_encounters", "id = " + ID.ToString());
                                        response.AddOrReplace("highlightid", "O" + ID.ToString());
                                    }


                                    // UPDATE RELATED MEASUREMENTS IF THE HAR CHANGED
                                    if(harupdate)
                                    {
                                        updateMeasurements(ref db, appt_id, hd.Get("original_har"), hd.Get("har"), hd.Get("seq_val").AsInteger(), hd.Get("original_seq_val").AsInteger(), visit_type);
                                    }

                                    addAction(ref action, "RELOAD");
                                }
                            }


                            if (haserrors)
                            {
                                // transpose original data to form elements
                                // in the case of errors and the form needs
                                // to be re-rendered.

                            }
                            Save = false;
                            if (submit_action == "saveandsdf")
                            {
                                action = "";
                                addAction(ref action, "REDIRECT");
                                if(visit_type == "I")
                                {
                                    response.AddOrReplace("url", "/patients/encounter/" + hd.Get("appt_id") + "?subunit=" + ID.ToString());
                                }
                                else
                                {
                                    response.AddOrReplace("url", "/patients/encounter/" + hd.Get("appt_id"));
                                }
                            }

                            break;


                        //user have some special handling that needs to be accomplished here.
                        case "users":
                            target = "admin_provider";
                            id_criteria = "scamps_prov_id=" + ID;
                            Save = true;
                            if (Tools.GetValue("path", hd) == "provider")
                            {
                                successdata = "/admin/providers";
                            }

                            if (db.IsActive)
                            {
                                int userid = Tools.AsInteger(db.GetValue("select scamps_prov_id from admin_provider where upper(user_name)='" + Tools.GetValue("user_name", hd).ToUpper() + "'"));
                                if (userid > 0 && userid != ID)
                                {
                                    form.SetError("user_name", "User name already in use");
                                    Save = false;
                                }
                            }

                            //is the form still valid?  Continue to fix up the user...
                            if (form.IsValid)
                            {
                                if (Tools.GetValue("password", hd).Length>0)    //the password field has something in it.  Do the password dance
                                {
                                    if (Tools.GetValue("password", hd) != Tools.GetValue("password.confirm", hd))
                                    {
                                        form.SetError("password", "Passwords do not match");
                                        Save = false;
                                    }
                                    else
                                    {
                                        //passwords match, any password validation checking?
                                        //create the element int he data dictionary
                                        Tools.AddOrReplace("password_txt", BCrypt.Net.BCrypt.HashPassword(Tools.GetValue("password", hd),12), ref hd);
                                    }
                                }
                            }

                            //TODO: validate the email address for the user...

                            if(form.IsValid)
                            {

                                // clear "AUTO-ADD" once a record is edited with the edit form
                                // (instead or automatically added via combobox)
                                hd.AddEdit("password_question", "");
                                //create or update full name???
                                //let's only do this on new adds, so we don't mess up current entries 
                                //that could be used for linking in Access.  Yup, you read that right.
                                if (ID == 0)
                                {
                                    tmp = Tools.GetValue("first_name", hd) + " " + Tools.GetValue("last_name", hd);
                                    switch (Tools.GetValue("title", hd).ToLower())
                                    {
                                        case "mr":
                                        case "mr.":
                                        case "mrs":
                                        case "mrs.":
                                        case "ms":
                                        case "ms.":
                                        case "dr":
                                        case "dr.":
                                            tmp = Tools.EndWith(Tools.GetValue("title", hd), ".") + " " + tmp;
                                            break;

                                        default:
                                            tmp += " " + Tools.NotEndWith(Tools.GetValue("title", hd), ".");
                                            break;
                                    }
                                    Tools.AddOrReplace("full_name", tmp, ref hd);
                                }
                            }

                            break;
                        default:
                            //do it by reflection if necessary...
                            try
                            {
                                //Handles specific forms by name
                                Type type = typeof(FormHandler);
                                MethodInfo info = type.GetMethod(entity);

                                info.Invoke(null, new object[] { });
                            }
                            catch (Exception ex)
                            {

                            }
                            break;
                    }



                }

                //regardless if the form is valid or not
                //we can still save it, if the Save flag 
                //has not been tripped.
                if (Save)
                {
                    if (db.IsActive)
                    {
                        if (ID == 0)
                        {
                            db.Insert(hd, target);
                        }
                        else
                        {
                            db.Update(hd, target, id_criteria);
                        }
                    }
                    else
                    {
                        //TODO: log an error and alert the user...
                    }

                }

                //========================================================
                //          H A N D L E    F O R M    E R R O R S
                //========================================================

                if (!form.IsValid)
                {
                    currentstatus = status.HASERRORS;
                    //there could be a number of thing we might want to do
                    //for now, we will just re-render the form, without the form tag
                    //and return that as the markup for this page.
                    form.Options.InnerFormOnly = true;
                    results = form.Render();

                    // hackity hackity hackity
                    // get rid of the extra buttons on an encounter form if it is on the SDF page.
                    if(formname == "encounter")
                    {
                        // prepare to relaod the form if errors
                        hd.AddEdit("encounter_type", hd.Get("set_encounter_type"));
                        if (con.Request.Url.AbsolutePath.ToLower().StartsWith("/patients/encounter"))
                        {
                            results = Tools.AddClass(results, "hide", ".deleteencounter");
                            results = Tools.AddClass(results, "hide", ".saveandsdf");
                        }
                    }

                }
                else if (currentstatus == status.ERR)
                {

                }
                else
                {
                    switch (action)
                    {
                        case "redirect":
                            break;
                    }

                }
            }
            else
            {
                currentstatus = status.ERR;
                results = "No Form";
            }

            response.Add("status", currentstatus.ToString());
            response.Add("action",action);
            response.Add("data",successdata);
            response.Add("markup",results);
            response.Add("disable", disablecontrols);
            response.Add("hide", hidecontrols);

            string theoutput = Newtonsoft.Json.JsonConvert.SerializeObject(response);

            con.Response.Write(theoutput);
            con.Response.End();
        }


        public string ScampName(ref Scamps.DataTools db, int scamp_id)
        {
            string results = "";
            string sql = "select scamp_name from scamp where scamp_id = " + scamp_id;
            results = Tools.AsText(db.GetValue(sql));
            return results;
        }



        public bool updateMeasurements(ref Scamps.DataTools db, string appointment_ID,string original_HAR,string new_HAR, int seq_val,int original_seq_val,string visit_type)
        {
            bool ok = true;
            db.haserrors = false;

            appointment_ID = Tools.AsLong(appointment_ID).ToString();

            string sql = "update measurements set har = '" + new_HAR + "', subunit_seq_val = " + seq_val.ToString()
                       + " WHERE appt_id = " + appointment_ID
                       + " and har = '" + original_HAR + "' and visit_type = '" + visit_type + "'"
                       + " and subunit_seq_val = " + original_seq_val.ToString();

            int i = db.Execute(sql);
            if (db.haserrors)
            {
                return false;
            }

            // now, we need to check on orphanded pt_encounters based on a subunit HAR change
            if (visit_type == "I")
            {
                // how many OTHER forms are part of the same set of inpatient encounters?  If none
                // are left, we can delete the ID from the pt_encounter table, i.e. delete the stub
                sql = "select distinct e.id c from pt_encounters e left join visits_inpt_subunit s on e.har = s.har "
                    + " where e.encounter_cd = 'I' and e.har = '" + original_HAR + "' and s.har is null";

                i = Tools.AsInteger(db.GetValue(sql));
                if(db.haserrors)
                {
                    return false;
                }
                if(i != 0)
                {
                    // i = 0 when the query returns no values and null is cast to an integer
                    // i will now be the ID of the pt_encounter row that no longer has any
                    // subunits and can therefore be deleted.  
                    db.Delete("pt_encounters", "appt_id = " + appointment_ID);
                    if(db.haserrors)
                    {
                        ok = false;
                    }
                }
            }
            return ok;
        }



        // Add records ad hoc based on combobox entry
        /// <summary>
        /// Insert a new location based on text string from combobox
        /// </summary>
        /// <param name="db">DataTools reference</param>
        /// <param name="newitem">The new string to add</param>
        /// <param name="iosetting">I or O for inpatient/outpatient</param>
        /// <returns></returns>
        public int setLocation(ref Scamps.DataTools db, string newitem,string iosetting)
        {
            newitem = Tools.ProperCase(newitem);
            int n = 0;
            Dictionary<string, string> d = new Dictionary<string, string>();

        if(iosetting == "I")
            {
                d.Add("lookup_cd", "LOCTN_INPT");
                d.Add("value_txt", (newitem + "(IN)").ToUpper());
                d.Add("description_txt", newitem + " (IN)");
            }
            else
            {
                d.Add("lookup_cd", "LOCTN_OUTPT");
                d.Add("value_txt", (newitem + "(OUT)").ToUpper());
                d.Add("description_txt", newitem + " (OUT)");
            }

            d.Add("visible_abbr", "Y");

            n = Tools.AsInteger(db.GetValue("select id from admin_lookup where value_txt = '"
                               + d.Get("value_txt") + "' and lookup_cd = '" + d.Get("lookup_cd") + "'"));
            if(n==0)
            {
                db.Insert(d, "admin_lookup");
                n = Tools.AsInteger(db.GetValue("select id from admin_lookup where value_txt = '"
                                               + d.Get("value_txt") + "' and lookup_cd = '" + d.Get("lookup_cd") + "'"));
            }

            Scamps.Cache c = new Scamps.Cache();
            c.cachePath = con.Server.MapPath("/cache/");
            c.Clear("*.*", "lists");
           
            return n;
        }

        /// <summary>
        /// Insert new provider (or DC)
        /// </summary>
        /// <param name="db">DataTools reference</param>
        /// <param name="newitem">String item to insert</param>
        /// <param name="isDC">bool - is the new user a data coordinator?</param>
        /// <returns>Integer - record ID of item inserted</returns>
        public int setProvider(ref Scamps.DataTools db, string newitem,bool isDC = false)
        {
            int n = 0;
            bool LForder = true;
            Dictionary<string, string> d = new Dictionary<string, string>();
            string firstname = "";
            string lastname = "";
            n = newitem.IndexOf(",");
            if(n==-1)
            {
                n = newitem.IndexOf(" ");
                LForder = false;
            }
            if (n > -1)
            {
                if(LForder)
                {
                    lastname = Tools.NotEndWith(newitem.Substring(0, n).Trim(),",");
                    firstname = newitem.Substring(n + 1).Trim();
                }
                else
                {
                    firstname = newitem.Substring(0, n).Trim();
                    lastname = newitem.Substring(n + 1).Trim();
                }
            }
            else
            {
                firstname = newitem;
            }

            d.Add("first_name", firstname);
            d.Add("last_name", lastname);
            d.Add("full_name", newitem);
            d.Add("active_status", "Y");
            if (isDC)
            {
                d.Add("profile_name", "DC");
            }
            else
            {
                d.Add("profile_name", "PROVIDER");
            }
            d.Add("password_question", "AUTO-ADD");

            n = Tools.AsInteger(db.GetValue("select id from admin_provider where upper(first_name) = '"
                               + d.Get("first_name").ToUpper() + "' and upper(last_name) = '" + d.Get("last_name").ToUpper()
                               + "' and active_status = 'Y'"));
            if(n==0)
            {
                db.Insert(d, "admin_provider");
                n = Tools.AsInteger(db.GetValue("select id from admin_provider where full_name = '"
                                               + d.Get("full_name") + "' and password_question = '" + d.Get("password_question") + "'"));
            }

            Scamps.Cache c = new Scamps.Cache();
            c.cachePath = con.Server.MapPath("/cache/");
            c.Clear("*.*", "lists");

            return n;
        }

        // ============  Delete Rcords ==================
        /// <summary>
        /// Remove all measurement records based on the given appt ID, visit type, and scamp ID
        /// </summary>
        /// <param name="db">A DataTools object</param>
        /// <param name="appt_id"></param>
        /// <param name="visit_type"></param>
        /// <param name="scamp_ID"></param>
        /// <returns></returns>
        public bool DeleteMeasurements(ref Scamps.DataTools db, string appt_id,string visit_type,string scamp_ID)
        {
            bool ok = false;
            Dictionary<string,string> p = new Dictionary<string, string>();
            p.Add("appt_id", Tools.AsText(Tools.AsLong(appt_id)));
            p.Add("visit_type", visit_type.TrimTo(1));
            p.Add("scamp_id", Tools.AsText(Tools.AsInteger(scamp_ID)));
            string sql = "appt_id = " + p.Get("appt_id")  + " and visit_type = '" + p.Get("visit_type") + "' and scamp_id = " + p.Get("scamp_id");
            int results = db.Delete("measurements", sql);

            return ok;
        }

        public bool DeleteInpatient(ref Scamps.DataTools db, int ID,string appt_id,string HAR)
        {
            db.haserrors = false;
            string sql = "";
            bool ok = true;
            db.Delete("visits_inpt_subunit", "id = " + ID.ToString());
            if(db.haserrors)
            {
                ok = false;
            }
            else
            {
                // was this the only subunit record for this encounter?
                // if so, the master appointment record can also be removed.
                // This would ideally be by SCAMP, but that construct is not
                // considered or enforced with the current schema.
                sql = "select count(id) c from visits_inpt_subunit where har = '" + HAR + "'";
                if(Tools.AsInteger(db.GetValue(sql)) == 0)
                {
                    // no additional records, the parent encounter can be deleted.
                    sql = "appt_id = " + appt_id + " and har = '" + HAR + "'  and  encounter_cd = 'I'";
                    db.Delete("pt_encounters",sql);
                }
                
            }
            return ok;
        }

        public bool DeleteOutpatient(ref Scamps.DataTools db, int ID)
        {
            db.haserrors = false;
            bool ok = true;
            db.Delete("pt_encounters", "id = " + ID.ToString());
            if (db.haserrors)
            {
                ok = false;
            }
            return ok;
        }

        private void garbagepail()
        {
                            //folder = "\\extract";
                //extension = ".json";
                //if (Directory.Exists(sourcepath + folder))
                //{
                //    d = new DirectoryInfo(sourcepath + folder);
                //    foreach (var file in d.GetFiles("*" + extension))
                //    {
                //        if (Tools.FileExists(con.Server.MapPath("\\resources\\" + file.Name)))
                //        {
                //            Tools.AppendLine(ref loginfo, "Back up file " + file.Name);
                //            File.Move(con.Server.MapPath("\\resources\\" + file.Name), con.Server.MapPath("\\resources\\") + Tools.RandomSeed(3) + Tools.DateStamp() + file.Name + ".bak");
                //        }
                //        Directory.Move(file.FullName, con.Server.MapPath("\\resources\\" + file.Name));
                //        Tools.AppendLine(ref loginfo, "Installed " + file.Name);
                //    }
                //}


                // update system forms...
                //folder = "\\forms";
                //extension = ".form";
                //if (Directory.Exists(sourcepath + folder))
                //{
                //    d = new DirectoryInfo(sourcepath + folder);
                //    foreach (var file in d.GetFiles("*" + extension))
                //    {
                //        if (Tools.FileExists(con.Server.MapPath("\\resources\\forms\\" + file.Name)))
                //        {
                //            Tools.AppendLine(ref loginfo, "Back up file " + file.Name);
                //            File.Move(con.Server.MapPath("\\resources\\forms\\" + file.Name), con.Server.MapPath("\\resources\\forms\\") + Tools.RandomSeed(3) + Tools.DateStamp() + file.Name + ".bak");
                //        }
                //        Directory.Move(file.FullName, con.Server.MapPath("\\resources\\forms\\" + file.Name));
                //        Tools.AppendLine(ref loginfo, "Installed form " + file.Name);
                //    }
                //}

                // update system templates...

                //folder = "\\templates";
                //extension = ".dbt";

                //if (Directory.Exists(sourcepath + folder))
                //{
                //    d = new DirectoryInfo(sourcepath + folder);
                //    foreach (var file in d.GetFiles("*" + extension))
                //    {
                //        if (Tools.FileExists(con.Server.MapPath("\\resources\\templates\\" + file.Name)))
                //        {
                //            Tools.AppendLine(ref loginfo, "Back up file " + file.Name);
                //            File.Move(con.Server.MapPath("\\resources\\templates\\" + file.Name), con.Server.MapPath("\\resources\\templates\\") + Tools.RandomSeed(3) + Tools.DateStamp() + file.Name + ".bak");
                //        }
                //        Directory.Move(file.FullName, con.Server.MapPath("\\resources\\templates\\" + file.Name));
                //        Tools.AppendLine(ref loginfo, "Installed template " + file.Name);
                //    }
                //}

                // update system page templates...
                //folder = "\\pages";
                //extension = ".dbt";
                //if (Directory.Exists(sourcepath + folder))
                //{
                //    d = new DirectoryInfo(sourcepath + folder);
                //    foreach (var file in d.GetFiles("*" + extension))
                //    {
                //        if (Tools.FileExists(con.Server.MapPath("\\resources\\templates\\page\\" + file.Name)))
                //        {
                //            Tools.AppendLine(ref loginfo, "Back up file " + file.Name);
                //            File.Move(con.Server.MapPath("\\resources\\templates\\page\\" + file.Name), con.Server.MapPath("\\resources\\templates\\page\\") + Tools.RandomSeed(3) + Tools.DateStamp() + file.Name + ".bak");
                //        }
                //        Directory.Move(file.FullName, con.Server.MapPath("\\resources\\templates\\page\\" + file.Name));
                //        Tools.AppendLine(ref loginfo, "Installed page file " + file.Name);
                //    }
                //}

                // update system templates...
                //folder = "\\data";
                //extension = ".sql";
                //if (Directory.Exists(sourcepath + folder))
                //{
                //    d = new DirectoryInfo(sourcepath + folder);
                //    foreach (var file in d.GetFiles("*" + extension))
                //    {
                //        if (Tools.FileExists(con.Server.MapPath("\\resources\\data\\" + file.Name)))
                //        {
                //            Tools.AppendLine(ref loginfo, "Back up file " + file.Name);
                //            File.Move(con.Server.MapPath("\\resources\\data\\" + file.Name), con.Server.MapPath("\\resources\\data\\") + Tools.RandomSeed(3) + Tools.DateStamp() + file.Name + ".bak");
                //        }
                //        Directory.Move(file.FullName, con.Server.MapPath("\\resources\\data\\" + file.Name));
                //        Tools.AppendLine(ref loginfo, "Installed " + file.Name);
                //    }
                //}

                // update system templates...
                //folder = "\\external";
                //extension = ".xml";
                //if (Directory.Exists(sourcepath + folder))
                //{
                //    d = new DirectoryInfo(sourcepath + folder);
                //    foreach (var file in d.GetFiles("*" + extension))
                //    {
                //        if (Tools.FileExists(con.Server.MapPath("\\resources\\external\\" + file.Name)))
                //        {
                //            Tools.AppendLine(ref loginfo, "Back up file " + file.Name);
                //            File.Move(con.Server.MapPath("\\resources\\external\\" + file.Name), con.Server.MapPath("\\resources\\external\\") + Tools.RandomSeed(3) + Tools.DateStamp() + file.Name + ".bak");
                //        }
                //        Directory.Move(file.FullName, con.Server.MapPath("\\resources\\external\\" + file.Name));
                //        Tools.AppendLine(ref loginfo, "Installed template " + file.Name);
                //    }
                //}

            // alright, how about root css?
            //folder = "\\css";
            //extension = ".css";
            //if(Directory.Exists(sourcepath + folder))
            //{
            //    d = new DirectoryInfo(sourcepath + folder);
            //    foreach (var file in d.GetFiles("*" + extension))
            //    {
            //        if (Tools.FileExists(con.Server.MapPath("\\css\\" + file.Name)))
            //        {
            //            Tools.AppendLine(ref loginfo, "Back up file " + file.Name);
            //            File.Move(con.Server.MapPath("\\css\\" + file.Name), con.Server.MapPath("\\css\\") + Tools.RandomSeed(3) + Tools.DateStamp() + file.Name + ".bak");
            //        }
            //        Directory.Move(file.FullName, con.Server.MapPath("\\css\\" + file.Name));
            //        Tools.AppendLine(ref loginfo, "Installed styles " + file.Name);
            //    }
            //}

            // java script?
            //folder = "\\scripts";
            //extension = ".js";
            //if (Directory.Exists(sourcepath + folder))
            //{
            //    d = new DirectoryInfo(sourcepath + folder);
            //    foreach (var file in d.GetFiles("*" + extension))
            //    {
            //        if (Tools.FileExists(con.Server.MapPath("\\scripts\\" + file.Name)))
            //        {
            //            Tools.AppendLine(ref loginfo, "Back up file " + file.Name);
            //            File.Move(con.Server.MapPath("\\scripts\\" + file.Name), con.Server.MapPath("\\scriptsl\\") + Tools.RandomSeed(3) + Tools.DateStamp() + file.Name + ".bak");
            //        }
            //        Directory.Move(file.FullName, con.Server.MapPath("\\scripts\\" + file.Name));
            //        Tools.AppendLine(ref loginfo, "Installed styles " + file.Name);
            //    }
            //}


        }
        public void addAction(ref string action, string newaction)
        {
            action += "|" + newaction;
            action = Tools.NotStartWith(action,"|");

        }

        public int UserKeys()
        {
            int keys = 0;
            var profKeys = new string[] { "password_answer" };
            var prov_id = ScampsDB.Provider.ProfileMethods.getProfile("user_name", HttpContext.Current.User.Identity.Name, profKeys);
            if (prov_id.ContainsKey("password_answer")) { keys = Tools.AsInteger(prov_id["password_answer"]); }
            return keys;
        }

        public string[] Location(string source, int linenumber, ref bool has_errors, ref string output)
        {

            string lineref = "Line " + (linenumber + 1).ToString();
            string[] p = source.Split(',');
            if (p.Length != 3)
            {
                has_errors = true;
                Tools.AppendLine(ref output, lineref + " - Line signature not the correct length - 3 columns required, this row has " + p.Length.ToString());
            }
            else
            {
                p[0] = p[0].Trim().ToUpper();  // Location Type
                p[1] = p[1].Trim();
                p[1] = p[1].Clean();
                p[2] = p[2].Trim().ToUpper();
                p[2] = p[2].Clean();

                if (p[0] != "I" && p[0] != "O" && p[0] != "E")
                {
                    has_errors = true;
                    Tools.AppendLine(ref output, lineref + " - Location type '" + p[0] + "' is not recognized, column 1");
                }

                // Location Description
                if (ContainsBadChars(p[1]))
                {
                    has_errors = true;
                    Tools.AppendLine(ref output, lineref + " - Location description contains illegal characters, column 2");
                }

                if (p[1].Length>255)
                {
                    has_errors = true;
                    Tools.AppendLine(ref output, lineref + " - Location description must be less than 256 characters, column 2");
                }


                // Location Value
                if (ContainsBadChars(p[2]))
                {
                    has_errors = true;
                    Tools.AppendLine(ref output, lineref + " - Location value contains illegal characters, column 3");
                }

                if (p[2].Length > 100)
                {
                    has_errors = true;
                    Tools.AppendLine(ref output, lineref + " - Location value must be less than 100 characters, column 3");
                }
            }
            return p;
        }

        public string[] Encounter(string source, int linenumber, ref bool has_errors, ref string output)
        {

            string lineref = "Line " + (linenumber + 1).ToString();
            string[] p = source.Split(',');
            if (p.Length != 7)
            {
                has_errors = true;
                Tools.AppendLine(ref output, lineref + " - Line signature not the correct length for an encounter. Encounters require 7 columns.");
            }
            else
            {
                // Lindsay uncovered this, so trimming now.
                for (int i = 0; i < p.Length; i++)
                {
                    p[i] = p[i].Trim();
                }
                p[0] = p[0].ToUpper();  // form name
                p[4] = p[4].ToUpper();  // MRN

                // form name
                if (p[0].IsNullOrEmpty())
                {
                    has_errors = true;
                    Tools.AppendLine(ref output, lineref + " - Scamp form name required, column 1");
                }  

                // Appointment Date

                if (Tools.AsDate(p[1]) == "")
                {
                    has_errors = true;
                    Tools.AppendLine(ref output, lineref + " - Missing or invalid Appointment Date, column 2");
                }

                // Appointment ID
              
                if (Tools.AsLong(p[2]) > 2000000000)
                {
                    has_errors = true;
                    Tools.AppendLine(ref output, lineref + " - Appointment ID out of range, column 3");
                }
                if (ContainsBadChars(p[2]))
                {
                    has_errors = true;
                    Tools.AppendLine(ref output, lineref + " - Appointment ID needs to be a numeric value, column 3");
                }
                if (Tools.AsInteger(p[2]) == 0)
                {
                    has_errors = true;
                    Tools.AppendLine(ref output, lineref + " - Appointment ID is required and cannot be 0 (zero), column 3");
                }


                // HAR/FIN
                if (p[3].IsNullOrEmpty())
                {
                    has_errors = true;
                    Tools.AppendLine(ref output, lineref + " - HAR/FIN is required, column 4");
                }

                if (p[3].Length>10)
                {
                    has_errors = true;
                    Tools.AppendLine(ref output, lineref + " - HAR/FIN is limited to 10 characters, column 4");
                }


                // MRN
                if (p[4].IsNullOrEmpty())
                {
                    has_errors = true;
                    Tools.AppendLine(ref output, lineref + " - MRN is required, column 5");
                }

                if (p[4].Length > 20)
                {
                    has_errors = true;
                    Tools.AppendLine(ref output, lineref + " - MRN too long - limited to 20 characters total, column 5");
                }
                if (ContainsBadChars(p[4]))
                {
                    has_errors = true;
                    Tools.AppendLine(ref output, lineref + " - MRN contains illegal characters, column 5.  Alphanumeric only.");
                }

                // no validation for locations or attending, normalize
                p[5] = p[5].Clean().Trim().ToUpper();

                p[6] = p[6].Clean().Trim();
                p[6] = Tools.ProperCase(p[6]);
            }
            return p;
        }


        public string[] Patient(string source, int linenumber, ref bool has_errors, ref string output)
        {

            string lineref = "Line " + (linenumber + 1).ToString();
            string[] p = source.Split(',');
            if (p.Length != 5)
            {
                has_errors = true;
                Tools.AppendLine(ref output, lineref + " - Line signature not the correct length - 5 columns required, this row has " + p.Length.ToString());
            }
            else
            {
                // Lindsay uncovered this, so trimming now.
                for(int i = 0;i<p.Length;i++)
                {
                    p[i] = p[i].Trim();
                }
                p[4] = p[4].ToUpper();  // Gender
                p[0] = p[0].ToUpper();  // MRN

                if (p[0].IndexOf(' ') >-1)
                {
                    has_errors = true;
                    Tools.AppendLine(ref output, lineref + " - MRN contains spaces");
                }
                if (p[0].Length > 20)
                {
                    has_errors = true;
                    Tools.AppendLine(ref output, lineref + " - MRN too long - limited to 20 characters total, column 1");
                }
                if (ContainsBadChars(p[0]) || Tools.hasPunctuation(p[0]))
                {
                    has_errors = true;
                    Tools.AppendLine(ref output, lineref + " - MRN contains illegal characters, column 1.  Alphanumeric only");
                }
                if (ContainsBadChars(p[1]))
                {
                    has_errors = true;
                    Tools.AppendLine(ref output, lineref + " - Last name contains illegal characters, column 2");
                }

                if (ContainsBadChars(p[2]))
                {
                    has_errors = true;
                    Tools.AppendLine(ref output, lineref + " - First name contains illegal characters, column 3");
                }

                if (Tools.AsDate(p[3]) == "")
                {
                    has_errors = true;
                    Tools.AppendLine(ref output, lineref + " - Missing or invalid DOB.  DOB must be a valid date or date/time.  Column 4");
                }
                
                if (p[4] != "M" && p[4] != "F")
                {
                    has_errors = true;
                    Tools.AppendLine(ref output, lineref + " - Gender '" + p[4] + "' invalid, column 5");
                }
            }
            return p; 
        }


        public bool ContainsBadChars(string source)
        {
            bool ok = false;
            string badchars = "~!@#$%^&*()={}[]|\\/<>;\"";
            for (int i = 0; i < badchars.Length; i++)
            {
                if(source.Contains(badchars.Substring(i,1)))
                {
                    ok = true;
                    break;
                }
            }
            return ok;
        }
        public bool ImportAppFiles(string sourcepath, ref string loginfo)
        {
            bool ok = false;

            // TODO: add support for sub directories?
            //       other validation checks?
            //       roll this all up into a single operation as a sub routine
            //       and pass in the variables.  These are all very consistent
            //       lets just make sure we've thought of everything first.
            //
            //       OK will always be the last result.  We log the errors, do we really
            //       need to bubble up these internal issues?  

            try
            {
                // each possible folder contains possible files that need to be installed
                // for each folder, look for files and install them.


                // update the extract file configuration....
                ok = InstallFiles(sourcepath, "\\extract", "\\resources\\", ".json", "Installed", ref loginfo);

                ok = InstallFiles(sourcepath, "\\forms", "\\resources\\forms\\", ".form", "Installed form", ref loginfo);

                ok = InstallFiles(sourcepath, "\\templates", "\\resources\\templates\\", ".dbt", "Installed template", ref loginfo);

                ok = InstallFiles(sourcepath, "\\strings", "\\resources\\templates\\", ".xml", "Installed strings file", ref loginfo);

                ok = InstallFiles(sourcepath, "\\pages", "\\resources\\templates\\page\\", ".dbt", "Update page", ref loginfo);

                ok = InstallFiles(sourcepath, "\\widgets", "\\resources\\widgets\\", ".xml", "", ref loginfo);

                ok = InstallFiles(sourcepath, "\\widgets", "\\resources\\widgets\\", ".report", "", ref loginfo);

                ok = InstallFiles(sourcepath, "\\data", "\\resources\\data\\", ".sql", "", ref loginfo);

                ok = InstallFiles(sourcepath, "\\external", "\\resources\\external\\", ".xml", "", ref loginfo);

                ok = InstallFiles(sourcepath, "\\css", "\\css\\", ".css", "", ref loginfo);

                ok = InstallFiles(sourcepath, "\\scripts", "\\scripts\\", ".js", "", ref loginfo);


            }
            catch (Exception ex)
            {
                //log this exception...
                Tools.AppendLine(ref loginfo, "<span class=\"error\">Install failure:" + ex.Message + "</span>");
                ok = false;
            }

            return ok;
        }


        private bool InstallFiles(string sourcepath,string folder, string server_path,string extension, string logmessage, ref string loginfo)
        {
            bool ok = true;
            bool setlog = false;
            DirectoryInfo d = null;
            if(logmessage.IsNullOrEmpty())
            {
                logmessage = "Installed";
                setlog = true;
            }
            try
            {
                if (Directory.Exists(sourcepath + folder))
                {
                    d = new DirectoryInfo(sourcepath + folder);
                    foreach (var file in d.GetFiles("*" + extension))
                    {
                        if (Tools.FileExists(con.Server.MapPath(server_path + file.Name)))
                        {
                            Tools.AppendLine(ref loginfo, "Back up file " + file.Name);
                            File.Move(con.Server.MapPath(server_path + file.Name), con.Server.MapPath(server_path) + Tools.RandomSeed(3) + Tools.DateStamp() + "_" + file.Name + ".bak");
                            if (setlog) { logmessage = "Update"; }
                        }
                        Directory.Move(file.FullName, con.Server.MapPath(server_path + file.Name));
                        Tools.AppendLine(ref loginfo, logmessage + " " + file.Name);
                    }
                }
            }
            catch(Exception ex){

                Tools.AppendLine(ref loginfo, "<span class=\"error\">Install Error: " + ex.Message + "</span>");
                ok = false;
            }




            return ok;
        }
        public bool MoveScampImages(string sourcepath,ref string loginfo)
        {
            bool ok = false;
            string extensions = ".gif,.jpg,.jpeg,.png";
            string ext = "";

            try
            {
                for(int i = 1; i< Tools.WordCount(extensions) +1; i++)
                {
                DirectoryInfo d = new DirectoryInfo(sourcepath);
                ext = Tools.Word(i,extensions);
                foreach (var file in d.GetFiles("*" + ext))
                    {
                        Directory.Move(file.FullName, con.Server.MapPath("\\scamps\\images\\" + file.Name));
                        Tools.AppendLine(ref loginfo, "Moved " + file.Name + " to scamp images.");
                    }
                }
                ok=true;
            }
            catch(Exception ex)
            {
                //log this exception...
                Tools.AppendLine(ref loginfo, "<span class=\"error\">Image File failure:" + ex.Message + "</span>");
                ok = false;
            }
            return ok;
        }

        public bool ImportLists(string sourcepath, ref string loginfo)
        {
            bool ok = false;
            string list_code = "";
            string list = "";
            try
            {

                DirectoryInfo d = new DirectoryInfo(sourcepath);
                foreach (var file in d.GetFiles("*.list"))
                {
                    ImportList(file.FullName,ref loginfo);
                }

                ok = true;
            }
            catch (Exception ex)
            {
                //log this exception...
            }
            return ok;
        }

        public bool ImportList(string listfile, ref string loginfo)
        {
            bool ok = false;

            string list_cd = "";
            string thelist = "";
            string tmp = "";
            string key = "";
            string value = "";
            string visible = "";
            int i = 0;
            bool isnewlist = false;

            //if the listfile is a path, read the file, otherwise, the list is the full list
            if (Tools.FileExists(listfile))
            {
                tmp = Tools.ReadFile(listfile);
            }

            if (!tmp.IsNullOrEmpty())
            {
                list_cd = Tools.NormalString(Tools.XNode(ref tmp, "name")).ToUpper();
                thelist = Tools.NormalString(Tools.XNode(ref tmp, "elements"));
                if (!list_cd.IsNullOrEmpty())
                {
                    //clear the cache, if this list is cached.
                    
                    try{
                        Scamps.Cache cache = new Cache(HttpContext.Current.Request.MapPath("/cache/"));

                        if (cache.Exists(list_cd, "lists") || cache.Exists(list_cd + ".filtered", "lists"))
                        {
                            cache.Clear(list_cd + ".*", "lists");
                        }
                    } catch (Exception ex)
                    {
                        System.Diagnostics.Trace.WriteLine("Cache clear failed: " + ex.ToString(), "eportal");
                    }
                    

                    //so we have a list by name.  Is this a new list, i.e. not already registered locally?
                    var db = new DataTools();
                    var csettings = ConfigurationManager.ConnectionStrings["SCAMPs"];
                    db.Provider = csettings.ProviderName;
                    db.ConnectionString = csettings.ConnectionString;

                    db.OpenConnection();

                    Dictionary<string, string> hd = new Dictionary<string, string>();

                    string sql = "select list_cd from admin_element_list where list_cd='" + list_cd + "'";
                    db.GetResultSet(sql);
                    if (db.ResultSet.Rows.Count == 0)
                    {
                        //new list ...
                        isnewlist = true;
                    }
                    
                    //process the list...
                    if (!thelist.IsNullOrEmpty())
                    {
                        //there is actually a list supplied in the file
                        //if this is NOT a new list, clear out the old 
                        //list entries before we add new ones...
                        //with the new schema, this can be handled differently
                        if (!isnewlist) { db.Delete("admin_element_list", "list_cd='" + list_cd + "'"); }

                        thelist = thelist.escapeList();
                        string[] aM = thelist.Split(',');
                        string listitem = "";
                        for(i = 0;i<aM.Length;i++)
                        {
                            Tools.splitList(aM[i], ref key, ref value, ref visible);
                            if (visible.IsNullOrEmpty()) { visible = "Y"; }
                            //in case there are stray or odd characters that get put in there by someone (T/F, etc.) ...
                            if (visible != "Y") { visible = "N"; }

                            listitem = value.Trim().AsHTML();

                            //insert the item...
                            hd.Clear();
                            hd.Add("list_cd", list_cd);
                            hd.Add("description_txt", value);
                            hd.Add("value_txt", key.Trim());       //yeah, yeah, I know the data schema is backwards...
                            hd.Add("seq_num", (i + 1).ToString());
                            hd.Add("visible_abbr",visible);

                            db.Insert(hd, "admin_element_list");
                        }

                        db.Dispose();
                        //all elements added
                        //with the current schema, this is all we do
                        //new schema will now delete all the old list items marked as such, above
                        //and enable the list.
                        Tools.AppendLine(ref loginfo, "List " + list_cd + " installed successfully, " + i.ToString() + " elements added.");
                    }
                    else
                    {
                        //log this exception - LIST IS EMPTY
                        Tools.AppendLine(ref loginfo, "The list " + list_cd + " is empty or invalid");
                        ok = false;
                    }
                }
                else
                {
                    //log this exception
                    Tools.AppendLine(ref loginfo, "No list name is specified in " + listfile);
                    ok = false;
                }
            }
            else
            {
                //log this exception
                Tools.AppendLine(ref loginfo, "List file " + listfile + " is empty or invalid.");
                ok = false;
            }
            return ok;
        }

        public bool ImportReports(string sourcepath, ref string loginfo)
        {
            bool ok = false;
            try
            {

                DirectoryInfo d = new DirectoryInfo(sourcepath);
                foreach (var file in d.GetFiles("*.report"))
                {
                    ValidateReport(file.FullName, ref loginfo);
                }

                ok = true;
            }
            catch (Exception ex)
            {
                //log this exception...
                Tools.AppendLine(ref loginfo, "Report Import error:" + ex.Message);
            }
            return ok;
        }

        public bool ImportForms(string sourcepath, ref string loginfo)
        {
            bool ok = false;
            try
            {
            
            DirectoryInfo d = new DirectoryInfo(sourcepath);
            foreach (var file in d.GetFiles("*.form"))
                {
                    if (file.Name.ToLower().StartsWith("local."))
                    {
                        //ZAPHOD - local form install
                        //no need to register the form as it is never looked up
                        //or associated with an encounter.
                        if (Tools.FileExists(con.Server.MapPath("\\scamps\\" + file.Name)))
                        {
                            Tools.AppendLine(ref loginfo, "Back up local form " + file.Name);
                            File.Move(con.Server.MapPath("\\scamps\\" + file.Name), con.Server.MapPath("\\scamps\\") + Tools.DateStamp() + "_" + Tools.RandomSeed(3) +  "_" + file.Name + ".bak");
                        }
                        Directory.Move(file.FullName, con.Server.MapPath("\\scamps\\" + file.Name));
                        Tools.AppendLine(ref loginfo, "Install local form " + file.Name);
                    }
                    else
                    {
                        ValidateForm(file.FullName, ref loginfo);
                    }
                }

                ok=true;
            }
            catch(Exception ex)
            {
                //log this exception...
                Tools.AppendLine(ref loginfo, "Form Import error:" + ex.Message);
            }
            return ok;
        }

        public bool ValidateReport(string fullpath, ref string loginfo)
        {
            bool ok = true;

            string info = "";
            string name = "";
            string title = "";
            string category = "";   // report category
            string scamp = "";
            string major = "";
            string minor = "";

            try
            {
                string theform = Tools.ReadFile(fullpath);
                info = Tools.XNode(ref theform, "info").Trim();
                name = Tools.XNode(ref info, "name");
                title = Tools.XNode(ref info, "title");
                scamp = Tools.XNode(ref info, "scamp");
                major = Tools.XNode(ref info, "major");
                minor = Tools.XNode(ref info, "minor");
                category = Tools.XNode(ref info, "category");

                if (info.IsNullOrEmpty())
                {
                    ok = false;
                    Tools.AppendLine(ref loginfo, "Report file invalid.  No info block.");
                }
                if (Tools.CleanString(name) != name)
                {
                    ok = false;
                    Tools.AppendLine(ref loginfo, "Report name is invalid (" + name + ").");
                }
                // etc. - other checks for valid reporting here

                // TODO: we should have the ability to install general reports, perhaps in a different location...
                if (Tools.FileExists(con.Server.MapPath("\\scamps\\" + name + ".report")))
                {
                    // back up the old version
                    Tools.AppendLine(ref loginfo, "Back up old report version of " + name + ".report");
                    File.Move(con.Server.MapPath("\\scamps\\" + name + ".report"), con.Server.MapPath("\\scamps\\") + Tools.RandomSeed(3) + Tools.DateStamp() + name + ".report.bak");
                }

                Directory.Move(fullpath, con.Server.MapPath("\\scamps\\" + name + ".report"));
                Tools.AppendLine(ref loginfo, "Installed " + name + ".report");

            }
            catch (Exception ex)
            {
                Tools.AppendLine(ref loginfo, "Report " + title + " (" + name + ")" + " failed to install: " + ex.Message);
            }

            return ok;
        }

        public bool ValidateForm(string fullpath, ref string loginfo)
        {
            bool ok = false;

            string info = "";
            string name = "";
            string title = "";
            string lists = "";      //list of lists
            string listsin = "";    //same list, with single quotes to be used in SQL IN statement
            string scamp = "";
            string major = "";
            string minor = "";
            string formname = "";
            string missinglists = "";
            int mlc = 0;
            int precedence = 0;
            int ID = 0;
            int scamp_id = 0;
            bool isNewEncounter = false;
            Dictionary<string,string> hd = new Dictionary<string,string>();

            try
            {
                string theform = Tools.ReadFile(fullpath);
                info = Tools.XNode(ref theform, "info");
                name = Tools.XNode(ref info, "name");
                title = Tools.XNode(ref info, "title");
                scamp = Tools.XNode(ref info, "scamp");
                major = Tools.XNode(ref info, "major");
                minor = Tools.XNode(ref info, "minor");
                lists = Tools.XNode(ref info, "distinctlists");
                precedence = Tools.AsInteger(Tools.XNode(ref info, "precedence"));

                // create an instance of the scamp.data object to use for database access
                var db = new DataTools();
                var csettings = ConfigurationManager.ConnectionStrings["SCAMPs"];
                db.Provider = csettings.ProviderName;
                db.ConnectionString = csettings.ConnectionString;
                db.OpenConnection();

                // the ID is the form's unique identifier used to relate individual encounters
                // to the SDF form.  This number is wetware managed, mostly, but we can 
                // generate a random number if one is not supplied.  This will mean that 
                // encounters of the same scamp are not going to be grouped together, so this
                // is a failover response which is not the ideal path.
                ID = Tools.AsInteger(Tools.XNode(ref info, "encountertype"));

                while (ID == 0)
                {
                    ID = Tools.RandomNumber.Next(1, 9999);
                    ID = ID + 990000;
                    if (Tools.AsInteger(db.GetValue("select encounter_id from admin_encounters where encounter_id = " + ID.ToString())) != 0)
                        ID = 0;
                    if (ID != 0) { Tools.AppendLine(ref loginfo, "<span class=\"error\">Invalid encountertype (0).  Setting random encounter ID = " + ID + "</span>"); }
                }
                //check if all the lists are present
                lists = Tools.NotEndWith(lists, ",").ToUpper();
                listsin = lists.Replace(",", "','");
                listsin = "'" + listsin + "'";

                //look up the scamp and get the scamp ID OR create the scamp and get the scamp ID
                string sql = "";
                sql = "select distinct list_cd from admin_element_list where list_cd in (" + listsin + ")";
                db.GetResultSet(sql);
                listsin = "";
                while (!db.EOF)
                {
                    listsin += db.Get("list_cd") + ",";
                    db.NextRow();
                }
                listsin = Tools.NotEndWith(listsin, ",").ToUpper();

                lists = Tools.CompareList(lists, listsin);
                if (!lists.IsNullOrEmpty())
                {
                    Tools.AppendLine(ref loginfo, "<span class=\"error\">Missing required list(s) (" + Tools.WordCount(lists) + ") - " + lists + "</span>");
                }
                sql = "select scamp_id from scamp where scamp_cd = '" + scamp.ToUpper() + "'";
                scamp_id = Tools.AsInteger(db.GetValue(sql));
                if (scamp_id == 0)
                {
                    // do not already have this 
                    // scamp and need to add it...
                    db.haserrors = false;
                    hd.Clear();
                    hd.Add("scamp_cd", scamp.ToUpper());
                    hd.Add("scamp_name", scamp.ToUpper());
                    hd.Add("sql_inpt", "added by import from " + fullpath + " on " + DateTime.Now.ToString());
                    hd.Add("subunit_name", "DAY");
                    hd.Add("visible", "Y");
                    hd.Add("display_name", scamp);

                    db.Insert(hd, "scamp");
                    if (db.haserrors)
                    {
                        Tools.AppendLine(ref loginfo, "Register SCAMP failed for " + scamp + ": " + db.errors);
                    }
                    else
                    {
                        Tools.AppendLine(ref loginfo, "Added a new SCAMP - " + scamp);
                        //now set the scamp ID.  This construct will be going away with the new SCHEMA
                        scamp_id = Tools.AsInteger(db.GetValue(sql));
                    }

                    //------------- ORIGINAL SCHEMA BLOCK -------------------
                    //in addition to adding a scamp, we need to add a specific set of scamp status' 
                    //which is a real odd thing given that all entries are the same for each scamp
                    //TODO: Make this go away when we finally get the new schema rolled out.
                    db.haserrors = false;
                    hd.Clear();
                    hd.Add("scamp_id", scamp_id.ToString());
                    hd.Add("seq_num", "1");
                    hd.Add("severity_cd", "0");
                    hd.Add("status_val", "2");
                    hd.Add("status_txt", "To be screened");
                    db.Insert(hd, "admin_scamps_status");

                    Tools.AddOrReplace("seq_num", "2", ref hd);
                    Tools.AddOrReplace("severity_cd", "1", ref hd);
                    Tools.AddOrReplace("status_val", "1", ref hd);
                    Tools.AddOrReplace("status_txt", "Included", ref hd);
                    db.Insert(hd, "admin_scamps_status");

                    Tools.AddOrReplace("seq_num", "3", ref hd);
                    Tools.AddOrReplace("severity_cd", "3", ref hd);
                    Tools.AddOrReplace("status_val", "0", ref hd);
                    Tools.AddOrReplace("status_txt", "Excluded (temporary)", ref hd);
                    db.Insert(hd, "admin_scamps_status");

                    Tools.AddOrReplace("seq_num", "4", ref hd);
                    Tools.AddOrReplace("severity_cd", "2", ref hd);
                    Tools.AddOrReplace("status_val", "-1", ref hd);
                    Tools.AddOrReplace("status_txt", "Excluded (permanent)", ref hd);
                    db.Insert(hd, "admin_scamps_status");

                    Tools.AddOrReplace("seq_num", "5", ref hd);
                    Tools.AddOrReplace("severity_cd", "4", ref hd);
                    Tools.AddOrReplace("status_val", "-2", ref hd);
                    Tools.AddOrReplace("status_txt", "Exited from SCAMP", ref hd);
                    db.Insert(hd, "admin_scamps_status");

                    Tools.AddOrReplace("seq_num", "6", ref hd);
                    Tools.AddOrReplace("severity_cd", "0", ref hd);
                    Tools.AddOrReplace("status_val", "3", ref hd);
                    Tools.AddOrReplace("status_txt", "To be Rescreened", ref hd);
                    db.Insert(hd, "admin_scamps_status");

                    if (db.haserrors)
                    {
                        Tools.AppendLine(ref loginfo, "Insert SCAMP status for  " + scamp + " failed: " + db.errors);
                    }
                    db.haserrors = false;
                }

                // the form name will be how we refer to this form.  Not ideal, but transitional until further refined
                // the "formname" includes the common name and the major and minor version number giving the form a
                // unique constructed name (assuming it is a new version of an existing form).
                formname = Tools.CleanString(name.ToUpper()) + "." + Tools.AsInteger(major) + "." + Tools.AsInteger(minor);

                // do we already have this form?  We use the encounter ID supplied with the XML form to determine if
                // this is an existing row.  If the number does not change, it is the SAME encounter reference.  This ID
                // is used to relate encounters to a specific form instance, so this is VERY important.
                sql = "select encounter_id from admin_encounters where encounter_id = " + ID;
                int formid = Tools.AsInteger(db.GetValue(sql));
                if (formid == 0)
                {
                    //new form based on new encounter type/ID
                    isNewEncounter = true;
                }

                //make all other forms not visible where the name is the same as the short name...
                hd.Clear();
                hd.Add("visible_flag", "N");
                //huh?  Why is this here?  ZAPHOD
                sql = "update admin_encounters set visible_flag = 'N' where LOWER(encounter_name_short) = '" + name.ToLower() + "'";
                db.Update(hd, "admin_encounters", "LOWER(encounter_name_short) = '" + name.ToLower() + "'");

                //add or update the form record...
                hd.Clear();
                hd.Add("encounter_id", ID.ToString());
                hd.Add("scamp_id", scamp_id.ToString());
                hd.Add("encounter_name", title);
                hd.Add("encounter_name_short", name);
                hd.Add("form_name", formname);
                hd.Add("seq_num", precedence.ToString());
                hd.Add("web_form_approved", "Y");
                hd.Add("visible_flag", "Y");

                if (isNewEncounter)
                {
                    db.Insert(hd, "admin_encounters");
                    Tools.AppendLine(ref loginfo, "Created reference to new form " + formname);
                }
                else
                {
                    db.Update(hd, "admin_encounters", "encounter_id = " + ID);
                    Tools.AppendLine(ref loginfo, "Updated existing reference to form " + formname);
                }
                // does this form currently exist?
                if (File.Exists(con.Server.MapPath("\\scamps\\" + formname + ".form")))
                {
                    //it exists, so back up the old version
                    File.Move(con.Server.MapPath("\\scamps\\" + formname + ".form"), con.Server.MapPath("\\scamps\\") + Tools.RandomSeed(3) + Tools.DateStamp() + formname + ".form.bak");
                }
                // now move the form into the /scamps path...
                Directory.Move(fullpath, con.Server.MapPath("\\scamps\\" + formname + ".form"));
                Tools.AppendLine(ref loginfo, "Form " + formname + " prepped and staged in /scamps");


                var sqlret = db.GetValue("select ATTRIB_VAL from admin_config where attrib = 'BACKFILL_SDFS'");

                if (sqlret == null || sqlret.ToString().ToLower() != "false")
                {
                    // ------------------------  ORIGINAL SCHEMA BLOCK -----------------------------
                    // --------------  BACKFILL SHIM TO SUPPORT EXTERNAL REPORTING  ----------------
                    // This work is not necessary but was requested to support on-site reporting
                    // that has never been described or displayed except an example that did not use
                    // any of what we are about to do.
                    //
                    // We need to back fill elements and their definition, the core reason we abandoned
                    // this approach in the first place.  This will be problematic and has the potential 
                    // for element code collisions, which the requester(s) are aware of.  New schema rollout
                    // will be necessary before this gets any traction.
                    //
                    // We will re-use database constructs that we abandoned for a more flexible approach
                    // so that the report creators can "look up" stuff.  The two retired tables we
                    // want to back-fill are:
                    //          ADMIN_ELEMENT
                    //          ADMIN_FORM_FIELDS
                    //
                    // A quick look and you can see what someone figured was a good idea, the problems with
                    // that, and the reason they have been retired.
                    //
                    // Here we go...
                    db.Clear();

                    Scamps.Form form = new Scamps.Form();
                    form.BasePath = con.Server.MapPath("/");
                    form.FormPath = con.Server.MapPath("/scamps/");
                    form.ConnectionString = csettings.ConnectionString;
                    form.Provider = csettings.ProviderName;
                    form.ImagePath = "/scamps/images/";

                    form.Options.VersionWithHiddenElements = true;

                    Tools.AppendLine(ref loginfo, "Backfill option ON, begin backfill of form elements for " + formname);

                    if (File.Exists(con.Server.MapPath("\\scamps\\" + formname + ".form")))
                    {
                        try
                        {
                            // load the form...
                            form.Load(formname);

                            // now, iterate the form and register each element
                            Element element = new Element();
                            string[] fields = null;
                            foreach (KeyValuePair<string, Panel> panel in form.theform)
                            {
                                Dictionary<string, Element> d = panel.Value.dictionary;
                                foreach (KeyValuePair<string, Element> e in d)
                                {
                                    // we want to examine element codes for this
                                    // and register each.  There could be element
                                    // collisions but this is unavoidable under
                                    // the circumstances.  This is a vestigial 
                                    // artifact from the old church.

                                    // If there are multiple, we must dig in a bit
                                    element = e.Value;

                                    // local stacks to split out our compound values
                                    string[] rowlabels = element.rowlabels.Split(',');
                                    string[] typez = element.types.Split(',');
                                    string[] unitz = Tools.SimpleList(element.units, true);
                                    string thistype = "";
                                    int recid = 0;

                                    for (int i = 0; i < element.elementcodes.Length; i++)
                                    {
                                        // This needs to go away eventually, otherwise 
                                        // i'd put it in a sub routine(s) to make it easier
                                        // to manage.  For now, the two table blocks 
                                        // translate the news element expression back 
                                        // into the old data layout.

                                        //===================================================
                                        //               ADMIN FORM FIELDS
                                        //===================================================
                                        hd.Clear();
                                        hd.Add("form_name", formname);
                                        hd.Add("element_cd", element.elementcodes[i]);

                                        //convert our control types to one of the original control types
                                        //multiselect, checklist = LISTBOX
                                        //select, radio = COMBOBOX
                                        //else TEXTBOX (could be multiline)
                                        thistype = element.type;
                                        if (typez.Length > 0 && typez.Length > i)
                                        {
                                            thistype = typez[i].Trim().ToLower();
                                        }
                                        switch (thistype)
                                        {
                                            case "select":
                                            case "selectbox":
                                            case "check":
                                            case "checksingle":
                                            case "radio":
                                            case "radiolist":

                                                hd.Add("control_type", "COMBOBOX");
                                                break;

                                            case "checklist":
                                            case "multiselect":

                                                hd.Add("control_type", "LISTBOX");
                                                break;

                                            case "section":

                                                hd.Add("control_type", "SECTION");
                                                break;

                                            case "image":

                                                hd.Add("control_type", "IMAGE");
                                                break;

                                            case "infobox":

                                                hd.Add("control_type", "INFOBOX");
                                                break;

                                            case "button":
                                            case "submit":

                                                hd.Add("control_type", "BUTTON");
                                                break;

                                            default:

                                                hd.Add("control_type", "TEXTBOX");
                                                break;

                                        }
                                        if (element.type == "textarea")
                                        {
                                            hd.Add("textmode", "MULTILINE");
                                        }
                                        else
                                        {
                                            hd.Add("textmode", ""); //there is no value for single line, just null.
                                        }

                                        if (Tools.AsBoolean(element.required))
                                        {
                                            hd.Add("required_flag", "1");
                                        }
                                        else
                                        {
                                            hd.Add("required_flag", "0");
                                        }

                                        if (element.type == "hidden")
                                        {
                                            hd.Add("visible_flag", "0");
                                        }
                                        else
                                        {
                                            hd.Add("visible_flag", "1");
                                        }

                                        hd.Add("desc_txt", element.label);

                                        recid = Tools.AsInteger(db.GetValue("select id from admin_form_fields where upper(form_name) = '" + formname.ToUpper() + "' and upper(element_cd) = '" + element.elementcodes[i].ToUpper() + "'"));
                                        if (recid == 0)
                                        {
                                            db.Insert(hd, "ADMIN_FORM_FIELDS");
                                            if (db.haserrors) { Tools.AppendLine(ref loginfo, "Error inserting into ADMIN_FORM_FIELDS: " + db.errors); }
                                        }
                                        else
                                        {
                                            db.Update(hd, "ADMIN_FORM_FIELDS", "id = " + recid.ToString());
                                            if (db.haserrors) { Tools.AppendLine(ref loginfo, "Error updating ADMIN_FORM_FIELDS: " + db.errors); }
                                        }

                                        db.haserrors = false;

                                        //===================================================
                                        //              ADMIN ELEMENT POPULATION
                                        //===================================================
                                        if (element.label.IsNullOrEmpty()) { element.label = " "; }
                                        hd.Clear();
                                        hd.Add("element_cd", element.elementcodes[i]);
                                        hd.Add("name_txt", element.label);
                                        hd.Add("description_txt", element.subtitle);

                                        hd.Add("list_cd", element.list_cd);
                                        hd.Add("unit_cd", Tools.SelectDefault(unitz, i, element.units));

                                        //convert our control types to ANOTHER one of the original control types
                                        //multiselect, checklist = Picklist
                                        //select, radio = Picklist
                                        //float, integer, datetime also used inconsistently 
                                        //else String or Integer (could be multiline)
                                        thistype = element.type;
                                        if (typez.Length > 0 && typez.Length > i)
                                        {
                                            thistype = typez[i].Trim().ToLower();
                                        }
                                        switch (thistype)
                                        {
                                            case "select":
                                            case "selectbox":
                                            case "check":
                                            case "checksingle":
                                            case "radio":
                                            case "radiolist":

                                                hd.Add("data_type_cd", "Picklist");
                                                hd.Add("is_multi_select", "N");
                                                break;

                                            case "checklist":
                                            case "multiselect":

                                                hd.Add("data_type_cd", "Picklist");
                                                hd.Add("is_multi_select", "Y");
                                                break;

                                            case "section":

                                                hd.Add("data_type_cd", "SECTION");
                                                break;

                                            case "image":

                                                hd.Add("data_type_cd", "IMAGE");
                                                break;

                                            case "infobox":

                                                hd.Add("data_type_cd", "INFOBOX");
                                                break;

                                            case "button":
                                            case "submit":

                                                hd.Add("data_type_cd", "BUTTON");
                                                break;

                                            case "integer":
                                            case "signedint":
                                            case "number":

                                                hd.Add("data_type_cd", "Integer");
                                                break;

                                            case "float":
                                            case "signedfloat":

                                                hd.Add("data_type_cd", "Float");
                                                break;

                                            case "date":
                                            case "datetime":
                                            case "pastdate":
                                            case "futuredate":

                                                hd.Add("data_type_cd", "DateTime");
                                                break;

                                            default:

                                                hd.Add("data_type_cd", "String");
                                                break;

                                        }

                                        recid = Tools.AsInteger(db.GetValue("select id from admin_element where upper(element_cd) = '" + element.elementcodes[i].ToUpper() + "'"));
                                        if (recid == 0)
                                        {
                                            db.Insert(hd, "ADMIN_ELEMENT");
                                            if (db.haserrors) { Tools.AppendLine(ref loginfo, "Error inserting into ADMIN_ELEMENTS: " + db.errors); }
                                        }
                                        else
                                        {
                                            // NOTE: This could blindly overwrite like named elements due to the 
                                            // architectural changes that abandoned this table in favor of 
                                            // a document model.  At this level, there is no form level
                                            // distinction for a named element, so the same named element on
                                            // different forms will overwrite on another.  
                                            db.Update(hd, "ADMIN_ELEMENT", "id = " + recid.ToString());
                                            if (db.haserrors) { Tools.AppendLine(ref loginfo, "Error updating ADMIN_ELEMENTS: " + db.errors); }
                                        }

                                        // ok, inserted or updated these two tables.  This second one could stomp all over data, as discussed at length
                                        // but ok'ed by analytics because "even bad context data is better than no context data" (their words).

                                    }   // each element code iteration
                                }  // each form element
                            }  // each panel

                            db.Dispose();
                            //form process complete.

                        }
                        catch (Exception ex)
                        {
                            Tools.AppendLine(ref loginfo, "Form " + formname + " failed to load: " + ex.Message);
                        }
                    }

                    db.Clear();
                    db.Dispose();
                }
                ok = true;
            }
            catch (Exception ex)
            {
                //log this...
                Tools.AppendLine(ref loginfo, "Validate/Register Scamp Form error:" + ex.Message);
            }


            return ok;
        }

        public string UploadFile(ref string filename,ref string tempdirectory)
        {

             //figure a temp directory to put the file in...
            tempdirectory = Tools.RandomSeed(16);
            filename = "";
            string loginfo = "";
            Stream filehandle = null;

            Tools.AppendLine(ref loginfo, "");
            Tools.AppendLine(ref loginfo, "--- " + DateTime.Now.ToString() + " ---");

            int b = 0;
            byte[] ba = new byte[8 * 1024];

            //does our root file path exist?  If not, create it...
            string filepath = con.Server.MapPath("/assets/");
            if (!Directory.Exists(filepath))
            {
                Directory.CreateDirectory(filepath);
                Tools.AppendLine(ref loginfo, "Created root assets path");
            }

            //does this directory exist?  If so, kill it
            if(Directory.Exists(filepath + tempdirectory))
            {
                Tools.DeleteFiles(filepath + tempdirectory);
                Directory.Delete(filepath + tempdirectory);
                Tools.AppendLine(ref loginfo, "Remove pre-existing temporary directory ");
            }
            // create it anew...
            Directory.CreateDirectory(filepath + tempdirectory);
            Tools.AppendLine(ref loginfo, "Created temporary directory ");

            if(con.Request.Files.Count>0)
            {
                filehandle = con.Request.Files[0].InputStream;
                filename = Path.GetFileName(con.Request.Files[0].FileName);
            }
            else if (con.Request.InputStream != null && con.Request.InputStream.Length>0)
            {
                filehandle = con.Request.InputStream;
                filename = Tools.AsText(con.Request.QueryString["filename"]);
                if(filename.IsNullOrEmpty())
                {
                    filename = "scamp.zip";
                }
            }

            filename = filename.ToLower();

            //save the zip file into the temp directory
            Stream writefile = new FileStream(filepath + tempdirectory + "\\" + filename, FileMode.Create, FileAccess.Write);

            //Stream writefile = File.Create(filepath + tempdirectory + "\\" + filename,(int)filehandle.Length);
            while ((b = filehandle.Read(ba, 0, ba.Length)) > 0)
            {
                writefile.Write(ba, 0, b);
            }

            writefile.Close();
            filehandle.Close();
            writefile.Dispose();
            filehandle.Dispose();

            return filepath + tempdirectory + "\\" + filename;
        }

    }
}
