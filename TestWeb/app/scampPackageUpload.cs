﻿using System;
using System.IO;
using System.Text;
using Scamps;
using Ionic.Zip;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Web;

namespace ePortal.app
{
    public static class StrPathExt
    {
        private static readonly string DirSeparatorString = Path.DirectorySeparatorChar.ToString();
        public static string AppendDirectorySeparator(this string path)
        {
            if (path.EndsWith(DirSeparatorString))
                return path;
            return string.Concat(path, DirSeparatorString);
        }
    }

    public static class scampPackageUpload
    {
        private static readonly long maxFileSizeBytes = 2097152;
        private static readonly string[] badExtensions = new string[] { ".xlsx", ".xlsm", ".xls", ".docx" };
        
        public static void ProcessUpload(Tuple<Stream, string> fileHandle, StringBuilder logInfo, ConnectionStringSettings csettings, string basePath, string SDFpath, string imagePath, string cachePath){
            var startPos = fileHandle.Item1.Position;
            //test the zip file
            if (!ZipFile.IsZipFile(fileHandle.Item1,false))
                throw new IOException("Not a valid package");
            //rewind stream
            fileHandle.Item1.Seek(startPos, SeekOrigin.Begin);

            //create scamp and image directories if not already there
            if (!Directory.Exists(SDFpath)){
                Directory.CreateDirectory(SDFpath);
                logInfo.AppendLine("Created SCAMPS folder");
            }
            if (!Directory.Exists(imagePath)){
                Directory.CreateDirectory(SDFpath);
                logInfo.AppendLine("Created SCAMPS folder");
            }

            //pass stream directly to zipreader
            using (var zip = ZipFile.Read(fileHandle.Item1)){
                //get file candidates using 'SelectEntries' selection syntax (for criteria syntax, see http://dotnetzip.herobo.com/DNZHelp/Index.html )
                    //supports parathensis () and conjunctions AND/OR

            //1. Process .form and .list files
                var payloadCandidates = zip.SelectEntries("name = *.list OR name = *.form");
                List<ZipEntry> listFiles = null;
                List<ZipEntry> formFiles = null;
                
                var hasCandidates = false;
                //scan candidates, check filesize and put them into segmented lists
                foreach (var candidate in payloadCandidates)
                {
                    //check each candidate file for an appropriate filesize, if it's not appropriate, skip it
                    if (candidate.UncompressedSize > maxFileSizeBytes)
                    {
                        logInfo.AppendLine(string.Concat("Entry '", candidate.FileName, "' larger than 2mb"));
                        continue;
                    }

                    //if the candidate filename contains the expected extension, put it into the appropriate list
                    if (candidate.FileName.EndsWith(".list", StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (listFiles == null)
                            listFiles = new List<ZipEntry>();
                        listFiles.Add(candidate);
                    }
                    else if (candidate.FileName.EndsWith(".form", StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (formFiles == null)
                            formFiles = new List<ZipEntry>();
                        formFiles.Add(candidate);
                    }
                }
                hasCandidates |= (listFiles != null && listFiles.Any());
                hasCandidates |= (formFiles != null && formFiles.Any());
                //1.a process lists
                if (listFiles != null){
                    foreach (var l in listFiles)
                    {
                        try
                        {
                            var zs = l.OpenReader() as Stream;
                            var res = InstallList(zs, l.FileName, csettings, cachePath, logInfo);
                        }
                        catch (Exception e)
                        {
                            logInfo.AppendLine(string.Concat("Error Installing list '", l.FileName, "'"));
                            Console.Error.WriteLine("Error Copying Image from Upload: " + e.Message);
                        }
                    }
                }
                //1.b process forms
                if (formFiles != null){
                    foreach (var f in formFiles)
                    {
                        try
                        {
                            var zs = f.OpenReader() as Stream;
                            var res = InstallForm(zs, f.FileName, csettings, basePath, SDFpath, imagePath, logInfo);
                        }
                        catch (Exception e)
                        {
                            Console.Error.WriteLine("Error Installing form from Upload: " + e.Message);
                            logInfo.AppendLine(string.Concat("Error Installing form '", f.FileName, "'"));
                        }
                    }
                }

            //2. handle images -- whats a reasonable size limit for images ??
                    //supported extensions: gif,jpg,jpeg,png
                payloadCandidates = zip.SelectEntries("name = *.gif OR name = *.jpg OR name = *.jpeg OR name = *.png");

                hasCandidates |= (payloadCandidates != null && payloadCandidates.Any());
                
                if (!hasCandidates){
                    logInfo.AppendLine("Error: Invalid package");
                    return;
                }

                foreach (var img in payloadCandidates){
                    try
                    {
                        //streamcopy to file
                        using (var fw = File.OpenWrite(imagePath + img.FileName))
                        {
                            img.InputStream.CopyTo(fw);
                        }
                        logInfo.AppendLine(string.Concat("Copied image '", img.FileName, "' to images"));
                    }
                    catch (Exception e)
                    {
                        Console.Error.WriteLine("Error Copying Image from Upload: "+e.Message);
                        logInfo.AppendLine(string.Concat("Error copying image '", img.FileName, "'"));
                    }
                }
            }
        }

        public static bool InstallForm(Stream s, string filename, ConnectionStringSettings csettings, string basePath, string SDFPath, string imagePath, StringBuilder logInfo){
            bool ok = false;

            string info = "";
            string name = "";
            string title = "";
            string lists = "";      //list of lists
            string listsin = "";    //same list, with single quotes to be used in SQL IN statement
            string scamp = "";
            string major = "";
            string minor = "";
            string formname = "";
            int precedence = 0;
            int ID = 0;
            int scamp_id = 0;
            bool isNewEncounter = false;
            var hd = new Dictionary<string, string>();

            try
            {
                var theform = string.Empty;
                using (var rd = new StreamReader(s))
                    theform = rd.ReadToEnd();

                info = Tools.XNode(ref theform, "info");
                name = Tools.XNode(ref info, "name");
                title = Tools.XNode(ref info, "title");
                scamp = Tools.XNode(ref info, "scamp");
                major = Tools.XNode(ref info, "major");
                minor = Tools.XNode(ref info, "minor");
                lists = Tools.XNode(ref info, "distinctlists");
                precedence = Tools.AsInteger(Tools.XNode(ref info, "precedence"));

                // create an instance of the scamp.data object to use for database access
                var db = new DataTools();
                db.Provider = csettings.ProviderName;
                db.ConnectionString = csettings.ConnectionString;
                db.OpenConnection();

                // the ID is the form's unique identifier used to relate individual encounters
                // to the SDF form.  This number is wetware managed, mostly, but we can 
                // generate a random number if one is not supplied.  This will mean that 
                // encounters of the same scamp are not going to be grouped together, so this
                // is a failover response which is not the ideal path.
                ID = Tools.AsInteger(Tools.XNode(ref info, "encountertype"));

                while (ID == 0){
                    ID = Tools.RandomNumber.Next(1, 9999);
                    ID = ID + 990000;
                    if (Tools.AsInteger(db.GetValue("select encounter_id from admin_encounters where encounter_id = " + ID.ToString())) != 0)
                        ID = 0;

                    if (ID != 0)
                        logInfo.AppendLine(string.Concat("<span class=\"error\">Invalid encountertype (0).  Setting random encounter ID = ",ID,"</span>"));
                }

                //check if all the lists are present
                lists = Tools.NotEndWith(lists, ",").ToUpper();
                listsin = lists.Replace(",", "','");
                listsin = "'" + listsin + "'";

                //look up the scamp and get the scamp ID OR create the scamp and get the scamp ID
                string sql = "";
                sql = "select distinct list_cd from admin_element_list where list_cd in (" + listsin + ")";
                db.GetResultSet(sql);
                listsin = "";
                while (!db.EOF)
                {
                    listsin += db.Get("list_cd") + ",";
                    db.NextRow();
                }
                listsin = Tools.NotEndWith(listsin, ",").ToUpper();

                lists = Tools.CompareList(lists, listsin);
                if (!lists.IsNullOrEmpty()){
                    logInfo.AppendLine(string.Concat("<span class=\"error\">Missing required list(s) (",Tools.WordCount(lists),") - ",lists,"</span>"));
                }

                sql = "select scamp_id from scamp where scamp_cd = '" + scamp.ToUpper() + "'";
                scamp_id = Tools.AsInteger(db.GetValue(sql));
                if (scamp_id == 0)
                {
                    // do not already have this 
                    // scamp and need to add it...
                    hd.Clear();
                    hd.Add("scamp_cd", scamp.ToUpper());
                    hd.Add("scamp_name", scamp.ToUpper());
                    hd.Add("sql_inpt", "added by import on " + DateTime.Now.ToString());
                    hd.Add("subunit_name", "DAY");
                    hd.Add("visible", "Y");
                    hd.Add("display_name", scamp);

                    db.Insert(hd, "scamp");

                    logInfo.AppendLine(string.Concat("Added a new SCAMP - ", scamp));

                    //now set the scamp ID.  This construct will be going away with the new SCHEMA
                    scamp_id = Tools.AsInteger(db.GetValue(sql));

                    //------------- ORIGINAL SCHEMA BLOCK -------------------
                    //in addition to adding a scamp, we need to add a specific set of scamp status' 
                    //which is a real odd thing given that all entries are the same for each scamp
                    //TODO: Make this go away when we finally get the new schema rolled out.
                    hd.Clear();
                    hd.Add("scamp_id", scamp_id.ToString());
                    hd.Add("seq_num", "1");
                    hd.Add("severity_cd", "0");
                    hd.Add("status_val", "2");
                    hd.Add("status_txt", "To be screened");
                    db.Insert(hd, "admin_scamps_status");

                    Tools.AddOrReplace("seq_num", "2", ref hd);
                    Tools.AddOrReplace("severity_cd", "1", ref hd);
                    Tools.AddOrReplace("status_val", "1", ref hd);
                    Tools.AddOrReplace("status_txt", "Included", ref hd);
                    db.Insert(hd, "admin_scamps_status");

                    Tools.AddOrReplace("seq_num", "3", ref hd);
                    Tools.AddOrReplace("severity_cd", "3", ref hd);
                    Tools.AddOrReplace("status_val", "0", ref hd);
                    Tools.AddOrReplace("status_txt", "Excluded (temporary)", ref hd);
                    db.Insert(hd, "admin_scamps_status");

                    Tools.AddOrReplace("seq_num", "4", ref hd);
                    Tools.AddOrReplace("severity_cd", "2", ref hd);
                    Tools.AddOrReplace("status_val", "-1", ref hd);
                    Tools.AddOrReplace("status_txt", "Excluded (permanent)", ref hd);
                    db.Insert(hd, "admin_scamps_status");

                    Tools.AddOrReplace("seq_num", "5", ref hd);
                    Tools.AddOrReplace("severity_cd", "4", ref hd);
                    Tools.AddOrReplace("status_val", "-2", ref hd);
                    Tools.AddOrReplace("status_txt", "Exited from SCAMP", ref hd);
                    db.Insert(hd, "admin_scamps_status");

                    Tools.AddOrReplace("seq_num", "6", ref hd);
                    Tools.AddOrReplace("severity_cd", "0", ref hd);
                    Tools.AddOrReplace("status_val", "3", ref hd);
                    Tools.AddOrReplace("status_txt", "To be Rescreened", ref hd);
                    db.Insert(hd, "admin_scamps_status");

                }

                // the form name will be how we refer to this form.  Not ideal, but transitional until further refined
                // the "formname" includes the common name and the major and minor version number giving the form a
                // unique constructed name (assuming it is a new version of an existing form).
                formname = Tools.CleanString(name.ToUpper()) + "." + Tools.AsInteger(major) + "." + Tools.AsInteger(minor);

                // do we already have this form?  We use the encounter ID supplied with the XML form to determine if
                // this is an existing row.  If the number does not change, it is the SAME encounter reference.  This ID
                // is used to relate encounters to a specific form instance, so this is VERY important.
                sql = "select encounter_id from admin_encounters where encounter_id = " + ID;
                int formid = Tools.AsInteger(db.GetValue(sql));
                if (formid == 0)
                {
                    //new form based on new encounter type/ID
                    isNewEncounter = true;
                }

                //make all other forms not visible where the name is the same as the short name...
                hd.Clear();
                hd.Add("visible_flag", "N");
                sql = "update admin_encounters set visible_flag = 'N' where LOWER(encounter_name_short) = '" + name.ToLower() + "'";
                db.Update(hd, "admin_encounters", "LOWER(encounter_name_short) = '" + name.ToLower() + "'");

                //add or update the form record...
                hd.Clear();
                hd.Add("encounter_id", ID.ToString());
                hd.Add("scamp_id", scamp_id.ToString());
                hd.Add("encounter_name", title);
                hd.Add("encounter_name_short", name);
                hd.Add("form_name", formname);
                hd.Add("seq_num", precedence.ToString());
                hd.Add("web_form_approved", "Y");
                hd.Add("visible_flag", "Y");

                if (isNewEncounter)
                {
                    db.Insert(hd, "admin_encounters");
                    logInfo.AppendLine(string.Concat("Created reference to new form ", formname));
                }
                else
                {
                    db.Update(hd, "admin_encounters", "encounter_id = " + ID);
                    logInfo.AppendLine(string.Concat("Updated existing reference to form ", formname));
                }
                var _SDFPath = SDFPath.AppendDirectorySeparator();
                // does this form currently exist?
                var SDFfilepath = string.Concat(_SDFPath, formname, ".form");
                //if (File.Exists(con.Server.MapPath("\\scamps\\" + formname + ".form")))
                if (File.Exists(SDFfilepath)){
                    //it exists, so back up the old version
                    //File.Move(con.Server.MapPath("\\scamps\\" + formname + ".form"), con.Server.MapPath("\\scamps\\") + Tools.RandomSeed(3) + Tools.DateStamp() + formname + ".form.bak");
                    File.Move(SDFfilepath, string.Concat(_SDFPath, Tools.RandomSeed(3),Tools.DateStamp(),formname,".form.bak"));
                }
                // now move the form into the /scamps path...
                //Directory.Move(fullpath, con.Server.MapPath("\\scamps\\" + formname + ".form"));
                using (var sw = new StreamWriter(File.OpenWrite(SDFfilepath)))
                    sw.Write(theform);

                logInfo.AppendLine(string.Concat("Form ",formname," prepped and staged in /scamps"));

                var sqlret = db.GetValue("select ATTRIB_VAL from admin_config where attrib = 'BACKFILL_SDFS'");

                if (sqlret == null || sqlret.ToString().ToLower() != "false")
                {
                    // ------------------------  ORIGINAL SCHEMA BLOCK -----------------------------
                    // --------------  BACKFILL SHIM TO SUPPORT EXTERNAL REPORTING  ----------------
                    // This work is not necessary but was requested to support on-site reporting
                    // that has never been described or displayed except an example that did not use
                    // any of what we are about to do.
                    //
                    // We need to back fill elements and their definition, the core reason we abandoned
                    // this approach in the first place.  This will be problematic and has the potential 
                    // for element code collisions, which the requester(s) are aware of.  New schema rollout
                    // will be necessary before this gets any traction.
                    //
                    // We will re-use database constructs that we abandoned for a more flexible approach
                    // so that the report creators can "look up" stuff.  The two retired tables we
                    // want to back-fill are:
                    //          ADMIN_ELEMENT
                    //          ADMIN_FORM_FIELDS
                    //
                    // A quick look and you can see what someone figured was a good idea, the problems with
                    // that, and the reason they have been retired.
                    //
                    // Here we go...
                    db.Clear();

                    var form = new Form();
                    form.BasePath = basePath;
                    form.FormPath = SDFPath;
                    form.ConnectionString = csettings.ConnectionString;
                    form.Provider = csettings.ProviderName;
                    form.ImagePath = "/scamps/images/";

                    form.Options.VersionWithHiddenElements = true;

                    if (File.Exists(SDFfilepath))
                    {
                        try
                        {
                            // load the form...
                            form.Load(formname);

                            // now, iterate the form and register each element
                            Element element = new Element();
                            foreach (KeyValuePair<string, Panel> panel in form.theform)
                            {
                                Dictionary<string, Element> d = panel.Value.dictionary;
                                foreach (KeyValuePair<string, Element> e in d)
                                {
                                    // we want to examine element codes for this
                                    // and register each.  There could be element
                                    // collisions but this is unavoidable under
                                    // the circumstances.  This is a vestigial 
                                    // artifact from the old church.

                                    // If there are multiple, we must dig in a bit
                                    element = e.Value;

                                    // local stacks to split out our compound values
                                    string[] rowlabels = element.rowlabels.Split(',');
                                    string[] typez = element.types.Split(',');
                                    string[] unitz = Tools.SimpleList(element.units, true);
                                    string thistype = "";
                                    int recid = 0;

                                    for (int i = 0; i < element.elementcodes.Length; i++)
                                    {
                                        // This needs to go away eventually, otherwise 
                                        // i'd put it in a sub routine(s) to make it easier
                                        // to manage.  For now, the two table blocks 
                                        // translate the news element expression back 
                                        // into the old data layout.

                                        //===================================================
                                        //               ADMIN FORM FIELDS
                                        //===================================================
                                        hd.Clear();
                                        hd.Add("form_name", formname);
                                        hd.Add("element_cd", element.elementcodes[i]);

                                        //convert our control types to one of the original control types
                                        //multiselect, checklist = LISTBOX
                                        //select, radio = COMBOBOX
                                        //else TEXTBOX (could be multiline)
                                        thistype = element.type;
                                        if (typez.Length > 0 && typez.Length > i)
                                        {
                                            thistype = typez[i].Trim().ToLower();
                                        }
                                        switch (thistype)
                                        {
                                            case "select":
                                            case "selectbox":
                                            case "check":
                                            case "checksingle":
                                            case "radio":
                                            case "radiolist":

                                                hd.Add("control_type", "COMBOBOX");
                                                break;

                                            case "checklist":
                                            case "multiselect":

                                                hd.Add("control_type", "LISTBOX");
                                                break;

                                            case "section":

                                                hd.Add("control_type", "SECTION");
                                                break;

                                            case "image":

                                                hd.Add("control_type", "IMAGE");
                                                break;

                                            case "infobox":

                                                hd.Add("control_type", "INFOBOX");
                                                break;

                                            case "button":
                                            case "submit":

                                                hd.Add("control_type", "BUTTON");
                                                break;

                                            default:

                                                hd.Add("control_type", "TEXTBOX");
                                                break;

                                        }
                                        if (element.type == "textarea")
                                        {
                                            hd.Add("textmode", "MULTILINE");
                                        }
                                        else
                                        {
                                            hd.Add("textmode", ""); //there is no value for single line, just null.
                                        }

                                        if (Tools.AsBoolean(element.required))
                                        {
                                            hd.Add("required_flag", "1");
                                        }
                                        else
                                        {
                                            hd.Add("required_flag", "0");
                                        }

                                        if (element.type == "hidden")
                                        {
                                            hd.Add("visible_flag", "0");
                                        }
                                        else
                                        {
                                            hd.Add("visible_flag", "1");
                                        }

                                        hd.Add("desc_txt", element.label);

                                        recid = Tools.AsInteger(db.GetValue("select id from admin_form_fields where upper(form_name) = '" + formname.ToUpper() + "' and upper(element_cd) = '" + element.elementcodes[i].ToUpper() + "'"));
                                        if (recid == 0)
                                        {
                                            db.Insert(hd, "ADMIN_FORM_FIELDS");
                                        }
                                        else
                                        {
                                            db.Update(hd, "ADMIN_FORM_FIELDS", "id = " + recid.ToString());
                                        }


                                        //===================================================
                                        //              ADMIN ELEMENT POPULATION
                                        //===================================================

                                        hd.Clear();
                                        hd.Add("element_cd", element.elementcodes[i]);
                                        hd.Add("name_txt", element.label);
                                        hd.Add("description_txt", element.subtitle);

                                        hd.Add("list_cd", element.list_cd);
                                        hd.Add("unit_cd", Tools.SelectDefault(unitz, i, element.units));

                                        //convert our control types to ANOTHER one of the original control types
                                        //multiselect, checklist = Picklist
                                        //select, radio = Picklist
                                        //float, integer, datetime also used inconsistently 
                                        //else String or Integer (could be multiline)
                                        thistype = element.type;
                                        if (typez.Length > 0 && typez.Length > i)
                                        {
                                            thistype = typez[i].Trim().ToLower();
                                        }
                                        switch (thistype)
                                        {
                                            case "select":
                                            case "selectbox":
                                            case "check":
                                            case "checksingle":
                                            case "radio":
                                            case "radiolist":

                                                hd.Add("data_type_cd", "Picklist");
                                                hd.Add("is_multi_select", "N");
                                                break;

                                            case "checklist":
                                            case "multiselect":

                                                hd.Add("data_type_cd", "Picklist");
                                                hd.Add("is_multi_select", "Y");
                                                break;

                                            case "section":

                                                hd.Add("data_type_cd", "SECTION");
                                                break;

                                            case "image":

                                                hd.Add("data_type_cd", "IMAGE");
                                                break;

                                            case "infobox":

                                                hd.Add("data_type_cd", "INFOBOX");
                                                break;

                                            case "button":
                                            case "submit":

                                                hd.Add("data_type_cd", "BUTTON");
                                                break;

                                            case "integer":
                                            case "signedint":
                                            case "number":

                                                hd.Add("data_type_cd", "Integer");
                                                break;

                                            case "float":
                                            case "signedfloat":

                                                hd.Add("data_type_cd", "Float");
                                                break;

                                            case "date":
                                            case "datetime":
                                            case "pastdate":
                                            case "futuredate":

                                                hd.Add("data_type_cd", "DateTime");
                                                break;

                                            default:

                                                hd.Add("data_type_cd", "String");
                                                break;

                                        }

                                        recid = Tools.AsInteger(db.GetValue("select id from admin_element where upper(element_cd) = '" + element.elementcodes[i].ToUpper() + "'"));
                                        if (recid == 0)
                                        {
                                            db.Insert(hd, "ADMIN_ELEMENT");
                                        }
                                        else
                                        {
                                            // NOTE: This could blindly overwrite like named elements due to the 
                                            // architectural changes that abandoned this table in favor of 
                                            // a document model.  At this level, there is no form level
                                            // distinction for a named element, so the same named element on
                                            // different forms will overwrite on another.  
                                            db.Update(hd, "ADMIN_ELEMENT", "id = " + recid.ToString());
                                        }

                                        // ok, inserted or updated these two tables.  This second one could stomp all over data, as discussed at length
                                        // but ok'ed by analytics because "even bad context data is better than no context data" (their words).

                                    }   // each element code iteration
                                }  // each form element
                            }  // each panel

                            //form process complete.

                        }
                        catch (Exception ex)
                        {
                            logInfo.AppendLine(string.Concat("Form ", formname, " failed to load: ", ex.Message));
                        }
                    }

                    db.Clear();
                    db.Dispose();
                }
                ok = true;
            }
            catch (Exception ex)
            {
                //log this...
                logInfo.AppendLine(string.Concat("Validate/Register Scamp Form error:", ex.Message));
            }

            return ok;
        }

        public static bool InstallList(Stream s, string filename, ConnectionStringSettings csettings, string cachePath, StringBuilder logInfo){

            var contents = string.Empty;
            using (var rd = new StreamReader(s))
                contents = rd.ReadToEnd();

        //begin importlist code
            string list_cd = "";
            string thelist = "";
            string key = "";
            string value = "";
            string visible = "";
            int i = 0;
            bool isnewlist = false;

            list_cd = Tools.NormalString(Tools.XNode(ref contents, "name")).ToUpper();
            thelist = Tools.NormalString(Tools.XNode(ref contents, "elements"));

            if(string.IsNullOrEmpty(list_cd)){
                logInfo.AppendLine(string.Concat("No list code defined for '",filename,"'"));
                return false;
            }
            //clear the cache, if this list is cached.
            try
            {
                var cache = new Cache(cachePath);

                if (cache.Exists(list_cd, "lists") || cache.Exists(list_cd + ".filtered", "lists"))
                    cache.Clear(list_cd + ".*", "lists");
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("Cache clear failed: " + ex.ToString(), "eportal");
            }


            //so we have a list by name.  Is this a new list, i.e. not already registered locally?
            var db = new DataTools();
            db.Provider = csettings.ProviderName;
            db.ConnectionString = csettings.ConnectionString;

            db.OpenConnection();

            var hd = new Dictionary<string, string>();

            var sql = string.Concat("select list_cd from admin_element_list where list_cd='",list_cd,"'");
            db.GetResultSet(sql);
            //new list ... 
            isnewlist = (db.ResultSet.Rows.Count == 0);

            //if list is empty, error
            if (string.IsNullOrEmpty(thelist)){
                logInfo.AppendLine(string.Concat("The list ",list_cd," is empty or invalid"));
                return false;
            }
            //process the list...

            //there is actually a list supplied in the file
            //if this is NOT a new list, clear out the old 
            //list entries before we add new ones...
            //with the new schema, this can be handled differently
            if (!isnewlist) { db.Delete("admin_element_list", "list_cd='" + list_cd + "'"); }

            Element.escapeList(ref thelist);
            string[] aM = thelist.Split(',');
            for (i = 0; i < aM.Length; i++)
            {
                Tools.splitList(aM[i], ref key, ref value, ref visible);
                if (visible.IsNullOrEmpty()) { visible = "Y"; }
                //in case there are stray or odd characters that get put in there by someone (T/F, etc.) ...
                if (visible != "Y") { visible = "N"; }

                //insert the item...
                hd.Clear();
                hd.Add("list_cd", list_cd);
                hd.Add("description_txt", value);
                hd.Add("value_txt", key);       //yeah, yeah, I know the data schema is backwards...
                hd.Add("seq_num", (i + 1).ToString());
                hd.Add("visible_abbr", visible);

                db.Insert(hd, "admin_element_list");
            }

            //all elements added
            //with the current schema, this is all we do
            //new schema will now delete all the old list items marked as such, above
            //and enable the list.
            logInfo.AppendLine(string.Concat("List ",list_cd," installed successfully, ",i," elements added."));

            return true;
        //end importlist code
        }

        /// <summary>
        /// Alternate handling of a SCAMP package which handles the stream without extracting to the file system
        /// </summary>
        /// <param name="context">HTTPContext with file upload stream in request</param>
        public static void HandleUpload(HttpContext context){
            if(context == null || context.Request == null)
                return;

            if(context.Request.QueryString == null)
                return;

            var QSfilename = string.Empty;
            if (!context.Request.QueryString.HasKeys() || string.IsNullOrEmpty((QSfilename = context.Request.QueryString["filename"])))
                return;
            
            var loginfo = new StringBuilder();

            //need to be able to read stream
            if(!context.Request.InputStream.CanRead){
                return;
            }
            //Create a tuple of a stream and filename
            var fileHandle = (context.Request.Files.Count > 0) ?
                Tuple.Create(context.Request.Files[0].InputStream,context.Request.Files[0].FileName) :
                Tuple.Create(context.Request.InputStream, QSfilename);

            //since this will not be a nice function to test, pass the Tuple to another method
            try
            {
                ProcessUpload(fileHandle, loginfo,
                    ConfigurationManager.ConnectionStrings["SCAMPs"],
                    context.Server.MapPath("/"),
                    context.Server.MapPath("/scamps"), 
                    context.Server.MapPath("/scamps/images"),
                    context.Server.MapPath("/cache/")
                    );
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error processing SCAMP package: " + e.Message);
            }
        }
    }
}