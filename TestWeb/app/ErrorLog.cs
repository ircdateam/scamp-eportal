using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;

namespace Scamps.ErrorLog
{
    public interface IErrorLog
    {
        void init(HttpApplication app);
        void LogError(Exception e, HttpContext c, object sender); 
    }

    public class TextLogger : IErrorLog
    {
        //maxfilesize in bytes
        private static readonly long maxfilesize = 1024 * 1024 * 10;
        private long currsize = 0;
        private TextWriter logger;
        private string fullpath = "";
        private StringBuilder logMessage;

        public void init(HttpApplication app)
        {
            var basepath = app.Server.MapPath("~");
            var filename = "_eplog.txt";
            fullpath = string.Concat(basepath, filename);

            FileStream fs = null;
            try
            {
                bool createFile = true;
                
                if (File.Exists(fullpath))
                {
                    var fileinfo = new FileInfo(fullpath);
                    currsize = fileinfo.Length;
                    createFile = (currsize >= maxfilesize);
                }
                
                fs = createFile ? (File.Create(fullpath, 2048, FileOptions.Asynchronous)) : (File.Open(fullpath, FileMode.Append, FileAccess.Write, FileShare.Read));

                logger = new StreamWriter(fs);
                logMessage = new StringBuilder();
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.ToString());
                logger = null;
            }
        }

        public void LogError(Exception e, HttpContext c, object sender)
        {
            
            if (logger == null)
                return;

            if (currsize >= maxfilesize)
            {
                try
                {
                    logger.Close();
                    logger = new StreamWriter(File.Create(fullpath, 2048, FileOptions.Asynchronous));
                    currsize = 0;
                }
                catch (Exception err)
                {
                    Console.Error.WriteLine(err.ToString());
                    logger = null;
                }
            }

            logMessage.Clear();

            var user = "{UnAuthenticated}";
            if(c.User != null && c.User.Identity != null)
                user = c.User.Identity.Name;
            var hostip = c.Request.UserHostAddress;
            var hostname = c.Request.UserHostName;
            var refhost = c.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            var timeof = c.Timestamp;
            var senderns = string.Concat(sender.GetType().Namespace,sender.GetType().Name);
            
            if(refhost.IsNullOrEmpty())
                refhost = c.Request.ServerVariables["REMOTE_ADDR"];

            logMessage.AppendFormat("#{0} Exception {1} from handler {2} requested by {3} ({4}) ", timeof, e.GetType().Name, c.CurrentHandler.GetType().Name, hostname, hostip);
            if (!refhost.IsNullOrEmpty())
                logMessage.AppendFormat("[{0}] ", refhost);
            logMessage.AppendFormat("User '{0}':", user);
            logMessage.AppendLine();

            logMessage.AppendFormat("Exception Message: {0}",e.Message);
            logMessage.AppendLine();

            if (e.InnerException != null)
            {
                logMessage.AppendFormat("\tInner Exception '{0}': {1}",e.InnerException.GetType().Name, e.InnerException.Message);
                logMessage.AppendLine();
            }
            logMessage.AppendLine(e.StackTrace);

            var logstr = logMessage.ToString();

            var msgsize = logger.Encoding.GetByteCount(logstr);
            currsize += msgsize;
            logger.Write(logstr);
            logger.Flush();
        }
    }
}