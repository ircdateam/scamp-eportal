﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Reflection;
using System.Web;
using System.Web.Handlers;
using System.Web.SessionState;
using Newtonsoft.Json;
using System.Linq;
using ScampsDB.Query;
using ScampsDB.ViewModels;
using ScampsDB.Extensions.Json;
using System.Globalization;
using ExtensionMethods;

namespace Scamps.Handlers
{
    public class jsSCAMPError
    {
        public bool error { get; set; }
        public string msg { get; set; }
        public string[] errlist { get; set; }
    }

    public class Ajax : Scamps.Site, IHttpHandler, IRequiresSessionState
    {
        //(if read and write is required), IReadOnlySessionState (if readonly access required)

        public bool IsReusable
        {
            get { return false; }
        }

        status currentstatus = status.OK;
        string output = "";

        System.Web.HttpContext current;
        public enum status : int
        {
            OK = 0,
            ERR = 1
        }

        public void ProcessRequest(HttpContext con)
        {
            init();
            string modal = "";
            // Stop Caching in IE
            con.Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            // Stop Caching in Firefox
            con.Response.Cache.SetNoStore();


            // data parameters
            int id = 0;
            int encounter_id = 0;
            long appt_id = 0;
            int scamp_id = 0;
            int encounter_type = 0;
            string scamp_cd = "";
            string scamp_name = "";
            string encounter_name = "";
            string mrn = "";           
            string har = "";
            string idcol = "id";
            string entity = "";
            string version = "";
            bool isnew = false;
            try
            {
                //note:inheritance is not working as expected.  Setting parameters  are lost
                //when a new object is invoked.  Alas, anything with query string parameters
                //should be called from the switch, below...
                current = con;
                string theFunk = Path.GetFileNameWithoutExtension(con.Request.Url.AbsolutePath);
                if ((theFunk.Length > 0))
                {
                    if (!con.Request.QueryString.ToString().IsNullOrEmpty())
                    {
                        Scamps.Tools.QSParse(ref Parameters, con.Request.QueryString.ToString());
                    }

                    //Note! theFunk will be toLowered!
                    string[] loggedOutWhiteList = { "auth", "isauthenticated", "loginmodal" };

                    if ((!con.User.Identity.IsAuthenticated || con.User.IsInRole("NO_ACCESS")) && !loggedOutWhiteList.Contains(theFunk.ToLower()))
                    {
                        con.Response.Write("{\"error\":true,\"errormessage\":\"You are not logged in.\",\"msg\":\"You are not logged in.\"}");
                        return;
                    }

                    switch(theFunk.ToLower())
                    {

                        case "modalform":

                            string form = Tools.GetValue("what", Parameters);
                            version = Tools.GetValue("v", Parameters);
                            entity = Tools.GetValue("e", Parameters);
                            // because some idiot made the boneheaded decision years ago that
                            // mrn was a GREAT primary key, we have to f with it here.
                            mrn = Tools.GetValue("pid", Parameters);
                            id = Tools.AsInteger(Tools.GetValue("id", Parameters).Trim());

                            Dictionary<string, string> data = new Dictionary<string, string>();

                            // if(form.ToLower() == "encounter") { idcol = "appt_id"; }

                            string infoblock = "";

                            if(!form.IsNullOrEmpty())
                            {
                                Form f = new Scamps.Form();

                                if (entity.IsNullOrEmpty())
                                {
                                    output = Tools.ReadFile(con.Server.MapPath("//resources//forms//" + form + ".form"));
                                    infoblock = Tools.XNode(ref output, "info");
                                    entity = Tools.XNode(ref infoblock, "dbentity");
                                    if (entity.IsNullOrEmpty()) { entity = Tools.XNode(ref infoblock, "entity"); }

                                    // we should have the form entity at this point.  
                                    // which is either passed or derived from the form
                                    // now we can fetch the data, if there is an id
                                    if (id > 0 || !mrn.IsNullOrEmpty())
                                    {
                                        DataTools dt = new DataTools();
                                        dt.Provider = this.Provider;
                                        dt.ConnectionString = this.ConnectionString;
                                        if (!output.IsNullOrEmpty())
                                        {
                                            string query = "";
                                            // TODO: exceptions to this rule?  There are a few in our current schema

                                            // deal with MRN stuff. Ugh
                                            if (!mrn.IsNullOrEmpty() && id == 0)
                                            {
                                                // leave entity dynamic as, hopefully, we will move to a new schema soon.
                                                query = "select p.*, p.mrn as testedmrn from " + entity + " p where mrn ='" + mrn + "'";
                                            }
                                            else
                                            {
                                                query = "select * from " + entity + " where " + idcol + " = " + id;
                                            }
                                            dt.OpenConnection();
                                            dt.GetResultSet(query);
                                            if (!dt.EOF)
                                            {
                                                data = dt.GetRowStrings();
                                            }
                                        }

                                        data.AddOrReplace("_recordid", data.getValueOrDefault("id"));
                                        f.FormData = data;

                                        dt.Dispose();
                                    }
                                }

                                f.Version = version;
                                f.BasePath = con.Server.MapPath("/");
                                f.Options.UseHTML5Markup = false;
                                f.Load(con.Server.MapPath("//resources//forms//" + form + ".form"));

                                output = f.Render();

                                modal = Tools.ReadFile(con.Server.MapPath("//resources//templates//" + modal + ".dbt"));

                            }
                            else
                            {
                                // no form specified, nothing to do, but we might want to mention it.
                            }

                            break;

                        // =======     E N R O L L M E N T   F O R M      ===============
                        case "enrollment":

                            version = Tools.GetValue("v", Parameters);

                            mrn = Tools.GetValue("pid", Parameters);

                            Dictionary<string, string> enroll_d = new Dictionary<string, string>();

                            enroll_d.Add("mrn", mrn);
                            enroll_d.Add("scamp_id", Parameters.Get("scamp"));
                            enroll_d.Add("currentstatus", Parameters.Get("currentstatus"));
                            enroll_d.Add("old_val", Parameters.Get("currentstatus"));

                            switch (enroll_d.Get("currentstatus"))
                            {
                                case "1":
                                    enroll_d.Add("status_val", "-2");  // exit from SCAMP default
                                    break;
                                default:
                                    enroll_d.Add("status_val", "1");  // enroll
                                    break;

                            }
                            Form erf = new Scamps.Form();
                            erf.Provider = this.Provider;
                            erf.ConnectionString = this.ConnectionString;
                            erf.Version = version;
                            erf.BasePath = con.Server.MapPath("/");
                            erf.Options.UseHTML5Markup = false;
                            erf.FormData = enroll_d;
                            erf.Load(con.Server.MapPath("//resources//forms//enrollment.form"));

                            output = erf.Render();

                            Results.AddOrReplace("modal_title", Parameters.Get("scampname") +  " - Patient Enrollment");

                            modal = Tools.ReadFile(con.Server.MapPath("//resources//templates//" + modal + ".dbt"));
                            
                            break;

                        // =======     E N C O U N T E R    M O D A L     ===============
                        case "encounterform":

                            string enc_sql = "";
                            string fversion = "";
                            Dictionary<string, string> edata = new Dictionary<string, string>();

                            DataTools edt = new DataTools();
                            edt.Provider = this.Provider;
                            edt.ConnectionString = this.ConnectionString;
                            edt.OpenConnection();

                            encounter_id = Tools.AsInteger(Tools.GetValue("id", Parameters).Trim());
                            appt_id = Tools.AsLong(Tools.GetValue("aid", Parameters).Trim());
                            if(encounter_id == 0 && appt_id == 0) { isnew = true;  }

                            if(isnew)
                            {
                                // ==============  new ENCOUNTER ==================
                                fversion = "add";
                                // need to know the patient
                                mrn = Tools.URLString(Tools.GetValue("mrn", Parameters)).ToUpper().Trim();

                                // and the SCAMP...
                                scamp_id = Tools.AsInteger(Tools.GetValue("scamp_id", Parameters).Trim());
                                scamp_cd = Tools.URLString(Tools.GetValue("scamp", Parameters)).ToUpper().Trim();

                                // or the form, even better! 
                                encounter_type = Tools.AsInteger(Tools.GetValue("formtype", Parameters).Trim());

                                if (encounter_type > 0)
                                {
                                    // resolve scamp and scamp_ID from encounter type
                                    // we might have scamp info passed to us, but with the encounter ID
                                    // we reset them based on that context...
                                    enc_sql = Tools.ReadBlock(Server.MapPath("//resources//data//scampsweb.sql"), "scamp.forms");
                                    enc_sql += Environment.NewLine + " where encounter_id =" + encounter_type;
                                    edt.GetResultSet(enc_sql);
                                    if (!edt.EOF)
                                    {
                                        // get the resulting data
                                        scamp_id = Tools.AsInteger(edt.Get("scamp_ID"));
                                        scamp_cd = edt.Get("scamp_cd");
                                    }
                                } else
                                {
                                    if(scamp_id == 0)
                                    {
                                        // resolve scamp ID from scamp CD
                                        scamp_id = Tools.AsInteger(edt.GetValue("select scamp_id from scamp where UPPER(scamp_cd) ='" + scamp_cd + "'"));
                                    } else
                                    {
                                        if(scamp_cd.IsNullOrEmpty())
                                        {
                                            // resolve scamp name from ID
                                            scamp_name = ScampName(ref edt, scamp_id);
                                        }
                                    }
                                }

                                enc_sql = "select * from pt_master where mrn = '" + mrn + "'";


                                edt.GetResultSet(enc_sql);
                                if (!edt.EOF)
                                {

                                    edata = edt.GetRowStrings();

                                    edata.AddEdit("mrn", mrn.ToString());
                                    edata.AddEdit("scamp_id", scamp_id.ToString());
                                    edata.AddEdit("scamp_cd", scamp_cd);
                                    edata.AddEdit("scamp_name", scamp_name);
                                    edata.AddEdit("original_scamp_name", scamp_name);
                                    edata.AddEdit("original_har", edata.Get("har"));
                                    edata.AddEdit("encounter_type", encounter_type.ToString());
                                    
                                    if (userRole().ToUpper() == "PROVIDER")
                                    {
                                        edata.AddEdit("scamps_prov_id", UserID());
                                    }
                                    else
                                    {
                                        edata.AddEdit("encounter_user", UserID());
                                    }

                                    //edata.AddEdit("clinic_no", "0");

                                    Results.AddEdit("modal_title", edata.getValueOrDefault("lname") + ", " + edata.getValueOrDefault("fname") + " - New Form Info");
                                }

                                // end NEW encounter
                            }
                            else

                            // ==================  existing ENCOUNTER  ===================
                            {
                                fversion = "edit";
                                int sub_id = Tools.AsInteger(Tools.GetValue("sid", Parameters).Trim());
                                enc_sql = Tools.ReadBlock(Server.MapPath("//resources//data//scampsweb.sql"), "search.encounters.base");

                                if (sub_id > 0)
                                {
                                    // this is an inpatient encounter.  Yes, yes, very stupid and
                                    // extremely time consuming.  Hopefully the person who came up
                                    // with this schema is suffering a horrible plague of fleas
                                    enc_sql += Environment.NewLine + "select * from enc where visit_type = 'I' and  sub_id = " + sub_id;
                                }
                                else
                                {
                                    if (encounter_id > 0)
                                    {
                                        // this is a normal reference.  Hurray!  Normal at last
                                        enc_sql += Environment.NewLine + "select * from enc where id = " + encounter_id;
                                    }
                                    else
                                    {
                                        if (appt_id > 0)
                                        {
                                            // this is a sub optimal method of getting this record.  One that we all wish would go
                                            // away.  Alas, forethought and planning was not in the original developer's quiver. 
                                            // we require an appointment ID
                                            enc_sql += Environment.NewLine + "select * from enc where sub_id is null and appt_id = " + appt_id;
                                        }
                                    }
                                }


                                edt.GetResultSet(enc_sql);
                                if (!edt.EOF)
                                {
                                    edata = edt.GetRowStrings();
                                    scamp_name = ScampName(ref edt, edata.Get("scamp_id").AsInteger());
                                    edata.AddOrReplace("original_scamp_name", scamp_name);
                                    // transpose data for the form.  Encounter data 
                                    // is challenging, as it comes from two tables and
                                    // different columns depending on the weather or
                                    // the user's shoe size.

                                    edata.AddEdit("original_har", edata.Get("har"));
                                    edata.AddEdit("set_encounter_type", edata.Get("encounter_type"));

                                    // set some base defaults 
                                    if (edata.Get("scamps_prov_id").IsNullOrEmpty())
                                    {
                                        edata.AddEdit("scamps_prov_id", "0");
                                    }
                                    if (edata.Get("encounter_user").IsNullOrEmpty())
                                    {
                                        edata.AddEdit("encounter_user", "0");
                                    }
                                    if (edata.Get("clinic_no").IsNullOrEmpty())
                                    {
                                        edata.AddEdit("clinic_no", "0");
                                    }

                                    // user role & set default for either provider or coordinator
                                    // depending on the role.  Only do this for NEW encounters?
                                    // provider lock?
                                    if (edata.Get("visit_type")=="I")
                                    {
                                        Results.AddOrReplace("modal_title", edata.getValueOrDefault("lastname") + ", " + edata.getValueOrDefault("firstname") + " - " + edata.Get("scamp_name").ToUpper() + ": Edit " + edata.Get("encounter_name") + " Inpatient Form");
                                    }
                                    else
                                    {
                                        Results.AddOrReplace("modal_title", edata.getValueOrDefault("lastname") + ", " + edata.getValueOrDefault("firstname") + " - " + edata.Get("scamp_name").ToUpper() + ": Edit " + edata.Get("encounter_name") + " Outpatient Form");
                                    }
                                }
                                
                                edata.AddOrReplace("_recordid", edata.getValueOrDefault("id"));

                                // end existing encounter
                            }


                            fversion = (fversion + " " + Tools.GetValue("v", Parameters)).Trim();

                            Form ef = new Scamps.Form();
                            ef.Provider = this.Provider;
                            ef.ConnectionString = this.ConnectionString;
                            ef.FormData = edata;

                            ef.Version = fversion;

                            //f.Version = version;
                            ef.BasePath = con.Server.MapPath("/");
                            ef.Options.UseHTML5Markup = false;
                            ef.Load(con.Server.MapPath("//resources//forms//encounter.form"));

                            output = ef.Render();

                            modal = Tools.ReadFile(con.Server.MapPath("//resources//templates//modal.dbt"));

                            edt.CloseConnection();
                            edt.Dispose();

                            break;
                        case "requestextract":
                            RequestZip(con);
                            return;
                        case "checkmrn":
                            output = checkmrn();
                            break;

                        case "linkedencounterform":
                            output = linkedencounterform();
                            break;
                        case "encounterlist":
                            output = encounterlist();
                            break;
                        case "recentlyviewedpatients":
                            var strlist = Tools.GetValue("pats", Parameters);

                            if (strlist.IsNullOrEmpty())
                                break;
                            //for each item delimited by ',':
                            //  filter empty strings and strings that are 20 characters or under (database constraint), and does not contain SQL string escape character "'".
                            //  concatenate each valid string, wrap in single quotes and delimit by comma
                            //  cast to uppercase
                            strlist = strlist.Split(',')
                            .Where(st => !st.IsNullOrEmpty() && st.Length <= 20 && st.All(ch => ch != '\'') )
                            .Aggregate((a, b) => a + "\',\'" + b).ToUpper();
                            output = strlist.IsNullOrEmpty() ? output : recentlyviewedpatients(string.Concat('\'',strlist,'\''));
                            break;
                        case "recentlyviewedencounters":
                            strlist = Tools.GetValue("appts", Parameters);
                            
                            if (strlist.IsNullOrEmpty())
                                break;
                            //for each item delimited by ',':
                            //  filter strings that only contain numeric characters
                            //  concatenate each valid string and delimit by comma separated value
                            strlist = strlist.Split(',')
                            .Where(st => st.All(ch => char.IsNumber(ch)))
                            .Aggregate((a, b) => a + ',' + b);

                            output = strlist.IsNullOrEmpty() ? output : recentlyviewedencounters(strlist);
                            break;
                        case "search":
                            sSearchHandler.ProcessRequest(con);
                            return;
                        case "auth":
                            authProcess.ProcessRequest(con);
                            return;
                        case "datadictionary":
                            DictionaryProcess.HandleDictionaryQuery(con);
                            return;
                        case "patient":
                            PatientProcess.HandlePatientData(con);
                            return;
                        case "encounter":
                            HandleEncounterData(con);
                            return;
                        case "getPivotData":
                            var idval = con.Request.Form.Get("formid");
                            MeasurementPivotProcess.getFormData(con, "encounter_id", idval);
                            return;
                        case "encounterstatus":
                            
                            var template = "[\"[status_text]\", [c]],";
                            var sql = Tools.ReadBlock(con.Server.MapPath("/resources/data/scampsweb.sql"), "dashboard.encounter.chart");
                            var t = new Templating();
                            t.Provider = this.Provider;
                            t.ConnectionString = this.ConnectionString;
                            var results = t.Results(sql, template);
                            if (results.EndsWith(","))
                                results = results.Substring(0, results.Length - 1);
                            output = "[" + results + "]";
                            break;
                        case "externaldata":
                            RequestExternalData(con);
                            break;
                        default:
                            Type type = typeof(Scamps.Site);
                            MethodInfo info = type.GetMethod(theFunk);
                            object instance = Activator.CreateInstance(type, null);
                            output = Scamps.Tools.AsText(info.Invoke(instance, new object[] { }));
                            break;
                    }

                }
                else
                {
                    currentstatus = Ajax.status.ERR;
                }

            }
            catch (Exception ex)
            {
                currentstatus = Ajax.status.ERR;
            }
            finally
            {
                if (output.Length > 0)
                {
                    Results.Add("status", Enum.GetName(typeof(status), currentstatus));
                    Results.Add("markup", output);
                    Results.Add("alert", output);
                    if (!modal.IsNullOrEmpty()) { Results.Add("modal", modal); }
                    con.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(Results));
                }
            }
        }

        public string ScampName(ref Scamps.DataTools db, int scamp_id)
        {
            string results = "";
            string sql = "select scamp_name from scamp where scamp_id = " + scamp_id;
            results = Tools.AsText(db.GetValue(sql));
            return results;
        }
        public static void RequestExternalData(HttpContext con)
        {
            try
            {
                string mrn = con.Request.QueryString["mrn"];
                string appt_id = con.Request.QueryString["apptid"];
                string har = con.Request.QueryString["har"];

                string form = con.Request.QueryString["form"];

                string formmappedpath = con.Request.MapPath(ExternalData.GetExternalFormRelativePath(form));

                var data = ExternalData.ImportMeasurements(mrn, appt_id, har, formmappedpath);
                con.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(data.ToArray()));
            }
            catch (Exception ex)
            {
                var jsWrite = new JsonTextWriter(con.Response.Output);
                jsWrite.WrtStartObject().WrtPropertyValue("error", true).WrtPropertyValue("errormessage", ex.Message).WrtEndObject();
            }
            
        }

        public static void RequestZip(HttpContext con)
        {
            var jsWrite = new JsonTextWriter(con.Response.Output);
            jsWrite.WrtStartObject();

            try
            {
                var req = con.Request.Params["req"];

                var extractpath = System.Configuration.ConfigurationManager.AppSettings["ExtractPath"];

                if (string.IsNullOrWhiteSpace(extractpath))
                    throw new Exception("Extracts are not configured. An ExtractPath needs to be added to this site for Extracts to be available.");

                var mappedextractpath = con.Request.MapPath(extractpath);

                bool error = false;

                switch (req)
                {
                    case "1":
                        if (Scamps.Extract.ExtractManager.IsExtracting())
                        {
                            error = true;
                            jsWrite.WrtPropertyValue("errormessage", "Extract already started");
                        }
                        else
                        {
                            if (!Directory.Exists(mappedextractpath))
                                Directory.CreateDirectory(mappedextractpath);

                            var t = ScampsDB.Models.SCAMPstore.Get();
                            var siteCode = t.ExecuteStoreQuery<string>("SELECT ATTRIB_VAL FROM ADMIN_CONFIG WHERE ATTRIB = 'SITENAME'").FirstOrDefault();

                            var filename = string.Format("{0,0:yyyy-MM-dd_HH-mm}.extract", DateTime.Now);

                            if (!String.IsNullOrWhiteSpace(siteCode))
                            {
                                filename = siteCode.Replace(' ', '_').ToUpper() + "-" + filename;
                            }
                            
                            Scamps.Extract.ExtractManager.BeginExtract(mappedextractpath, con.Request.PhysicalApplicationPath, filename);

                            //The extract takes a few cycles to start up, to avoid returning "No Extract Running" we're just going to hard-code that we're extracting and bail.
                            jsWrite.WrtPropertyValue("error", error).WrtPropertyValue("statemessage", "Extracting").WrtPropertyValue("statecode", 1).WrtEndObject();
                            return;
                        }
                        break;
                    case "2":
                        //get list of extract files 
                        
                        IEnumerable<string> extdir = null;

                        try
                        {
                            extdir = Directory.EnumerateFiles(mappedextractpath, "*.extract");
                        }
                        catch (IOException) //Can throw if the extract folder doesn't exist yet (it gets created the first time you make an extract.)
                        {
                            extdir = new string[] { };
                        }
                        catch (UnauthorizedAccessException) //Will be thrown if we don't have rights to the folder. Should get logged once that is available.
                        {
                            extdir = new string[] { };
                            error = true;
                            jsWrite.WrtPropertyValue("errormessage", "The path exists, but we do not have permission to view it.");
                        }
                         
                        jsWrite.WrtPropertyName("files").WrtStartArray();
                        foreach (var file in extdir)
                        {
                            var filename = Path.GetFileName(file);
                            if (Scamps.Extract.ExtractManager.getExtractStateCode() != 1 || !filename.Equals(Scamps.Extract.ExtractManager.getExtractFilename()))
                                jsWrite.WrtStartObject().WrtPropertyValue("filename", Path.Combine(extractpath, filename).Replace('\\', '/')).WrtEndObject();
                        }
                        jsWrite.WrtEndArray();

                        break;
                    case "3":
                        if (Scamps.Extract.ExtractManager.IsPrepared())
                        {
                            jsWrite.WrtPropertyValue("extractedfilename", Path.Combine(extractpath, Scamps.Extract.ExtractManager.getExtractFilename()).Replace('\\', '/'));
                        }
                        break;
                    case "4":

                        //for extract files specified in the querystring parameter files[]
                        var files = con.Request.QueryString.GetValues("files[]")
                            //filter out values which are either empty or do not have the expected minimum specified length if it contains the extract path
                            .Where(fn => !string.IsNullOrEmpty(fn) && fn.Length > extractpath.Length)
                            //transform the values to exclude the extract path
                            .Select(fn => fn.Substring(extractpath.Length));

                        //whitelist the incoming file names against the extract archives in the extract directory
                        var extlist = Directory.GetFiles(mappedextractpath, "*.extract")
                            .Where(fn => files.Contains(fn.Substring(mappedextractpath.Length)));

                        //delete the requested files that passed the whitelist
                        foreach (var delfile in extlist)
                            File.Delete(delfile);

                        break;
                    default:
                        error = true;
                        jsWrite.WrtPropertyValue("errormessage", "Invalid request");
                        break;
                }

                if (Scamps.Extract.ExtractManager.getExtractError() != null)
                {
                    error = true;
                    jsWrite.WrtPropertyValue("errormessage", Scamps.Extract.ExtractManager.getExtractError().Message);
                }

                //Just always write the current state and if we had an error for each request.
                jsWrite.WrtPropertyValue("error", error).WrtPropertyValue("statemessage", Scamps.Extract.ExtractManager.getExtractState()).WrtPropertyValue("statecode", Scamps.Extract.ExtractManager.getExtractStateCode()).WrtEndObject();
            }
            catch (Exception ex)
            {
                jsWrite.WrtPropertyValue("error", true).WrtPropertyValue("errormessage", ex.Message).WrtEndObject();
            }
            
        }
        /*********************************/
#region GETrequests
        public static void getPatientSCAMPEnrollmentsNew(HttpContext _context) 
        {
            var jsWrite = new JsonTextWriter(_context.Response.Output);
            var mrn = _context.Request.QueryString["mrn"].ToUpper();

            if (string.IsNullOrEmpty(mrn))
            {
                _context.Response.StatusCode = 400;
                jsWrite.WrtStartObject()
                    .WrtPropertyValue("error", true).WrtPropertyValue("msg", "no patient MRN specified")
                .WrtEndObject();
                //_context.Response.Write("{\"error\":true,\"msg\":\"no patient MRN specified\"}");
                return;
            }

            try
            {
                var keyvalColl = ScampsDB.Query.Scamps.getPatientSCAMPenrollment(mrn);
                if (keyvalColl == null)
                {
                    _context.Response.StatusCode = 404;
                    return;
                }
                _context.Response.StatusCode = 200;
                keyvalColl.StreamListToJSON(_context.Response.Output);
                //_context.Response.Write(ScampsDB.ViewModels.KeyValueModel.ListToJSON(keyvalColl));
            }
            catch (Exception e)
            {
                _context.Response.StatusCode = 400;
                if (e.GetType() == typeof(SCAMPException))
                {
                    jsWrite.WrtStartObject()
                    .WrtPropertyValue("error", true).WrtPropertyValue("msg", e.Message)
                .WrtEndObject();
                    //_context.Response.rspWrite("{\"error\":true,\"msg\":\"").rspWrite(e.Message).rspWrite("\"}");
                }
                else
                    jsWrite.WrtStartObject()
                    .WrtPropertyValue("error", true).WrtPropertyValue("msg", "Unable retrieve patient's SCAMP enrollments")
                .WrtEndObject();
                //_context.Response.Write("{\"error\":true,\"msg\":\"Unable retrieve patient's SCAMP enrollments\"}");
            }
        }

        private static void getEncounter(HttpContext _context)
        {
            var apptid = _context.Request.QueryString["appt_id"];

            var enc = Encounters.getEncounter(apptid);

            if (enc == null)
            {
                _context.Response.StatusCode = 404;
                return;
            }

            _context.Response.StatusCode = 200;
            _context.Response.Write(enc.ToJSON());
        }

        private static void getEncounterSubUnit(HttpContext _context)
        {
            var subid = _context.Request.QueryString["sub_id"];

            var enc = Encounters.getSubUnitEncounter(subid);

            if (enc == null)
            {
                _context.Response.StatusCode = 404;
                return;
            }
            if (enc.appt_id == 0)
            {
                long tmp = 0;
                long.TryParse(_context.Request.QueryString["appt_id"], out tmp);
                enc.appt_id = tmp;
            }

            _context.Response.StatusCode = 200;
            _context.Response.Write(enc.ToJSON());
        }

        private static void getEncounterSubUnits(HttpContext _context)
        {
            var apptid = _context.Request.QueryString["appt_id"];
            if (string.IsNullOrEmpty(apptid))
            {
                _context.Response.StatusCode = 400;
                return;
            }
            var enlist = Encounters.getSubUnitEncounters(apptid);
            if (enlist.Any())
            {
                _context.Response.Write("{\"data\":");
                enlist.StreamListToJSON(_context.Response.Output);
                _context.Response.Write('}');
            }
            else
            {
                _context.Response.Write("{\"data\":[]}");
            }

        }
        private static void getPatientEncounters(HttpContext _context)
        {
            var mrn = _context.Request.QueryString["mrn"];
            if (string.IsNullOrEmpty(mrn))
            {
                _context.Response.StatusCode = 400;
                return;
            }

            var enlist = Encounters.getPatientEncounters(mrn);
            _context.Response.StatusCode = 200;
            if (enlist.Any())
            {
                _context.Response.Write("{\"data\":");
                enlist.StreamListToJSON(_context.Response.Output);
                _context.Response.Write('}');
            }
            else
            {
                _context.Response.Write("{\"data\":[]}");
            }
        }
        private static void getLinkedEncounters(HttpContext _context)
        {
            var apptid = _context.Request.QueryString["appt_id"];
            if (string.IsNullOrEmpty(apptid))
            {
                _context.Response.StatusCode = 400;
                return;
            }

            var enlist = Encounters.getLinkedEncounters(apptid);
            _context.Response.StatusCode = 200;
            if (enlist.Any())
            {
                _context.Response.Write("{\"data\":");
                enlist.StreamListToJSON(_context.Response.Output);
                _context.Response.Write('}');
            }
            else
            {
                _context.Response.Write("{\"data\":[]}");
            }
        }
        #endregion

        #region POSTrequests
        private static void insertSubUnitEncounter(HttpContext _context)
        {
            var errList = new List<string>();
            var en = populateEncounter(_context, new string[] { "scamp_id", "encounter_type" }, out errList);

            var jsWrite = new JsonTextWriter(_context.Response.Output);

            if (errList.Any())
            {
                _context.Response.StatusCode = 400;
                jsWrite.WrtStartObject().WrtPropertyValue("error", true);

                System.Text.StringBuilder sbErr = new System.Text.StringBuilder("Error when processing encounter data");
                foreach (var err in errList)
                {
                    sbErr.Append("; ");
                    sbErr.Append(err);
                }

                jsWrite.WrtPropertyValue("msg", sbErr.ToString()).WrtEndObject();
                return;
            }

            var enrolled = ScampsDB.Query.Scamps.isPatientEnrolledInSCAMP(en.mrn, en.scamp_id.Value);

            if (!enrolled)
            {
                ScampsDB.Query.Scamps.patientEnrollment(_context.User.Identity.Name, en.mrn, en.scamp_id.Value, 1, "Enrolled by adding an encounter.");
            }

            string sub_id = string.Empty;
            try
            {
                Encounters.createSubUnitEncounter(en, out sub_id);
                jsWrite.WrtStartObject()
                    .WrtPropertyValue("error", false).WrtPropertyValue("msg", "subunit encounter inserted successfully")
                    .WrtPropertyValue("sub_id", sub_id)
                    .WrtPropertyValue("enrolledbyencounter", !enrolled)
                .WrtEndObject();
            }
            catch (Exception e)
            {
                _context.Response.StatusCode = 400;
                if (e.GetType() == typeof(SCAMPException))
                {
                    jsWrite.WrtStartObject()
                        .WrtPropertyValue("error", true).WrtPropertyValue("msg", e.Message)
                    .WrtEndObject();
                }
                else
                    jsWrite.WrtStartObject()
                        .WrtPropertyValue("error", true).WrtPropertyValue("msg", "Unable to insert encounter record")
                    .WrtEndObject();
            }
        }


        /// <summary>
        /// Returns an EncounterViewModel, populating an error list List<string> of validation issues with the model.
        /// </summary>
        /// <returns>Returns an EncounterViewModel if one could be constructed, otherwise null/Nothing. It may return an incomplete EncounterViewModel, be sure to check the errList for fields that could not be retrieved.</returns>
        private static EncounterViewModel populateEncounter(HttpContext _context, IEnumerable<string> requiredFields, out List<string> errList)
        {
            var en = new ScampsDB.ViewModels.EncounterViewModel();
            errList = new List<string>();

            try
            {
                DateTime dtmp;
                long ltmp;
                int itmp;
                short stmp;
                var _form = _context.Request.Form;

                var cstyle = CultureInfo.CreateSpecificCulture("en-US"); //TODO: This is tremendously suspect.

                //TODO: Verify this:
                if (long.TryParse(_form["appt_id"], out ltmp))
                    en.appt_id = ltmp;
                else if (requiredFields.Contains("appt_id"))
                    errList.Add("invalid appointment specified");

                if (int.TryParse(_form["sub_id"], out itmp))
                    en.sub_id = itmp;
                else if (requiredFields.Contains("sub_id"))
                    errList.Add("Invalid subunit encounter identifier");

                if (!_form["encounter_cd"].IsNullOrEmpty())
                    en.encounter_cd = _form["encounter_cd"];
                else if (requiredFields.Contains("encounter_cd"))
                    errList.Add("missing visit type");

                if (int.TryParse(_form["scamp_id"], out itmp))
                    en.scamp_id = itmp;
                else if (requiredFields.Contains("scamp_id"))
                    errList.Add("invalid scamp id");

                if (int.TryParse(_form["scamps_prov_id"], out itmp))
                    en.scamps_prov_id = itmp;
                else if (requiredFields.Contains("scamps_prov_id"))
                    errList.Add("missing provider");

                if (int.TryParse(_form["encounter_type"], out itmp))
                    en.encounter_type = itmp;
                else if (requiredFields.Contains("encounter_type"))
                    errList.Add("invalid encounter type specified");

                if (!string.IsNullOrWhiteSpace(_context.Request.Form["loctn_abbr"]))
                    en.loctn_abbr = _context.Request.Form["loctn_abbr"];
                else if (requiredFields.Contains("loctn_abbr"))
                    errList.Add("missing location");

                if (!string.IsNullOrWhiteSpace(_context.Request.Form["har"]))
                    en.har = _context.Request.Form["har"];
                else if (requiredFields.Contains("har"))
                    errList.Add("missing har");

                //required or critical data
                if (DateTime.TryParse(_form["encounter_date"], cstyle, DateTimeStyles.None, out dtmp))
                    en.encounter_date = dtmp;
                else
                    errList.Add("invalid encounter date");

                if (!_form["mrn"].IsNullOrEmpty())
                    en.mrn = _form["mrn"];
                else
                    errList.Add("could not read patient MRN");


                //if current user is within any of the roles below, add them as responsible user
                var currUserRoles = System.Web.Security.Roles.GetRolesForUser();
                var responsible_roles = new string[] { "dc" };
                if (currUserRoles.Any(ur => responsible_roles.Contains(ur.ToLower())))
                {
                    //get scamps_prov_id for current user
                    var profKeys = new string[] { "scamps_prov_id" };
                    var userInfo = ScampsDB.Provider.ProfileMethods.getProfile("user_name", HttpContext.Current.User.Identity.Name, profKeys);
                    if (userInfo.ContainsKey("scamps_prov_id") && int.TryParse(userInfo["scamps_prov_id"], out itmp))
                        en.encounter_user = itmp;
                }


                //optional
                //if (!string.IsNullOrWhiteSpace(_context.Request.Form["har"]))
                //en.har = _context.Request.Form["har"];
                if (DateTime.TryParse(_form["encounter_email"], cstyle, DateTimeStyles.None, out dtmp))
                    en.encounter_email = dtmp;
                if (DateTime.TryParse(_form["encounter_prereview"], cstyle, DateTimeStyles.None, out dtmp))
                    en.encounter_prereview = dtmp;
                if (DateTime.TryParse(_form["encounter_postreview"], cstyle, DateTimeStyles.None, out dtmp))
                    en.encounter_postreview = dtmp;
                if (DateTime.TryParse(_form["inpt_disch"], cstyle, DateTimeStyles.None, out dtmp))
                    en.inpt_disch = dtmp;


                if (int.TryParse(_form["encounter_user"], out itmp))
                    en.encounter_user = itmp;

                en.appt_notes = _form["appt_notes"];
                en.encounter_cd = _form["encounter_cd"];
                en.encounter_notes = _form["encounter_notes"];

                if (!_form["prov_lock_status"].IsNullOrEmpty() && _form["prov_lock_status"].Equals("-1"))
                    en.prov_lock_status = -1;

                if (short.TryParse(_form["encounter_status"], out stmp))
                    en.encounter_status = stmp;
                else
                    en.encounter_status = 1;
            }
            catch (Exception e)
            {
                errList.Add("Server error when processing encounter data");
                return null;
            }

            return en;
        }

        public static EncounterViewModel partialPopulateEncounter(IDictionary<string, string> values, out List<string> errList)
        {
            //can't use errList directly because you can't access out vars in lambdas/delegates/actions
            var errorList = new List<string>();

            string appt_id = values.getValueOrDefault("appt_id");
            string sub_id = values.getValueOrDefault("sub_id");

            if (String.IsNullOrWhiteSpace(appt_id))
            {
                //If we don't have an apptid there's nothing to do.

                errorList.Add("Missing appt_id.");
                errList = errorList;
                return null;
            }

            EncounterViewModel en = null;

            if (String.IsNullOrWhiteSpace(sub_id))
                en = Encounters.getEncounter(appt_id);
            else
                en = Encounters.getSubUnitEncounter(sub_id);

            if (en == null)
            {
                errorList.Add("No matching appointment found.");
                errList = errorList;
                return null;
            }

            Exports.TryDelegate(() => { en.appt_notes = values.getValueOrOriginal<string>("appt_notes", en.appt_notes); }, (e) => { errorList.Add("appt_notes appears to be in an incorrect format."); });
            Exports.TryDelegate(() => { en.encounter_date = values.getValueOrOriginal<DateTime?>("encounter_date", en.encounter_date); }, (e) => { errorList.Add("Encounter date does not appear to be a valid date."); });
            Exports.TryDelegate(() => { en.encounter_email = values.getValueOrOriginal<DateTime?>("encounter_email", en.encounter_email); }, (e) => { errorList.Add("encounter_email appears to be in an incorrect format."); });
            Exports.TryDelegate(() => { en.encounter_notes = values.getValueOrOriginal<string>("encounter_notes", en.encounter_notes); }, (e) => { errorList.Add("encounter_notes appears to be in an incorrect format."); });
            Exports.TryDelegate(() => { en.encounter_postreview = values.getValueOrOriginal<DateTime?>("encounter_postreview", en.encounter_postreview); }, (e) => { errorList.Add("encounter_postreview appears to be in an incorrect format."); });
            Exports.TryDelegate(() => { en.encounter_prereview = values.getValueOrOriginal<DateTime?>("encounter_prereview", en.encounter_prereview); }, (e) => { errorList.Add("encounter_prereview appears to be in an incorrect format."); });
            Exports.TryDelegate(() => { en.encounter_status = values.getValueOrOriginal<short?>("encounter_status", en.encounter_status); }, (e) => { errorList.Add("encounter_status appears to be in an incorrect format."); });
            Exports.TryDelegate(() => { en.encounter_user = values.getValueOrOriginal<int?>("encounter_user", en.encounter_user); }, (e) => { errorList.Add("encounter_user appears to be in an incorrect format."); });
            Exports.TryDelegate(() => { en.har = values.getValueOrOriginal<string>("har", en.har); }, (e) => { errorList.Add("har appears to be in an incorrect format."); });
            Exports.TryDelegate(() => { en.inpt_disch = values.getValueOrOriginal<DateTime?>("inpt_disch", en.inpt_disch); }, (e) => { errorList.Add("inpt_disch appears to be in an incorrect format."); });
            Exports.TryDelegate(() => { en.loctn_abbr = values.getValueOrOriginal<string>("loctn_abbr", en.loctn_abbr); }, (e) => { errorList.Add("loctn_abbr appears to be in an incorrect format."); });
            //Exports.TryDelegate(() => { en.mrn = values.getValueOrOriginal<string>("mrn", en.mrn);  }, (e) => { errorList.Add("Failed to parse mrn"); }); // Nope?
            Exports.TryDelegate(() => { en.prov_lock_status = values.getValueOrOriginal<short?>("prov_lock_status", en.prov_lock_status); }, (e) => { errorList.Add("prov_lock_status appears to be in an incorrect format."); });
            Exports.TryDelegate(() => { en.scamps_prov_id = values.getValueOrOriginal<int?>("scamps_prov_id", en.scamps_prov_id); }, (e) => { errorList.Add("scamps_prov_id appears to be in an incorrect format."); });

            //HAR can't be null or whitespace
            if (string.IsNullOrWhiteSpace(en.har))
                errorList.Add("har can not be empty");
            //nope
            //en.encounter_cd
            //en.encounter_type 
            //en.encounter_type
            //en.scamp_id
            //en.sub_id
            errList = errorList;
            return en;
        }

        private static void insertEncounter(HttpContext _context)
        {
            var errList = new List<string>();
            var encounterRequired = new string[] { "encounter_cd", "scamp_id", "encounter_type" };
            var en = populateEncounter(_context, encounterRequired, out errList);

            var jsWrite = new JsonTextWriter(_context.Response.Output);

            if (errList.Any())
            {
                _context.Response.StatusCode = 400;
                jsWrite.WrtStartObject().WrtPropertyValue("error", true);
                var sbErr = string.Concat("Error when processing encounter data; ", errList.Aggregate((a, b) => a + "; " + b));
                jsWrite.WrtPropertyValue("msg", sbErr).WrtEndObject();
                return;
            }

            var enrolled = ScampsDB.Query.Scamps.isPatientEnrolledInSCAMP(en.mrn, en.scamp_id.Value);

            if (!enrolled)
            {
                ScampsDB.Query.Scamps.patientEnrollment(_context.User.Identity.Name, en.mrn, en.scamp_id.Value, 1, "Enrolled by adding an encounter.");
            }

            string appt_id = string.Empty;

            try
            {

                Encounters.createEncounter(en, out appt_id);

                //check if appt_id was generated
                if (string.IsNullOrEmpty(appt_id))
                {
                    jsWrite.WrtStartObject()
                        .WrtPropertyValue("error", true).WrtPropertyValue("msg", "encounter inserted but unable to retrieve it's information")
                    .WrtEndObject();
                    //_context.Response.Write("{\"error\":true,\"msg\":\"encounter inserted but unable to retrieve it's information\"}");    
                }
                else
                {
                    jsWrite.WrtStartObject()
                        .WrtPropertyValue("error", false).WrtPropertyValue("msg", "encounter added").WrtPropertyValue("appt_id", appt_id).WrtPropertyValue("enrolledbyencounter", !enrolled)
                    .WrtEndObject();
                }
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error creating encounter in database: ", e.ToString());
                _context.Response.StatusCode = 400;
                if (e.GetType() == typeof(SCAMPException))
                {
                    jsWrite.WrtStartObject()
                        .WrtPropertyValue("error", true).WrtPropertyValue("msg", e.Message)
                    .WrtEndObject();
                }
                else
                    jsWrite.WrtStartObject()
                        .WrtPropertyValue("error", true).WrtPropertyValue("msg", "Unable to insert encounter record")
                    .WrtEndObject();
            }
        }
        /*
                      	                    editing sub
            encounter date/time	
            sched notes	
            provider	
            provider lock	
            location	
            encounter status	
            encoutner_type (can't edit)	
            email sent on	
            dc entered on	
            rn post-reviewd on	
            responsible person	
            encounter notse	
	
	                                        editing root
            admit date/time	
            discharge date/time	
            location	
            notes	

         */
        private static void updateSubUnitEncounter(HttpContext _context)
        {
            var errList = new List<string>();
            //var en = populateEncounter(_context, new string[] { "appt_id", "sub_id", "scamp_id", "encounter_type"}, out errList);
            var en = populateEncounter(_context, new string[] { "appt_id", "sub_id" }, out errList);

            var jsWrite = new JsonTextWriter(_context.Response.Output);


            if (errList.Any())
            {
                _context.Response.StatusCode = 400;
                jsWrite.WrtStartObject().WrtPropertyValue("error", true);

                System.Text.StringBuilder sbErr = new System.Text.StringBuilder("Error when processing encounter data");
                foreach (var err in errList)
                {
                    sbErr.Append("; ");
                    sbErr.Append(err);
                }

                jsWrite.WrtPropertyValue("msg", sbErr.ToString()).WrtEndObject();
                return;
            }

            //after processing form data, update database
            try
            {
                Encounters.updateSubUnitEncounter(en);
                jsWrite.WrtStartObject()
                    .WrtPropertyValue("error", false).WrtPropertyValue("msg", "Subunit Encounter updated").WrtPropertyValue("appt_id", en.appt_id)
                .WrtEndObject();
                //_context.Response.Write("{\"error\":false,\"msg\":\"Subunit Encounter updated\"}");
            }
            catch (Exception e)
            {
                _context.Response.StatusCode = 400;
                Console.Error.WriteLine("error updating subunit encounter in database: {0}", e.ToString());
                if (e.GetType() == typeof(SCAMPException))
                {
                    jsWrite.WrtStartObject()
                        .WrtPropertyValue("error", true).WrtPropertyValue("msg", e.Message)
                    .WrtEndObject();
                    //_context.Response.Write("{\"error\":true,\"msg\":\"");
                    //_context.Response.Write(e.Message);
                    //_context.Response.Write("\"}");
                }
                else
                    jsWrite.WrtStartObject()
                        .WrtPropertyValue("error", true).WrtPropertyValue("msg", "Unable to update subunit encounter record")
                    .WrtEndObject();
                //_context.Response.Write("{\"error\":true,\"msg\":\"Unable to update subunit encounter record\"}");
            }
        }

        private static void updateEncounter(HttpContext _context)
        {
            var errList = new List<string>();

            EncounterViewModel en = null;

            //If this is the parent of sub-encounters we have different required fields.
            if (ScampsDB.Query.Encounters.getSubUnitEncounters(_context.Request.Form["appt_id"]).Where((encounter, counts) => { return encounter.sub_id != null; }).Count() > 0)
            {
                en = populateEncounter(_context, new string[] { "appt_id", "har" }, out errList);
            }
            else
            {
                en = populateEncounter(_context, new string[] { "appt_id", "har" }, out errList);
            }

            var jsWrite = new JsonTextWriter(_context.Response.Output);

            if (errList.Any())
            {
                _context.Response.StatusCode = 400;
                jsWrite.WrtStartObject().WrtPropertyValue("error", true);

                System.Text.StringBuilder sbErr = new System.Text.StringBuilder("Error when processing encounter data");
                foreach (var err in errList)
                {
                    sbErr.Append("; ");
                    sbErr.Append(err);
                }

                jsWrite.WrtPropertyValue("msg", sbErr.ToString()).WrtEndObject();
                return;
            }

            try
            {
                Encounters.updateEncounter(en);
                jsWrite.WrtStartObject()
                    .WrtPropertyValue("error", false).WrtPropertyValue("msg", "Encounter updated").WrtPropertyValue("appt_id", en.appt_id)
                .WrtEndObject();
            }
            catch (Exception e)
            {
                _context.Response.StatusCode = 400;
                Console.Error.WriteLine("error updating encounter in database: {0}", e.ToString());

                if (e.GetType() == typeof(SCAMPException))
                {
                    jsWrite.WrtStartObject()
                        .WrtPropertyValue("error", true).WrtPropertyValue("msg", e.Message)
                    .WrtEndObject();
                }
                else
                    jsWrite.WrtStartObject()
                        .WrtPropertyValue("error", true).WrtPropertyValue("msg", "Unable to update encounter record")
                    .WrtEndObject();
            }
        }

        private static void partialUpdateEncounter(HttpContext _context)
        {

            var errList = new List<string>();

            var requestDict = _context.Request.Form.ToDictionary();

            var en = partialPopulateEncounter(requestDict, out errList);

            var jsWrite = new JsonTextWriter(_context.Response.Output);

            if (errList.Any())
            {
                _context.Response.StatusCode = 400;
                jsWrite.WrtStartObject().WrtPropertyValue("error", true);

                System.Text.StringBuilder sbErr = new System.Text.StringBuilder("Error when processing encounter data");
                foreach (var err in errList)
                {
                    sbErr.Append("; ");
                    sbErr.Append(err);
                }

                jsWrite.WrtPropertyValue("msg", sbErr.ToString()).WrtEndObject();
                return;
            }

            try
            {
                if (en.encounter_cd.ToUpper() == "I")
                    Encounters.updateSubUnitEncounter(en);
                else
                    Encounters.updateEncounter(en);

                var writer = jsWrite.WrtStartObject()
                    .WrtPropertyValue("error", false).WrtPropertyValue("msg", "Encounter updated").WrtPropertyValue("appt_id", en.appt_id);

                //Write back the current value of the object after saving to the client. 
                var enType = en.GetType();
                foreach (var key in requestDict.Keys)
                {
                    if (key != "appt_id" && key != "sub_id")
                    {
                        try
                        {
                            var prop = enType.GetProperty(key);
                            if (prop != null)
                                writer.WrtPropertyValue(key, prop.GetValue(en, null).ToString());
                        }
                        catch (Exception)
                        { }
                    }
                }

                writer.WrtEndObject();
            }
            catch (Exception e)
            {
                _context.Response.StatusCode = 400;
                Console.Error.WriteLine("error updating encounter in database: {0}", e.ToString());

                if (e.GetType() == typeof(SCAMPException))
                {
                    jsWrite.WrtStartObject()
                        .WrtPropertyValue("error", true).WrtPropertyValue("msg", e.Message)
                    .WrtEndObject();
                }
                else
                    jsWrite.WrtStartObject()
                        .WrtPropertyValue("error", true).WrtPropertyValue("msg", "Unable to update encounter record")
                    .WrtEndObject();
            }
        }

        private static void enrollPatientSCAMP(HttpContext _context)
        {

            string currUser, mrn, scampName, status, comment;

            var jsWrite = new JsonTextWriter(_context.Response.Output);

            try
            {
                status = _context.Request.Form["stat"];
                currUser = _context.User.Identity.Name;
                mrn = _context.Request.Form["mrn"];
                mrn = mrn.ToUpper();
                scampName = _context.Request.Form["sn"];
                comment = _context.Request.Form["c"];
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("error enrolling patient into SCAMP: {0}", e.ToString());
                _context.Response.StatusCode = 500;
                jsWrite.WrtStartObject()
                    .WrtPropertyValue("error", true).WrtPropertyValue("msg", "Server error when processing data for SCAMP enrollment")
                .WrtEndObject();
                //_context.Response.Write("{\"error\":true,\"msg\":\"Server error when processing data for SCAMP enrollment\"}");
                return;
            }

            //if status is empty, withdrawl enrollment
            if (string.IsNullOrEmpty(status))
            {
                try
                {
                    ScampsDB.Query.Scamps.withdrawPatient(mrn, scampName, _context.User.Identity.Name, comment);
                    jsWrite.WrtStartObject()
                        .WrtPropertyValue("error", false).WrtPropertyValue("msg", "patient is now withdrawn from SCAMP")
                    .WrtEndObject();
                    //_context.Response.Write("{\"error\":false,\"msg\":\"patient is now withdrawn from SCAMP\"}");
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine("error removing enrollment of patient from SCAMP: ", e.ToString());

                    _context.Response.StatusCode = 400;
                    if (e.GetType() == typeof(SCAMPException))
                    {
                        jsWrite.WrtStartObject()
                            .WrtPropertyValue("error", true).WrtPropertyValue("msg", e.Message)
                        .WrtEndObject();
                        //_context.Response.Write("{\"error\":true,\"msg\":\"");
                        //_context.Response.Write(e.Message);
                        //_context.Response.Write("\"}");
                    }
                    else
                        jsWrite.WrtStartObject()
                            .WrtPropertyValue("error", true).WrtPropertyValue("msg", "could not enroll patient into SCAMP")
                        .WrtEndObject();
                    //_context.Response.Write("{\"error\":true,\"msg\":\"could not enroll patient into SCAMP\"}");
                }
                return;
            }
            //otherwise, attempt to enroll patient into SCAMP
            try
            {
                ScampsDB.Query.Scamps.patientEnrollment(currUser, mrn, scampName, status, comment);
                jsWrite.WrtStartObject()
                    .WrtPropertyValue("error", false).WrtPropertyValue("msg", "patient enrolled in SCAMP")
                .WrtEndObject();
                //_context.Response.Write("{\"error\":false,\"msg\":\"patient enrolled in SCAMP\"}");
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("error enrolling patient into SCAMP: ", e.ToString());

                _context.Response.StatusCode = 400;
                if (e.GetType() == typeof(SCAMPException))
                {
                    jsWrite.WrtStartObject()
                        .WrtPropertyValue("error", true).WrtPropertyValue("msg", e.Message)
                    .WrtEndObject();
                    //_context.Response.Write("{\"error\":true,\"msg\":\"");
                    //_context.Response.Write(e.Message);
                    //_context.Response.Write("\"}");
                }
                else
                    jsWrite.WrtStartObject()
                        .WrtPropertyValue("error", true).WrtPropertyValue("msg", "could not enroll patient into SCAMP")
                    .WrtEndObject();
                //_context.Response.Write("{\"error\":true,\"msg\":\"could not enroll patient into SCAMP\"}");
            }
        }

        #endregion
        public static void HandleEncounterData(HttpContext _context)
        {
            var reqtype = _context.Request.RequestType;
            string reqCode = string.Empty;

            //get requests retrieve data
            if (reqtype.Equals("GET"))
            {
                reqCode = _context.Request.QueryString["r"];

                switch (reqCode)
                {
                    case "en"://get encounter
                        if (_context.Request.QueryString["sub_id"].IsNullOrEmpty())
                            getEncounter(_context);
                        else
                            getEncounterSubUnit(_context);
                        break;
                    case "pe"://patient encounters
                        getPatientEncounters(_context);
                        break;
                    case "se"://scamp enrollment
                        string lang = "";
                        //Just calling enrollment buttons, prevent 2 sets of buttons, disables enrollment click
                        Dictionary<string,string> qs = new Dictionary<string, string>();
                        Dictionary<string,string> ps = new Dictionary<string, string>();
                        //Scamps.Site.enrollmentButtons(_context,lang,qs,ps);
                        //This is basically the original functionality, causes 2 sets of buttons, old & new
                        Scamps.Handlers.Ajax.getPatientSCAMPEnrollmentsNew(_context);
                        break;
                    case "le"://scamp enrollment
                        getLinkedEncounters(_context);
                        break;
                    case "esub"://get adjacent subunits for an encounter given a unique subid (VISITS_INPT_SUBUNIT)
                        getEncounterSubUnits(_context);
                        break;
                    default:
                        _context.Response.StatusCode = 400;
                        break;
                }
            }
            //post requests submit data
            else if (reqtype.Equals("POST"))
            {
                reqCode = _context.Request.Form["r"];
                switch (reqCode)
                {
                    case "ie"://insert encounter
                        insertEncounter(_context);
                        break;
                    case "is"://insert subunit
                        insertSubUnitEncounter(_context);
                        break;
                    case "ue"://update encounter
                        if (_context.Request.Form["sub_id"].IsNullOrEmpty())
                            updateEncounter(_context);
                        else
                            updateSubUnitEncounter(_context);
                        break;
                    case "pu"://partial update
                        partialUpdateEncounter(_context);
                        break;
                    case "se"://enroll or change enrollment for a patient in a particular SCAMP
                        enrollPatientSCAMP(_context);
                        break;
                    default:
                        _context.Response.StatusCode = 400;
                        break;
                }

            }
        }
        /******/
    }

    public static class DictionaryProcess
    {
        public static void HandleDictionaryQuery(HttpContext context)
        {
            var method = context.Request.HttpMethod;
            string req;
            if (method == "GET")
            {
                var qs = context.Request.QueryString;
                req = qs["r"].ToLower();

                switch (req)
                {
                    case "scamps":
                        DataDictionary.getSCAMPs(context.Response.Output);
                        break;
                    case "forms":
                        DataDictionary.getSCAMPforms(qs["scamp_id"], context.Response.Output);
                        break;
                    case "fields":
                        //generate a form reference code and list of elements given an appt_id (or subunit id) 
                        var appt = qs["appt_id"];
                        var subunit = qs["sub_id"];
                        long appt_id = 0;
                        long? sub_id = null;
                        long tmp;
                        
                        //verification of input
                        if (string.IsNullOrEmpty(appt))
                        {
                            context.Response.StatusCode = 400;
                            context.Response.Write("{\"error\":true,\"msg\":\"Invalid appt_id\"}");
                            return;
                        }

                        if (!long.TryParse(appt,out appt_id))
                        {
                            context.Response.StatusCode = 400;
                            context.Response.Write("{\"error\":true,\"msg\":\"appt_id is invalid\"}");
                            return;
                        }
                        //validate subunit id if applicable
                        if (!string.IsNullOrEmpty(subunit))
                        {
                            if (long.TryParse(subunit, out tmp))
                                sub_id = tmp;
                            else
                            {
                                context.Response.StatusCode = 400;
                                context.Response.Write("{\"error\":true,\"msg\":\"subunit is invalid\"}");
                                return;
                            }
                        }
                        
                        //set the base path for the SCAMP form
                        var basepath = context.Server.MapPath("/");
                        var formpath = context.Server.MapPath("/scamps/");
                        var cs = System.Configuration.ConfigurationManager.ConnectionStrings["SCAMPs"];
                        var frm = new Form()
                        {
                            BasePath = basepath,
                            FormPath = formpath,
                            ConnectionString = cs.ConnectionString,
                            Provider = cs.ProviderName
                        };
                        frm.Options.VersionWithHiddenElements = true;
                        frm.Entity = "measurements";
                        //once form info is set, attempt to load the form by encounter information
                        frm.LoadAppointmentForm(appt_id,sub_id);
                        //populate ignoreTypes array which ignores elements which do not have element codes (i.e. elements that wont end up in the measurement table)
                        var ignoreTypes = new string[]{ "image","section","panel","html","infobox","label","submit","button"};
                        context.Response.Write("{");
                        context.Response.Write("\"_formreference\":\"");
                        context.Response.Write(frm.FormReference);
                        context.Response.Write("\",");
                        context.Response.Write("\"element_cds\":[");
                        var first = true;
                        //foreach of the form elements, get their list of element codes and write it to the response stream
                        foreach(var ele in frm.theform.SelectMany(panel => panel.Value.getElementList().Where(element => !ignoreTypes.Contains(element.type.ToLower()))) ){
                            foreach(var elecd in ele.elementcodes){
                                if(first)
                                    first = false;
                                else
                                    context.Response.Write(",");
                                context.Response.Write("{\"element_cd\":\"");
                                context.Response.Write(elecd);
                                context.Response.Write("\",\"list_cd\":");
                                if(!string.IsNullOrEmpty(ele.list_cd)){
                                    context.Response.Write("\"");
                                    context.Response.Write(ele.list_cd);
                                    context.Response.Write("\"");
                                }
                                else{
                                    context.Response.Write("null");
                                }
                                context.Response.Write("}");
                                /*
                                context.Response.Write("\"");
                                context.Response.Write(elecd);
                                context.Response.Write("\"");
                                 */
    }
}
                        context.Response.Write("]}");
                        break;
                    case "element":
                        DataDictionary.getElementInfo(qs["element_cd"], context.Response.Output);
                        break;
                    case "list":
                        DataDictionary.getElementList(qs["list_cd"], context.Response.Output);
                        break;
                    default:
                        context.Response.StatusCode = 400;
                        break;
                }
            }
        }
    }
    public static class PatientProcess
    {
        //patient field to description lookup
        public static readonly Dictionary<string, string> patFieldLookup = new Dictionary<string, string>(){
            {"mrn","MRN"},{"lname","Last Name"},{"fname","First Name"},{"gender","Gender"},{"comment","Comments"},{"dob","Date of Birth"}
        };

        private static string[] _reqFields = new string[0];
        
        /// <summary>
        /// determines which patient fields are required via Application Settings key "OptionalPatientFields". 
        /// Sets _reqFields accordingly. mrn is always a required field. 
        /// </summary>
        private static void getReqFields(){
            lock (_reqFields){
                if (_reqFields.Length != 0)
                    return;
                var optFields = string.Empty;
                if (!string.IsNullOrEmpty((optFields = System.Configuration.ConfigurationManager.AppSettings["OptionalPatientFields"])))
                {
                    //optfieldarray will define fields that should be optional via web config
                    var optFieldArr = optFields.Split(',')
                        //since mrn is a 'primary key', it is never optional
                        .Where(opt => !opt.Equals("mrn", StringComparison.OrdinalIgnoreCase)).ToArray();
                    //required fields are the fields which are not contained in the optional field array above
                    _reqFields = patFieldLookup.Select(kvp => kvp.Key).Where(field => !optFieldArr.Contains(field, StringComparer.InvariantCultureIgnoreCase)).ToArray();
                }
                else//in the case that the appkey is missing, mrn, lname and fname are required by default
                    _reqFields = new string[] { "mrn", "lname", "fname", "gender" };
            }
        }

        //required fields acccessor .. lazily loaded
        static string[] reqFields{
            get{
                if (_reqFields.Length == 0)//if not loaded, process it via 'getReqFields'
                    PatientProcess.getReqFields();
                return PatientProcess._reqFields;
            }
        }

        public static void HandlePatientData(HttpContext context)
        {
            var method = context.Request.HttpMethod;
            //Separate actions based on method (POST updates/inserts patient, GET retrieves patient)
            if (method == "POST"){
                //check for required fields missing
                var reqErrors = reqFields.Where(field => context.Request.Form[field] == null || string.IsNullOrWhiteSpace(context.Request.Form[field]))
                    .Select(field => string.Concat("Missing required field '", PatientProcess.patFieldLookup[field], "'"));

                //if reqErrors is not empty, then a required field is missing
                if(reqErrors.Any()){
                    context.Response.StatusCode = 400;
                    using (var jsWrite = new JsonTextWriter(context.Response.Output))
                    {
                        jsWrite.WrtStartObject()
                        .WrtPropertyValue("error", true).WrtPropertyValue("msg", reqErrors.Aggregate((a, b) => a + "; " + b))
                        .WrtEndObject();
                    }
                    return;
                }
                
                //populate local variables with the parameters passed from the POST/PUT request
                string mrn = context.Request.Form["mrn"];
                string lname = context.Request.Form["lname"];
                string fname = context.Request.Form["fname"];
                string gender = context.Request.Form["gender"] == null ? "" : context.Request.Form["gender"];
                string comment = context.Request.Form["comment"];
                string dobstr = context.Request.Form["dob"];
                DateTime? dob = null;

                //if a dob is given, make sure that it is a valid date representation and does not occur in the future
                if (!string.IsNullOrEmpty(dobstr)){
                    var cstyle = CultureInfo.CreateSpecificCulture("en-US");
                    DateTime tmp;
                    //try to parse the date
                    if (DateTime.TryParse(context.Request.Form["dob"], out tmp))
                        dob = tmp;
                    else//invalid date
                    {
                        context.Response.StatusCode = 400;
                        using (var jsWrite = new JsonTextWriter(context.Response.Output))
                        {
                            jsWrite.WrtStartObject()
                            .WrtPropertyValue("error", true).WrtPropertyValue("msg", "Invalid date for Date of birth")
                            .WrtEndObject();
                        }
                        return;
                    }
                    if (tmp > DateTime.Now){
                        context.Response.StatusCode = 400;
                        using (var jsWrite = new JsonTextWriter(context.Response.Output))
                        {
                            jsWrite.WrtStartObject()
                            .WrtPropertyValue("error", true).WrtPropertyValue("msg", "Date of birth can not occur in the future")
                            .WrtEndObject();
                        }
                        return;
                    }
                }

                //finally, route the data based on patient request type
                string req = context.Request.Form["r"];

                switch (req)
                {
                    case "ip":
                        insertPatient(context, mrn, fname, lname, dob, gender, comment);
                        break;
                    case "up":
                        updatePatient(context, mrn, fname, lname, dob, gender, comment);
                        break;
                    default:
                        context.Response.StatusCode = 400;
                        return;

                }
            }
            else if (method == "GET"){
                string mrn = context.Request.QueryString["mrn"];
                findPatient(context, mrn);
            }
            else
                context.Response.StatusCode = 400;

        }

        private static bool findPatient(HttpContext context, string mrn)
        {

            var jsWrite = new JsonTextWriter(context.Response.Output);
            try
            {
                //try to find patient in local db
                var p = Patients.findPatient(mrn);
                //if not found, check external source (if it exists)
                if (p == null)
                {
                    if (System.Configuration.ConfigurationManager.ConnectionStrings["External"] != null && System.Configuration.ConfigurationManager.ConnectionStrings["ExternalEntities"] != null)
                    {
                        try{
                            p = Patients.getExternalPatient(mrn);
                            if(p != null){
                                jsWrite.WrtStartObject()
                                    .WrtPropertyValue("external",true)
                                    .WrtPropertyName("patient")
                                    .WrtStartObject()
                                        .WrtPropertyValue("mrn",p.mrn)
                                        .WrtPropertyValue("fname",p.fname)
                                        .WrtPropertyValue("lname",p.lname)
                                        .WrtPropertyValue("dob",(p.dob.HasValue ? p.dob.Value.ToString("d") : ""))
                                        .WrtPropertyValue("gender",p.gender)
                                        .WrtPropertyValue("comment",p.comment)
                                    .WrtEndObject()
                                .WrtEndObject();
                                return true;
                            }
                        }catch(Exception e){
                            Console.Error.WriteLine("Error trying to find patient from external source: ",e.ToString());
                        }
                    }

                    //before proceeding, validate MRN syntax
                    if (Patients.ValidMRN(mrn))
                    {
                        context.Response.Write("{\"error\":false,\"msg\":\"No patient record exists for MRN '"+mrn.ToUpper()+"'\"}");
                        return false;
                    }
                    context.Response.Write("{\"error\":true,\"msg\":\"invalid MRN\"}");
                    return false;

                }
                else
                {
                    jsWrite.WrtStartObject()
                        .WrtPropertyValue("external", false)
                        .WrtPropertyName("patient")
                        .WrtStartObject()
                            .WrtPropertyValue("mrn", p.mrn)
                            .WrtPropertyValue("fname", p.fname)
                            .WrtPropertyValue("lname", p.lname)
                            .WrtPropertyValue("dob", (p.dob.HasValue ? p.dob.Value.ToString("d") : ""))
                            .WrtPropertyValue("gender", p.gender)
                            .WrtPropertyValue("comment", p.comment)
                        .WrtEndObject()
                    .WrtEndObject();
                    //context.Response.Write(string.Format("{{\"external\":false,\"patient\":{{\"mrn\":\"{0}\",\"fname\":\"{1}\",\"lname\":\"{2}\",\"dob\":\"{3}\",\"gender\":\"{4}\",\"comment\":\"{5}\"}}}}", p.mrn, p.fname, p.lname, (p.dob.HasValue ? p.dob.Value.ToString("d") : ""), p.gender, p.comment));
                }
            }
            catch (Exception e)
            {
                context.Response.StatusCode = 500;
                Console.Error.WriteLine("Error finding patient in database:", e);
            }
            return false;
        }

        private static bool insertPatient(HttpContext context, string mrn, string fname, string lname, DateTime? dob, string gender, string comment)
        {
            var jsWrite = new JsonTextWriter(context.Response.Output);
            try
            {
                //validate MRN
                if (!Patients.ValidMRN(mrn))
                {
                    context.Response.Write("{\"error\":true,\"msg\":\"invalid MRN\"}");
                    return false;
                }

                var pat = new ScampsDB.ViewModels.PatientViewModel
                {
                    mrn = mrn,
                    fname = fname,
                    lname = lname,
                    dob = dob,
                    gender = gender,
                    comment = comment
                };

                Patients.insertPatient(pat);

                jsWrite.WrtStartObject()
                        .WrtPropertyValue("error", false).WrtPropertyValue("msg", "new patient added")
                    .WrtEndObject();
                    
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error inserting patient into database:", e);
                context.Response.StatusCode = 400;
                if(e.GetType() == typeof(SCAMPException)){
                    jsWrite.WrtStartObject()
                        .WrtPropertyValue("error", true).WrtPropertyValue("msg", e.Message)
                    .WrtEndObject();
                }
                else
                    jsWrite.WrtStartObject()
                        .WrtPropertyValue("error", true).WrtPropertyValue("msg", "could not insert patient")
                    .WrtEndObject();
            }

            return false;
        }
        private static bool updatePatient(HttpContext context, string mrn, string fname, string lname, DateTime? dob, string gender, string comment)
        {
            var jsWrite = new JsonTextWriter(context.Response.Output);
            
            try
            {
                var pat = new ScampsDB.ViewModels.PatientViewModel
                {
                    mrn = mrn,
                    fname = fname,
                    lname = lname,
                    dob = dob,
                    gender = gender,
                    comment = comment
                };
                Patients.updatePatient(pat);
                
                jsWrite.WriteStartObject();
                    jsWrite.WritePropertyName("error");
                    jsWrite.WriteValue(false);
                    jsWrite.WritePropertyName("msg");
                    jsWrite.WriteValue("patient information updated");
                jsWrite.WriteEndObject();
                return true;
            }
            catch (Exception e)
            {
                Console.Error.WriteLine("Error updating patient in database:", e);

                if (e.GetType() == typeof(SCAMPException))
                {
                    jsWrite.WrtStartObject()
                        .WrtPropertyValue("error", true).WrtPropertyValue("msg", e.Message)
                    .WrtEndObject();
                }
                else
                {
                    context.Response.StatusCode = 400;
                    jsWrite.WrtStartObject()
                        .WrtPropertyValue("error", true).WrtPropertyValue("msg", "could not insert patient")
                    .WrtEndObject();
                }
            }
            return false;
        }
    }

    public static class MeasurementPivotProcess
    {
        
        public static void getFormData(HttpContext con, string identifier, string id)
        {
            var _query = string.Empty;

            try
            {
                _query = FormMeasurementPivotQuery(identifier,id);
                var db = new DataTools();
                var csettings = System.Configuration.ConfigurationManager.ConnectionStrings["SCAMPs"];
                db.Provider = csettings.ProviderName;
                db.ConnectionString = csettings.ConnectionString;
                db.OpenConnection();

                db.GetResultSet(_query);

                //using httpcontext response writer, build JSON object
                //writes object in the following format: {"columns":["a","b"],"rows":[["1","2"],["3","4"]]}
                var jWriter = new JsonTextWriter(con.Response.Output);
                jWriter.WriteStartObject();
                jWriter.WritePropertyName("columns");
                jWriter.WriteStartArray();
                //write each column
                foreach (DataColumn c in db.ResultSet.Columns)
                {
                    jWriter.WriteValue(c.ColumnName);
                }
                jWriter.WriteEndArray();

                jWriter.WritePropertyName("rows");
                jWriter.WriteStartArray();
                //write each row
                foreach (DataRow r in db.ResultSet.Rows)
                {
                    //write each column in current row
                    jWriter.WriteStartArray();
                    foreach (var i in r.ItemArray)
                        jWriter.WriteValue(i.ToString());
                    jWriter.WriteEndArray();
                }
                jWriter.WriteEndArray();
                jWriter.WriteEndObject();
            }
            catch (Exception e)
            {
                Console.WriteLine("Unable to get pivot form data: " + e.ToString());
            }
        }

        /// <summary>
        /// Builds a SQL statement for generating a pivot view of a patient's MRN, age, and form results given a form property and value to uniquely identify a form
        /// </summary>
        /// <param name="identifier">name of a column or identifier property by which a form can be uniquely identified</param>
        /// <param name="id">the string representation of the identifier value</param>
        /// <returns>SQL statement for the pivot query</returns>
        private static string FormMeasurementPivotQuery(string identifier, string id)
        {
            var validIdentifiers = new string[] { "id", "encounter_id", "form_name", "form" };
            if (!validIdentifiers.Contains(identifier, StringComparer.OrdinalIgnoreCase))
                return "";

            var idstr = Tools.SQLSafe(id).ToUpper();
            const string _pivotQueryTemplate = "select * from ({0}) pivot (max(element_val) for (element_cd) in ({1}))";
            string formFieldQuery = "";
            string innerSelectTemplate = "";

            switch (identifier.ToLower())
            {
                case "id":
                case "encounter_id":
                    formFieldQuery = "select distinct el.element_cd from admin_encounters e join admin_form_fields el on el.form_name = e.form_name where e.encounter_id = {0}";
                    innerSelectTemplate = "select p.mrn, to_char(round((coalesce(e.clinic_appt, e.inpt_admit, e.event_dt_tm) - p.dob) / 365.242199,2)) as age, m.element_cd, listagg(coalesce(ell.description_txt,m.element_val),',') within group (order by m.update_dtm) as element_val  from pt_master p join pt_encounters e on e.mrn = p.mrn  join measurements m on m.appt_id = e.appt_id left join admin_element ele on m.element_cd = ele.element_cd left join admin_element_list ell on ele.list_cd = ell.list_cd and m.element_val = ell.value_txt join admin_encounters en on e.encounter_type = en.encounter_id  where en.encounter_id = '{0}'  group by p.mrn, ((coalesce(e.clinic_appt, e.inpt_admit, e.event_dt_tm) - p.dob) / 365.242199 ), m.element_cd";
                    break;
                case "form_name":
                case "form":
                    formFieldQuery = "select distinct element_cd from admin_form_fields where upper(form_name) = '{0}'";
                    innerSelectTemplate = "select p.mrn, to_char(round((coalesce(e.clinic_appt, e.inpt_admit, e.event_dt_tm) - p.dob) / 365.242199,2)) as age, m.element_cd, listagg(coalesce(ell.description_txt,m.element_val),',') within group (order by m.update_dtm) as element_val  from pt_master p join pt_encounters e on e.mrn = p.mrn  join measurements m on m.appt_id = e.appt_id  left join admin_element ele on m.element_cd = ele.element_cd left join admin_element_list ell on ele.list_cd = ell.list_cd and m.element_val = ell.value_txt join admin_encounters en on e.encounter_type = en.encounter_id  where upper(en.form_name) = '{0}' group by p.mrn, ((coalesce(e.clinic_appt, e.inpt_admit, e.event_dt_tm) - p.dob) / 365.242199 ), m.element_cd";
                    break;
                default:
                    return "";
            }

            //get a list of unique form names for a given form
            var db = new DataTools();
            var csettings = System.Configuration.ConfigurationManager.ConnectionStrings["SCAMPs"];
            db.Provider = csettings.ProviderName;
            db.ConnectionString = csettings.ConnectionString;
            
            db.OpenConnection();
            db.GetResultSet(string.Format(formFieldQuery, idstr));

            var elementList = new List<string>();
            var dtbl = db.ResultSet;
            foreach (DataRow r in dtbl.Rows)
            {
                elementList.Add(r.Field<string>(0));
            }

            if (elementList.Count <= 0)
                return "";

            var csvFormNames = new System.Text.StringBuilder();
            bool first = true;
            foreach (var e in elementList)
            {
                if (first)
                    first = false;
                else
                    csvFormNames.Append(",");
                csvFormNames.AppendFormat("'{0}'", e);
            }

            var _innerSelectQuery = string.Format(innerSelectTemplate, idstr);
            return string.Format(_pivotQueryTemplate, _innerSelectQuery, csvFormNames.ToString());
        }
    }


}
