﻿var ddlData = { scamp:null, prov: null, loc: null, event: null, enstatus: null, encounter: null, scampform:null, enscamps:null };

(function(){
	//linked encounter methods definition
	var len = {
		//replaces populateLinkedEncounterPanel
		populatePanel: function(d){
			$('.endetail > div').hide();
			$('.endetail .linkinfo').show();
			$('.endetail').slideDown('fast');
			var subtable = $('.LinkEncTable tbody');
			subtable.empty();

			var attrib = ['actions','prov','date','scamp','encounter','status'];
			for(var i=0;i<d.length;i++){
				var row = document.createElement('TR');
				for(var j=0;j<attrib.length;j++){
					var col = document.createElement('TD');
					if (attrib[j] === 'actions'){
			                        var a = document.createElement('A');
			                        a.className = 'icon-edit-1 editencounter';
									a.setAttribute('data-apptid', d[i].edata.apptid);
			                        a.setAttribute('title', 'View/Edit Encounter');
			                        //$(a).click(editEncounter);
			                        col.appendChild(a);
						col.appendChild(createSDFurl(d[i].edata));
                    			}
					else if (attrib[j] == 'encounter') {
						if (d[i].encounter && d[i].encounter.length > 32) {
							var span = document.createElement('SPAN');
							$(span).text(d[i].encounter.substring(0, 32) + '..');
							span.title = d[i].encounter;
							col.appendChild(span);
						}
						else {
							$(col).text(d[i].encounter);
						}
					}
                    			else if(attrib[j] == 'date'){
                        			$(col).text(d[i].date.substring(0, 16).replace('T', ' '));
                    			}
					else {
						var txt = d[i][attrib[j]];
						if (!txt)
							txt = '';
						$(col).text(txt);
					}
					row.appendChild(col);
				}
				subtable.append(row);
			}
		},
		//gets linked events given an apptid stored in data-link
		getEvents: function(e){
			e.stopPropagation();
			var drow;
			if((drow = $(this).parents('tr[role=row]')).length > 0){
				if(!drow.hasClass('selected')){
					etbl.$('tr.selected').removeClass('selected');
					$(drow).addClass('selected');
				}
			}
			$('.endetail').slideUp(50);
			$('html, body').animate({ scrollTop: $('body').offset().top }, 200);
			var src = document.getElementById('frmEncounter');
			var payload = 'r=le&appt_id=' + $(this).attr('data-link');
			$.ajax({
				url: src.action,
				type: 'GET',
				data: payload,
				complete: function (j, t) {
					if (j.status === 200) {
						linken = $.parseJSON(j.responseText);
						PDetailHandler.LinkedEncounter.populatePanel(linken.data);
					}
					else {
						//unable to load subunit encounters
					}
				}
			});
		}
	};
	//subunit encounters definition
	var su = {
		//replaces populateSubUnitPanel
		populatePanel: function(d){
			$('.endetail > div').hide();
			$('.endetail .subinfo').css('display','block');
			$('.endetail').slideDown('fast');
			var subtable = $('.subEncTable tbody');
			subtable.empty();
			// sdf, apptid, date, attending,scamp, encounter,status
			var attrib = ['sdf','encDate','attending','scampName','encounterName','statusDescription'];

			for(var i=0;i<d.length;i++){
				var row = document.createElement('TR');
				for(var j=0;j<attrib.length;j++){
					var col = document.createElement('TD');
					if (attrib[j] === 'sdf'){
                        //create edit encounter link
                        var a = document.createElement('A');
                        a.className = 'icon-edit-1 editencounter';
						a.setAttribute('data-apptid', d[i].appt_id);
                        a.setAttribute('data-subid', d[i].sub_id);
                        a.setAttribute('title', 'View/Edit SubUnit Encounter');
                        //$(a).click(editEncounter);
                        col.appendChild(a);
                        //create SDF link
						col.appendChild(createSDFurl(d[i].edata));
                    }
					else if (attrib[j] == 'encounterName') {
						if (d[i].encounterName && d[i].encounterName.length > 32) {
							var span = document.createElement('SPAN');
							$(span).text(d[i].encounterName.substring(0, 32) + '..');
							span.title = d[i].encounterName;
							col.appendChild(span);
						}
						else {
							$(col).text(d[i].encounterName);
						}
					}
                    else if(attrib[j] == 'encDate'){
                        $(col).text(d[i].encDate.substring(0, 16).replace('T', ' '));
                    }
					else {
						var txt = d[i][attrib[j]];
						if (!txt)
							txt = '';
						$(col).text(txt);
					}
					row.appendChild(col);
				}
				subtable.append(row);
			}
		},
		//replaces getSubUnitEvents
		//gets subunit encounters given an apptid stored in data-apptid
		getEvents: function(e) {
			e.stopPropagation();
			var drow;
			if((drow = $(this).parents('tr[role=row]')).length > 0){
				if(!drow.hasClass('selected')){
					etbl.$('tr.selected').removeClass('selected');
					$(drow).addClass('selected');
				}
			}
			
			$('.endetail').slideUp(50);
			$('html, body').animate({ scrollTop: $('body').offset().top }, 200);
            $('.endetail .enNewSub').attr('data-apptid',$(this).attr('data-apptid'));
			var src = document.getElementById('frmEncounter');
			var payload = 'r=esub&appt_id=' + $(this).attr('data-apptid');
			$.ajax({
				url: src.action,
				type: 'GET',
				data: payload,
				complete: function (j, t) {
					if (j.status === 200) {
						sube = $.parseJSON(j.responseText);
						PDetailHandler.SubUnit.populatePanel(sube.data);
					}
					else {
						//unable to load subunit encounters
					}
				}
			});
		}
	};

	var pen = {
	};
	//base patient detail functions and objects with sub-functions
	//contains subunit and linked encounter objects for handling their respective functions
	var pd = {
		//not sure if this is necessary? should just be in markup instead?
		scampVal: {
			'default': {txt:'None',title:'Not Enrolled','class':'ssDefault'},
			'-2': {txt: 'Exited from SCAMP',title:'Exited', 'class':'ssExit'},
			'-1': {txt: 'Excluded',title:'Excluded (permanent)', 'class':'ssExPerm'},
			'0': {txt: '*Excluded',title:'Excluded (temporarily)', 'class':'ssExTemp'},
			'2': {txt: 'To Screen',title:'To Be Screened', 'class':'ssScreen'},
			'3': {txt: 'Re-Screen',title:'To Be Re-Screened', 'class':'ssReScreen'},
			'1': {txt: 'Included',title:'Included', 'class':'ssInclude'}
		},
		//given an age in hours, transforms it into a more relatable value based on its magnitude
		//replaces processAge
		processAge: function(a){
			var i = parseInt(a);

			if(i <= 2880)
				return (i / 60).toFixed(1) + ' hrs';
			if (i <= 44640)
				return (i/1440).toFixed(1) + ' days';
			if (i <= 525600)
				return (i / 43800).toFixed(1) + ' months';

			return (i / 525600).toFixed(1) + ' years';
		},
		SubUnit: su,
		LinkedEncounter: len
	};
	window.PDetailHandler = pd;
})();


//properties needed: form name (fname), appt_id (apptid), encounter type (encd), encounter url (encurl), form exists (fexist)

//ajax call for retrieving linked encounters for an encounter



function editEncounter(){
	$('body').css('cursor', 'wait');
    var sub = $(this).attr('data-subid');
	var eobj = {sub: $(this).attr('data-subid'), appt: $(this).attr('data-apptid')};
	encounterAjax.load(eobj);
}

//scamp enrollment handler
(function () {
	var se = {
		_frm: null,
		patEnrollment:null,
		init: function(){
			this._frm = document.getElementById('ScreenPtSCAMP');
			$('.toggleEnSc').unbind('click').click( ScampEnrollHandler.toggleModal );
			$('.btnEnroll').unbind('click').click( ScampEnrollHandler.enrollPatient );
			
			//var form = $('.enrollscamp form')[0];
			//$(form).on('submit', function (e) {
			//	e.preventDefault();
			//	ScampEnrollHandler.enrollPatient();
			//});
			
			//$('.ScEn').unbind('click').click(function (e) {
			    
			//    //var form = $('.enrollscamp form')[0];
			//	////form.mrn.value=getParameterByName('PAT_NUM');
            //    //form.mrn.value=getPatMrn();
			//	//form.sn.value=this.getAttribute('data-sn');
            //    //$('.enrollscamp .scamptarget').text(form.sn.value);
            //    //$('.enrollscamp h4').text(this.innerText);
			//    //ScampEnrollHandler.toggleModal();

			//    var mrn = getPatMrn();
			//    var scamp = this.getAttribute('data-sn');
			//    var scampname = this.getAttribute('data-scamp');
			//    var currentstatus = this.getAttribute('data-enrollstatus');
			//    $.get("enrollment.ajax?id=" + mrn + "&v=edit&pid=" + mrn + "&scamp=" + scamp + "&currentstatus=" + currentstatus + "&scampname=" + scampname, function (data) {
			//        var ra = $.parseJSON(data);
			//        showModal(ra.modal_title, ra.markup);
			//        if (document.URL.indexOf('/patients/encounter/') > 0) {
			//            $('.deleteencounter').hide();
			//            $('.saveandsdf').hide();
			//        }
			//    });

			//});
		},
		localEnrollSCAMPs : function(){
			var l = ScampEnrollHandler.patEnrollment;
			var sc = [];
			if(!l)
				return sc;
			for(var i=0;i<l.length;i++){
				if(l[i].status_val != -1 && l[i].status_val != 0)
					sc.push(l[i].scamp_name);
			}
			return sc;
		},
		toggleModal: function(){
			var dispVal = $('.overlay').css('display');
			if(dispVal === 'none'){
                $('.overlay').css("display","block");
                $('.enrollscamp').show().centerInClient();
			}
			else{
                $('#'+ScampEnrollHandler._frm.id+' [name]').val('');
                $('.overlay, .enrollscamp').css('display','none');
			}
		},
		enrollPatient: function(){
			var src = ScampEnrollHandler._frm;
			var payload = 'r=se&'+$(src).serialize();
			$.ajax({
				url: src.action,
				type: src.method,
				data: payload,
				complete: function(j,t){
					var obj;
					try{
						obj = $.parseJSON(j.responseText);
					}catch(e){}
					var errmsg = 'error enrolling user into scamp';
					if(obj){
						if(obj.error === false){
							ScampEnrollHandler.toggleModal();
							ScampEnrollHandler.getSCAMPenrollment();
							var scampddl = $('#frmEncounter [name=scamp_id]')[0];
							$('.encountermanage [name=scamp_id]').val('');
							return;
						}
						if(typeof obj.msg === 'string')
							errmsg = errmsg+': '+obj.msg;
					}
					$('#ScreenPtSCAMP .frmmsg').text(errmsg);
				}
			});
		},
		getSCAMPenrollment: function(){
			var src = document.getElementById('frmEncounter');
			var scpayload = 'r=se&mrn=' + getPatMrn();
			$.ajax({
				async: false,
				url: src.action,
				type: 'GET',
				data: scpayload,
				complete: function (j, t) {
					//successful query
					if (j.status === 200) {
						//parse keyvalue pair for scamp enrollment (scamp name, scamp value)
						sc_en = $.parseJSON(j.responseText);
						if (sc_en) {
							ScampEnrollHandler.patEnrollment = [];
							$('.scampinfo').empty()

							var scamplist = document.createElement('DIV');
							//populate scamp enrollment for current page's patient in handler and markup
							for (var i = 0; i < sc_en.length; i++) {
								ScampEnrollHandler.patEnrollment.push(sc_en[i]);
								var li = document.createElement('SPAN');
								var sDisp,titleText;
								if (sc_en[i].status_val != null && PDetailHandler.scampVal[sc_en[i].status_val]){
                                    var adt, uid, rsn;
                                    adt = (sc_en[i].audit_dt_tm === null) ? '(no date)' : jsDateTimeConvert(sc_en[i].audit_dt_tm);
                                    uid = (sc_en[i].user_id === null) ? '(unknown)' : sc_en[i].user_id;
                                    rsn = (sc_en[i].cmnt_txt === null) ? '(not given)' : sc_en[i].cmnt_txt;
									sDisp = PDetailHandler.scampVal[sc_en[i].status_val];
									titleText = sDisp.title+' on '+adt+' by '+uid+' Reason: '+rsn;
								}
								else{
									sDisp = PDetailHandler.scampVal['default'];
									titleText = sDisp.title;
								}
								
								li.className = 'ScEn'.concat(' ', sDisp['class']);
								li.setAttribute('title', titleText);
								li.setAttribute('data-sn', sc_en[i].scamp_name);
								$(li).text(sc_en[i].scamp_name);
								scamplist.appendChild(li);
							}
							//populate ddl data with currently enrolled scamps
							if(ddlData){
								for(var i=0;i<ddlData.enscamps;i++){
									delete ddlData.enscamps[i];
								}
								ddlData.enscamps = [];
								$('select.currEnrolledSCAMPs').empty();
								ddlData.enscamps = ArrayUnionSet(ddlData.scamp,function(e){ return e.label;},ScampEnrollHandler.localEnrollSCAMPs(),function(e){ return e;});
                                $('select[name=scamp_id]').each(DOMBindHelper.initSelect);
							}
							
                            $('.scampinfo').append(scamplist);

							ScampEnrollHandler.init();
						}
						else {
							//unable to load scamp enrollment data
						}
					}
				}
			});
		}
	};
	window.ScampEnrollHandler = se;
})();


//encounter AJAX actions (load, insert, etc)
var encounterAjax = {
    insertSub: function () {
        var frm = document.getElementById('frmEncounter');
        $('#frmEncounter [name=mrn]').val(getPatMrn());
        var payload = $(frm).serialize().concat('&r=is');
        var apptid = $(frm).find('[name=appt_id]').val();
        encModalMgr.Transition('wait');
        $.ajax({
            url: frm.action,
            type: 'POST',
            data: payload,
            complete: function (j, t) {
                $('body').css('cursor', 'default');
                if (j.status === 200) {
                    $('#frmEncounter .frmmsg').text('subunit encounter inserted');
                    try {
                        var enobj = $.parseJSON(j.responseText);
                        $('#frmEncounter [name=sub_id]').removeAttr('disabled').val(enobj.sub_id);

                        if (enobj.enrolledbyencounter)
                            ScampEnrollHandler.getSCAMPenrollment();
                        $('a.ensubhar[data-apptid="' + apptid + '"]').click();
                    }
                    catch (e) { }
                    encModalMgr.Transition('update');
                    $('#frmEncounter').find('[data-flag~=S]').removeAttr('disabled').parent().css('display', 'block');
                    //reload datatable
                    etbl.api().ajax.reload();
                }
                else {
                    try {
                        var enobj = $.parseJSON(j.responseText);
                        $('#frmEncounter .frmmsg').text(enobj.msg);
                    }
                    catch (e) {
                        $('#frmEncounter .frmmsg').text('error inserting encounter');
                    }

                    encModalMgr.Transition('insert');
                    $('#frmEncounter').find('[data-flag~=S]').removeAttr('disabled').parent().css('display', 'block');
                    $('#frmEncounter').find('[name=encounter_cd]').attr('disabled', 'disabled').parent().css('display', 'none');
                    $('#frmEncounter').find('[name=appt_id]').removeAttr('disabled');
                }
            }
        });
    },
    insert: function () {
        var frm = document.getElementById('frmEncounter');
        $('#frmEncounter [name=mrn]').val(getPatMrn());
        var payload = $(frm).serialize().concat('&r=ie');
        encModalMgr.Transition('wait');
        $.ajax({
            url: frm.action,
            type: 'POST',
            data: payload,
            complete: function (j, t) {
                $('body').css('cursor', 'default');
                var enobj;
                //attempt to parse JSON status message
                try {
                    enobj = $.parseJSON(j.responseText);
                }
                catch (e) { }
                //if unparsable, request is likely an error
                if (!enobj) {
                    $('#frmEncounter .frmmsg').text('error inserting encounter');
                    encModalMgr.Transition('insert');
                    return;
                }
                //request is in error state
                if (enobj.error === true) {
                    var errmsg;
                    if (typeof enobj.msg === 'string')
                        errmsg = enobj.msg;
                    else
                        errmsg = 'error inserting encounter';

                    if (enobj.errlist && enobj.errlist.length) {
                        errmsg = errmsg + ': ' + enobj.errlist[0];
                        for (var i = 1; i < enobj.errlist.length; i++) {
                            errmsg = errmsg + ', ' + enobj.errlist[i];
                        }
                    }
                    $('#frmEncounter .frmmsg').text(errmsg);
                    encModalMgr.Transition('insert');
                }
                else {//no error
                    if (typeof enobj.msg === 'string')
                        $('#frmEncounter .frmmsg').text(enobj.msg);
                    else
                        $('#frmEncounter .frmmsg').text('encounter inserted');
                    //encModalMgr.Transition('update');
                    if (enobj.appt_id)
                        storehandler.encounter.add(enobj.appt_id);

                    if (enobj.enrolledbyencounter)
                        ScampEnrollHandler.getSCAMPenrollment();

                    $('.overlay').css('display', 'none');
                    $('.encountermanage').hide();
                    encModalMgr.Transition('reset');
                    etbl.api().ajax.reload();
                }
            }
        });
    },
    update: function () {
        var frm = document.getElementById('frmEncounter');
        $('#frmEncounter [name=mrn]').val(getPatMrn());
        var payload = $(frm).serialize().concat('&r=ue');
        var subdom = $(frm).find('[name=sub_id]');
        var sub_id = null;
        if (!subdom.attr('disabled'))
            sub_id = subdom.val();

        encModalMgr.Transition('wait');
        $.ajax({
            url: frm.action,
            type: 'POST',
            data: payload,
            complete: function (j, t) {
                $('body').css('cursor', 'default');
                if (j.status === 200) {
                    $('#frmEncounter .frmmsg').text('encounter updated');
                    try {
                        var enobj = $.parseJSON(j.responseText);
                        
						$('#frmEncounter [name=appt_id]').val(enobj.appt_id);
						
						if($('#frmEncounter [name=sub_id]').val())
							$('a.ensubhar[data-apptid="' + enobj.appt_id + '"]').click();
						
                        storehandler.encounter.add(enobj.appt_id);
                    }
                    catch (e) { }
                    //encModalMgr.Transition('update');
                    //reload datatable
                    $('.overlay').css('display', 'none');
                    $('.encountermanage').hide();
                    encModalMgr.Transition('reset');
                    if (sub_id !== null)
                        subdom.removeAttr('disabled').val(sub_id);
                    etbl.api().ajax.reload();
                    return;
                }
                else {
                    try {
                        var enobj = $.parseJSON(j.responseText);
                        $('#frmEncounter .frmmsg').text('Error: ' + enobj.msg);
                    }
                    catch (e) {
                        $('#frmEncounter .frmmsg').text('Error updating encounter');
                    }

                }
                encModalMgr.Transition('update');
                if (sub_id !== null)
                    subdom.removeAttr('disabled').val(sub_id);
            }
        });
    },
    load: function (eid) {
        var frm = document.getElementById('frmEncounter');
        var pdata = 'r=en';
        if (eid.sub) {
            pdata = pdata + '&sub_id=' + eid.sub;
            //manually set appt_id if sub
            $('.encountermanage [name=appt_id]').val(eid.appt);
        }
        if (eid.appt)
            pdata = pdata + '&appt_id=' + eid.appt;
        else
            return;
        encModalMgr.Transition('wait');
        $.ajax({
            url: frm.action,
            type: 'GET',
            data: pdata,
            complete: function (j, t) {
                $('body').css('cursor', 'default');
                if (j.status === 200) {
                    var enobj = $.parseJSON(j.responseText);
                    if (enobj) {

                        var eleList = $('.encountermanage .frmfields [name]');

                        //massage datetime fields
                        var dtf = ['encounter_date', 'encounter_email', 'dischargeDate', 'encounter_prereview', 'encounter_postreview', 'inpt_disch'];
                        for (var i = 0; i < dtf.length; i++) {
                            if (typeof enobj[dtf[i]] === 'string' && (idx = enobj[dtf[i]].indexOf('T')) !== -1)
                                enobj[dtf[i]] = jsDateTimeConvert(enobj[dtf[i]]);
                        }

                        var ddlopts = ['scamp_id', 'encounter_status', 'encounter_type', 'event_type'];
                        for (var i = 0; i < ddlopts.length; i++) {
                            if (enobj[ddlopts[i]] != null) {
                                $('[name=' + ddlopts[i] + '] option[value]').removeAttr('selected');
                                $('[name=' + ddlopts[i] + '] option[value=' + enobj[ddlopts[i]] + ']').attr('selected', 'selected');

                                if (ddlopts[i] === 'scamp_id')
                                    $('[name=scamp_id]').change(); /* We need to manually fire change if we updated scamp_id so the encounter form type filter fires. */
                            }
                        }

                        if (enobj && enobj.encounter_cd) {
                            $('[name=encounter_cd]').val(enobj.encounter_cd).change();
                        }

                        var acopts = ['scamps_prov_id', 'encounter_user', 'loctn_abbr'];
                        for (var i = 0; i < acopts.length; i++) {
                            if (enobj[acopts[i]])
                                DOMBindHelper.bindDLOptionByValue(acopts[i], enobj[acopts[i]]);
                        }

                        //bind data to their respective form elements
                        for (var i = 0; i < eleList.length; i++) {
                            if (eleList[i].name && enobj[eleList[i].name]) {
                                $(eleList[i]).val(enobj[eleList[i].name]);
                            }
                        }

                        var sdfURL = '/patients/encounter/';
                        if (enobj.appt_id) {
                            sdfURL = sdfURL + enobj.appt_id;
                            if (enobj.sub_id)
                                sdfURL = sdfURL + '?subunit=' + enobj.sub_id;
                            var sdflink = $('.enView');
                            sdflink.attr('href', sdfURL);
                            if (enobj.encounter_cd.indexOf('I') !== -1 || enobj.fexist === 0 || (typeof (enobj.enstat) === 'string' && enobj.enstat.indexOf('NOT_SCAMP_VISIT') !== -1)) {
                                sdflink.css('display', 'none');
                            }
                            else
                                sdflink.css('display', 'inline');

                        }
                    }
                    encModalMgr.Transition('update');
                    //toggle display
                    var target = '.encountermanage';
                    $(target).find('[data-flag]').attr('disabled', 'disabled').parent().css('display', 'none');

                    if (enobj.sub_id) {
                        $(target).find('[data-flag~=U]').removeAttr('disabled').parent().css('display', 'block');
                        $(target).find('[data-flag~=S]').removeAttr('disabled').parent().css('display', 'block');
                    }
                    else if (enobj.encounter_cd === 'I') {
                        $(target).find('[data-flag~=I]').removeAttr('disabled').parent().css('display', 'block');
                    }
                    else if (enobj.encounter_cd === 'E') {
                        $(target).find('[data-flag~=U]').removeAttr('disabled').parent().css('display', 'block');
                        $(target).find('[data-flag~=E]').removeAttr('disabled').parent().css('display', 'block');
                    }
                    else {
                        $(target).find('[data-flag~=U]').removeAttr('disabled').parent().css('display', 'block');
                    }

                    //if encounter has not been saved and encounter_type is null, it can be set
                    if (!enobj.hasData && enobj.encounter_cd !== 'I') {
                        $('[name=scamp_id]').removeAttr('disabled').parent().css('display', 'block');
                        $('[name=encounter_type]').removeAttr('disabled').parent().css('display', 'block');
                    }

                    var sdflink = $(target).find('.enView');
                    if (sdflink) {
                        if (sdflink.attr('disabled') === 'disabled') {
                            sdflink.addClass('disabled');
                        }
                        else {
                            sdflink.removeClass('disabled');
                        }
                    }
                    $('.overlay').css('display', 'block');
                    $('.encountermanage').show().centerInClient();
                    setFocus();
                    etbl.api().ajax.reload();
                }
                else {
                    encModalMgr.Transition('insert');
                }
            }
        });
    }
};

//definition of encounter modal states
var encModalStates = {
    start: { nextStates: ['init'], action: function () { this.Transition('init'); } },
    init: { nextStates: ['insert', 'load', 'reset'],
        action: function () {
            //bind all the controls
            //display model + new encounter form fields
            $('.enNew').click(function () {
                encModalMgr.Transition('insert');
                var target = '.encountermanage';
                $(target).find('[name=encounter_type]').parent().hide();
                $('.overlay').css('display', 'block');
                $('.encountermanage').show().centerInClient();
                setFocus();
            });

            $('.encountermanage [name=scamp_id]').change(function () {
                if ($(this).val != '')
                    $('.encountermanage [name=encounter_type]').parent().show();
            });
            //create subunit encounter
            $('.enNewSub').click(function () {
                encModalMgr.Transition('insert');
                var target = '.encountermanage';
                $(target).find('[data-flag~=S]').removeAttr('disabled').parent().css('display', 'block');
                var en_cd = $(target).find('[name=encounter_cd]');
                en_cd.val('I'); 
                en_cd.change();
                en_cd.attr('disabled', 'disabled').parent().css('display', 'none');
				$(target).find('[name=har]').parent().css('display', 'none');
                $(target).find('[name=appt_id]').removeAttr('disabled').val($(this).attr('data-apptid'));
                $('.overlay').css('display', 'block');
                $('.encountermanage').show().centerInClient();
                setFocus();
            });
            //hide
            $('.enClose, .fmodal .icon-cancel-1').click(function () {
                
                $('.encountermanage').hide();
                // not sure what the above does, but the scamp enrollment window is 
                // bound to this and also needs to close.
                // should probably just just the main modal, but, alas, too much
                // difference to do with any degree of confidence.
                // ZAPHOD
                $('.fmodal').hide();
                encModalMgr.Transition('reset');
                // moved this here.  Hide the modal first, then the overlay
                $('.overlay').css('display', 'none');
            });

            //create
            $('.enCreate').click(function () {
                $('body').css('cursor', 'wait');
                //detect encounter insert vs subunit insert
                if ($('.encountermanage [data-flag~=S]:disabled').length > 0)
                    encounterAjax.insert();
                else
                    encounterAjax.insertSub();

            });
            //update
            $('.enUpdate').click(function () {
                $('body').css('cursor', 'wait');
                //encModalMgr.Transition('wait');
                encounterAjax.update();
            });

            //on scamp type change
            $('[name=scamp_id]').change(function () {
                var sid = $(this).val();
                var ele = $('[name=encounter_type]')[0];
                if (ele)
                    DOMBindHelper.getDLdataSrcValFilter(ele, 'scampform', sid);
            });
            //on encounter-type change
            $('[name=encounter_cd]').change(function () {
                var t = $(this).val();
                if (t === 'E') {
                    $('.encountermanage [data-flag~=E]').removeAttr('disabled').parent().css('display', 'block');
                }
                else {
                    $('.encountermanage [data-flag~=E]').attr('disabled', 'disabled').parent().css('display', 'none');
                }
                //lookup table for location
                var lut = { 'I': 4, 'O': 3, 'E': 5 };
                if (lut[t]) {
                    $('[data-name=loctn_abbr]').attr('data-cat', lut[t]);
                }
            });
            //clear default values
            this.Transition('reset');
        }
    },
    wait: { nextStates: ['insert', 'update', 'reset'], action: function () {
        $('.encountermanage [data-flag]').attr('disabled', 'disabled');
        $('.encountermanage .enClose').removeAttr('disabled');
    }
    },
    reset: { nextStates: ['insert', 'load', 'reset'], action: function () {
        $('#frmEncounter .frmmsg').text('');
 	$('#frmEncounter .autoComplete').removeAttr('title');
        $('#frmEncounter .frmfields [data-flag]').val('');
        $('#frmEncounter .error').removeClass('error');
    }
    },
    load: { nextStates: ['wait', 'reset'], action: function () { }
    },
    insert: { nextStates: ['wait', 'reset'], action: function () {
        var target = '#frmEncounter';
        $(target).find('[data-flag]').attr('disabled', 'disabled').parent().css('display', 'none');
        $(target).find('[data-flag~=C]').removeAttr('disabled').parent().css('display', 'block');
    }
    },
    update: { nextStates: ['wait', 'reset'], action: function () {
        var target = '#frmEncounter';
        $(target).find('[data-flag]').attr('disabled', 'disabled').parent().css('display', 'none');
        $(target).find('[data-flag~=U]').removeAttr('disabled').parent().css('display', 'block');
        if ($(target).find('[name="scamp_id"]').val() === null)
            $(target).find('[name="encounter_type"]').parent().css('display', 'none');
    }
    }
};

function getPatMrn() {
    var str = window.location.pathname;
    var idx = -1;
    if (typeof str === 'string' && (idx = str.lastIndexOf('/')) !== -1 && idx < str.length - 1) {

        var res = str.substring(idx + 1);
        return res;
    }
    return '';
}

function ArrayUnionSet(targ,propf,filter,fpropf){
	var res = [];
	if(!targ || !filter)
		return res;
	var cl = {};
	for(var i=0;i<filter.length;i++){
		var fv = fpropf(filter[i]);
		if(fv && typeof cl[fv] === 'undefined')
			cl[fv] = true;
	}
	for(var i=0;i<targ.length;i++){
		var tv = propf(targ[i]);
		if(tv && cl[tv])
			res.push(targ[i]);
	}
	return res;
}

var encModalMgr;
var ddlData = { scamp: [], prov: [], loc: [], event: [], enstatus: [], encounter: [], scampform: [] };

function refreshPatientData(){
	var _mrn = getPatMrn();
	$.ajax({
	    async: false, type: 'GET', url: '/patient.ajax', data: 'mrn=' + _mrn,
	    complete: function (j, t) {
	        if (j.status === 200) {
	            try {
	                var obj = $.parseJSON(j.responseText);
	                $('.patientheader .fullname').append(toTitleCase(obj.patient.fname) + ' ' + toTitleCase(obj.patient.lname));
	                $('.patientheader .fullname').attr("data-id", _mrn);
	                if (typeof obj.patient.comment === 'string' && obj.patient.comment.length > 0) {
	                    $('div.comments p').text(obj.patient.comment);
	                    $('i.icon-chat').removeClass('none');
	                }
	                else {
	                    $('i.icon-chat').addClass('none');
	                }
	            }
	            catch (e) { }
	        }
	        else {
	            $('.patientheader .fullname').text('');
	            $('div.comments p').text('');
	        }
	        $('.patientheader .patmrn').text(_mrn + ' (MRN)');
	    }
	});
}
$(document).ready(function () {
    if (storehandler.getObject('plegend') === '0')
        $('.scamplegend').css('display', 'none');

    $('.scamplegend').click(function () {
        storehandler.setObject('plegend', 0);
        $('.scamplegend').css('display', 'none');
    });

    //$('.rollup').click(function () {
    //    var disp = $('.rollup').next().css('display');
    //    if (typeof(disp) !== 'string' || disp.indexOf('none') !== 0)
    //        return;
    //    storehandler.setObject('plegend', 1);
    //    $('.scamplegend').css('display','');
    //});

    $('.encountermanage [name=mrn]').val(getPatMrn());
	
	//get patient info
	refreshPatientData();
	
    //populate form options
    $.ajax({ async: false, type: 'POST', url: '/search.ajax', data: 'opt=0,1,2,3,4,5,6,7,8,9',
        complete: function (j, t) {
            if (j.status === 200) {
                var obj = $.parseJSON(j.responseText);
                if (obj) {
                    var arrLUT = ['scamp','prov','prov','loc','loc','loc','event','enstatus','encounter','scampform'];
                    for (var i = 0; i < obj.length; i++)
                        ddlData[arrLUT[obj[i].c]].push(obj[i]);
                }
            }
        }
    });

    //populate scamp enrollment dropdown
    ScampEnrollHandler.getSCAMPenrollment();

    //initialize datatable with patient encounter data
    etbl = $('#enclist').dataTable({
        ajax: {
            url: document.getElementById('frmEncounter').action,
            type: 'GET',
            //get patient by MRN via querystring
            data: function () {
                //var pnum = getParameterByName('PAT_NUM');
                var pnum = getPatMrn();
                if (pnum.length == 0)
                    pnum = '0';
                return 'r=pe&mrn=' + pnum;
            },
            complete: function () {
                $('body').css('cursor', 'default');
            }
        },
        columns: [
				{ 'data': 'edata', orderable: false, data: {filter: 'edata.apptid'} },
				{ 'data': 'provider' },
				{ 'data': 'encounterDate' },
				{ 'data': 'dischargeDate' },
				{ 'data': 'visitType' },
				{ 'data': 'ageMinutes' },
				{ 'data': 'location' },
				{ 'data': 'scampName' },
				{ 'data': 'encounterName',className: 'max' },
				{ 'data': 'encounterStatus',className: 'max' }
			],
        order: [[5, 'desc']],
        columnDefs: [
			{ targets: [3], visible: false }
		],
        //dom: '<"edtToolbar">frtip',
        oLanguage: {'sSearch': 'Filter:'},
            'lengthMenu': [[25, 50, -1], [25, 50, 'All']],

        createdRow: function (row, data, index) {
            $(row.children[0]).empty();
			
            if (data.edata && data.edata.apptid) {
                var a = document.createElement('A');
                a.className = 'icon-edit-1 editencounter';
                a.setAttribute('data-apptid', data.edata.apptid);
                a.setAttribute('title', 'View/Edit Encounter');
                //$(a).click(editEncounter);
                $(row.children[0]).append(a);
            }

            if (data.edata) {
                var a = createSDFurl(data.edata);
                $(row.children[0]).append(a);
                if(data.encounterLinkID){
                    var a = document.createElement('A');
                    a.className = 'icon-link-outline enlinked';
		    a.setAttribute('title','View linked encounters');
                    a.setAttribute('data-link', data.encounterLinkID);
                    $(a).click(PDetailHandler.LinkedEncounter.getEvents);
                    $(row.children[0]).append(a);
                }
                if (data.subunitID) {
                    var a = document.createElement('A');
                    a.className = 'icon-plus ensubhar';
		    a.setAttribute('title','View subunit encounters');
                    a.setAttribute('data-subid', data.subunitID);
                    a.setAttribute('data-apptid', data.edata.apptid);
                    $(a).click(PDetailHandler.SubUnit.getEvents);
                    $(row.children[0]).append(a);
                }
            }

            if (data.encounterDate)
                $(row.children[2]).text(data.encounterDate.substring(0, 16).replace('T', ' '));
            if (data.dischargeDate)
                $(row.children[2]).append(' - '+data.dischargeDate.substring(0, 16).replace('T', ' '));

            if (data.ageMinutes) {
                $('td', row).eq(4).attr('data-order', data.ageMinutes);
                $(row.children[4]).text(PDetailHandler.processAge(data.ageMinutes));
            }


            if (data.statusType && data.statusType.indexOf('NOT_SCAMP') > -1) {
                $('td', row).addClass('nonscamp');

            }
        }
    });
	
	//datatable currently selected row event listener
	$('#enclist tbody').on( 'click', 'tr', function(){
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            etbl.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
		$('.endetail').slideUp(50).css('left','').css('top','');
    });

    //add an item to the datatable toolbar for hiding non-scamp events
    //$('.edtToolbar').html('<a class="sortns">non-SCAMP sort</a><a class="NStoggle">Toggle non-SCAMP events</a>');

    //sorts by scamp/non-scamp events
    $('.sortns').click(function () {
        etbl.api().order([3, 'desc']).draw();
    });

    //bind options to input controls
    $('.sourcedOpts').each(DOMBindHelper.initSelect);
    //bind each autocomplete input with their dataset
    $('.autoComplete').each(DOMBindHelper.initAutoComplete);

    encModalMgr = StateMgr({ states: encModalStates });
    patModalMgr = StateMgr({ states: patModalStates });

    //searches: EVENT, OUTPATIENT, INPATIENT, SCAMP
    //etbl.api().column(6).data().search('EVENT').draw()
    //etbl.api().column(6).data().search('OUTPATIENT').draw()
    //etbl.api().column(2).data().search('NOT_SCAMP').draw()
    //TODO: this is a repeated function, should elsewhere
    $('.rollup').click(function (e) {
        $(this).next().slideToggle(200);
	//▲▼▲
        $(this).text($(this).text() === '\u25B2' ? '\u25BC' : '\u25B2');
    });
});
