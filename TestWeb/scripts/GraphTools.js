/**
@file Overview Plotly & D3 Graphing Toolkit 
@author Lindsay Morsillo IRCDA
@desc Easily generate "canned" multiple data series, multiple types of graphs for ePortal - Pulls data from table - using class selectors

@requires Plotly.js (and by extension include D3)
**/
var debug = true;

debugLog("LOADING GRAPHTOOLS.JS");

/*** Display data table ***/
/**
Simple show/hide so the graph can display without the data table showing, for cases with many rows of data
**/

$(document).ready(function () {
	$(".dataheader").on('click', function (e) {
		$header = $(this);
		//getting the next element
		$content = $header.next();
		//open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
		$content.slideToggle(500, function () {
			//execute this after slideToggle is done
			//change text of header based on visibility of content div
			$header.text(function () {
				//change text based on condition
				return $content.is(":visible") ? "Hide table data" : "Display table data";
			});
		});
	});
});

/*** Data Manipulation ***/
/**
Return a new array by collecting the results of the specified function
for each element in the current selection, passing in the current datum d
and index i, with the this context of the current DOM element.
@param {function}
@return {array} Flattened 
@function **/
Plotly.d3.selection.prototype.map_flat = function (f) {
	var arr = [];
	this.each(function (d, i) {
		arr[arr.length] = f.call(this, d, i);
	});
	return arr;
};

/**
Return a new nested array by collecting the results of the specified function
for each element in the current selection, passing in the current datum d
and indexes i and j with the this context of the current DOM element.
@param {function}
@return {array} Nested
@function **/
Plotly.d3.selection.prototype.map_nested = function (f) {
	var arr = Plotly.d3.range(this.length).map(function () {
			return [];
		});
	this.each(function (d, i, j) {
		arr[j].push(f.call(this, d, i, j));
	});
	return arr;
};

/**
Given a table selector, a class selector that will return the category names and a class selector that will return the corresponding data
@param {string} tableClass The selector for the table section to extract from
@param {string} categoryClass selector class for category items
@param {string} dataClass selector class for data items 
@return {object} return data as object with x values (categories), and y values (values)
 **/
function getDataSeriesFromTable(tableClass, categoryClass, dataClass) {
	//grab data from table ("tbody" graphing rows ".GrTotal" for categories ".GrCategory")
	var tds = Plotly.d3.select(tableClass).selectAll(dataClass);
	var categories = Plotly.d3.select(tableClass).selectAll(categoryClass);
	var cats = categories.map_flat(function (d, i, j) {
			return Plotly.d3.select(this).text();
		});

	// obtain array of each table cell as a number
	var vals = tds.map_flat(function (d, i, j) {
			vr = Plotly.d3.select(this).text();
			return parseInt(vr);
		});
	return {
		categories : cats,
		values : vals
	};
}
/**
Create json object out of table
@arg {string} tableId Id selector (prefixes 'tbody tr')
@returns {object} object containing array of objects with each column as property { [ {<column1name>: , <column2name>:,...<columnNname>:}, ... ] }
 **/
function tableToJson(tableId) {
	// Loop through grabbing everything
	var myRows = [];
	var $headers = $("th");
	var rowsSelector = "tbody tr";
	if (tableId.length > 0){
		rowsSelector = tableId +" "+ rowsSelector;
	}
	else {
		rowsSelector = defaultTableId +" "+ rowsSelector;		
	}
	var $rows = $(rowsSelector).each(function (index) {
			$cells = $(this).find("td");
			myRows[index] = {};
			$cells.each(function (cellIndex) {
				myRows[index][$($headers[cellIndex]).text()] = $(this).text();
			});
		});

	// Let's put this in the object like you want and convert to JSON
	var myObj = {};
	myObj.myrows = myRows;
	return myObj;
}

/**
Get data from a table into an array of objects with a property for each column
@arg {string} tableSelector tableId Id selector (prefixes 'tbody tr')
@returns  {object}
tabledata.myRows is[{ Count:"1", Date:"04-2000",Form:"Overt" }, ...]

fully json with:  JSON.stringify(tabledata);
 **/
function getDefaultData(tableSelector) {
    
    if (!tableSelector) {
        tableSelector = defaultTableId;
    }
	var tabledata = tableToJson(tableSelector);
	debugLog(tabledata);
	return tabledata;
}

/**
Put categorical data into separate bins for graphing as multiple data series
@arg {array} dataArray The array of data to separate into bins
@arg {string} the name of the object field that contains the bin names
@returns {object} {myrows:[{},{},...]}
 **/
function binData(dataArray, binFieldName) {
	/**
	Create an object, use as key:value store (bins:values)
	key=d[binField] value=array of values of that type
	 **/
	var addFieldToBin = function (d, binName, bins) {
		//make sure object has field, if not create empty erray for it
		if (typeof bins[d[binName]] == "undefined" || !bins[d[binName]] == null) {
			bins[d[binName]] = [];
		}
		bins[d[binName]].push(d);
		return bins;
	};

	var returnVal = {};
	for (var ii = 0; ii < dataArray.length; ii++) {
		returnVal = addFieldToBin(dataArray[ii], binFieldName, returnVal);
	}
	return returnVal;
}

/**
take object with bins (properties) and break into discrete arrays
@arg {arrray} binCollection binned data array to convert to discrete arrays
@arg {function} xAccessor function which will return the x value from each element
@arg {function} xAccessor function which will return the x value from each element
@returns {array}
 **/
function convertBinsToArrays(binCollection, xAccessor,yAccessor) {
	retVal = [];
	if (!xAccessor) {
		xAccessor = function (d) {
					return d.Count;
				};
	}
	if (!yAccessor) {
		yAccessor = function (d) {
					return d.Date;
				};
	}
	
	for (var arrayField in binCollection) {
		if (binCollection.hasOwnProperty(arrayField)) {
			var trace = {
				y : binCollection[arrayField].map(yAccessor),
				x : binCollection[arrayField].map(xAccessor)
			};
			retVal.push({
				arraydata : trace,
				name : arrayField
			});
		}
	}
	return retVal;
}
/** utility to return binned data for logging or other purpose 
@function **/
function getBinnedData(name) {
	var datas = getDefaultData().myrows;
	datas = binData(datas, name ? name : "");
	
	debugLog("datas");
	debugLog(datas);
	return datas;
}
/** utility to convert data series to arrays of binned data  **/
function convertToSeriesArrays(datas, xAccessor, yAccessor) {
	var arrraysOfBins = convertBinsToArrays(datas,xAccessor,yAccessor);
	debugLog("Bins:");
	debugLog(arrraysOfBins);
	return arrraysOfBins;
}
/**
Utility to provide default argument pre-ES6 style
 **/
function pick(arg, def) {
	return (typeof arg === "undefined" ? def : arg);
}
/** utility - is arg defined? **/
function isDefined(arg) {
	if (typeof arg == "undefined") {
		return false;
	} else {
		return true;
	}
}
/** utility - does arg have a value? **/
function hasValue(arg) {
	if (isDefined(arg)) {
		//null or undefined?
		if (arg == undefined) {
			return false;
		} else {
			return true;
		}
	} else {
		return false; //not defined
	}
}

/**
Debugging tool, nothing if debug is not defined
 **/
function debugLog(output) {
	if (typeof debug !== "undefined") {
		if (debug) {
			console.log(output);
		}
	}
}

/**
Find the max of an array, skipping nulls to avoid math.max.apply(blah,[2,,5,6,7]) // NaN
**/
function arrayMax(arr) {
  var len = arr.length, max = -Infinity;
  while (len--) {
    if (arr[len] > max) {
      max = arr[len];
    }
  }
  return max;
};


/*** Graph defaults attribute ***/
var defaultTableId = "#tabled";
var defaultXAxisClass = "x_axis";
var defaultYAxisClass = "y_axis";
var title = "";
var defaultChartDiv = "chart";
var defaultLocation = defaultChartDiv + "1";

/**ColorPalettes **/
var lineChartBlueYellowPairs = ["#ffd99a", "#225ea8", "#ffdb58", "#9dc4f4", "#ffbf58", "#257294", "#ff9658", "Cyan", "#ffa719", "#61a1f3", "#ffce19", "#8ddc86", "#ff6e19", "#ffe99a", "#2a82f2", "#ff9e00"];
/* var pieChartMix = ["RoyalBlue", "MediumSeaGreen", "MediumOrchid", "LightCoral", "DarkKhaki", "LightSeaGreen", "GreenYellow", "SteelBlue", "LightSlateGrey", "Thistle", "MediumSpringGreen", "PapayaWhip", "PaleTurquoise", "Coral", "Lime", "Cyan", "PowderBlue",
    "NavajoWhite", "Gainsboro", "MediumVioletRed", "RebeccaPurple", "FireBrick", "Tomato", "LemonChiffon", "MediumSpringGreen", "LightSkyBlue", "GoldenRod"];
*/
var pieChartMix = ["#c19e9e", "#a8c5a8", "#c995ce", "#878787", "#f9d961", "#6ef96e", "#e97878", "#9595bc", "#69f3cc", "#f3b884", "#64758f", "#FF6EB0", "#B7695C", "#CDBB79", "#51A39D", "#96C0CE",
    "NavajoWhite", "Gainsboro", "MediumVioletRed", "RebeccaPurple", "FireBrick", "Tomato", "LemonChiffon", "MediumSpringGreen", "LightSkyBlue", "GoldenRod"];

var barchartTriadPale = ["#ffe6be", "#735ac3", "#a1bfb8", "#e7c792", "#8f8ab2", "#7ca69c", "#cea768", "#736da0", "#5f9688", "#b2863f", "#59518f", "#297461", "#3f367c", "#ff9e00", "#438675", "#afacc7", "#6d9b34", "#aa9739", "#3914AF", "#80E800", "#FF5C00", "#4E8D00"];
var seqMultiLineBlueYellow = ["#afeeee", "#7fcdbb", "#41b6c4", "#1d91c0", "DodgerBlue", "RoyalBlue", "LightYellow", "Moccasin", "Tan","Khaki", "Yellow", "Gold"];
var seqVioletPalette = ["#d6c8e4","#b0e0e6","#dadaeb", "#bcbddc", "#9e9ac8", "#887caf","#6a51a3"];
var blues = ['#9ecae1', '#6baed6', "CadetBlue", "SkyBlue", '#5CB8ED', '#5198DB', '#1E7CCE', '#007dc1', "RoyalBlue", '#3163AF', "SteelBlue"];
var greens = ['#BCFCB8','#A9F9BB','#97C997','#a1d99b','#BCFFB5','#41ab5d','#238b45','#2CCC6C','#30A55F'];
var greys = ["Gainsboro","LightGray","Silver","DarkGray","DimGray","Gray","LightSlateGray","SlateGray","DarkSlateGray"];
var oranges = ['#FFF1D1', '#FCE8BD', '#FFE4AA', '#FFDF96', '#FFD57A', '#FFCE66', '#FFD756', '#FFC43A', '#a63603'];
var purples = ['#D1D1FC',  "Thistle","Orchid", "Fuchsia", '#9e9ac8', "MediumOrchid", '#6a51a3', "DarkMagenta", "RebeccaPurple"];
var reds = ["LightSalmon", '#fc9272', '#fb6a4a', '#ef3b2c', '#cb181d', "FireBrick", "Maroon"];
var barChartQuadDark = ["#ffe6be", "#e7c792", "#cea768", "#b2863f", "#a1bfb8", "#7ca69c", "#5f9688", "#438675", "#afacc7", "#8f8ab2", "#736da0", "#59518f", ];
var selectedColors2x12 = selectN(2, [createHues(240, 3, 1, 0.50), createHues(300, 5, 1, 0.50), createHues(360, 5, 1, 0.50), createHues(60, 5, 1, 0.50), createHues(120, 5, 1, 0.50), createHues(180, 5, 1, 0.50)]);

/** palettes list, vaguely grouped by 1st hues
most palettes go from light to dark
**/
/* IE is unhappy about this...
var palettes = {
    pieChartMix, lineChartBlueYellowPairs, barchartTriadPale, seqMultiLineBlueYellow, 
     oranges, reds, barChartQuadDark, blues,purples, seqVioletPalette, greens, greys, selectedColors2x12
};
*/
//Select n items from each of m arrays, starting at start=0 or specified value
function selectN(n, arrays, start) {
	if (!start) {start=0;}
	retVal = [];
	for (ii =0; ii<arrays.length;ii++){
		retVal = retVal.concat(arrays[ii].slice(start,n+start));
	}
	return retVal;
}

/** create color palette in a given hue 
@arg {number} hue 360 = 0 = red, 120 = green, 240=blues (think colorwheel)
@arg {number} count is number of hue gradations, 
@arg {number} maxSat is maximum saturation, 
@arg {number} lightness is constant to use (0.5 is neutral) **/
function createHues(hue,count, maxSat, lightness)
{
	var retVal = []; 
	for (sat = 0.1; sat < maxSat; sat = sat + (1/count)) {
		retVal.push(Plotly.d3.hsl(hue,sat,lightness).toString());
	}
	return retVal;
}

/** Debug/Test features **/

/** Dump and insert palettes to a table of id Palette, with corresponding HSL notation **/
function dumpPalettes(palettesIn) {
    if (palettesIn) { paletteList = palettesIn }
    else {paletteList = palettes}

    var $table = $("#Palette").find('tbody');
	var $row = $table;
	for (var prop in paletteList) {
        if (!paletteList.hasOwnProperty(prop)) continue;
		paletteList[prop].name = prop;
		$row = $table.append("<tr>").append("<td>" + prop +"["+ paletteList[prop].length +"]"+"</td>").append(tableRowofColors(paletteList[prop], $row)).append("</tr>");
	}
} 
/** return an html row of colors with HSL values listed **/
function tableRowofColors(array,$row) {
	for (var jj=0;jj < array.length; jj++ ){
		var colorHSL = Plotly.d3.rgb(array[jj]).hsl();
		var colorString = "H:" + colorHSL.h.toPrecision(3) + ",S:" + colorHSL.s.toPrecision(2) + ",L:" + colorHSL.l.toPrecision(2);
		var row = "<td style='width:80px; background-color:" + array[jj] + " '>" + colorString;
		$row.append(row);
	}		
	return $row;
}
/* end of test functions */

var defaultLayout = {
};

var xAccessorFunction;
var yAccessorFunction;
/** @default **/
var dataBinsDefault = {
	binName: "Form",
	//bins is a funtion to return arrays of binned data to graph
	bins: function(name) {
		var binnedData = getBinnedData(typeof name !== "undefined" ? name : "");
		var retBins;
		if ($.isFunction(xAccessorFunction) || $.isFunction(yAccessorFunction)) {
			retBins = convertToSeriesArrays(binnedData, xAccessorFunction,yAccessorFunction);
		} else {
			retBins = convertToSeriesArrays(binnedData);
		}
		return retBins;
	}
};
/**@default **/
var graphOptionsDefault = {
	chartLocation:defaultLocation,
	dataBins:dataBinsDefault,
	layoutOptions:defaultLayout,
	traceOptions: undefined,
  tableSelector:defaultTableId
};

function setResizeHandler(graphLocation) {
    var gd3 = Plotly.d3.select('body').select('#'+graphLocation).node();
    window.addEventListener('resize', function() { 
				Plotly.Plots.resize(gd3); 
			});
}

/** Set title for a graph **/
function chartTitle(name) {
	if (hasValue(name) && name.length > 0) {
		title = name;
	}
	return title;
}

/** make sure things that we are counting on are set - like where the graph should go 
@returns {object} minimal graphOptions object or minimal with given object merged
**/
function validateGraphOptions(graphOpts) {
	if (isDefined(graphOpts)) {
	    if (!graphOpts.hasOwnProperty("chartLocation") || graphOpts.chartLocation.length < 1 ) {
		    graphOpts.chartLocation = defaultLocation;
		} 
	    if (!graphOpts.hasOwnProperty("tableSelector") || graphOpts.tableSelector.length < 1) {
		    graphOpts.tableSelector = defaultTableId;
	    }
	    //make sure dataBins are valid - add them if not present
	    if (graphOpts.hasOwnProperty("dataBins")) {
	        if (!graphOpts.dataBins.hasOwnProperty("bins")) {
	            graphOpts.dataBins["bins"] = dataBinsDefault.bins;
	        }
	        if (!graphOpts.dataBins.hasOwnProperty("xAccessorFunction")) {
	            xAccessorFunction = graphOpts.dataBins.xAccessorFunction;
	        }
	        if (!graphOpts.dataBins.hasOwnProperty("yAccessorFunction")) {
	            yAccessorFunction = graphOpts.dataBins.yAccessorFunction;
	        }
	    } else /* no dataBins provided, add them */
	    {
	        graphOpts["dataBins"] = dataBinsDefault;
	    }
	    if (!graphOpts.hasOwnProperty("layoutOptions")) {
	        graphOpts.layoutOptions = {};
	    }
	    if (!graphOpts.hasOwnProperty("traceOptions")) {
	        graphOpts.traceOptions = undefined;
	    }
	} else /*no options provided, use default */
	{
	    graphOpts = graphOptionsDefault;
	}

	return graphOpts;
}

/** object defining defaults for bar chart (see plotly docs) 
*@default 
**/
var barChartDef = {
	orientation : "h",
	marker : {
		color : barchartTriadPale,
		width : 1
	},
	type : "bar"
};

/** create a bargraph  **/
function ShowBarGraph(graphOptions) {
	graphOptions = validateGraphOptions(graphOptions);
	var bins;
	var dataBins=false;
	var graphData = [];
	//call binning function to return arrays of data series
	bins = graphOptions.dataBins.bins(graphOptions.dataBins.binName);
	
	var trace1 = {};
	var xdata, ydata;
	var colorPalette = barchartTriadPale;
	//if graphoptions specified data.bins for multiple data series
	if (bins && bins.length > 0){
		for (var ii = 0; ii < bins.length; ii++) {
			trace1 = {}; 
			if (graphOptions.dataBins.hasOwnProperty("x")){
				xdata = graphOptions.dataBins.x;
			} else { 
				xdata = bins[ii].arraydata.x;
			}
			if (graphOptions.dataBins.hasOwnProperty("y")){
				ydata = graphOptions.dataBins.y;
			} else { 
				ydata = bins[ii].arraydata.y;
			}
			if (isDefined(graphOptions) && hasValue(graphOptions.marker) && hasValue(graphOptions.marker.color)) {
				colorPalette = graphOptions.marker.color;
			}
				
			//create trace by merging defaults with data and customizations
			trace1 = $.extend(true, trace1, barChartDef, {
					x : $.isFunction(xdata)?xdata(bins[ii]):xdata, 
					y : $.isFunction(ydata)?ydata(bins[ii]):ydata,
					name : bins[ii].name,
					marker: {
						color : $.isArray(colorPalette)? colorPalette[ii]:colorPalette //at this point out of our hands...
					},
				});
			if (isDefined(graphOptions) && hasValue(graphOptions.traceOptions)) {
				trace1 = $.extend(true, trace1, graphOptions.traceOptions);
			}
			graphData[ii] = trace1;
		}
	}
	else {
		//dataBins.bins list is non existent or 0
		if (graphOptions.dataBins.hasOwnProperty("x")){
			xdata = graphOptions.dataBins.x;
		} 
		if (graphOptions.dataBins.hasOwnProperty("y")){
			ydata = graphOptions.dataBins.y;
		}
		//no data, leave
		if (!($.isFunction(xdata)) && xdata.length == 0 && !($.isFunction(ydata)) && ydata.length==0) {
			return;
		}
		//create trace by merging defaults with data and customizations
		trace1 = $.extend(true, trace1, barChartDef, {
				x : $.isFunction(xdata)?xdata(xdata):xdata, 
				y : $.isFunction(ydata)?ydata(ydata):ydata,
				marker: {
					color : seqMultiLineBlueYellow//at this point out of our hands...
				},
			});
		if (isDefined(graphOptions) && hasValue(graphOptions.traceOptions)) {
			trace1 = $.extend(true, trace1, graphOptions.traceOptions);
		}
		graphData[0] = trace1;
	}
	var layOutMulti = {};
	if (bins.length > 1) {
		layOutMulti = {
			barmode : "multi",
		};
	}
	var layout = $.extend(defaultLayout, {
			title : chartTitle()
		}, layOutMulti);
	//merge defaults with cusomizations for layout
	if (isDefined(graphOptions) && hasValue(graphOptions.layoutOptions)) {
		layout = $.extend(true, layout, graphOptions.layoutOptions);
	}

	var graphLocation = isDefined(graphOptions) && graphOptions.chartLocation ? graphOptions.chartLocation : defaultLocation;
	
	setResizeHandler(graphLocation);

	//call plotly with trace def and layout
	Plotly.newPlot(graphLocation, graphData, layout, {
		displayModeBar : false
	});
	

}
/** @default **/
var lineChartDef = {
	marker : {
		color : seqMultiLineBlueYellow,
		width : 1
	},
	type : "scatter",
	line : {
		shape : "spline"
	}
};

/** Show a line chart of default data here - one for each column 
@arg {object} graphOptions customize with this... **/
function ShowLineCharts(graphOptions) {
	graphOptions = validateGraphOptions(graphOptions);
	var hasDataBins = hasValue(graphOptions.dataBins);
	var bins;

	if (hasDataBins) {
		if ($.isFunction(xAccessorFunction) || $.isFunction(yAccessorFunction)) {
			bins = convertToSeriesArrays(getBinnedData(graphOptions.dataBins.binName), xAccessorFunction,yAccessorFunction);
		} else {
		bins = convertToSeriesArrays(getBinnedData(graphOptions.dataBins.binName));
		}
	}
	//check to see if function or data array was provided....
	var xdata, ydata;	
	var traces = [];
	//create traces by merging defaults with data and customizations
	for (var ii = 0; ii < bins.length; ii++) {
		if (graphOptions.dataBins.hasOwnProperty("x")){
			xdata = graphOptions.dataBins.x;
		} else { 
			xdata = bins[ii].arraydata.x;
		}
		if (graphOptions.dataBins.hasOwnProperty("y")){
			ydata = graphOptions.dataBins.y;
		} else { 
			ydata = bins[ii].arraydata.y;
		}
		var trace = {};
		var mergeIn = {
			x : $.isFunction(xdata)? xdata(bins[ii],ii): xdata, // bins[ii].arraydata.x,
			y : $.isFunction(ydata)? ydata(bins[ii],ii): ydata, // bins[ii].arraydata.y,
			name : bins[ii].name
		};
		trace = $.extend(true, trace, lineChartDef, mergeIn); //extend

		if (hasValue(graphOptions.traceOptions) && $.isArray(graphOptions.traceOptions)) {
			trace = $.extend(true, trace, graphOptions.traceOptions[ii]);
		} else if (hasValue(graphOptions.traceOptions)) {
			trace = $.extend(true, trace, graphOptions.traceOptions);
		}
		traces[ii] = trace;
	}; //end of for
	var layout = $.extend(defaultLayout, {
			title : chartTitle()
		});
	//merge defaults with cusomizations for layout
	if (hasValue(graphOptions.layoutOptions)) {
		layout = $.extend(true, layout, graphOptions.layoutOpts);
	}
	
	var graphLocation = graphOptions.chartLocation ? graphOptions.chartLocation : defaultLocation;
	
	setResizeHandler(graphLocation);
		
	//call plotly with trace def and layout
	Plotly.newPlot(graphLocation, traces, layout, {
		displayModeBar : false
	});
}
/** @default **/
var pieChartDef = {
	type : "pie",
	direction : "clockwise",
	marker : {
		colors : pieChartMix,
	}
};
/** Create a pie chart 
@arg {object} graphOptions customize with this... **/
function ShowPieChart(graphOptions) {
	graphOptions = validateGraphOptions(graphOptions);
	var hasDataBins = graphOptions.hasOwnProperty("dataBins");
	var bins;

	if (hasDataBins) {
		bins = convertToSeriesArrays(getBinnedData(graphOptions.dataBins.binName));
	}
	var trace1 = {};
	var xdata = null, ydata = null;
	if (graphOptions.dataBins.hasOwnProperty("x")){
		xdata = graphOptions.dataBins.x;
	}
	if (graphOptions.dataBins.hasOwnProperty("y")){
		ydata = graphOptions.dataBins.y;
	}
	// $.isFunction(xdata)? xdata() : xdata,
	var mergeIn = {
		values : xdata != null ? xdata : bins[0].arraydata.x,
		labels : ydata != null ? ydata : bins[0].arraydata.y
	};

	trace1 = $.extend(true, trace1, pieChartDef, mergeIn);

	if (hasValue(graphOptions.traceOptions)) {
		trace1 = $.extend(true, trace1, graphOptions.traceOptions);
	}

	var graphData = [trace1];
	var layout = $.extend(defaultLayout, {
			title : chartTitle()
		});
	//problem with extra stuff if in sequence of other graphs...
	delete layout.xaxis;
	delete layout.yaxis;
	
	//merge defaults with cusomizations for layout
	if (hasValue(graphOptions.layoutOptions)) {
		layout = $.extend(true, layout, graphOptions.layoutOptions);
	}

	var graphLocation = graphOptions.chartLocation ? graphOptions.chartLocation : defaultLocation;
	
	setResizeHandler(graphLocation);
	//call plotly with trace def and layout
	Plotly.newPlot(graphLocation, graphData, layout, {
		displayModeBar : false
	});

};

		function ShowRunningChart(locationdiv) {
      $('#' + locationdiv + ' .GrChart').prop('id', locationdiv + 'Chart');
      var chartdiv =  locationdiv + 'Chart';
			var charttitle = $('#' + locationdiv + ' h3').text();
			var counttitle = $('#' + locationdiv + ' .GrCountTitle').text();
			var tablediv = '#' + locationdiv + ' .GrTable';
			
			var data = getDataSeriesFromTable(tablediv, ".GrCategory", ".GrTotal"); 
			var ymaxval = Math.max.apply(Math, data.values) + (Math.max.apply(Math, data.values) * .25);
			var trace1 = {
				x:data.categories,
				y:data.values,
				type: 'bar',
				name: counttitle,
				mode: 'text',
				text: data.values,
				textposition: 'outside'
			};

			var data2 = getDataSeriesFromTable(tablediv, ".GrCategory", ".GrRunning");
			var y2maxval = Math.max.apply(Math, data2.values) + (Math.max.apply(Math, data2.values) * .25);
			var trace2 = { 
				x: data2.categories, 
				y: data2.values, 
				text: data2.values,
				textposition: "top center",
				mode: 'lines+markers+text',
				yaxis: 'y2',
				name: 'Running Total'
			};

			var data3 = [ trace1, trace2 ];

			var layout = {
				legend: {
					x: .75,
					y: .50,
					traceorder: 'reversed'
				},
				title: charttitle,
				yaxis: {
					domain: [0, 0.45],
					title: counttitle,
					range: [0, ymaxval ]
				},
				yaxis2: {
					title: 'Running Total',
					anchor: 'x',
					domain: [0.60, 1],
					range: [0, y2maxval ]
				}
			};
      
			Plotly.newPlot(chartdiv, data3, layout, {displayModeBar: false});
      
    	setResizeHandler(chartdiv);
		};
    
