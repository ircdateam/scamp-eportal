﻿$(document).ready(function () {
    //$.get("encounterstatus.ajax", plotData)
    
    updateRecentEncounters();
	updateRecentPatients();
	
    //if (!window.patModalMgr)
    //    window.patModalMgr = StateMgr({ states: patModalStates });
    
	 $(window).bind('storage', function (e) {
		 if(e.originalEvent.key == 'encounter')
			 updateRecentEncounters();
		 else if(e.originalEvent.key == 'patient')
		     updateRecentPatients();
	 });
	
    // get rid of this.... v1.3, can be removed v1.4.  Unnecessary with new modals
    //$('.quikinfo .lists').click(function (e) {
    //    if (!e.target)
    //        return;
    //    var _mrn = e.target.getAttribute('data-mrn');
    //    if (typeof _mrn !== 'string')
    //        return;
    //    e.preventDefault();
    //    patAjax.loadPatient(_mrn);
    //});

});

function updateRecentEncounters()
{
	//var tmp;
    //if ((tmp = serializeLastView(storehandler.getObject('encounter')))) {
    //    getViewedBase('recentlyviewedencounters.ajax', 'appts='.concat(tmp), '.quikinfo .lists', 'enlastview');
    //}
}

function updateRecentPatients()
{
	var tmp;
    if ((tmp = serializeLastView(storehandler.getObject('patient')))) {
        getViewedBase('recentlyviewedpatients.ajax', 'pats='.concat(tmp), '.quikinfo .lists', 'patlastview');
    }
     
}

function serializeLastView(obj) {
    if (!obj || typeof obj.i !== 'number' || !(obj.a instanceof Array))
        return '';
    var len = obj.a.length;
    var i = 0;
    var idx = obj.i;
    var str = '';
    //loop backward through array in object using last modified index and build string
    while (i < len) {
        if (typeof obj.a[idx] === 'string') {
            if (str.length > 0)
                str = str.concat(',');
            str = str.concat(obj.a[idx]);
        }
        idx--;
        if (idx < 0)
            idx = len - 1;
        i++;
    }
    return str;
}

function getViewedBase(aurl,strdata,container,targetclass) {
    $.ajax({
        url: aurl, type: 'GET', data: strdata,
        complete: function (j, t) {
            var obj = $.parseJSON(j.responseText);
            $('.'.concat(targetclass)).remove();
            var el = $(obj.markup).addClass(targetclass);
            $(container).append(el);
            // added v1.3 so they are, well, bound 
            bindModalControls();
        }
    });
}

//function pieChart(data) {
//    j = $.parseJSON(data);
//    var d = eval(j.markup);
//    var ctx = document.getElementById("chart1").getContext("2d");
//    var thechart = new Chart(ctx).Doughnut(d, {
//        animateScale: true,
//        segmentShowStroke : true,
//        segmentStrokeWidth : 2,
//        segmentStrokeColor : "#fff",
//        percentageInnerCutout : 50,
//        labels: ["January", "February", "March", "April", "May", "June", "July"]
//    });


//    var ctx = document.getElementById("chart2").getContext("2d");
//    var thechart = new Chart(ctx).Doughnut(d, {
//        animateScale: true,
//        segmentShowStroke: true,
//        segmentStrokeWidth: 2,
//        segmentStrokeColor: "#fff",
//        percentageInnerCutout: 50,
//        labels: ["January", "February", "March", "April", "May", "June", "July"]
//    });
//}

//function plotData(data) {
//    //if plot target doesn't exist, don't render plot
//    if(!document.getElementById('chart1'))
//    	return;
	
//    j = $.parseJSON(data); 
//    var d = eval(j.markup);
//    var plot2 = jQuery.jqplot('chart1', [d],
//	{
//	    seriesDefaults: {
//	        renderer: jQuery.jqplot.DonutRenderer,
//	        rendererOptions: {
//	            // Turn on filling of slices.
//	            fill: true,
//	            showDataLabels: true,
//                dataLabels: 'percent',
//	            //dataLabelFormatString: '%n',
//                // Add a margin to seperate the slices.
//                thickness:120,
//	            sliceMargin: 0,
//	            // stroke the slices with a little thicker line.
//	            lineWidth: 1
//	            //shadowOffset: 4,
//	            //shadowDepth: 5,
//	            //shadowAlpha: 0.08
//	        }
//	    },
//	    //seriesColors: ['#0F42CD', '#072474', '#374F8F', '#5468A0', '#19388C', '#3C4660'],
//	    legend: { show: true, location: 's' },
//	    highlighter: {
//	        show: true,
//	        formatString: '%s',
//	        tooltipLocation: 'sw',
//	        sizeAdjust: 24,
//	        useAxesFormatters: false
//	    },
//	    title: 'Encounters by Status, last 120 Days'
//	});
//}
