﻿var filetable;  //DataTable jQuery object
var intervalID = -1; //Extract watch interval id

$(document).ready(function () {

    $('input:file').change(function (e) {
        $("#uploadresults").html("");
        $("#importresults").html("");

        if ($(this).val()) {
            $(this).parent().find('input:submit').removeAttr('disabled');
        }
    });

    $('input:file').submit(function (e) {
        $('.installbutton').attr('disabled', 'disabled');
    });

    $('#extractstart').click(function (event) {
        event.preventDefault();

        $('#extractstart').prop('disabled', true);
        $('#extractwaitwindow').show();

        extract(ExtractOperation.Start, extractUpdate);
        startExtractWatch();
    }).prop('disabled', true);

    startExtractWatch(); //In the event there's an extract already running.

    $('#extractdelete').click(function (event) {
        event.preventDefault();

        var selected = $('.deletefile:checked');

        if (selected.length < 1) {
            alert("You need to select one or more files to delete.");
            return;
        }

        var files = selected.map(function (index, obj) { return obj.value; }).toArray(); //Strip the DOM, make selected an array of filenames we need to delete.
        deletefiles(files);
    });

    $.fn.dataTable.ext.errMode = 'throw'; //Don't alert a message if DataExtracts are not set up.

    filetable = $('#filelist').dataTable({
        'autoWidth': false,
        'ajax': {
            'url': 'requestextract.ajax?req=' + ExtractOperation.RequestCompletedFiles,
            'dataSrc': 'files'
        },
        'columns': [{
            'targets': 0,
            'render': function (data, type, row) { return '<a href="' + row.filename + '" target="_blank">' + row.filename + '</a>' },
            'order': 'desc'
        },
            {
                'targets': 1,
                'render': function (data, type, row) { return '<input type="checkbox" class="deletefile" value="' + row.filename + '"/>' },
                'data': null
            }],
        'searching': false,
        'paginate': false,
        'dom': '<t>'
    });


});

var ExtractOperation =
{
    'Start': '1',
    'RequestCompletedFiles': '2',
    'RequestStatus': '3',
    'Delete': '4'
};

var ExtractStateCode =
{
    'NoneRunning': 0,
    'Extracting': 1,
    'PreparingFile': 2,
    'Finished': 3
};

function startExtractWatch() {
    if (intervalID < 0) {
        intervalID = setInterval(function () { extract(ExtractOperation.RequestStatus, extractUpdate); }, 1000);
    }
}

function stopExtractWatch() {
    clearInterval(intervalID);
    intervalID = -1;
    $('#extractstart').prop("disabled", false);
}

function extractUpdate(statusObj) {
    switch (statusObj.statecode) {
        case ExtractStateCode.Extracting:

            $('#extractstatus').html('Extracting...').effect('highlight');
            $('#extractwaitwindow').show();
            break;
        case ExtractStateCode.PreparingFile:
            $('#extractstatus').html('Preparing File...').effect('highlight');
            $('#extractwaitwindow').show();
            break;
        case ExtractStateCode.NoneRunning:
            if (statusObj.error && statusObj.errormessage) {
                $('#extractstatus').html('No extract is running. An error has been reported: ' + statusObj.errormessage).effect('highlight');
                $('#extractwaitwindow').show();
            } else {
                $('#extractstatus').html('No extract is running.').effect('highlight');
                $('#extractwaitwindow').hide();
            }
            stopExtractWatch();
            break;
        case ExtractStateCode.Finished:
            $('#extractstatus').html('Done. Your extract file is <a href="' + statusObj.extractedfilename + '">' + statusObj.extractedfilename + '</a>').effect('highlight');
            filetable.api().ajax.reload(); //Reload table.
            stopExtractWatch();
            break;
        default:
            if (statusObj.errormessage) {
                $('#extractstatus').html('<div>An error has been reported: ' + statusObj.errormessage + '</div>').effect('highlight');
                $('#extractwaitwindow').show();
            }
            stopExtractWatch();
            //Note! stopExtractWatch clears disabled on the extract button. We need to disable it after calling stopExtractWatch.

            $('#filelist').hide();
            $('#extractstart').hide();
            $('#extractdelete').hide();

            break;
    }
}

function deletefiles(files) {
    if (files.length < 1) {
        alert("You need to select one or more files to delete.");
        return;
    }

    $.ajax({
        type: 'GET',
        response: 'json',
        data: {
            req: ExtractOperation.Delete,
            files: files
        },
        url: 'requestextract.ajax',
        complete: function (j, t) {
            filetable.api().ajax.reload(); //Reload table.
        }
    });
}

function extract(operation, callback) {
    $.ajax({
        type: 'GET',
        response: 'json',
        data: {
            req: operation
        },
        url: 'requestextract.ajax',
        complete: function (j, t) {
            if (j.statusText === "error" || j.statusText === "Unknown") {
                stopExtractWatch();
                if (callback) { callback({ 'errormessage': 'There was an error communicating with the server. There may be network issues.' }); }
            } else {
                var obj = $.parseJSON(j.responseText);
                if (callback) { callback(obj); }
            }
        }
    });
}