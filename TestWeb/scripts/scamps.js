﻿var menutimer;
var outok = true;
var isDirty = false;
var initFormVal;
var view_mode_manual = false;

$(document).ready(function () {

    $('body').addClass('wizard');

    $('div.panel').first().addClass('first');
    $('div.panel').last().addClass('last');

    // do all this stuff when DOM is ready
    var tot = 0;

    $('.content').on('click', function (e) {
        $("ul.menu").hide();
    });
    $(".menu .button").on("click", function (e) {
        $("ul.menu").hide();
        $('#form_action').val($(this).attr('class'));
    });
    $(".sdf-menu").on('click', function (e) {
        e.preventDefault();
        if ($(".toolbar ul.menu").is(":visible")) {
            $(".toolbar ul.menu").hide();
        }
        else {
            outok = false;
            menutimer = window.setTimeout(function () { outok = true; }, 1500);
            $(".toolbar ul.menu").show();
        }
    });


    $(window).on('beforeunload', function () {
        if ($('form.login').length > 0)
            return;

        var curr = $(document.forms[0]).serialize();
        if (curr.substring(curr.indexOf('&_ID')) !== initFormVal.substring(initFormVal.indexOf('&_ID')))
            return 'Form has been changed. Close without saving ?';
    });

    $('.closesdf').click(function () {

        //var url = '/dashboard';
        //var tmp = qs('url');

        //if (tmp.length > 0) {
        //    url = window.location.origin + tmp;
        //}
        //else  //if ((tmp = document.referrer) && tmp.length > 0 && tmp.indexOf('/login?') === -1) 
        //{
        //    //url = tmp;
        //    url = localStorage.getItem("lastpage");
        //}
        window.location.href = returnPage();
    });

    $('input.externaldata').click(function (event) {
        event.preventDefault();
        var message = '<table id="externaldatatable" class="display"><thead><tr><th class="narrowColumn">Import?</th><th>Name</th><th>Question</th><th>Value</th><th>Answer</th></tr></thead><tbody></tbody></table>';
        modalMessage("External Data Import", message, "info", function () {
            hideModal();
            updateExternalData();
        });

        $('.modal').css('width', '500px');

        // add the ok button
        //$('.modal').removeAttr('style');  <--- this really messes up Jon's code.  
        $('.buttons').append('<input type="submit" value="OK" class="ok"/>');

        wireExternalData($(this).data('scamps-mrn'), $(this).data('scamps-apptid'), $(this).data('scamps-har'), $(this).data('scamps-form'));

    });

    $('.panelNavSwitch').click(function (event) {
        event.preventDefault();
        $('.panelNavContainer').toggleClass('show');
        $('.panelNavSwitch').toggleClass('icon-angle-circled-left icon-angle-circled-right');
    }).addClass('icon-angle-circled-left');

    //======== show/hide panels ========

    //initializes the validation widget
    //ValidationHandler.init();
    //initializes the trigger scripts for elements and panels
    //evaluatorHandler.init();
    //determines which panels should be displayed/expanded
    //init_panels();


    //=== IE is a bad, bad browser
    $('input').focus(function () {
        if ($(this).val() == '') $(this).val('').select();
    });

    $(window).scroll(function (event) {
        var nav = $('.panelNavContainer');
        if (nav.css('position') != 'fixed' && nav.offset().top - $(document).scrollTop() <= $('.toolbar').height()) {
            nav.css({
                'position': 'fixed',
                'top': $('.toolbar').height() + 'px'
            });
        } else if (nav.css('position') != 'absolute' && $(document).scrollTop() < $('.stub').height()) {
            nav.css({
                'position': 'absolute',
                'top': 'auto'
            });
        }
    });

    //$('<div />').addClass('progress').html('&nbsp;').appendTo($('.nextbar'));
    $('.panel').on('click', function (e) { if ($(e.target).parents('.nextbar').length == 0) evaluatorHandler.makeCurrent(e.currentTarget, true); });

    var viewmode = $.parameter['viewmode'];
    if (typeof viewmode == 'string') {
        switch (viewmode) {
            case "O":
            case "0":
            case "open":
                $('body').removeClass('wizard').addClass('displayall');
                openAll();
                break;
            case "N":
            case "1":
            case "normal":
                $('body').removeClass('displayall').addClass('wizard');
                break;
            case "V":
            case "2":
            case "validate":
                //errorsOnly();
                break;
        }

    }
    // === bind keyboard shortcuts ===
    //$(document).keyup(keyshortcuts);
    $(document).keydown(keyshortcuts);
    //document.addEventListener('keyup', keyshortcuts, false);

    // ==== select form view state ====
    $("#formview").on("change", function () {
        switch ($(this).val()) {
            case "0":
                $('body').removeClass('displayall').addClass('wizard');
                break;
            case "1":
                openAll();
                $('body').removeClass('wizard').addClass('displayall');
                break;
            case "2":
                // form validation and hide function  
                //errorsOnly();
                break;
        }
        $(".toolbar .menu").hide();
        evaluatorHandler.updatePanelNavigation();
    });

    $(".icon-flag").on("click", function (e) {
        $(".toggle").toggle();
        if ($(".icon-flag").hasClass("disabled")) {
            $(".icon-flag").removeClass("disabled");
        }
        else {
            $(".icon-flag").addClass("disabled")
        }
    });
    //====== compound table calculation =======
    $(".complex input, .complex select").on("blur", function () {

        var classList = $(this).attr('class').split(/\s+/);
        var target = classList[0];
        var rowclass = '';
        var colclass = '';
        for (var i = 0; i < classList.length; i++) {
            if (classList[i].indexOf('_row_') > -1) {
                rowclass = classList[i];
            }
            if (classList[i].indexOf('_col_') > -1) {
                colclass = classList[i];
            }
        }

        tot = 0;
        if (rowclass.length > 0) {
            $('.' + rowclass).each(function (i) {
                var tmp = parseInt($(this).val());
                if (!isNaN(tmp)) {
                    tot += tmp;
                }
            });

            $('.' + rowclass).children("span").html(tot);
        }

        tot = 0;
        if (colclass.length > 0) {
            $('.' + colclass).each(function (i) {
                var tmp = parseInt($(this).val());
                if (!isNaN(tmp)) {
                    tot += tmp;
                }
            });

            $('.' + colclass).children("span").html(tot);
        }

        // == now figure out the triggers ==
        var low;
        var high;
        tot = 0;
        var i = 0;
        var ii = 0;
        var a = 0;
        var av = 0;
        var over = false;
        var under = false;
        var allempty = true;
        var anyempty = false;
        if (target != undefined) {
            var show = false;
            try {
                var tt = "table." + target;
                var trigger = parseFloat($(tt).attr("data-value"));
                var op = $(tt).attr("data-operator");

                tt = "input." + target + ", select." + target;
                $(tt).each(function (e) {
                    i++;
                    var tmp = $(this).val();
                    if (!isNaN(parseFloat(tmp))) {
                        allempty = false;
                        ii++;
                        tmp = parseFloat(tmp);
                        tot += tmp;
                        if (isNaN(low)) { low = tmp; }
                        if (isNaN(high)) { high = tmp; }
                        if (tmp < low) { low = tmp; }
                        if (tmp > high) { high = tmp; }
                        if (tmp < trigger) { under = true; }
                        if (tmp > trigger) { over = true; }
                    } else {
                        anyempty = true;
                    }
                });
                if (tot == 0 || i == 0) {
                    a = 0;
                }
                else {
                    a = tot / i;
                }
                if (tot == 0 || ii == 0) {
                    av = 0;
                }
                else {
                    av = tot / ii;
                }
                switch (op) {

                    case "allempty":
                        if (allempty) { show = true; }
                        break;
                    case "anyempty":
                        if (anyempty) { show = true; }
                        break;

                    case "totalless":
                        if (tot < trigger) { show = true; }
                        break;

                    case "totalmore":
                        if (tot >= trigger) { show = true; }
                        break;

                    case "anyless":
                        if (low < trigger) { show = true; }
                        break;

                    case "anymore":
                        if (high > trigger) { show = true; }
                        break;

                    case "allless":
                        if (!over) { show = true; }
                        break;

                    case "allmore":
                        if (!under) { show = true; }
                        break;

                    case "averageless":
                        if (a < trigger) { show = true; }
                        break;

                    case "averagemore":
                        if (a > trigger) { show = true; }
                        break;

                    case "averagevalueless":
                        if (av < trigger) { show = true; }
                        break;

                    case "averagevaluemore":
                        if (av > trigger) { show = true; }
                        break;
                }
            }
            catch (err) {
                console.error('Complex.Trigger>>' + err);
            }

            if (show) {
                //$("." + target + "_child").css("border-top", "none");
                $("." + target + "_child").fadeIn(100);
                //$("." + target + "_child").children("input, select")[0].focus();
                $("." + target + "_child").removeClass("trigger_hidden");

            } else {
                $("." + target + "_child").val(null);
                $("." + target + "_child").val(null);
                $("." + target + "_child").val(null);
                $("." + target + "_child").fadeOut(100);
                //$("." + target + "_child").css("border-top", "1px solid #9c9c9c");
                $("." + target + "_child").addClass("trigger_hidden");
            }
        }
        // == done with triggers ==
    });
    // ==== up arrow circle icon class    ====
    // ==== collapse panels by user click ====
    $(".collapse").on("click", function (e) {
        e.preventDefault();

        view_mode_manual = true;

        var sec = $(this).parent().parent().attr("id");
        if ($("table#" + sec + "_block").is(":hidden")) {
            /* expanded */
            $("table#" + sec + "_block").fadeIn(200);
            $(this).removeClass("icon-angle-circled-down");
            $(this).addClass("icon-angle-circled-up");
            $(this).parent().removeClass("hidden");

        } else {
            /* collapsed */
            $("table#" + sec + "_block").fadeOut(100);
            $(this).removeClass("icon-angle-circled-up");
            $(this).addClass("icon-angle-circled-down");
            $(this).parent().addClass("hidden");
        }
    });

    // ==========  image popup ============
    $(".popimage").on("click", function (e) {
        var img = $(this).attr("src");
        $(".modal .contents").html('<img src="' + img + '"/>');
        $(".modalcontrol h4").html("Image");
        $(".overlay").css("display", "block");
        $(".modal").centerInClient();
        $(".modal").show();

        // this shouldn't be here, but got to do something with this tangled web
        $(".modalcontrol .box").unbind("click").on("click", function (e) {
            e.preventDefault()
            $(".modal").hide();
            $(".fmodal").hide();
            $(".overlay").css("display", "none");
        });
    });

    // ====== image selector control ========
    $(".imageselect img").on("click", function (e) {
        var selector = $(this).closest(".imageselect").siblings(".selector").children("select");
        selector.val($(this).attr("value"));
        $(this).closest("ul").find("li").removeClass("selected");
        $(this).parent().addClass("selected");
    });
    $("select.imageselect").on("change", function (e) {
        var v = $(this).val();
        $(this).parent().parent().children("div.imageselect").children().children().removeClass("selected");
        $(this).parent().parent().children("div.imageselect").children().children().children("[value=" + v + "]").parent().addClass("selected");
    });

    // ======= trigger child element display  ==========
    $(".trigger").on("change", function (e) {
        var show = false;
        var numeric_comp = false;
        var testname = $(this).attr('name');
        var vs;
        var _name = $(this).attr('name');
        var _operator = $(this).attr('data-operator');
        var eleVal = evaluatorHandler.getValByName(_name).valueOf();

        if ($(this).attr("type") == "checkbox") {
            vs = $("i[name='" + $(this).attr("name") + "']:checked").attr("data-value");
        }
        else {
            vs = $(this).attr("data-value");
        }

        if (this.tagName === 'INPUT' && (this.type === 'checkbox' || this.type === 'radio')) {
            var ename = this.getAttribute('name');
            var tmpcoll = $('[name=' + ename + ']');
            if (tmpcoll.length > 0) {
                _name = $(tmpcoll[0]).attr('name');
                _operator = $(tmpcoll[0]).attr('data-operator');
                vs = $(tmpcoll[0]).attr("data-value");
            }
        }

        var params = {
            comparevalue: eleVal,
            name: _name,
            target: '.' + _name + '_child',
            valueset: vs,
            op: _operator + ""
        };
        if (showThis(params)) {
            $(params.target).removeClass("trigger_hidden");
            $(this).closest('tr').addClass("parent");
            var infobox = $(params.target).hasClass("infobox");
            if (infobox) {
                $(params.target).fadeIn(100);
            }
            else {
                $(params.target).css("border-top", "none");
                $(params.target).fadeIn(100);
                var c;
                //                if ((c = $(params.target)) && (c = c.children("input, select")) && c.length > 0)
                //                    c[0].focus();
                //                else
                //                    c.focus();
            }
        } else {
            $(params.target).addClass("trigger_hidden");
            $(this).closest('tr').removeClass("parent");
            if (infobox) {
                $(params.target).hide();
            }
            else {
                $(params.target + '.hide').val(null);
                $(params.target).fadeOut(50).css("border-top", "1px solid #9c9c9c");
            }
        }

        $(params.target).find('input,select').first().blur().change(); //If we're validating this will update the validation status on the sub-element.
    });

    // forms only
    init_imageselect();
    init_complex();

    //initialize form serializer -- also used to detected change in form state
    //FormProc.init();
    //initialize calc elements which derive their value from other form fields
    //calcElement.init();

    //functions shown in reverse order in init array
    //search '*TimeoutIE7' for info
    setTimeout(tfuncInit, 50);
});

function init_imageselect() {
    $("div.imageselect").each(function (e) {
        var v = $(this).siblings(".selector").children("select").val();
        $(this).children().children().children("[value=" + v + "]").parent().addClass("selected");
    });

}


//======= initialize panel display ========
function init_panels() {

    //remove all hidden rows
	$('tr.hide').each(function(){
		$(this).children('td').removeClass('hide');
		$(this).removeClass('hide');
	});
	//remove all valid paths
    $('.vpath').removeClass('vpath');
	
	//iterate through each panel, starting with the first
	var visited = [evaluatorHandler.plist.length];
	
	var curr, next;
    //start with the first panel
	next = evaluatorHandler.nextPanel();
	//assume first panel is always a part of the valid path
	if (next !== null)
	    $(next).addClass('vpath');
	
    //iterate through each panel, determining its successive panel based on it's evaluator
    //if there is no evaluator, panel falls through to the next successive panel
	while(next != null){
		if(!visited[evaluatorHandler.plut[next.id]]);
			visited[evaluatorHandler.plut[next.id]] = true;

			$(next).addClass('vpath');
			evaluatorHandler.path.push(next);

		curr = next;
		next = evaluatorHandler.nextPanel(curr);
		
		//take the current table's contents and determine if data has been entered
		//Exclude hidden, calculated, and empty fields.
		
		//If empty this really *should be* an empty string. No promises though.
		var res = $('#'+curr.id +' table [name][value][type!=hidden]:not(.calculated)').serialize();
		if(res.length === 0)
			next = null;
		
		var hasVal = false;
		var ii = -1;
		while((ii = res.indexOf('=',ii+1)) !== -1){
			if(ii + 1 < res.length){
				if(res.charAt(ii + 1) !== '&')
					hasVal = true;
			}
		}
		
		//if no data has been entered, assume that this panel is the 'current' one
		if(!hasVal)
			next = null;
	}
	
    //for each panel traversed or 'visited', fade in
	for(var i=0,l=evaluatorHandler.plist.length;i<l;i++){
		if(visited[i])
			FadeInPanel(evaluatorHandler.plist[i]);
		else
			FadeOutPanel(evaluatorHandler.plist[i]);
	}

	//set active panel to curr if it is not null
    	if (curr !== null) {
        	$('.currPanel').removeClass('currPanel');
        	$(curr).addClass('currPanel');
	}

	evaluatorHandler.updatePotentialPaths();
    evaluatorHandler.updatePanelNavigation();

	//note: below is turned off now. need to determine best conditions for scrolling to active panel
	/*
	if(curr !== null){
		var offset = $(curr).offset().top - $('.toolbar').height();
		//before scroll, unhide panel
		$('html, body').animate({ scrollTop: offset }, 200);
	}
	*/

}

function FadeInPanel(p){
    if(p && p.id) //This will weed out psuedo-panels (.alert)
	$('table#' + p.id + '_block').fadeIn(200)
		.children('.heading').removeClass('hidden')
		.children('i')
			.removeClass('icon-angle-circled-down')
			.addClass('icon-angle-circled-up');
}
function FadeOutPanel(p){
    if(p && p.id) //This will weed out psuedo-panels (.alert)
    $('table#' + p.id + '_block').fadeOut(100)
		.siblings('.heading').addClass('hidden')
		.children('i')
			.removeClass('icon-angle-circled-up')
			.addClass('icon-angle-circled-down');
}

// ======== show errors only ===========
function errorsOnly(){
	//var errPanels = [];
	var errPanels = {};
	
	if(typeof ValidationHandler === 'undefined')
		return;
	
	//turn off every row
	$('.scamp tbody > tr').each(function(){
		$(this).addClass('hide');
	});
	
	for(var i=0,li=ValidationHandler.vList.length;i<li;i++){
		if(ValidationHandler.vList == null)
			continue;
		//unhide panel
		var eobj = ValidationHandler.vList[i];
		if(eobj === null || eobj === undefined)
			continue;
		if(!errPanels[eobj.panel.id]){
			errPanels[eobj.panel.id] = true;
			$(eobj.panel).removeClass('hide');
		}
		eobj.DOM.parents('.answer').parent().removeClass('hide');
	}

	$('.panel').each(function () {
	    if (errPanels[this.id]) {
	        $("#" + this.id).show();
	        $('table#' + this.id + '_block').fadeIn(100);
	        $('#' + this.id + '>.heading')
				.removeClass('hidden')
				.children('i')
				.removeClass('icon-angle-circled-down')
				.addClass('icon-angle-circled-up');
	        return;
	    }
	    else {
	        $('table#' + this.id + '_block').fadeOut(100);
	        $('#' + this.id + '>.heading').addClass('hidden')
				.children('i')
				.removeClass('icon-angle-circled-up')
				.addClass('icon-angle-circled-down');
	    }
	});
	$("#formview").val(2);
}

// ======== open all panels ===========
function openAll() {
	//clear each row element currently with class 'hide'
    $('.scamp tbody > tr.hide').each(function(){
		$(this).removeClass('hide');
	});
    $("div.panel").each(function (i) {
        var sec = $(this).attr("id");
        $("table#" + sec + "_block").fadeIn(100);
        $(this).children("i").removeClass("icon-angle-circled-down");
        $(this).children("i").addClass("icon-angle-circled-up");
        $(this).children(".heading").removeClass('hidden');
        $("#formview").val(1);
    });
}

//========= initialize parent/child display of elements ===========
function init_complex() {

    var groupchecked = {};
    var show = false;
    var numeric_comp = true;
    $(".trigger").each(function (index, object) {
        var vs;
        var valGroup = $(this);
        if ($(this).attr("type") == "checkbox" || $(this).attr("type") == "radio") {
            var gname = $(this).attr('name');
            if(groupchecked[gname])
            	return;
            groupchecked[gname] = true;
            vs = $("[name='" + gname + "']").attr("data-value");
            valGroup = $("[name='" + gname + "']:checked");
        }
        else {
            vs = $(this).attr("data-value");
        }
		
        var params = { comparevalue: valGroup.val(),
            name: $(this).attr("name"),
            target: '.' + $(this).attr("name") + '_' + 'child',
            valueset: vs,
            op: $(this).attr("data-operator") + ""
        };

        if (showThis(params)) {
            //$(params.target).css("border-top", "none");
            $(this).closest('tr').addClass("parent");
            $(params.target).show();
        } else {
            $(this).closest('tr').removeClass("parent");
			$(params.target).addClass("trigger_hidden");
		}
    });
	
	$('.complex[data-operator]').each(function(i,o) { $(o).find('input,select').first().blur();}); //This inits complex objects using any of the compound table calculations.
}

function showThis(passed_parameters) {

    var jparams = passed_parameters;
    var show = false;
    var numeric_comp = false;

    if (jparams.valueset == undefined) {
        return false;
    }
    // split the values into an array.  Any matching value can trigger the display
    try {
        var val = jparams.valueset.split(",");
    }
    catch (err) {
        console.error('APP>>' + err + ' for element ' + jparams.name);
        var val = jparams.valueset;
    }

    // test the first value in the array to see if it is numeric.
    numeric_comp = isNumber(val[0]);
    var thisval;
    var valArray;
    if (isArray(jparams.comparevalue)) {
        valArray = jparams.comparevalue;
    }
    else {
        valArray = new Array(1);
        valArray[0] = jparams.comparevalue;
    }

    for (v = 0; v < valArray.length; v++) {
        thisval = valArray[v];
        if (numeric_comp) {
            for (var i = 0; i < val.length; i++)
                switch (jparams.op) {
                case "<":
                    if (parseFloat(thisval) < parseFloat(val[i])) { show = true; }
                    break;

                case ">":
                    if (parseFloat(thisval) > parseFloat(val[i])) { show = true; }
                    break;

                case "=>":
                case ">=":
                    if (parseFloat(thisval) >= parseFloat(val[i])) { show = true; }
                    break;

                case "=<":
                case "<=":
                    if (parseFloat(thisval) <= parseFloat(val[i])) { show = true; }
                    break;



                //this really can't take an array because the value will certainly not be one    
                //of the array values if it is another array value.  Need to code around this    
                //or just handle it as an exception.   
                case "!=":
                    if (parseFloat(thisval) != parseFloat(val[i])) { show = true; }
                    break;

                default:
                    if (parseFloat(thisval) == parseFloat(val[i])) { show = true; }
            };

        }
        else {

            for (var i = 0; i < val.length; i++)

                switch (jparams.op) {
                case "<":
                    if (thisval < val[i]) { show = true; }
                    break;

                case ">":
                    if (thisval > val[i]) { show = true; }
                    break;

                case "!=":
                    if (thisval != val[i]) { show = true; }
                    break;

                default:
                    if (thisval == val[i]) { show = true; }
            }

        }
    }
    return show;
}
//helper functions for collapsing and expanding panels 
//============================================================//
(function() {
	var p = {
		expand: function(e){
			var pb = $(e).parent().next()[0];//.parentElement.nextElementSibling;
			if(pb)
				$(pb).fadeIn(100);
			$(e).removeClass('icon-angle-circled-down')
				.addClass('icon-angle-circled-up')
				.parent().css('background-color','#92927F');
		},
		collapse: function(e){
			var pb = $(e).parent().next()[0];//parentElement.nextElementSibling;
			if(pb)
				$(pb).fadeOut(100);
			$(e).removeClass('icon-angle-circled-up')
			.addClass('icon-angle-circled-down')
			.parent().css('background-color','#AAAAA0');
		}
	};
	window.Panel = p;
})();

//evaluatorHandler which stores action events, triggers, functions for panels and elements when elements are changed
/*
 * handler for driving panel and element triggers and their logic.
 * in the form markup, these statements are contained in an evaluator element
 
 * e.g. <panel> <evaluator> {statement goes here} </evaluator> </panel>
 * when the markup is rendered on a page, the evaluator's contents are put inside a <var> element
 
 * using a logic syntax in JSON format, the display of panels and elements is driven by a set of boolean statements
 * e.g. ".panel":["[selectfield] == 1","[textbox] !== undefined"],
		".alert":[""],
		"other":["[selectfield] == 2"]
		
 * the above statement would trigger:
		
		- a panel with a name of "panel" will be triggered display if the two boolean conditions evaluate to true
		- ".alert" is a special case trigger and tells the user that the panel is in an unacceptable state. The evaluator
			condition for ".alert" is "" which is an empty string. An empty string always evaluates to true.
			when the panel ".alert" is triggered, it displays a generic message that states that it's panel is in an invalid state
		- the order of the panel evaluator statements matter, meaning that if the evaluators for the first panel statement
			(in this case, ".panel" is the first panel statement) don't all evaluate to true then the next panel evaluator 
			will be triggered. If the first panel evaluator does return true, then any following panel evaluators will not 
			be evaluated.
		  example: if ".panel" evaluates to true, ".alert" does not get triggered at all. If ".panel evaluates to false, 
			then ".alert" will evaluate next. 
		- an element with the name of "other" is shown when a form element with the name "selectfield" is equal to 2
 
 * a few facts to consider about the logic statement:
 * 		- the '.' before panel denotes that the target to hide/show based on the boolean statements is a panel and not an element.
			the reason this is done is because how a panel is hidden is different than how an element is hidden
		- a panel is uniquely identified by it's name in the markup (e.g. <panel> <name> panel </name> </panel> ), and the 
			markup renders the panel as a div with an id of "panel"
 *	    - if we are referring to the value of an element in the form, wrapping the element's name in brackets (e.g. [name])
			tells the logic statement processor that it needs to look up the value of a form element.
*/
(function (win) {
//begin valobj definitions
	var strvalobj = function(dom){
		this.dom = dom;
	};
	strvalobj.prototype.valueOf = function(){
		var visible = FormProc.isVisible(this.dom);
		var isNum = this.dom.hasClass('num');
		
		if(!visible)
		{
			if(isNum)
				return 0;
			else
				return '';
		}	
		
		if(isNum)
			return numberOrOriginal(this.dom.val());
		else
			return this.dom.val();
	};
	strvalobj.prototype.contains = function(){
		if(arguments.length === 0)
			return false;
		var val = this.valueOf();
		for(var i=0,l=arguments.length;i<l;i++){
			if(val.indexOf(arguments[i]) === -1)
				return false;
			return true;
		}
	};
	//
	var checkvalobj = function(dom){
		this.dom = dom;
	};
	
	//If the val can be made in to a finite number, do so. Otherwise return the original val
	var numberOrOriginal = function(val)
	{
		var num = +val;
		return (num == val && isFinite(num)) ? num : val;		
	}
	
	var sum =  function(val) 
	{ 
		if(typeof val === 'undefined')
			val = this.valueOf();
        
		//I'd use array.reduce, but it isn't IE < 9 friendly.
		if($.isArray(val))
		{
			var currcount = 0;
			var currval = 0;
			while(currval = val.pop())
				currcount += +currval;
			
			return currcount;
		} else 
		{
			var num = numberOrOriginal(val);
			if (typeof num == 'number')
				return num;
		}
		
        return 0;
	}
	
	checkvalobj.prototype.valueOf = function(){
		
		if(!FormProc.isVisible(this.dom))
			return '';
		
		var valarr = [];
		var elcheck = this.dom.filter(':checked');
		if(elcheck.length === 0)
			return '';
		for(var i=0,l=elcheck.length;i<l;i++){
			valarr.push($ (elcheck[i]).val() );
		}
		if(valarr.length === 1)
			return valarr[0];
		return valarr;
	};
	checkvalobj.prototype.contains = function(){
		if(arguments.length === 0)
			return false;
		var val = this.valueOf();
		if(val.length === 0)
			return false;
		for(var i=0,j=arguments.length;i<j;i++){
			var s = arguments[i];
			if(typeof s === 'number')
				s = ''+s;
			if(val.indexOf(s) === -1)
				return false;
		}
		return true;
	};
    checkvalobj.prototype.any = function(){
        var val = this.valueOf();
        var isnullempty = (val === null || val.length === 0);
        if(arguments.length === 0)
            return !isnullempty;
        if(isnullempty)
            return false;
        var i=arguments.length;
        while(i--){
            var s = arguments[i];
            if(typeof s === 'number')
                s = ''+s;
            if(val.indexOf(s) >= 0)
                return true;
        }
        return false;
    };
	checkvalobj.prototype.sum = sum;
	
	//
	var arrvalobj = function(dom){
		this.dom = dom;
	};
	arrvalobj.prototype.valueOf = function(){
		if(!FormProc.isVisible(this.dom))
			return '';
		
		var val = this.dom.val();
		if(val === null || val === undefined)
			return '';
		if(val.length === 1)
			return val[0];
		return val;
	};
	arrvalobj.prototype.contains = function(){
		if(arguments.length === 0)
			return false;
		var val = this.valueOf();
		if(val === null)
			return false;
		for(var i=0,j=arguments.length;i<j;i++){
			var s = arguments[i];
			if(typeof s === 'number')
				s = ''+s;
			if(val.indexOf(s) === -1)
				return false;
		}
		return true;
	};
    arrvalobj.prototype.any = function(){
        var val = this.valueOf();
        var isnullempty = (val === null || val.length === 0);
        if(arguments.length === 0)
            return !isnullempty;
        if(isnullempty)
            return false;
        var i=arguments.length;
        while(i--){
            var s = arguments[i];
            if(typeof s === 'number')
                s = ''+s;
            if(val.indexOf(s) >= 0)
                return true;
        }
        return false;
    };
	arrvalobj.prototype.sum = sum;
	
	var datevalobj = function(dom){
		this.dom = dom;
	};
	datevalobj.prototype.valueOf = function(){
		var dval = Date.parse(this.dom.val());
		if(isNaN(dval))
			return '';
		return dval;
	};
//end valobj definitions
	var evh = function(){
		this.EvalArray = {};
		this.NameTargets = {};
		this.NextPanels = {};
		this.NameValObjs = {};
		this.NameEvalDOM = {};
		this.plist = null;
		this.plut = {};
		this.path = [];
        this.pDistances = {};
	};

    

    function panelDistance(panel,depth){
        var nobj;
        
	    if(typeof (nobj = this.pDistances[panel]) === 'undefined'){
            nobj = {name:panel,depth:depth,distance:0};
            this.pDistances[panel] = nobj;
        }
        else{
            if(nobj.depth < depth)
                nobj.depth = depth;
        }

	    var maxdepth = -1;
        var nextpanels;
        var nid;
        if(typeof (nextpanels = this.NextPanels[panel]) === 'undefined'){
            var nid = $('#'+panel+' + .panel').attr('id');
            if(typeof nid === 'undefined')
                return depth;
        }
	    if(!nextpanels || nextpanels.length === 0)
		    return depth;
	    var iter = nextpanels.length;
	    var np = [];
        
        if(typeof nid !== 'undefined')
            np.push(nid);

	    while(iter--){
		    if(nextpanels[iter] === '.alert')
			    continue;
		    np.push(nextpanels[iter].substring(1));
	    }
	    iter = np.length;
	    if(iter === 0)
		    return depth;
	    while(iter--){
		    var branchdepth = panelDistance.call(this,np[iter],(depth + 1));
		    if(branchdepth > maxdepth)
			    maxdepth = branchdepth;
	    }
	    nobj.distance = (maxdepth - depth);
        return maxdepth;
    }
/*
 * initializes the evaluator handler by processing all the logic statements in <var> elements, 
 * converting them into evaluatable functions, and associating the show/hide triggers with their respective targets
*/
	evh.prototype.init = function () {
			this.plist = $('.panel');
			
			//populate key-value lookup which maps a panel id to an index array
			for(var i=0,l=this.plist.length;i<l;i++){
				this.plut[this.plist[i].id] = i;
			}
			
			//retrieve all var elements in page to process their logic statements
            var lv = document.getElementsByTagName('var');
			//if no logic statements are found, exit
            if (!lv) {
                return;
            }
			var _eh = this;
		//below are the definitions of look-up objects
			// key-value reference of a target (element or panel) to display, and the array of boolean functions 
			//   to evaluate in order for the target to displayed.
			// e.g. this.EvalArray{e:[b,c]} - in order for element e to display, functions a and b must both evaluate to true.

			// key-value reference of a element's trigger targets
			//	e.g. this.NameTargets{a:[b,c]} - evaluators for elements with the name 'b' and 'c' will be triggered when 'a' changes.
			
			// key-value reference of a panel's successive panels
			// e.g. this.NextPanels{p:[b,c]} - for panel p, the next panel to try to display after it is panel b ONLY if 
			// 	panel b's evaluators evaluate to true. If they don't, try to display panel c.
			
			
			//for each var element, process their scripts and populate the key-value stores for panels and 
			//evaluation evaluation functions
			(function(self,lv){
				var iter = lv.length;
				while(iter--){
					self.procVarEle(lv[iter],self.EvalArray,self.NextPanels);
					//this.procVarEle(lv[iter]);
				}
				
				//for each evaluator string in a, process and transform them into boolean functions
				for (var p in self.EvalArray){
					if(p.length === 0)
						continue;
					//pk tells us if current key is for a panel
					
					var pk = (p.length > 1 && p.charAt(0) === '.');
					
					if(pk){
						for(spanel in self.EvalArray[p]){
							iter = self.EvalArray[p][spanel].length;
							while(iter--){
								self.EvalArray[p][spanel][iter] = self.procPanelStr(self.EvalArray[p][spanel][iter]);
							}
							//for(var i=0;i<a[p][spanel].length;i++){
							//	a[p][spanel][i] = this.procPanelStr(a[p][spanel][i]);
							//}
						}
					}
					//for each string statement for the current target, transform them into functions based on what the target it (e.g. statement for a panel vs statement for an element)
					else{
						iter = self.EvalArray[p].length;
						while(iter--){
							self.EvalArray[p][iter] = self.procEleStr(self.EvalArray[p][iter],p,self.NameTargets);
						}
						//for (var i = 0; i < a[p].length; i++){
						//	a[p][i] = this.procEleStr(a[p][i],p,nt);
						//}
					}
				}
			})(this,lv);
			
			//for each trigger element defined in nt, bind them to an action where
			// if a trigger's value changes, it triggers an evaluation of it's targets conditions
			(function(self){
				var elechangefunction = function(){
					if( $(this).hasClass('singlesel') ){
						$('[name='+$(this).attr('name')+']').not(this).removeAttr('checked');
					}
					self.evalElement(this);
				};

				var currEle = null;
				for (var p in self.NameTargets){
					var nmEle = $('[name=' + p + ']');
					
					if(nmEle.length === 0){
						console.log('Evaluator warning: could not find element with name "'+p+'"');
						continue;
					}
					nmEle.change(elechangefunction);
                    ValidationHandler.setDisplay();
				}
			})(this);
            
            //calculate panels depth+distance
            panelDistance.call(this,$('.panel').first().attr('id'),0);

			//evaluate all trigger elements ?

			//bind all the '.next' buttons which trigger a panel's evaluator function which determines what panel
			// to display or goto next
			$('.next').click(function(e){
				e.preventDefault();
				//console.log('eval clicked');
				var ppanel = _eh.getEleParentPanel(this);
				
				if(ppanel === null){
					console.log('parent panel not found for evaluator at sourceindex '+this.sourceIndex);
					return;
				}
				_eh.evalPanel(ppanel);
			});

			$('.prev').click(function (e) {
			    e.preventDefault();
			    //console.log('eval clicked');
			    var ppanel = _eh.getEleParentPanel(this);
                
			    if (ppanel === null) {
			        console.log('parent panel not found for evaluator at sourceindex ' + this.sourceIndex);
			        return;
			    }

			    _eh.prevPanel(ppanel);

			});

        };
	evh.prototype.nextPanel = function(p, ignorePanelLogic)
	{
		if(!p)
		{
			return this.plist[0];
		}
		
		var npanels, eval, idx;
		
		idx = this.plut[p.id];
		
		//if current panel has defined branches
		if(!ignorePanelLogic && (npanels = evaluatorHandler.NextPanels[p.id]) && evaluatorHandler.NextPanels[p.id].length > 0){
			
			//If there's only one possible panel, return that. Ignore .alert as it is not actually a panel
            var dummyPanelCount = 0;
            if(npanels.indexOf('.alert') > -1)
                dummyPanelCount++;

			if((npanels.length - dummyPanelCount) == 1)
			{
				idx = this.plut[npanels[0].substring(1)];
				return this.plist[idx];
			}
		
			var isValid = true;
			//find a valid branch for the current panel
			for(var j=0,ll=npanels.length;j<ll;j++){
				if((eval = evaluatorHandler.EvalArray[npanels[j]])){
					var eArr = eval[p.id];
					isValid = true;
					for(var i=0,l=eArr.length;i<l;i++){
						isValid = isValid && eArr[i]();
						if(!isValid)
							break;
					}
				}
				//if the branch condition is met and the panel exists in the lookup table, set it as the next panel
				if(isValid && this.plut[npanels[j].substring(1)]){
					idx = this.plut[npanels[j].substring(1)];
					return this.plist[idx];
				}
			}
		}
        	//if no branch is defined
		else{
			idx++;
			
			if(idx < this.plist.length)
				return this.plist[idx];
		}
	};
	evh.prototype.checkAllElements = function(){
		var iter = 0;
		for (var targ in this.EvalArray){
			if(targ.charAt(0) === '.')
				continue;
			//valid = true;
			var funclist = this.EvalArray[targ];
			iter = funclist.length;
			while(iter--){
				if( !funclist[iter]() )
					break;
			}
			var domEle = $('[name=' + targ + ']');
			if(domEle.length === 0)
				domEle = $(document.getElementById(targ+'_id'));
			if(domEle.length === 0)
				domEle = $('[data-section='+targ+']');
				
			domParent = domEle.parents('tr');
			
			if(iter === -1){
                domEle.removeClass('trigger_hidden');
				domParent.fadeIn(100);
			}
			else
            {
				domEle.addClass('trigger_hidden');
				domParent.fadeOut(100);
            }
		}
	};
	evh.prototype.evalElement = function(ele){
		var a = this.NameTargets[ele.name];
		
		var iteri = a.length;
		while(iteri--){
			var ev = this.EvalArray[a[iteri]];
			if(typeof ev === 'undefined'){
				console.log('evaluator for '+a[iteri]+' not found.');
				return;
			}
			
			var trig = true, iterj = ev.length;
			while(iterj--){
				trig = trig && ev[iterj]();
				if (!trig)
					break;
			}
			
			var eleTargs = $('[name=' + a[iteri] + ']');
			var eleTarg = $('[name=' + a[iteri] + ']')[0];
			
			if(typeof eleTarg === 'undefined')
			{
				eleTargs = $('#'+a[iteri]+'_id')
				eleTarg = eleTargs[0];
			}
			
			if(typeof eleTarg === 'undefined')
			{
				eleTargs = $('[data-section='+a[iteri]+']');
				eleTarg = eleTargs[0];
			}
				
			if (trig){
				eleTargs.removeClass('trigger_hidden').removeAttr('disabled');
				$(eleTarg).change();
				//$(eleTarg).removeAttr('disabled');
				$(this.getEleQuestionRoot(eleTarg)).fadeIn(100);
				//$(eleTarg).focus();
			}
			else{
				eleTargs.addClass('trigger_hidden').attr('disabled','disabled');
				$(eleTarg).attr('disabled','disabled').change();
				$(this.getEleQuestionRoot(eleTarg)).fadeOut(100);
			}
		}
	};

	evh.prototype.checkPath = function (panel)	{

        var pathIndex = this.path.indexOf(panel);

        if(pathIndex > -1)
	    {
			if(pathIndex == 0)
			{
				if(this.nextPanel().id == this.path[0].id) //If our first panel is different we have a big problem.
					pathIndex++;
				else
				{
					//path[0] != our first panel. Nuke the stack and start over.
					this.rewindPanelStack(this.path[0]);
					this.path[0] = this.nextPanel();
					
					this.makeCurrent(this.path[0]);

					return;
				}
			}

			//Otherwise, starting at panel[1] or current panel (Whichever is higher) walk the path to make sure the stack didn't change.
			
			var nextpanel;
			for(var i = pathIndex; i < this.path.length; i++)
			{
				nextpanel = this.nextPanel(this.path[i-1]);
				
				if(!nextpanel || nextpanel.id != this.path[i].id)
				{
					this.rewindPanelStack(this.path[i-1]);
					this.makeCurrent(this.path[i-1]);
					
					return;
				}
			}
        }
	}
	
	evh.prototype.updatePotentialPaths = function ()
	{
		$('.ppath').removeClass('ppath');
		
		var next = evaluatorHandler.nextPanel(evaluatorHandler.path[evaluatorHandler.path.length - 1]);
	
		while(next)
		{
			$(next).addClass('ppath');
			next = evaluatorHandler.nextPanel(next);
		}
	};

    evh.prototype.rewindPanelStack = function (panel)
        {
            var pathIndex = this.path.indexOf(panel);

            if(pathIndex > -1)
	        {
                while (this.path[this.path.length - 1] != panel)
	                $(this.path.pop()).removeClass('currPanel vpath');
            }
        };

    evh.prototype.makeCurrent = function(panel, skipAnimate) {
		if(!panel)
		{
			console.log('makeCurrent error, invalid panel.', panel);
			return;
		}

        var panel = $(panel);

		evaluatorHandler.updatePotentialPaths();
		
        //If we're already the current panel bail.
        if(panel.hasClass('currPanel'))
            return;
		
        $('.currPanel').removeClass('currPanel');
        panel.addClass('currPanel');
        FadeInPanel(panel[0]);
        this.updatePanelNavigation();

		if(!skipAnimate)
		{
			var offset = panel.offset().top - $('.toolbar').height();
			$('html, body').animate({ scrollTop: offset }, 200);
		}
    };
		
    evh.prototype.updatePanelNavigation = function()
    {
		var scratchpad = isScratchPad();

		var panelList = $('.panelLink').empty();
		
		if(!scratchpad)
		{
			//Only show panels in the path and potential future panels.
			for(var i = 0; i < evaluatorHandler.path.length;i++)
			{
			   panelList.append(this.buildLink(evaluatorHandler.path[i]));
			}
			
			var next = evaluatorHandler.nextPanel(evaluatorHandler.path[evaluatorHandler.path.length - 1]);
	
			while(next)
			{
				panelList.append(this.buildLink(next));
				next = evaluatorHandler.nextPanel(next);
			}
		
		} else {
			this.checkPath($('.currPanel')[0]); //Check to ensure we haven't altered our path.
			
			//Scratchpad. Show all panels.
			//Need to get through panels that aren't in the this.path or can't be visited as part of ppath.
			
			var next = evaluatorHandler.nextPanel(); //nextPanel w/ no panel specified gets the first.
			
			while(next)
			{
				panelList.append(this.buildLink(next));
				next = evaluatorHandler.nextPanel(next, true); //Ignore logic when getting the next panel (to show all.)
			}
		}

        var currPanel = $('.currPanel');
        if(!currPanel.hasClass('vpath'))
        {
            currPanel.find('.prev input').val('RESUME');
        } else {
            currPanel.find('.prev input').val('PREV');
        }

        $('.progress').css('width', Math.round(this.getPanelPercent(currPanel.attr('id')) * 100)  + '%');
    };

    evh.prototype.buildLink = function(panel)
    {
        panel = $(panel);
      
        var panelli = $('<li></li>').click(function(event) { event.preventDefault; evaluatorHandler.makeCurrent($('#' + $.data(this, 'panel'))); })
        var panellink = $('<a></a>').text(panel.find(".heading h2").text()).appendTo(panelli);

        $.data(panelli[0], 'panel', panel.attr('id'));

		if(panel.hasClass('currPanel'))
			panelli.addClass('current');

        if(panel.hasClass('vpath'))
            panelli.addClass('visited');
        
		if(panel.hasClass('ppath'))
            panelli.addClass('potential');
		
		if(!panel.hasClass('vpath') && !panel.hasClass('ppath'))
			panelli.addClass('notinpath');
		
		return panelli;
    };

	evh.prototype.evalPanel = function(panel){

	    this.checkPath(panel);

		if(this.path.indexOf(panel) == -1)
			return;
		
	    var a = this.NextPanels[panel.id];
		var ev = this.EvalArray;
		//check if evaluators found
		if(typeof a === 'undefined'){
			return;
		}
		var cond = false;
		var lastP = null;
		var nextP = null;
		//loop through and call each evaluator function contained within panel
		for(var i=0,li=a.length;i<li;i++){
			//lastP is the target panel to check evaluation criteria for
			lastP = a[i].substring(1);
			cond = true;
			for(var j=0,lj=ev[a[i]][panel.id].length;j<lj;j++){
				cond = cond && ev[a[i]][panel.id][j]();
				if(!cond){
					$(a[i].replace(".", "#"));
                    FadeOutPanel($(a[i].replace(".", "#"))[0]);
					break;
				}
			}
			if(cond){
				if(nextP == null)
					nextP = lastP;
			}
		}
		lastP = nextP;

        if(!lastP) //We couldn't find the next panel through evaluation, see if we can just go to the next panel.
        {
            var _next = this.nextPanel(panel);
            if(_next && _next.id)
                lastP = _next.id;
        }

        if(!lastP && this.plut[panel.id] == this.plist.length - 1) //If we *still* don't have another panel, check to see if we're at the last panel. Force it to the complete panel.
        {
            lastP = 'complete';
        }

		if(lastP === 'alert'){
			var pheading = $('#'+panel.id+' > .heading > h2').text();
			alert('panel "'+pheading+'" is either missing information or contains invalid information and can not proceed');
			return;
		}

		//In this case, this is the next logical panel to go to next.
		if(lastP !== null && this.plut[lastP]){

			$('.currPanel').removeClass('currPanel');
			var nextPanel = $('#' + lastP).addClass('vpath');

			this.checkPath(nextPanel[0]);
			
			if(this.path.indexOf(nextPanel[0]) < 0)
				this.path.push(nextPanel[0]);	
			
			Panel.expand(nextPanel.find('.heading i')[0]);

			this.makeCurrent(nextPanel[0]);
			
			nextPanel.find('[name]').first().focus();
			
			//foreach validatable element in panel, check validation
			var pelist = ValidationHandler.pElements[lastP];
			var iter;
			if(pelist && (iter = pelist.length)){
				while(iter--){
					ValidationHandler.checkElementError(pelist[iter]);
				}
			}

            this.updatePanelNavigation();
		} else {
            if(lastP)
                console.log('We have a panel to jump to, but it does not exist in our look up table: ' + lastP);
        }
	};

	evh.prototype.prevPanel = function (panel) {
	    this.checkPath(panel);
        
		var pathIndex = this.path.indexOf(panel);
	    
		if(pathIndex == 0)
			return;
		else if (pathIndex > 0)
	        this.makeCurrent(this.path[pathIndex - 1]);
	    else 
			this.makeCurrent(this.path[this.path.length - 1]);
	};

	evh.prototype.procVarEle = function (ve, a, np) {
		var o = $(ve).text();
		var k;
				var vparent = evaluatorHandler.getEleParentPanel(ve);
		try{
			k = $.parseJSON('{'+o+'}');
		}
		catch(e){
			var errmsg = 'error parsing JSON string';
			if(vparent !== null)
				errmsg += ' for panel \''+vparent.id+'\'';
			if(ve && ve.sourceIndex)
				errmsg += ' at sourceindex '+ve.sourceIndex;
			if(e && e.message)
				errmsg += ': '+e.message;
			console.log(errmsg);
			console.log(o);
			return;
		}
		
		var pid = null;
		//initialize next panel array
		if(vparent !== null){
			pid = vparent.id;
			np[pid] = [];
		}
		//k should be an object where each property is an array of strings
		for (var p in k) {
			//if current key starts with '.' it describes a panel, otherwise its considered an element
			var isPanel = false;
			var pkey = p.toLowerCase();
			if(pkey.length > 1 && pkey.charAt(0) === '.' && pid !== null){
				np[pid].push(pkey);
				isPanel = true;
			}
			
			//populate key-value reference of target (the key) and its evaluators (the value)
			for (var i = 0; i < k[p].length; i++) {
				if (typeof a[pkey] === 'undefined'){
					if(isPanel)
						a[pkey] = {};
					else
						a[pkey] = [];
				}
				
				if(isPanel){
					if(typeof a[pkey][pid] === 'undefined')
						a[pkey][pid] = [];
					a[pkey][pid].push(k[p][i]);
				}
				else
					a[pkey].push(k[p][i]);
			}
		}
	};
	evh.prototype.procEleStr = function (str, targ, nt) {
		if (str.length === 0)
			return function () { return true; };
		var i = -1;
		var j = 0;
		var c = '';
		while ((i = str.indexOf('[', i+1)) !== -1) {
			if (j < i)
				c = c.concat(str.substring(j, i));
			j = str.indexOf(']', i);
			if (j !== -1 && j > i) {
				var nm = str.substring(i + 1, j).toLowerCase();
				var sub = 'evaluatorHandler.getValByName(\'' + nm + '\')';
				if (!sub) {
					console.log('error getting value of element with name \'' + nm + '\'');
					return;
				}
				c = c.concat(sub);

				if (typeof nt[nm] === 'undefined')
					nt[nm] = [];
				var tlow = targ.toLowerCase();
				if(nt[nm].indexOf(tlow) === -1)
					nt[nm].push(tlow);
			}
			j=j+1;
			i=j;
		}
		if (i < str.length)
			c = c.concat(str.substring(j));
		
		try{
			var fn = Function('return ' + c + ';');
			return fn;
		}
		catch(e){
			console.log('Evaluator Warning: invalid statement "'+c+'"');
		}
		return Function('return true;');

	};
	evh.prototype.procPanelStr = function (str) {
		if (str.length === 0)
			return function () { return true; };
		var i = -1;
		var j = 0;
		var c = '';
		while ((i = str.indexOf('[', i+1)) !== -1) {
			if (j < i)
				c = c.concat(str.substring(j, i));
			j = str.indexOf(']', i);
			if (j !== -1 && j > i) {
				var nm = str.substring(i + 1, j).toLowerCase();
				var sub = 'evaluatorHandler.getValByName(\'' + nm + '\')';
				if (!sub) {
					console.log('error getting value of element with name \'' + nm + '\'');
					return;
				}
				c = c.concat(sub);
			}
			j = j+1;
			i = j;
		}
		if(i < str.length)
		c = c.concat(str.substring(j));

		try{
			var fn = Function('return ' + c + ';');
			return fn;
		}
		catch(e){
			console.log('Evaluator Warning: invalid statement "'+c+'"');
		}
		return Function('return true;');

	};
	evh.prototype.getEleQuestionRoot = function(el){
		if(typeof el === 'undefined' || typeof el.parentElement === 'undefined')
			return null;
		return $(el).parents('tr')[0];
	};
	evh.prototype.getEleParentPanel = function(el){
		if(typeof el === 'undefined' || typeof el.parentElement === 'undefined')
			return null;
		return $(el).parents('.panel')[0];
		/*
		var ppanel = el.parentElement;
		while(ppanel !== null){
			if(ppanel.className.indexOf('panel') !== -1)
				break;
			ppanel = ppanel.parentElement;
		}
		return ppanel;
		*/
	};
	evh.prototype.getValByName = function (nm) {
		var nvo = this.NameValObjs[nm];
		if(!nvo){
			this.NameValCreate(nm);
			nvo = this.NameValObjs[nm];
			if(!nvo)
				return false;
		}
		//var thisval = nvo.dom.val();
		//if ($.isNumeric(thisval))
		//{
		//    if(isFloat(thisval))
		//    {
		//        return parseFloat(thisval);
		//    }
		//    else
		//    {
		//        return parseFloat(thisval);
		//    }
		//}
		//else
		//{
		//    if (thisval != undefined && thisval.length == 0)
		//    {
		//        return 0;
		//    }
		//    else
		//    {
		//        return thisval;
		//    }
		    
		//}

		return nvo;
	};
    evh.prototype.NameValCreate = function(name){
		var els = $('[name='+name+']');
		if(els.length === 0)
			return;
		if(this.NameValObjs[name])
			return;
		var nvo = null;
		var eltype = els.attr('type');
		//var single = els.hasClass('singlesel');
		
		if(eltype === 'checkbox' || eltype === 'radio'){// && !single){
			nvo = new checkvalobj(els); 
		}
		else if(els.attr('multiple')){
			nvo = new arrvalobj(els); 
		}
		else if(els.hasClass('date') || els.hasClass('datetime') || els.hasClass('pastdate')){
			nvo = new datevalobj(els);
		}
		else{
			nvo = new strvalobj(els); 
		}
		
		if(nvo !== null)
			this.NameValObjs[name] = nvo;
	};
    //given a panel's identifer, determine what % of traversal of the entire path it represents. returns -1 if not a valid panel
    evh.prototype.getPanelPercent = function(panel){
        var pobj;
        if(typeof (pobj = this.pDistances[panel]) === 'undefined')
            return -1;
        return ((pobj.depth) / (pobj.distance + pobj.depth));
    };
	win.evaluatorHandler = new evh();
	
})(window);
//============================================================//
//FormProc : handles the serialization and submission of form data
(function (win) {
    var fz = {
		action: 0,
		invisibleWithData: [],
        init: function () {
            $(document.forms[0]).submit(function (e) {
	    	    var src = document.activeElement;
		        e.returnValue = false;
                e.preventDefault();
                var srcclass = src.className;
                if (srcclass.indexOf('button') == -1) {srcclass = $('#form_action').val(); }
                $('body').css('cursor', 'progress');
                $('input[type=submit]:enabled').attr('disabled','disabled').addClass('enableaftersave');
                
				var fAction = '';
				if(src && src.className){
                    if(srcclass.indexOf('savesign') !== -1){
                        fAction='sign';
						FormProc.action = 1;
                    }
                    else if(srcclass.indexOf('savecomplete') !== -1){
                        fAction='complete';
						FormProc.action = 2;
                    }
					else if(srcclass.indexOf('saveclose') !== -1){
						FormProc.action = 3;
					}
                }

                //src tells which button tiggered form submit
                var payload = FormProc.SSerialize(this);
				
				if(!$(src).hasClass('override') && FormProc.invisibleWithData.length > 0)
				{
					var willLose = FormProc.BuildSaveLostDialog(false, function(saved) {  
						if(!saved)
						{
							$('body').css('cursor', 'default');
							$('.enableaftersave').removeAttr('disabled').removeClass('enableaftersave');
						}
					});
					if(willLose) return;
				}
				
			
		        payload = payload.concat('&faction=',fAction);
                $.ajax({
                    url: this.action, type: this.method, data: payload,
                    complete: function (j, t) {
                        $('body').css('cursor', 'default');
                        $('.enableaftersave').removeAttr('disabled').removeClass('enableaftersave');
						//if save and close, immediately redirect
						
						
						var obj = $.parseJSON(j.responseText);
						if(obj){
                            //form is saved even if in error state, update form state
                            initFormVal = $(document.forms[0]).serialize();
							if(!obj.error){

                                if(fAction != 'sign' && fAction != 'complete')
                                {
                                    modalMessage('Success', obj.msg);
                                    $('.buttons').append('<input type="submit" value="OK" class="ok"/>');
                                }

                                if(fAction == 'sign')
                                {
                                    $('.button.savesign').attr('disabled', 'disabled');
                                    $('.button.save').attr('disabled', 'disabled');
                                    $('.button.saveclose').attr('disabled', 'disabled');
                                    $('div.stub').prepend('<div class="encstate">Form Signed</div>');
                                    //location.reload();
                                }
                                else 
                                { if(fAction == 'complete')
                                    {
                                        $('.button.savecomplete').attr('disabled', 'disabled');
                                        $('.button.save').attr('disabled', 'disabled');
                                        $('.button.saveclose').attr('disabled', 'disabled');
                                        $('div.stub').prepend('<div class="encstate">Form Completed</div>');
                                        //location.reload();
                                    }
                                }
							}
							else{
								//form saved but with errors
								modalMessage('Form Status',obj.msg);
								ValidationHandler.populateValidation();
								if (ValidationHandler.vLength > 0 && $('.validationcpanel').css('display') === 'none') {
								    $('.buttons').append('<input type="submit" value="OK" class="ok"/>');
									var togv = $('.togglevcpanel');
									if(togv.length > 0)
										togv[0].click();
								}
								if(obj.errlist && obj.errlist.length !== ValidationHandler.vList.length){
									console.log('Warning: mismatch on client side and serverside validation');
								}
								/*
								if(obj.errlist && obj.errlist.length > 0){
									var elist = '';
									for(var i=0;i<obj.errlist.length;i++)
										elist = elist + obj.errlist[i].name+':'+obj.errlist[i].error+'<br />';
										modalMessage('Error',elist,obj.msg);
								}
								else
									modalMessage('Error',obj.msg);
								*/
							}
						}
						
						if(obj && !obj.error && FormProc.action === 3 ){
							//var url = '/dashboard';
							//var tmp = qs('url');
							//if (tmp.length > 0) {
							//	url = window.location.origin + tmp;
							//}
							//else if ((tmp = document.referrer) && tmp.length > 0 && tmp.indexOf('/login?') === -1) {
							//	url = tmp;
							//}
							window.location.href = returnPage();
							return;
						}
					}
                });
            });
        },
        isVisible: function (e, saving, scratchmode) {
            if (!e)
                return false;
            
            var el = $(e);

            //isVisible is used to decide what values to send to the server. Always send fields of type hidden.
            //Don't send fields that are hidden due to triggers. They, or their parents, will have a class of trigger_hidden applied.
            //Only send fields that have been visited (vpath) or can be visited (ppath)

            //Oh, we've extended this once too many times. Breaking the logic up in the digestible chunks.

            //If type is hidden, always true;
            if(el.prop('type') === 'hidden')
                return true;
            
            //If trigger hidden, false.
            if(el.hasClass('trigger_hidden') || (el.parents('.trigger_hidden').length > 0))
                return false;
            
            //If there is an element form_saveallpanels and the value is 'true' return even if it isn't on a valid panel.
            if(saving && evaluatorHandler.getValByName('form_saveallpanels') && evaluatorHandler.getValByName('form_saveallpanels').valueOf().toLowerCase() == 'true')
                return true;
			
            //If we're in the current path, true.
            if(el.parents('.vpath').length > 0 || el.parents('.ppath').length > 0)
                return true;
            
            //This lets trigger logic on the current panel work if you've skipped around and aren't on a panel in the vpath.
            if(!saving && el.parents('.currPanel').length > 0)
                return true;

            //If we're in scratch mode and saving, return true unless it's trigger hidden.
            if(scratchmode && saving)
                return true;

            return false;
        },
        SSerialize: function (t) {
   
			this.invisibleWithData = [];

			evaluatorHandler.updatePotentialPaths();
			
            var scratchpad = isScratchPad();

            var procHidden = {};
            var procEmpty = {};
	        var visited = {};
            //for each element with a name attribute, determine visibility
            $('[name]').each(function () {
            
                if(visited[this.name])
			    return;

		        visited[this.name] = true;

			        var _self = $('[name='+this.name+']');
			        var _type = this.type;
					
			        if(_type === 'radio' || _type === 'checkbox'){
				        _self = _self.filter(':checked');
			        }
					
                    var cval = _self.val();
                if (FormProc.isVisible(this, true, scratchpad)) {
			        
                    if (!cval || cval.length === 0) {
                        if (!procEmpty[this.name]){
                            procEmpty[this.name] = '{empty}';
						    this.setAttribute('disabled','disabled');
						}
                    }
                } else {
			if(cval && $('.panel [name=' + this.name + ']').length > 0)
			{
				FormProc.invisibleWithData.push(this.name);
			}
					
                    $('[name='+this.name+']').attr('disabled', 'disabled');

                    if (!procHidden[this.name]) {
                        procHidden[this.name] = '';
                    }
                }
            });
            var res = $(t).serialize();
            var hres = $.param(procHidden);
            var eres = $.param(procEmpty);
            if (hres)
                res = res + '&' + hres;
            if (eres)
                res = res + '&' + eres;
					
			if(view_mode_manual)
			{
				res = res + '&form_mode=MANUAL';
			} else {
				res = res + '&form_mode=' + (isScratchPad() ? 'SCRATCH' : 'NORMAL');
			}
			
			ValidationHandler.populateValidation();
			res = res + '&form_state=' + (ValidationHandler.vLength > 0 ? 'INVALID' : 'VALID');

            //remove disable
            $('[disabled]').not('input[type=submit]').removeAttr('disabled');

            return res;
        }, labelForEleName : function (name)
		{
			var lbl = $('label[for=' + name + '_id]');
			//We have a label:
			if(lbl.length > 0)
			{
				if(lbl.text().length > 0) //If length == 0 it means someone didn't give it a label in the form.
					return lbl.text();
				else //Check for the sub-title.
					return lbl.siblings('span').text();
			}

			//If we're here it's not a standard element.
			var ele = $('[name=' + name + ']');
			
			//Check for complex elements.
			if(ele.parents('table.complex').length > 0)
				return ele.parent().prev('.rlabel').children().text();
			else if(ele.parents('.compound').length > 0) //And compounds.
				return FormProc.labelForEleName(ele.parents('.compound').attr('name'));
			
			console.log('We missed something?!?', name);
			return;
		},
		//showIfNone will build the dialog even if there are no fields that will be lost if saved. 
		//callback is called when the dialog is dismissed. Passes in true if the form was saved, false if cancelled.
		BuildSaveLostDialog: function(showIfNone, callback)
		{
			//This is a bit of a hack, we can't pass in a callback that gets called when hitting the 'cancel' X
			//We will bind to it, and then remove our binding when the dialog closes.
			var cleanup;
			if(typeof callback === 'function')
			{
				cleanup = function(saved) { 
					$('.modalcontrol .box').off('click', cleanup);
					if(saved !== true)
						saved = false;
					callback(saved);
				};
				
				$('.modalcontrol .box').on('click', cleanup);
			}
			
			var hasInvisible = false;
			var panels = {};
			
			for(var i = 0; i < FormProc.invisibleWithData.length; i++)
			{
				var ele = $('[name='+ FormProc.invisibleWithData[i] +']').not('.trigger_hidden');
				if(ele.length > 0)
				{
					hasInvisible = true;
					
					var panel = ele.parents('.panel').attr('id');
					
					if(!panels.hasOwnProperty(panel))
					{
						panels[panel] = [];
					}
					
					panels[panel].push(FormProc.invisibleWithData[i]);
				}
			}
			
			//We can end up false here if only trigger_hidden elements are invisible.
			if(hasInvisible)
			{
				var dataWarningMessage = '<p>The form contains data in sections that are not required and are invalid according to the form\'s requirements.</p><p>You can: </p><ul class="circle"><li>Save only valid data (any invalid data will be lost)</li><li>Save the form as-is in an invalid state</li><li>Cancel and correct the data manually</li></ul>';
				
				//var dataWarningMessage = '<b>Your options:</b><br/><ul class="circle simplelist"><li><a onclick="$(\'#formview\').val(1).change();$(document.forms[0]).submit();">Save all data</a></li><li><a onclick="forcesave = true;$(document.forms[0]).submit();">Save only valid data, you will lose data.</a></li><li><a onclick="$(\'#formview\').val(1).change();hideModal();">Review all data (cancels save)</a></li></ul><hr /><br /><h2>Panels and questions with invalid data:</h2>';
								
				/*for (var panelid in panels) {
					
					//<a onclick="if($(\'#' + panelid + ' .heading\').hasClass(\'hidden\')) $(\'#' + panelid + '\').find(\'.collapse\').click(); FormProc.SSerialize(); FormProc.BuildSaveLostDialog(true);">'
					
					if (panels.hasOwnProperty(panelid)) {
						if(panels[panelid].length > 0)
						{
							dataWarningMessage += $('#' + panelid).children('.heading').text().trim() + '<ul class="circle simplelist">';

							for(var i = 0; i < panels[panelid].length; i ++)
							{
								dataWarningMessage += '<li>'+FormProc.labelForEleName(panels[panelid][i]) + '</li>';
							}
							dataWarningMessage += '</ul>';
						}
					}
				}*/
				
				

				modalMessage('You have filled out answers that are not displayed/in the valid path:', dataWarningMessage, "info");
				modalButtons(["Save only VALID data","Save ALL data","Cancel"]);
				$(".modal .buttons .cancel").click(function() { hideModal(); if(cleanup) cleanup(false); });
				$(".modal .buttons .valid").addClass('override').click(function() { $(document.forms[0]).submit(); if(cleanup) cleanup(true);});
				$(".modal .buttons .all").addClass('override').click(function() { $('#formview').val(1).change();$(document.forms[0]).submit(); if(cleanup) cleanup(true);});
				hideOverlay();
				
				return true;
			} else if(showIfNone)
			{
				modalMessage('Safe to save', '<h2>The form is safe to save.</h2>', "info");
				
				modalButtons(["Cancel","Save"]);
				$(".modal .buttons .save").addClass('override').click(function() { $(document.forms[0]).submit(); if(cleanup) cleanup(true);});
				$(".modal .buttons .cancel").click(function() { hideModal(); if(cleanup) cleanup(false);});
				hideOverlay();
			}
			
			return false;
		}
    };
    win.FormProc = fz;
})(window);
//============================================================//
//begin floating validation handler definition
(function (win) {
	var vh = function(){
		//key-value collection of name->errobj validatable elements
		this.vLookup = {};
		//list of current invalid items
		this.vList = [];
		//list of 
        this.pElements = {};
		this.vLength = 0;
		this.vIdx = 0;
        this.displayIdx = 0;
		this.displayDOM = null;
		this.countDOM = null;
	};
	var verrobj = function(){
		this.code = 0;
		//current text display applicable to error
		this.label = null;
		//native DOM object of element's panel
		this.panel = null;
		//jquery DOM object of element
		this.DOM = null;
		//index of error object in vList array
		this.idx = -1;
	};
	
	vh.prototype.init = function(){
		/*foreach form input, check for:
			-required : 1
			-max length : 2
			-min : 4
			-max : 8
		*/

		this.displayDOM = $('.currvaliditem');
		this.countDOM = $('.invalidcount');
		var visited = {};
		var vl = this.vLookup;
		var _vh = this;
		var erridxcounter = 0;
		
		$('input, select, textarea').each(function(){
			if(!this.id || !this.name)
				return;
			if(visited[this.name])
				return;
			visited[this.name] = true;
			//need label ? panel ?
			
			var errcode = 0;
            		var errlbl = '';

			//element-level required
			if ((o = $(this).prev()[0]) && o.className && o.className.indexOf('required') !== -1){
				errcode += 1;
			}
			//checkbox/radio required
			else if((o = this.parentElement) && (o = $(o).prev()[0]) && o.className && o.className.indexOf('required') !== -1){
				errcode += 1;
			}
			//complex control required
			else if((o = $(this).parents('table.complex').prev()[0]) && o.className && o.className.indexOf('required') !== -1){
				errcode += 1;
			} 
			//compound
			else if ((o = $(this).parents('.compound')).length > 0 && o.parents('tr').children('.question').hasClass('required')) {
				 errcode += 1;
			}
			
			//maxlength
			if((o = this.getAttribute('maxlength')) && (o = parseInt(o))){
				errcode +=2;
			}
			
			//min value
			if ((o = this.getAttribute('min'))){
				errcode += 4;
			}
			//max value
			if ((o = this.getAttribute('max'))){
				errcode += 8;
			}
			
			if(errcode > 0){
                		var errobj = new verrobj();
                		errobj.code = errcode;
				errobj.panel = $(this).parents('.panel')[0];
                		if(errlbl === ''){
                   			errobj.label = FormProc.labelForEleName(this.name);
		                }
                		else{
		                    errobj.label = errlbl;
                		}
					
				errobj.DOM = $(this);
                		errobj.idx = erridxcounter++;
				vl[this.name] = errobj;
                		//add errobj to panel's list of elements
                		if(!_vh.pElements[errobj.panel.id])
                    			_vh.pElements[errobj.panel.id] = [];
                
                			_vh.pElements[errobj.panel.id].push(errobj);
			}
		});
		
	//bind functions to DOM elements 
		
		//toggle display of panel
		$('.togglevcpanel').click(function(){
			
			var disp = $('.validationcpanel').css('display');
			//set default position
			$('.validationcpanel').css('top', '6em').css('right','1.5em');
			if (disp === 'none') {
				_vh.populateValidation();
				//$('#formview').val(1).change();
				_vh.setDisplay.call(_vh);
				$('.validationcpanel').css('display','block');
			}
			else {
				$('.validationcpanel').css('display','none');
			}
		});
		
		//make panel draggable
		$('.validationcpanel').draggable();
		
		//buttons for cycling through each error
		$('.btnerrnext').click(function(){ _vh.getNextError.call(_vh) });
		$('.btnerrprev').click(function(){ _vh.getPrevError.call(_vh) });
		
		//button for scrolling + highlighting current error
		//$('.currvaliditem').click(function() {
		this.displayDOM.click(function(){
			if(_vh.vLength === 0)
				return;
            var currerr = _vh.getCurrError();
            if(currerr)
			    _vh.findTarget.call(_vh,currerr.DOM.attr('id'));
		});
		
        var eleChangeListen = function(e){
			var target = e.target || e.srcElement;
			var eobj = _vh.checkElementError.call(_vh,target.name);
			_vh.setDisplay.call(_vh);
		};

		//attach listener to SDF form element
		//$('form[action="formprocessor.aspx"]').change(eleChangeListen);
        $('form[action="formprocessor.aspx"]').focusout(eleChangeListen);

	};
	vh.prototype.populateValidation = function(){
		//get list of valid panel paths via .vpath selector
		var plist = [];
		var _vh = this;
		$('.vpath').each(function(){
			var nm = this.id;
			if(_vh.pElements[nm])
				plist.push(nm);
		});

		//Also check the next panels (as far ahead as we can, starting with the last panel in the path.)
		
		evaluatorHandler.updatePotentialPaths();
		
		var next = evaluatorHandler.nextPanel(evaluatorHandler.path[evaluatorHandler.path.length -1]);
		
		while(next)
		{
			if(_vh.pElements[next.id])
				plist.push(next.id);
			
			next = evaluatorHandler.nextPanel(next);
		}

		//with each valid panel that maps to a .vpath that has elements to validate..
		var iteri = plist.length, iterj=0, olist = null;
		while(iteri--){
			olist = _vh.pElements[plist[iteri]];
			iterj = olist.length;
			while(iterj--){
				this.checkElementError(olist[iterj]);
			}
		}
		
		//for(var i=0,li=plist.length;i<li;i++){
		//	var olist = _vh.pElements[plist[i]];
		//	for(var j=0,lj=olist.length;j<lj;j++){
		//		this.checkElementError(olist[j]);
		//	}
		//}
		
	};
	vh.prototype.getNextError = function(){
		if(this.vLength === 0)
			return;
        var counter = 0;
        while(counter < this.vList.length && this.vList[++this.vIdx] == null){
            if(this.vIdx > this.vList.length)
                this.vIdx = -1;
			counter++;
		}

        if(++this.displayIdx >= this.vLength)
            this.displayIdx=0;

		this.setDisplay();
        this.findTarget(this.vList[this.vIdx].DOM.attr('id'));
	};
	vh.prototype.getPrevError = function(){
		if(this.vLength === 0)
			return;
		var counter = 0;
        while(counter < this.vList.length && this.vList[--this.vIdx] == null){
            if(this.vIdx <= 0)
                this.vIdx = this.vList.length;
			counter++;
		}

        if(--this.displayIdx < 0)
            this.displayIdx=this.vLength - 1;
		this.setDisplay();
        this.findTarget(this.vList[this.vIdx].DOM.attr('id'));
	};
	vh.prototype.setDisplay = function(){

		if(this.vLength === 0){
			this.displayDOM.addClass('valid').text('No validation errors');
            this.countDOM.text('');
			return;
		}
        if(this.displayIdx >= this.vLength)
            this.displayIdx = this.vLength - 1;
		var displayobj = this.getCurrError();
		this.countDOM.text('('+(this.displayIdx+1)+'/'+this.vLength+')');
		this.displayDOM.removeClass('valid').text(displayobj.errormessage);
	};
	vh.prototype.findTarget = function(targetid){
		var target;
		if(targetid)
			target = $(document.getElementById(targetid));
		else if(this.vLength > 0)
			target = $('[name='+this.vList[this.vIdx]+']');
		else
			return;
		
		var rPanel = $(target).parents('.panel')[0];
        
        /* TODO: Review. */
        evaluatorHandler.checkPath(rPanel);

        $('.currPanel').removeClass('currPanel');
        $(rPanel).addClass('currPanel');
		
        Panel.expand( $(rPanel).children('.heading').children('i')[0] );
		$(target).focus().parents('.answer').parent().children().stop(true,true).effect('highlight',{},2000);
		
	};
	vh.prototype.checkElementError = function(name){
		var errobj;
		if(typeof name === 'string'){
			errobj = this.vLookup[name];
		}
		else if(name instanceof verrobj){
			errobj = name;
		}
		if(!errobj)
			return;
			
		var el = errobj.DOM;
		var eltype = el.attr('type');
		var elval = el.val();
        var haserror = false;
        var pevalArr;
        var applicable = true;

		if(!(el.parents('.ppath').length > 0 || el.parents('.vpath').length > 0) || (el.hasClass('trigger_hidden') || (el.parents('.trigger_hidden').length > 0))){
			haserror = true;
			applicable = false;
		}
		
		if(eltype === 'checkbox' || eltype === 'radio')
			el = $('[name='+el.attr('name')+']').filter(':checked');
		
		if(!haserror && (errobj.code & 1) === 1){
			if( el.length === 0 || !elval || elval.length === 0){
				errobj.errormessage = 'missing information for \''+errobj.label+'\'';
				haserror = true;
			}
		}
		//maxlength
		if(!haserror && (errobj.code & 2) === 2){
			var maxlength = el.attr('maxlength');
			var lengthval = parseInt(maxlength);
			if(elval && elval.length >= maxlength){
				errobj.errormessage = 'value exceeds max length for \''+errobj.label+'\'';
				haserror = true;
			}
		}
		//min
		if(!haserror && (errobj.code & 4) === 4){
			var minval = el.attr('min');

			if(el.hasClass('date') || el.hasClass('datetime') || el.hasClass('pastdate')){
				minval = new Date(minval);
				elval = new Date(elval);
			}
            else{
                minval = parseFloat(minval);
                elval = parseFloat(elval);
            }

			if(elval < minval){
				errobj.errormessage = 'value is below the minimum expected value for \''+errobj.label+'\'';
				haserror = true;
			}
		}
		//max
		if(!haserror && (errobj.code & 8) === 8){
			var maxval = el.attr('max');

			if(el.hasClass('date') || el.hasClass('datetime') || el.hasClass('pastdate')){
				maxval = new Date(maxval);
				elval = new Date(elval);
			}
            else{
                maxval = parseFloat(maxval);
                elval = parseFloat(elval);
            }

			if(elval > maxval){
				errobj.errormessage = 'value is above the maximum expected value for \''+errobj.label+'\'';
				haserror = true;
			}
		}
		
        if(!applicable)
            haserror = false;
		
        if (haserror)
        {
            el.addClass('error');
        } else {
            el.removeClass('error');
        }

		if(haserror && this.vList[errobj.idx] == null){
			if(this.vLength === 0){
				this.countDOM.text('(1/1)');
				this.displayDOM.removeClass('valid').text(errobj.errormessage);
				//this.displayIdx = 1;
			}
			this.vList[errobj.idx] = errobj;
			this.vLength++;
            this.countDOM.text('('+(this.displayIdx+1)+'/'+this.vLength+')');
		}
		else if(!haserror && this.vList[errobj.idx] != null){
			this.vList[errobj.idx] = null;
			this.vLength--;
			if(this.vLength === 0){
				this.displayDOM.addClass('valid').text('No validation errors');
				this.countDOM.text('');
			}
			//if current error is resolved and it is the currently displayed element
			else if(this.vIdx === errobj.idx){
				var counter = 0;
				//find the next element
				while(counter < this.vList.length && this.vList[++this.vIdx] == null){
					if(this.vIdx > this.vList.length)
						this.vIdx = -1;
					counter++;
				}
				if(++this.displayIdx >= this.vLength)
					this.displayIdx=0;
				//set the display with the next element
				var displayobj = this.getCurrError();
				this.countDOM.text('('+(this.displayIdx+1)+'/'+this.vLength+')');
				this.displayDOM.text(displayobj.errormessage);
			}
			else {
                this.countDOM.text('('+(this.displayIdx+1)+'/'+this.vLength+')');
			}
		}
		
		return errobj;
	};
	vh.prototype.getCurrError = function(){
		if(this.vLength === 0)
            return;
		var counter = 0;
		while(counter < this.vList.length && this.vList[this.vIdx] == null){
			this.vIdx++;
            if(this.vIdx > this.vList.length)
                this.vIdx = 0;
			counter++;
		}
		var elname = this.vList[this.vIdx];
		var errobj = this.checkElementError(elname);
		return errobj;
	}
	
	win.ValidationHandler = new vh();
})(window);

//end validation handler
//============================================================//

//============================================================//
//begin calc element
(function(win){
	var calcEle = function(){};
	var calcEleInitFn = function(){
		var f = this.getAttribute('data-formula');
		var targets = [];
		var func = calcElement.parseFormula(f,targets);
		var source = this;
		
		for(var i=0;i<targets.length;i++){
			var currtarget = $('[name='+targets[i]+']');
			var targtype = currtarget.attr('type');

			if(targtype === 'text')
				$('[name='+targets[i]+']').keyup(function(){
					$(source).val(calcElement.safeString(func())).change().keyup();
				});
			else
				$('[name='+targets[i]+']').change(function(){
					$(source).val(calcElement.safeString(func())).change().keyup();
				});
						
			$(source).val(calcElement.safeString(func()));
		}
	};
	
	calcEle.prototype.init = function(){
		$('.calculated').each(calcEleInitFn);
	};
	calcEle.prototype.parseFormula = function(str,targets){
		if (str.length === 0)
			return function () { return true; };
		var i = -1;
		var j = 0;
		var c = '';
		while ((i = str.indexOf('[', i+1)) !== -1) {
			if (j < i)
				c = c.concat(str.substring(j, i));
			j = str.indexOf(']', i);
			if (j !== -1 && j > i) {
				var nm = str.substring(i + 1, j).toLowerCase();
				var sub = 'evaluatorHandler.getValByName(\'' + nm +'\')';
				if (!sub) {
					console.log('error getting value of element with name \'' + nm + '\'');
					return;
				}
				c = c.concat(sub);

				if (typeof targets === 'undefined')
					targets = [];
				targets.push(nm);
			}
			j=j+1;
			i=j;
		}
		if (i < str.length)
			c = c.concat(str.substring(j));
		
		try{
			var fn = Function('return ' + c + ';');
			return fn;
		}
		catch(e){
			console.log('Warning: invalid statement "'+c+'"');
		}
		return Function('return null;');
	};
	calcEle.prototype.resolveSelector = function(nm){
		var obj = $('[name='+nm+']');
		if(obj.length === 0)
			return '';
		if(obj[0].tagName === 'INPUT' && (obj[0].type === 'radio' || obj[0].type === 'checkbox'))
			return '[name='+nm+']:checked';
		
		return '[name='+nm+']';
	};

    var safeNumberParse = function(val, parseFunct)
    {
		if(parseFunct && typeof parseFunct === 'function')
        {
			var parseVal = parseFunct(val);

			if(isFinite(parseVal))
				return parseVal;
			else
				return 0;
		} else {
			throw 'parseFunct missing or not a function.';
		}
    }

	//This is to keep NaN/Infinity math returns from displaying in calculated fields.
	calcEle.prototype.safeString = function(val) {
		if(typeof val === "number" && !isFinite(val))
			return 0;
		else
			return val;
	}
	
    calcEle.prototype.safeparseInt = function(val) {
        return safeNumberParse(val, parseInt);
    }

    calcEle.prototype.safeparseFloat = function(val) {
        return safeNumberParse(val, parseFloat);
    }

	win.calcElement = new calcEle();
	
})(window);
//end calc element code
//============================================================//

//============================================================//
//external data import code

(function(win) { 
    win.wireExternalData = function (mrn, apptid, har, form) {
    var table = $('#externaldatatable').dataTable({
            'ajax': { 
                url: 'externaldata.ajax?mrn=' + mrn + '&apptid=' + apptid + '&har=' + har + '&form=' + form,
            'dataSrc': ''
            },
            'columns': [{'data': null, 'render': function (data, type, row) { 
                    var foundElement =  $('[name=' + data.Key + ']').length > 0;
                    var checkedOrDisabled;

                    if(foundElement)
                    {
                        checkedOrDisabled = ' checked="checked"';
                    } else 
                    {
                        checkedOrDisabled = ' disabled="disabled"';
                    }
                    
                    return '<input type="checkbox" data-scamps-name="' + data.Key +'" value="' + data.Value  + '" ' + checkedOrDisabled  + ' />';
                }},
            {'data': 'Key', 'order': 'asc'},
			{'data': null, 'render': function (data, type, row) { 
                    return FormProc.labelForEleName(data.Key);
			}},
            {'data': 'Value'},
			{'data': null, 'render': function (data, type, row) { 
                    try{
						var ele = $('[name="' + data.Key + '"][value="' + data.Value + '"]');
						var text = $('label[for='+ ele.attr('id') +']').text();
					} catch (e) {}
					
					return (text) ? text : data.Value;
			}}],
            'dom': '<"#externaldatacontainer"t>'
        }).api();
		table.column(1).visible(false); //Hide 'Name' (element_cd)
		table.column(3).visible(false); //Hide raw value
    }

    win.updateExternalData = function () 
    {
        var selected = $('#externaldatatable input:checked'); 
        selected.each(function(index, obj) {  
             var jqobj = $(obj); 
 
             var field = $('[name="' + jqobj.data('scamps-name').toLowerCase() + '"]'); 
            
			if(field.attr('type') == 'radio' || field.attr('type') == 'checkbox')
			{
				var filtered = field.filter('[value=' + jqobj.val() +']');
					if(!filtered.is(':checked'))
						filtered.click();
			} else 
				field.val(jqobj.val());
			
			field.change().effect('highlight').addClass('frmimported').on('change', window.clearExternalSet); 
        });
    } 
 
    win.clearExternalSet = function() 
    { 
        $(this).removeClass('frmimported').off('change', window.clearExternalSet); 
    }

})(window);

//end external data import code
//============================================================//
//============================================================//
// *TimeoutIE7 - workarounds for 'long running scripts' on document load
// functions in timerfunc are loaded last to first
var timerfunc = [
	{func: function() { /*get initial value state of form */ initFormVal = $(document.forms[0]).serialize(); }, call: null},
	{func:calcElement,call:'init'},
	{func:FormProc,call:'init'},
	{func:evaluatorHandler,call:'checkAllElements'},
	{func:init_panels,call:null},
	{func:evaluatorHandler,call:'init'},
	{func:ValidationHandler,call:'init'}
];
var tfidx = timerfunc.length;

//calls long-running functions via timers
function tfuncInit(){
	if(--tfidx < 0)
			return;
	var ff = timerfunc[tfidx];
	if(ff.call === null)
		ff.func();
	else{
		ff.func[ff.call].call(ff.func);
	}
	//if(typeof ff === 'function')
	//	ff();
	//else
		//ff.init.call(ff);
	setTimeout(tfuncInit);
};
//============================================================//

window.isScratchPad = function()
{
    return $('body').hasClass('displayall');
};


(function (win) {
	var callbacks = {};
	
	var toggleEditorClick = function(evt)
	{
		var target = $(this).data('inline-target');
		toggleEditor(target);
	}
	
	var toggleEditor = function (target)
	{
		var targetActions = $('[data-inline-target="' + target + '"]');
		
		var display = targetActions.filter('[data-inline-action="display"]').toggle();
		var editor = targetActions.filter('[data-inline-action="editor"]').toggle();
		
		if(display.is(':visible')) //If we're back to the display, reset the input value.
		{
			targetActions.filter('[data-inline-action="value"]').val(display.text());
			eventCallback(target, 'toggleEditor', true);
		} else
			eventCallback(target, 'toggleEditor', false);
	}
	
	var eventCallback = function (target, action, obj)
	{
		if(callbacks['all'] && callbacks['all']['all'])
			callEvery(callbacks['all']['all'], target, action, obj);
		
		if(callbacks['all'] && callbacks['all'][action])
			callEvery(callbacks['all'][action], target, action, obj);
		
		if(callbacks[target] && callbacks[target]['all'])
			callEvery(callbacks[target]['all'], target, action, obj);
		
		if(callbacks[target] && callbacks[target][action])
			callEvery(callbacks[target][action], target, action, obj);
	}
	
	var callEvery = function (funcArr, target, action, obj)
	{
		for(var i=0; i < funcArr.length; i++)
		{
			try{
				funcArr[i](target, action, obj);
			} catch (e)
			{
				console.log('Callback threw', e);
			}
		}
	}

	var submitChange = function (evt)
	{
		var target = $(this).data('inline-target');
		var targetActions = $('[data-inline-target="' + target + '"]');
		
		var data = {r: 'pu', appt_id: location.pathname.substring(location.pathname.lastIndexOf('/') + 1), sub_id: qs('subunit')};
		data[target] = targetActions.filter('[data-inline-action="value"]').val();

		$.ajax({
			url: 'encounter.ajax',
			data: data,
			type: 'POST',
			success: function (evt)
			{
				try
				{
					var data = $.parseJSON(evt);

					if(data[target])
					{
						targetActions.filter('[data-inline-action="display"]').text(data[target]);
						toggleEditor(target);
					} else
						modalMessage('Save', 'Save completed, but new value was not confirmed by the server.');
					
					eventCallback(target, 'save', data);
					
				} catch (e)
				{
					console.log('Error trying to parse server return', e);
					modalMessage('Save', 'Save completed, but an error occurred trying to read server response.');
					eventCallback(target, 'save', e);
				}
			},
			error:function (evt)
			{
				try
				{
					var data = $.parseJSON(evt.responseText);
					if(data['msg'])
					{
						modalMessage('Save error', data['msg']);
					} else 
					{
						modalMessage('Save error', 'Save failed, but the server did not say why.');
					}
					
					eventCallback(target, 'save', data);
				}
				catch (e)
				{
					console.log('Error parsing return from server.', evt);
					modalMessage('Save error', 'Save failed, and an error occurred trying to read server response.');
					eventCallback(target, 'save', e);
				}
			}
		});
	}


	$(function() { 
		$('[data-inline-action="edit"]').on('click', toggleEditorClick);
		$('[data-inline-action="cancel"]').on('click', toggleEditorClick);
		$('[data-inline-action="submit"]').on('click', submitChange);
	});
	
	win.registerEditEvent = function (callback, target, event)
	{
		if(typeof callback !== 'function')
			throw new Error('Callback must be a function.');
		
		if(!target)
			target = 'all';
		
		if(!event)
			event = 'all';
		
		if(!callbacks[target])
			callbacks[target] = {};
			
		if(!callbacks[target][event])
			callbacks[target][event] = [];
		
		callbacks[target][event].push(callback);
	}
	
})(window);
