﻿var helptimer;
var helpeventtimer;
var privatetimer;
var globaltimer;
var isDirty = false;
var maskActive = false;
var highlightid = "";

// === Array Polyfills ===
// Reference: http://es5.github.io/#x15.4.4.14
if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function(searchElement, fromIndex) {
    var k;

    if (this == null) {
      throw new TypeError('"this" is null or not defined');
    }

    var O = Object(this);

    var len = O.length >>> 0;

    if (len === 0) {
      return -1;
    }

    var n = +fromIndex || 0;

    if (Math.abs(n) === Infinity) {
      n = 0;
    }

    if (n >= len) {
      return -1;
    }

    k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

    while (k < len) {
      if (k in O && O[k] === searchElement) {
        return k;
      }
      k++;
    }
    return -1;
  };
}
// Reference: http://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
if (!Array.prototype.filter) {
  Array.prototype.filter = function(fun/*, thisArg*/) {
    'use strict';

    if (this === void 0 || this === null) {
      throw new TypeError();
    }

    var t = Object(this);
    var len = t.length >>> 0;
    if (typeof fun !== 'function') {
      throw new TypeError();
    }

    var res = [];
    var thisArg = arguments.length >= 2 ? arguments[1] : void 0;
    for (var i = 0; i < len; i++) {
      if (i in t) {
        var val = t[i];
        if (fun.call(thisArg, val, i, t)) {
          res.push(val);
        }
      }
    }

    return res;
  };
}
/////////////////////////////////////////////////////////////////////////

//IE prior to 9 doesn't support console.log (correctly). Stub it so we don't throw errors.
if (!window.console) window.console = { 'log': function () { } };

$(document).ready(function () {
    // === do all this stuff when DOM is ready ===

    // register autocomplete events;
    setAutoComplete();

    // highlight something on the page based on a local storage item ID
    highlightid = localStorage.getItem("highlightid");
    if (highlightid != undefined && highlightid != '')
    {
        $("#" + highlightid + ">td").stop(true, true).effect('highlight', {}, 4800);
        localStorage.setItem("highlightid","");
    }

    // unique page stack
    setPage();

    //last viewed url catcher
    (function () {
        var a = [{ target: '/patient/', targethandler: storehandler.patient }, { target: '/patients/encounter/', targethandler: storehandler.encounter}];
        var path = window.location.pathname.concat(window.location.search);
        for (var i = 0; i < a.length; i++) {
            var idx;
            if ((idx = path.indexOf(a[i].target)) !== -1) {
                var val = path.substring(idx + a[i].target.length);
                a[i].targethandler.add(val);
                break;
            }
        }
    })();

    if ($().dataTable) {
        $('.tableize.filter.nopagination').dataTable({
            "oLanguage": { "sSearch": "Filter" },
            "bPaginate": false
        });
        $('.tableize.nofilter.nopagination').dataTable({
            "oLanguage": { "sSearch": "Filter" },
            "bPaginate": false,
            "bFilter": false
        });
        $('.tableize.nofilter.pagination').dataTable({
            "oLanguage": { "sSearch": "Filter" },
            "bPaginate": true,
            "bFilter": false,
            "lengthMenu": [[25, 50, -1], [25, 50, "All"]]
        });
        $('.tableize.filter.pagination').dataTable({
            "oLanguage": { "sSearch": "Filter" },
            "lengthMenu": [[25, 50, -1], [25, 50, "All"]]
        });
    }
    privatetimer = window.setTimeout(function () { screenmask() }, 720000);
    $("body").mousemove(function (e) { setscreenmask() });
    $("body").keydown(function (e) { setscreenmask() });

    $('#importfiletype').on('change', function () {

        var filetype = $('#importfiletype').val();
        if (filetype == 'encounter' || filetype == 'patient' || filetype == 'location') {
            $('#selectfileblock').show();
        }
        else {
            $('#selectfileblock').hide();
        }
        $("#importresults").html('');
    });

    // download excel
    $('i.download').on('click', function (e) {
        var name = $(this).closest("table").attr("name");
        if (name == undefined) { name = 'download';}
        var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        var hour = dt.getHours();
        var min = dt.getMinutes();
        name = name + '.' + year + '.' + month + '.' + day + '.' + hour + '.' + min + '.xls';
        var html = encodeURIComponent('<table>' + $(this).closest("table").html() + '</table>')
        var url = 'data:application/vnd.ms-excel,' + html;
        var a = document.createElement('a');
        a.href = url;
        a.download = name
        a.click();
        //location.href = url;
        e.preventDefault();
        return false;
    });


    // ========== inline numeric validation ==========
    $(".num").on("blur", function (e) {
        var min = parseFloat($(this).attr("min"));
        var max = parseFloat($(this).attr("max"));
        var val = parseFloat($(this).val());
        var isrequired = $(this).siblings('i').hasClass('required');
        $(this).removeClass('error');
        $(this).removeAttr('title');
        if (val < min || val > max) {
            $(this).addClass("error");
            $(this).attr('title', 'Range validation error: enter a value between ' + min + ' and ' + max);
        }
        else {
            if (!$(this).val() && isrequired) {
                $(this).addClass("error");
                $(this).attr('title', 'A value is required');
            }
        }
    });

    bindModalControls();

    // time entry 
    $('.zzztime').on("blur", function (e) {
        var valid = true;
        var hours = 0;
        var minutes = 0;
        var ap = { val: "" };
        var tmp = "";

        $(this).removeClass("error");
        if ($(this).val().length < 4) {
            valid = false;
        }
        else {
            tmp = setap($(this).val(), ap);
            alert("Time= " + tmp + " - - - " + ap.val);
        }

        if (!valid) {
            $(this).addClass("error");
        }
    });
    $('.icon-formcheck').on('click', function (e) {

        $('.author_formcheck').toggle();
    });

    // check rows and tables - single select check box groups - moved from scamps.js so
    // it can be used by other forms
    $(".singlesel").on("click", function () {
        var name = $(this).attr("name");
        $(".singlesel[name='" + name + "']").not(this).removeAttr('checked');
    });

    // ========== requried field inline validation =======
    $('input, select').on('blur', function (e) {
        if ($(this).siblings('i').hasClass('required')) {
            $(this).removeClass('error');
            $(this).removeAttr('title');
            if (!$(this).val()) {
                $(this).addClass('error');
                $(this).attr('title', 'This is a required question');
            }
        }
    });
    //=== clear them on focus...
    $('input, select').on('focus', function (e) {
        if ($(this).siblings('i').hasClass('required')) {
            $(this).removeClass('error');
            $(this).removeAttr('title');
        }
    });
    //============ modal control methods ===============
    $(function () {
        $(".draggable").draggable({
            handle: "h4"
        });
    });
    $(function () {
        $(".resizable").resizable({ handles: 'se' });
    });

    $(".demotoggle").on("click", function (e) {
        if ($("div.stub").is(":hidden")) {
            $("div.stub").fadeIn(100);
        }
        else {
            $("div.stub").fadeOut(150);
        }
    });
    $(".popup").on("click", function (e) {
        e.preventDefault();
        showModal();
    });
 
    $(".endetail .close").on("click", function () {
        $(".endetail").hide();
    });

    $(".back").on("click", function () {
        goBack();
    });
    $('.userlogout').click(function (e) {
        e.preventDefault();
        if ($(".userpanel").is(':hidden')) {
            $(".userpanel").show();
            window.clearTimeout(globaltimer)
            globaltimer = window.setTimeout(function () {
                $(".userpanel").hide();
                window.clearTimeout(globaltimer)
            }, 84000);
            $(".userpanel").on("click", function () {
                $(".userpanel").hide();
            });
        }
        else {
            $(".userpanel").hide();
        }
    });

    $('i.icon-chat').click(function () {
        if ($("div.comments").is(':hidden')) {
            $("div.comments").show();
            window.clearTimeout(globaltimer)
            globaltimer = window.setTimeout(function () {
                $("div.comments").hide();
                window.clearTimeout(globaltimer)
            }, 84000);
            $("div.comments").on("click", function () {
                $("div.comments").hide();
            });
        }
        else {
            $("div.comments").hide();
        }
    });

    $('.logout').click(function () {
        $.ajax({ type: 'POST', data: 'logout=true', url: 'auth.ajax',
            complete: function (j, t) {
                if (j.status === 200) { window.location = "/"; }
            }
        });
    });

    $('.icon-th-1').click(function () {
        screenmask();
    });

    form_init();

});
// =========== form init functions =============
function bindModalControls() {

    // == patient scamp enrollment modal ==
    $('.ScEn').unbind('click').click(function (e) {
        var mrn = getPatMrn();
        var scamp = this.getAttribute('data-sn');
        var scampname = this.getAttribute('data-scamp');
        var currentstatus = this.getAttribute('data-enrollstatus');
        $.get("enrollment.ajax?id=" + mrn + "&v=edit&pid=" + mrn + "&scamp=" + scamp + "&currentstatus=" + currentstatus + "&scampname=" + scampname, function (data) {
            var ra = $.parseJSON(data);
            showModal(ra.modal_title.replace('+',' '), ra.markup);
            if (document.URL.indexOf('/patients/encounter/') > 0) {
                $('.deleteencounter').hide();
                $('.saveandsdf').hide();
            }
        });
    });

    // == new patient ===
    $(".newpatient").unbind("click").on("click", function (e) {
        e.preventDefault();
        var reset = '<span class="resetpatientform">RESET</span>';
        $.get("patientform.ajax", function (data) {
            var ra = $.parseJSON(data);
            showModal("New Patient", ra.markup);

            $(".popmodal input, .popmodal textarea, .popmodal select").prop("disabled", true);
            resetPatient();
            $("#checkMRN").removeAttr("disabled");
            $("#checkMRN").on("blur", function () {
                var mrn = $(this).val().toUpperCase();
                if (mrn != '') {
                    $('.popmodal .message').append('<img id="workinggif" src="/images/working.gif"/>')
                    $.get("checkmrn.ajax?mrn=" + mrn, function (data) {
                        var ra = $.parseJSON(data);
                        var patient = "";
                        $('#workinggif').remove();
                        $("span.message").removeClass("green");
                        $("span.message").html('');
                        $("#testedmrn").val(mrn);
                        if (ra.markup.length > 12) {
                            patient = $.parseJSON(ra.markup);

                            $(".popmodal input, .popmodal textarea, .popmodal select").prop("disabled", false);
                            $(".popmodal button").prop("disabled", false);
                            $("#fname_id").val(patient.fname);
                            $("#lname_id").val(patient.lname);
                            $("#dob_id").val(patient.dob);
                            if (patient.sex == 'F') {
                                $("#sex_id_1").prop("checked", true);
                                $("#sex_id option[value='F']").prop("selected", true);
                            }
                            else {
                                $("#sex_id_2").prop("checked", true);
                                $("#sex_id option[value='M']").prop("selected", true);
                            }
                            $("span.message").html("Found in External Source " + reset);
                            $("#checkMRN").attr("disabled", "disabled");
                            $("#fname_id").focus();

                        }
                        else {

                            $("#fname_id").val('');
                            $("#lname_id").val('');
                            $("#dob_id").val('');
                            $("#sex_id_1").attr("checked", false);
                            $("#sex_id_2").attr("checked", false);

                            if (ra.markup == "NEW") {
                                $(".popmodal input, .popmodal textarea, .popmodal select").prop("disabled", false);
                                $(".popmodal button").prop("disabled", false);
                                $("span.message").html("VALID and NEW MRN " + reset);
                                $("span.message").addClass("green");
                                $("#checkMRN").attr("disabled", "disabled");
                                setFocus();
                            }
                            else {

                                resetPatient();
                                if (ra.markup == "EXISTS") {
                                    var link = '<a href="/patient/' + mrn + '">' + mrn + '</a>';
                                    $("span.message").html("Patient MRN " + link + " already exists " + reset);
                                }
                                if (ra.markup == "BADMRN") {
                                    $("span.message").html("The MRN is invalid " + reset);
                                }
                            }
                        }
                        $('.resetpatientform').on('click', function (e) {
                            resetPatient();
                        });

                    });
                }
            });
        });
    });

    $(".editpatient").unbind("click").on("click", function (e) {
        e.preventDefault();
        var id = $(this).attr("data-id");
        highlightid = $(this).parent().parent().attr("id");
        localStorage.setItem("highlightid", highlightid);
        recentListing('patient', id);
        $.get("modalform.ajax?what=patient&v=edit&pid=" + id, function (data) {
            var ra = $.parseJSON(data);
            showModal("Edit Patient", ra.markup);
        });
    });

    $(".newencounter").unbind("click").on("click", function (e) {
        e.preventDefault();
        var id = $(this).attr("data-id");
        var scampid = $(this).attr("data-scamp-id");
        var encounterid = $(this).attr("data-encounter-id");
        $.get("encounterform.ajax?mrn=" + id + "&scamp_id=" + scampid + "&eid=0&v=new", function (data) {
            var ra = $.parseJSON(data);
            showModal(ra.modal_title, ra.markup);
        });
    });

    $(".editencounter").unbind("click").on("click", function (e) {
        e.preventDefault();
        var id = $(this).attr("data-id");
        var apptid = $(this).attr("data-apptid");
        var subid = $(this).attr("data-subid");
        var scampid = $(this).attr("data-scamp-id");
        highlightid = $(this).parent().parent().attr("id");
        localStorage.setItem("highlightid", highlightid);
        recentListing('encounter', apptid);
        $.get("encounterform.ajax?id=" + id + "&v=edit&aid=" + apptid + "&sid=" + subid, function (data) {
            var ra = $.parseJSON(data);
            showModal(ra.modal_title, ra.markup);
            if(document.URL.indexOf('/patients/encounter/') >0)
            {
                $('.deleteencounter').hide();
                $('.saveandsdf').hide();
            }
        });
    });
    return true;
}
function resetPatient(e) {

    $("span.message").html("");
    $(".popmodal input").prop("checked", false);
    $(".popmodal button").prop("disabled", true);
    $(".popmodal input, .popmodal textarea, .popmodal select").attr("disabled", "disabled").not(".savepatient, input[type='radio'],input[type='hidden']").val("");
    $("#checkMRN").removeAttr("disabled").val("");
    $("#checkMRN").focus();

}
function loginmodal(e) {

    //We're at the login screen. Don't use the modal, just let them log in.
    if ($("div.centerform form[name='login'].login").length > 0) {
        hideOverlay();
        hideModal();
        return;
    } 

    //$('.modal').removeAttr('style');
    //$('.buttons').append('<input type="submit" value="OK" class="ok"/>');
    $.get("loginmodal.ajax", function (data) {
        var ra = $.parseJSON(data);
        $(".modal .buttons").html('');
        $(".modal .contents").html(ra.markup);
        $(".modal .modalcontrol h4").html("Login");
        $(".overlay").css("display","block");
        $(".modal").centerInClient();
        $(".modal .box").hide();
        setFocus();
        $(".modal").show();
        
        form_init(e);
        $('.buttons').append('<input type="submit" value="OK" class="ok"/>');
    });
}
function form_init(e){

    $(".icon-attach").on("click", function (e) {
        var apptid = $(this).attr("data-value");
        var pid = $(this).attr("data-id");
        $.get("linkedencounterform.ajax?encounter_id=" + apptid + "&pid=" + pid, function (data) {
            var ra;
            try {
                ra = $.parseJSON(data);
            } catch (err) { console.error(err); }
            //$(".modal .contents").html(ra.markup);
            //$(".modal .modalcontrol h4").html("Linked Encounters");
            //$(".overlay").css("display", "block");
            //$(".modal").centerInClient();
            //setFocus();
            showModal("Linked Encounters", ra.markup);
            //form_init();
        })
    });

    // ==== login support =====
    $('.btnlogin').on('click', function (e) {
        e.preventDefault();
        var payload = $(this.form).serialize();
        var action = $(this.form).attr('action');
        //disable form items
        document.body.style.cursor = 'progress';
        $('.login [name], .login [type=submit]').attr('disabled','disabled');
        var message = '';
        var title = '';

        $.ajax({ type: 'POST', data: payload, url: action,
            complete: function (j, t) {
                var obj;
                try{
                    obj = $.parseJSON(j.responseText);
                }catch(e){}
                document.body.style.cursor = '';
                var url = qs('url');
                if (typeof url === 'string' && url.toLowerCase() == 'default') { url = '/default'; }
                if (url.length == 0) { url = '/default'; }
                if (typeof qs === 'string' && qs.length > 0) { url = url + "?" + qs; }
                if (!obj) 
                {
                    modalMessage('Error!', 'Error reading server response','error',function(){
                        loginmodal();
                    });
                    return;
                }
                if (j.status === 200) {
                    if (obj.error === true) {
                         modalMessage('Error!', obj.msg,'error',function(){
                            loginmodal();
                        });
		                document.body.style.cursor = '';
				        $('.login [name], .login [type=submit]').removeAttr('disabled');
                    }
                    else {

                        $(".modal .box").show();
                        if (!url.length == 0 && $("#redirect_id").val() != 'false') 
                        {
                            window.location = url + window.location.hash;
                        }
                        else
                        {
                            hideModal();
                            if(window.location.pathname.toLowerCase().indexOf('/login') != -1)
                            {
                                window.location = '/';
                            }
                        }
                    }
                    $('.buttons').append('<input type="submit" value="OK" class="ok"/>');
                }
                else {
                    document.body.style.cursor = '';
                    $('.login [name], .login [type=submit]').removeAttr('disabled');
                    modalMessage('Server Error', obj.msg, 'error');
                }
            }
        });
    });

    /* ====== table editor ====== */
    $(".edittable-data").on("click", function (e) {
        e.preventDefault();
        if ($("table.edittable").find(".data").length == 0) {
            var editcell = $(this).parent();
            var editname = editcell.attr("data-code");
            var editval = editcell.attr("data-value");
            var editblock = '<input class="data" type="text" name="' + editname + '" value="' + editval + '"/>';
            editcell.find("span.edittable-data").html(editblock);
            editcell.find("input.data").focus();

            editcell.find("span.controls a.edit").removeClass('icon-edit');
            editcell.find("span.controls a.edit").addClass('icon-ok-circled');
            editcell.find("span.controls a.edit").addClass('submit');
            editcell.find("span.controls a.cancel").removeClass("disabled");
            $("table.edittable").find("a.icon-edit,a.disabled").hide();
        }
    });

    $(".edittable a.edit").on("click", function (e) {
        e.preventDefault();
        var editcell = $(this).parent().parent();
        var editname = editcell.attr("data-code");
        var editval = editcell.attr("data-value");

        // what is the action we are taking, create editor or submit editor?
        if ($(this).hasClass("submit")) {
            $("table.edittable").parent().submit();
            var val = editcell.find('[name=' + editname + ']').val();
            editcell.find("span.edittable-data").html(val);
            editcell.attr("data-value", val);
            $(this).addClass('icon-edit');
            $(this).removeClass('icon-ok-circled');
            $(this).removeClass('submit');
            $(this).siblings(".cancel").addClass("disabled");
            $("table.edittable").find("a.icon-edit,a.disabled").show();
        }
        else {
            var editblock = '<input class="data" type="text" name="' + editname + '" value="' + editval + '"/>';
            editcell.find("span.edittable-data").html(editblock);
            editcell.find("input.data").focus();

            $(this).removeClass('icon-edit');
            $(this).addClass('icon-ok-circled');
            $(this).addClass('submit');
            $(this).siblings(".cancel").removeClass("disabled");
            $("table.edittable").find("a.icon-edit,a.disabled").hide();
        }
    });

    $(".edittable a.cancel").on("click", function (e) {
        e.preventDefault();
        if (!$(this).hasClass("disabled")) {
                var editcell = $(this).parent().parent();
            var val = editcell.attr("data-value");
            editcell.find(".edittable-data").html(val);

            $(this).siblings(".edit").addClass('icon-edit');
            $(this).siblings(".edit").removeClass('icon-ok-circled');
            $(this).siblings(".edit").removeClass('submit');
            $(this).addClass("disabled");
            $("table.edittable").find("a.icon-edit,a.disabled").show();
        }
    });

    //==============================

    // ======= Bind Form, Serialize and Submit =======
    $("form.autoform, form.popmodal").unbind('submit').on("submit", function (e) {
        var f = $(this);
        var formData = null;
        var action = f.attr("action");
        //$('input [type=submit]').attr("disabled", true);
        // we will want to know which button caused the action
        // as it may determine the followup action
        var submit_button = $(document.activeElement);
        var he = document.createElement('INPUT');
        he.type = 'hidden';
        he.name = 'form_submit_action';
        he.value = $(submit_button).attr("data-action");
        f.append(he);
        he = document.createElement('INPUT');
        he.type = 'hidden';
        he.name = 'form_submit_text';
        he.value = $(submit_button).val();
        f.append(he);

        if (action.length > 0 && f.find('[name=sendto]').length === 0) {
            var he = document.createElement('INPUT');
            he.type = 'hidden';
            he.name = 'sendto';
            he.value = action;
            f.append(he);
        }

        var fileEle = $(this).find('[type=file]');
        var hasFormData = (typeof FormData === 'function');
        //if file upload occuring but browser doesn't support FormData, do vanilla form submit
        var submitobj = $(this).find('[type=submit]');

        submitobj.attr('disabled', true);

        //
        var succfun = function (data, j, t) {
            
            var ra = $.parseJSON(data);
            if (ra.status == "OK") {
                if (ra.highlightid != undefined && ra.highlightid != '')
                {
                    localStorage.setItem("highlightid", ra.highlightid);
                }
                var actions = ra.action.split('|');
                for (var ii = 0; ii < actions.length; ii++ )
                {
                    action = actions[ii];
                    switch (action.toUpperCase()) {

                        case "RELOAD":
                            hideModal();
                            document.location.reload();
                            break;

                        case "SUCCESSREFRESH":
                            window.location.href = window.location.href;
                            break;

                        case "ENCOUNTERBUTTONSHIDE":
                            $('.deleteencounter').hide();
                            $('.saveandsdf').hide();
                            break;

                        case "REDIRECT":
                            if (ra.url) {
                                window.location.href = ra.url;
                            }
                            else {
                                window.location.href = ra.data;
                            }
                            break;

                        case "HIGHLIGHT":

                            $("#" + ra.highlightid + ">td").stop(true, true).effect('highlight', {}, 3400);
                            highlightid = "";
                            break;

                        case "SETLSPATIENT":
                            recentListing('patient', ra.id);
                            break;

                        case "SUCCESSFORM":
                            $(f).html(ra.markup);
                            form_init();
                            break;
                        case "SUCCESSMODALFORM":
                            showModal(ra.modal_title,ra.markup)
                            break;
                        case "CLOSE":
                            hideModal();
                            if (ra.data.toUpperCase() == "ENCOUNTER") {
                                var apptid = $(".icon-attach").attr("data-value");
                                var pid = $(".icon-attach").attr("data-id");
                                $.get("encounterlist.ajax?encounter_id=" + apptid + "&pid=" + pid, function (data) {
                                    var ma = $.parseJSON(data);
                                    $(".linked-encounters").html(ma.markup);
                                    form_init();
                                });
                            }

                            break;
                        case "UPDATE":
                            $(ra.data).html(ra.markup);

                            if (ra.hide != '') {
                                $(ra.hide).hide();
                            }
                            if (ra.disable != '') {
                                $(ra.disable).attr("disabled", "disabled");
                            }
                            break;
                        default:
                            //do nothing...
                            /*$(f).trigger('success');
                            $(f).html(ra.markup);
                            form_init(); */

                            break;
                    }
                }

                //submitobj.removeAttr('disabled');

            } else if (ra.status == "HASERRORS") {
                $(f).html(ra.markup);

                form_init();
            } else {
                alert("Unexpected Error: " + ra.status);
            }
        };

        var failfun = function (jqXHR, textStatus, errorThrown) {
            $("#uploadresults").html("An unexpected error occurred while uploading your file. Ensure it is a valid scamp package."); /* This will only work for the admin page file upload. Which, I think is fine since it's the only thing using this. */
        };

        //We have a file, but form data isn't supported. Inject an iframe (IE <9)
        if (fileEle.length > 0 && !hasFormData) {
            var iframe = document.getElementById('iuploadfile');
            if (iframe === null) {
                iframe = document.createElement('iframe');
                iframe.id = 'iuploadfile';
                iframe.name = 'iuploadfile';
                iframe.style.display = 'none';
                document.body.appendChild(iframe);
                $(iframe).load(function () {
                    try {
                        succfun($(this).contents().find('body').html().replace(/'\\"/g, '\\"').replace(/\\\"'/g, '\\"'), null, null);
                    }
                    catch (err) {

                    }
                });
            }
            this.target = 'iuploadfile';
            return;
        }

        var aobj = null;
        aobj = {
            url: 'formsprocessor.form',
            type: 'POST',
            success: succfun,
            error: failfun

        };

        //We have a file and form data. Need can use FormData to send the file to the server.
        if (fileEle.length > 0 && hasFormData) {
            aobj.data = new FormData(this);
            aobj.processData = false;
            aobj.contentType = false;
        } else { //No file, just use f.serialize.
            aobj.data = f.serialize();
        }

        e.preventDefault();
        var aobj = $.ajax(aobj);

    });

    //=== set key masks ===

    $(".scientific").keypress(function (e) { return scientific(e, $(this).val()); })
		.on('paste', function (e) { if (!keylimited(getClipboardText(e), scientific)) e.preventDefault(); });
    $(".int").keypress(function (e) { return unsignedInteger(e, $(this).val()); })
		.on('paste',function(e){ if( !keylimited(getClipboardText(e),unsignedInteger) ) e.preventDefault(); });
    $(".sint").keypress(function (e) { return signedInteger(e, $(this).val()); })
		.on('paste',function(e){ if( !keylimited(getClipboardText(e),signedInteger) ) e.preventDefault(); });
    $(".fp, .float").keypress(function (e) { return floatPoint(e, $(this).val()); })
		.on('paste',function(e){ if( !keylimited(getClipboardText(e),floatPoint) ) e.preventDefault(); });
    $(".sfp, .sfloat").keypress(function (e) { return sfloatPoint(e, $(this).val()); })
		.on('paste', function (e) { if (!keylimited(getClipboardText(e), sfloatPoint)) e.preventDefault(); });
    $(".quicksearch").keypress(function (e) { return searchmask(e, $(this).val()); })
    .on('paste', function (e) { if (!keylimited(getClipboardText(e), searchmask)) e.preventDefault(); });
    $(".timezzz").keypress(function (e) { return time(e, $(this).val()); })
		.on('paste', function (e) { if (!keylimited(getClipboardText(e), time)) e.preventDefault(); });

    // ========== render calendars ==========
    $('.date').datepicker({
        dateFormat: localdateformat,
        changeMonth: true,
        changeYear: true,
        constrainInput: false,
        yearRange: "-100:+0",
        buttonImage: "/images/calendar.png",
        showOn: 'button',
        onSelect: function(date) {
			  $( "input[type=text][id*=Date]" ).datepicker("hide");
			  $(this).removeClass('error');
			  $(this).focus();
			  $(this).change();
          }
    });

    $('.pastdate').datepicker({
        dateFormat: localdateformat,
        changeMonth: true,
        changeYear: true,
        constrainInput: false,
        yearRange: "-100:+0",
        buttonImage: "/images/calendar.png",
        showOn: 'button',
        maxDate: '0',
        yearRange: "c-80:c",
        onSelect: function (date) {
            $("input[type=text][id*=Date]").datepicker("hide");
            $(this).removeClass('error').focus().change();
        }
    });

    $('.pastdatetime').datepicker({
        dateFormat: localdateformat,
        timeFormat: localtimeformat,
        constrainInput: false,
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0",
        buttonImage: "/images/calendar.png",
        showOn: 'button',
        maxDate: '0',
        yearRange: "c-80:c",
        onSelect: function (date) {
            $("input[type=text][id*=Date]").datepicker("hide");
            $(this).removeClass('error').focus().change();
        }
    });

    $('.militarytime').datetimepicker({
        dateFormat: localdateformat,
        timeFormat: miltimeformat,
        changeMonth: true,
        changeYear: true,
        constrainInput: false,
        yearRange: "-100:+6",
        buttonImage: "/images/calendar.png",
        showOn: 'button',
        onSelect: function (date) {
            $("input[type=text][id*=Date]").datepicker("hide");
            $(this).change();
        }
    });

    $('.datetime').datetimepicker({
        dateFormat: localdateformat,
        timeFormat: localtimeformat,
        changeMonth: true,
        changeYear: true,
        constrainInput: false,
        yearRange: "-100:+6",
        buttonImage: "/images/calendar.png",
        showOn: 'button',
        onSelect: function(date) {
          $( "input[type=text][id*=Date]" ).datepicker("hide");
		  $(this).change();
		}
    });

    $('.time').timepicker({
        hourGrid: 4,
        minuteGrid: 10,
        timeFormat: localtimeformat
    });

    $('.miltime').timepicker({
        hourGrid: 4,
        minuteGrid: 10,
        timeFormat: miltimeformat
    });


    $('.pastdate').blur(function (e) {
        var d = Date.parse($(this).val());
        var today = new Date();
        if (d > today) {
            $(this).addClass("error");
            $(this).attr("title", "This is not a date in the past");
        }
        else {
            $(this).removeClass("error");
            $(this).attr("title","");
        }

    });

    //=== set default form focus ===
    setFocus();

    $("#tabs").tabs();

    setAutoComplete();
    setToolTips();

    //========== HELP Tooltips ===============
    $(".helpinfo").each(function () {
        if ($(this).attr("data-info") == '') { $(this).addClass("hide"); }
    });
}


// =========== DOM binder functions ===========//
(function(){
	var ipb = {
		//binds an input to a list of select or autocomplete options generated by javascript which can be categorically filtered
		//based upon the DOM element's attributes
		getDLdataSrc: function (e) {
			var src = $(e).attr('data-src');
			var cat = $(e).attr('data-cat');
			if(e.tagName === 'SELECT')
				$(e).empty();
			if(src){
				if(cat){
					var catArr = cat.split(',');
					return ( ddlData[src].filter(function(e){ return (catArr.indexOf(String(e.c)) != -1); }) );
				}
				return ddlData[src];
			}
			return [];
		},
		getDLdataSrcValFilter: function (e,fprop,val){
            		if(e.tagName !== 'SELECT')
                		return;
			var src = $(e).attr('data-src');
			$(e).empty();
			if(src){
		                var lut = {};
                		for(var i=0;i<ddlData[fprop].length;i++){
		                    if(ddlData[fprop][i].label == val && typeof lut[ddlData[fprop][i].label] === 'undefined')
                		        lut[ddlData[fprop][i].value] = null;
                		}
	                	for(var i=0;i<ddlData[src].length;i++){
        	            		if(typeof lut[ddlData[src][i].value] === 'object'){
                		        	var o = document.createElement('OPTION');
                	        		o.label = ddlData[src][i].label;
                        			o.value = ddlData[src][i].value;
                        			$(o).text(o.label);
                        			$(e).append(o);
                    			}
                		}
			}
		},
		findDLOption: function(list,prop,cat,value){
			var opts;
			if(!list || !(opts = list[prop]))
				return false;

			for(var i=0;i<opts.length;i++){
				if((!cat || opts[i].c == cat) && opts[i].value == value)
					return opts[i];
			}
			return false;
		},
		bindDLOptionByValue: function(name,value){
			var targ = $('[data-name='+name+']')[0];
			var hiddentarg = $('[name='+name+']')[0];
			if(!targ || !hiddentarg)
				return;
			hiddentarg.value = value;
			var opt = DOMBindHelper.findDLOption(ddlData,targ.getAttribute('data-src'),false,value);
			if(!opt)
				return;
			targ.value = opt.label;
		},
		//creates options for a select from a javascript source
		initSelect: function(ele){
			if(!ele || !ele.className)
				ele = this;
			if(!ele || !ele.className)
				return;
			var tag = ele.getAttribute('data-src');
			var src = ddlData[tag];
			if(!src)
				return;
			//$(ele).empty();
			for(var i=0;i<src.length;i++){
				var e = document.createElement('OPTION');
				e.value=src[i].value;
				e.label=src[i].label;
                		$(e).text(src[i].label);
				ele.appendChild(e);
			}
		},
		//initializes autocomplete so that it can utilize a list that can be categorically filtered via javascript
		initAutoComplete: function(){
            		if(typeof ddlTree === 'undefined')
                		ddlTree = {};
            		if(typeof ddlKey === 'undefined')
                		ddlKey = {};

			var tag = $(this).attr('data-src');
			if(tag && ddlData[tag] instanceof Array && typeof ddlTree[tag] === 'undefined'){
		        	if(!(ddlTree[tag] instanceof TTree))
			        	ddlTree[tag] = new TTree({});

		        	ddlKey[tag] = {};
		        	for(var i=0;i<ddlData[tag].length;i++){
					var ss = ddlData[tag][i].label.toLowerCase();
					if(ss.length === 0)
						continue;
					if(typeof ddlKey[tag][ss] === 'undefined')
						ddlKey[tag][ss] = [];
					ddlTree[tag].add(ss);
					ddlKey[tag][ss].push(i);
					
			        var sp = ss.split(' ');
					
			        for(var j=1;j<sp.length;j++){
                        		if(sp[j].length === 0)
                            			continue;
				        ddlTree[tag].add(sp[j]);
				        if(typeof ddlKey[tag][sp[j]] === 'undefined')
					        ddlKey[tag][sp[j]] = [];
				        //ddlKey[tag][sp[j]].push(ddlData[tag][i]);
                        		ddlKey[tag][sp[j]].push(i);
			        }
		        }
	        }

		$(this).autocomplete({
			//source: DOMBindHelper.getDLdataSrc(this),
			source: function(req,resp){
				var tag = $(this.element).attr('data-src');
				var cat = $(this.element).attr('data-cat');
				if(cat !== undefined){
					cat = cat.split(',');
					var i = cat.length;
					while(i--)
						cat[i] = parseInt(cat[i]);
				}
				var a = [];
				var _t = ddlTree[tag].matches(req.term.toLowerCase());
				if (!_t || _t.length === 0){
					resp(a);
					return;
				}
				//build list of matches
				var dobj;
				for(var i=0;i<_t.length;i++){
					var k = ddlKey[tag][_t[i]];
					if(typeof k === 'undefined')
						continue;
					for(var j=0;j<k.length;j++){
						dobj = ddlData[tag][k[j]];
						if(a.indexOf(dobj.label) === -1){
							if(cat !== undefined && cat.indexOf(dobj.c) === -1)
								continue;
							a.push(dobj.label);
						}
					}
				}
		    	a.sort();
		    	resp(a);
		    },
		    change: function(event, ui){
				var currVal = $(this).val();
				var htarget = $('input[type=hidden][name=' + $(this).attr('data-name') + ']');
				$(this).removeAttr('title');
				if(currVal === ''){
					htarget.val('');
					$(this).removeClass('error');
					return;
				}
				var tag = $(this).attr('data-src');
				var src = ddlData[tag];
				var id;
				var idx;
				for (var i = 0; i < src.length; i++) {
					if (src[i].label === currVal) {
						id = src[i].value;
						break;
					}
				}
				// manually update the textbox and hidden field
				if (id) {
					//$(this).css('border', '1px solid green');
					$('input[type=hidden][name=' + $(this).attr('data-name') + ']').val(id);
					$(this).removeClass('error');
					$(this).prop('title', '');
				} else if(!$(this).val())
                {
                    $('input[type=hidden][name=' + $(this).attr('data-name') + ']').val('');
                    $(this).removeClass('error');
                }
                else {
					//$(this).css('border', '1px solid red');
					$('input[type=hidden][name=' + $(this).attr('data-name') + ']').val('-1');
					$(this).addClass('error');
					$(this).prop('title', 'Value not found in the list. This will not be saved until it matches one of the available auto-complete values.');
				}
			},
			focus: function(event, ui) {
				// prevent autocomplete from updating the textbox
				event.preventDefault();
				// manually update the textbox
	    			$(this).val(ui.item.label);
			},
			select: function(event, ui){
				event.preventDefault();
		                $(this).removeClass('error');
		                $(this).prop('title', '');
				// get data source, see if label value exists
				var tag = $(this).attr('data-src');
				var src = ddlData[tag];
				var id;
				var idx;
				for (var i = 0; i < src.length; i++) {
					if (src[i].label === $(this).val()) {
						id = src[i].value;
						break;
					}
				}
				// manually update the textbox and hidden field
				if (id) {
					//$(this).css('border', '1px solid green');
					$('input[type=hidden][name=' + $(this).attr('data-name') + ']').val(id);
			}
			else {
				//$(this).css('border', '1px solid red');
				$('input[type=hidden][name=' + $(this).attr('data-name') + ']').val('');
			}
		}
		}).autocomplete('widget').addClass('fixed-height');
		}
	};
	window.DOMBindHelper = ipb;
})();

function setToolTips(e) {

    // ======== generate 'tool tip' from info tag ========
    $(".helpinfo").unbind("mouseover").on("mouseover", function (e) {
        //reset the previous timeout, if any
        window.clearTimeout(helptimer);
        //hide/remove other helptext popups
        $("#helptext").hide();
        $("#helptext").each(function () {
            $(this).remove()
        });

        xOffset = 20;
        yOffset = -16;
        if ($("#helptext").notexists) {
            var info = "<p id='helptext'></p>";
            $("body").append(info);
        }
        $("#helptext").html($(this).attr("data-info").replace("\'", "'"));
        if ($("#helptext").html().length > 140) { $("#helptext").css("width", "480px"); }
        $("#helptext")
            .css("top", (e.pageY + yOffset) + "px")
            .css("left", (e.pageX + xOffset) + "px");
        $("#helptext").show();

        $("#helptext").unbind("mouseover").on("mouseover", function (e) {
            window.clearTimeout(helptimer);
        });

        $(".helpinfo, #helptext").unbind("mouseout").on("mouseout", function (e) {
            helptimer = window.setTimeout(function () { hideMe('#helptext') }, 1700);
        });

        $(".helpinfo, #helptext").unbind("click").on("click", function (e) {
            window.clearTimeout(helptimer);
            $("#helptext").toggle();
        });
    });



}
// =========== end DOM bind helper==========//

// =========== helper functions ===========//
function setFocus() {
    
    //$(" .hasfocus").focus();
    $(".hasfocus").noScrollFocus();
}

function setap(teststring, ap) {
    var tmp = "";
    if (teststring.indexOf('a') > 1) {
        ap.val = 'a';
        tmp = teststring.substring(0,teststring.indexOf('a'));
    }
    else {
        if (teststring.indexOf('p') > 1) {
            ap.val = 'p';
            tmp = teststring.substring(0,teststring.indexOf('p'));
        } else {
            if (teststring.indexOf(' ') > 1) {
                ap.val = 'm';
                tmp = teststring.substring(0, teststring.indexOf(' '));
            }
        }
    }
    if (tmp == '') { tmp = teststring; }
    return tmp.trim();

}

function setPage() {
    var ref = document.referrer;
    if (window.location.href != ref)
    {
        var lastpage = localStorage.getItem("lastpage");
        if ((ref + '').toLowerCase() != (lastpage + '').toLowerCase()) { localStorage.setItem("lastpage", ref) }
    }
}


function returnPage () {

    var url = '/dashboard';
    var tmp = qs('url');

    if (tmp.length > 0) {
        url = window.location.origin + tmp;
    }
    else
    {
        url = localStorage.getItem("lastpage");
        if (url.length == 0 || url.toString().indexOf('/login?') != -1)
        {
            if ((tmp = document.referrer) && tmp.length > 0 && tmp.indexOf('/login?') === -1) 
            {
                url = tmp;
            }  
        }
    }
    if (window.location.href == url || url.toString().indexOf('/login?') != -1)
    {
        url = "/dashboard";
    }
    return url;
}
//goback function fixed to support login redirect referrers or other no-direct locations...
function goBack(){
    var ref = document.referrer;
    if(document.URL.startsWith(document.referrer) || document.referrer.indexOf(login) == -1)
    {
        window.history.back();
    }
    else
    {
        //get back, get what?
        var url = '';
        var v = document.URL.split("/");
        for(i=0;i<v.length-1;i++)
        {
            url += v[i] + "/";
        }
        windows.location = url;
    }

}

function hideMe(elementid) {
    $(elementid).fadeOut();
}
function showModal(title,contents) {
    var t = (title == null) ? "Modal" : title;
    isDirty = false;
    // reset any active helptext tip windows
    hideMe('#helptext');
    window.clearTimeout(helptimer);

    // because of reasons (poor use of modal by other code)
    // we need to clean up the container a touch
    $('.modal').removeAttr('style');
    $('.buttons>input').remove();
    
    showOverlay();
    $(".modal .contents").html(contents);
    $(".modalcontrol h4").html(t);
    $(".modal").show();
    form_init();
    $(".modal").centerInClient();


    $(".modalcontrol .box").unbind("click").on("click", function (e) {
        e.preventDefault()
        $(".modal").hide();
        $(".fmodal").hide();
        $(".overlay").css("display", "none");
        if (highlightid != '')
        {
            $("#" + highlightid + ">td").stop(true, true).effect('highlight', {}, 3400);
        }
        
        highlightid = "";
    });

}
function hideModal(){
    $(".modal").hide();
    $(".fmodal").hide();
    hideOverlay();
	$('.modal').removeAttr('style');
}

function showOverlay() {
    $(".overlay").css("display","block");
}
function hideOverlay() {
    $(".overlay").css({
		"display": "none",
		"opacity": "0.85",
		"z-index": "999"		
	});
}

function modalMessage(title, message, messagetype, callback) {
    var icon = "";
    switch (messagetype) {
        case "error":
            icon = '<i class="icon-attention"></i>';
            break;
        case "info":
            icon = '<i class="icon-info-circled"></i>';
            break;

    }
    // $(".modal .modalcontrol h4").html(icon + title);
    // $(".modal .contents").html(message);
    modalButtons(['OK']);
    $('.modal .buttons').unbind('click');
    if(typeof(callback) === 'function')
    {
        $('.modal .buttons').on('click', '.ok', callback);
    }
    else
    {
        $('.modal .buttons').on('click', '.ok', function () {
            hideModal();
        });
    }
    showModal(icon + title, message);
}

function modalButtons(buttons) {
    var buttonmarkup = "";
    for (i = 0; i < buttons.length; i++)
    {
        buttonmarkup += '<input type="submit" value="' + buttons[i] + '" class="' + buttons[i].toLowerCase() + '"/>';
    }
    $(".modal .buttons").html(buttonmarkup);
}

function screenmask() {
    hideModal();
    $(".overlay").css("opacity", "1.0");
    $(".overlay").css("z-index", "1001");
    $(".overlay").css("display","block");
    $(".overlay").on("click", function () {
        $(".overlay").off("click");
        $.get("isAuthenticated.ajax", function(data) {
            try{
                var ra = $.parseJSON(data);
                if(ra.markup == 'True')
                {
                    $(".overlay").css("opacity", "0.85");
                    $(".overlay").css("z-index", "999");
                    $(".overlay").css("display","none");

                    return;                   
                }
            }
            catch(err) {}
            $(".overlay").css("z-index", "999");
            loginmodal();
            });
        
    }); 
}
function setscreenmask() {
    window.clearTimeout(privatetimer)
    privatetimer = window.setTimeout(function () { screenmask() }, 720000);

}
function keyshortcuts(e) {
    // test for ALT_O or ALT+Down Arrow= open All panels
    if (e.altKey && (e.keyCode == 79 || e.keyCode == 40 )) {
        openAll();
        $("#formview").val(1);
    }
    //test Alt-N/ALT+Up Arrow
    if (e.altKey && (e.keyCode == 78 || e.keyCode == 38 )) {
        init_panels();
        $("#formview").val(0);
    }
    //test Alt-V/ALT+F1 for form validation
    if (e.altKey && (e.keyCode == 86 || e.keyCode == 112 )) {
        init_panels();
        $("#formview").val(0);
    }

    if(e.ctrlKey && e.keyCode == 37)
    {
        e.preventDefault();
        evaluatorHandler.prevPanel(evaluatorHandler.path[evaluatorHandler.path.length - 1]);
    }

    if (e.ctrlKey && e.keyCode == 39) {
        e.preventDefault();
        evaluatorHandler.evalPanel(evaluatorHandler.path[evaluatorHandler.path.length - 1]);
    }
}

// LOCAL STORAGE 
// This duplicates functionality below which is
// STUPID but this is much more friendly and easier
// to use and doesn't require a single path concept
// to use.  The other one remains in case it is still
// called somewhere on the search or patient page
// when those are finally removed with great prejudice

function recentListing(key, val) {
    if (typeof val != "string" && val.toString)
        val = val.toString();

    var currobj = storehandler.getObject(key, { i: -1, a: new Array(10) });
    if (currobj && currobj.a && currobj.a.indexOf(val) === -1) {
        currobj.i = ++currobj.i % currobj.a.length;
        currobj.a[currobj.i] = val;

        localStorage.setItem(key, JSON.stringify(currobj));
    }
}

function salt() {
    var r = Math.floor(Math.random() * 5001);
    return "&salt=" + r;
}

function isArray(obj) {  
    if (!obj || obj.constructor.toString().indexOf("Array") == -1)
        return false;
    else
        return true;
}

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function isBetween(val, low, high) {
    if (val > low && val < high) {
        return true;
    } else {
        return false;
    }
}

//========== input key limiters ===================//

function searchmask(e, val) {
    return !keylimiter(e, ".");
}

function noNumbers(e,val) {
    return keylimiter(e, "/\D/");
}

function scientific(e, val) {
    if (val.indexOf(".") > -1) {
        return keylimiter(e, "0123456789^/");
    }
    else {
        return keylimiter(e, "0123456789+-^x/.");
    }
}

function unsignedInteger(e, val) {
    return keylimiter(e, "0123456789");
}

function signedInteger(e, val) {
    var charmap = "0123456789";
    if (val.indexOf("+") == -1 && val.indexOf("-") == -1) { charmap += "-+"; }
    return keylimiter(e, charmap);
}

function sfloatPoint(e, val) {
    var charmap = "0123456789.";
    if (val.indexOf(".") > -1) { charmap = "0123456789"; }
    if (val.indexOf("+") == -1 && val.indexOf("-") == -1  ) { charmap += "-+"; }
    return keylimiter(e, charmap);
}
function floatPoint(e, val) {
    var charmap = "0123456789.";
    if (val.indexOf(".") > -1) { charmap = "0123456789"; }
    return keylimiter(e, charmap);
}

function time(e, val) {
    var charmap = "0123456789";
    if (val.indexOf(":") > -1) { charmap = "0123456789 APMapm"; }
    if (val.length > 0) { charmap = "0123456789:"; }
    if (val.length > 4) { charmap = " apmAPM:"; }
    return keylimiter(e, charmap);
}

function getClipboardText(e){ 
	var clip = '';
	if(e && e.originalEvent && e.originalEvent.clipboardData)
		clip = e.originalEvent.clipboardData.getData('Text');
	else if(window.clipboardData)
		clip = window.clipboardData.getData('Text');
	return clip;
}

function keylimited(str,keyfunc){
	var l = str.length;
	var oKey = (window.event) ? 'keyCode' : 'which';
	var o = {};
	for(var i=0;i<l;i++){
		o[oKey] = str.charCodeAt(i);
		if(!keyfunc(o,str.substring(0,i)))
			return false;
	}
	return true;
}

function keylimiter(evt, keypattern) {
    var e = evt || window.event;
    var key = (window.event) ? e.keyCode : e.which;
    var keychar = String.fromCharCode(key);

    //special keys...
    if ((key == null) || (key == 0 /* alt/ctrl without any other keys*/) || (key == 8 /* backspace */) || 
    (key == 9 /* tab */) || (key == 13 /* enter */) || (key == 27 /* escape */) || e.ctrlKey /* control, possibly with other keys */)
    {
        return; //Return without handling the event. Let these propagate up for others to handle.
    }

    else if (((keypattern).indexOf(keychar) > -1))
        return true;
    else
        return false;

}

function toTitleCase(str) {
    return str.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
}

function isDate(date) {
    if (date instanceof Date) {
        //other checks, i.e. formatting

        return true;
    } else {
        //is this thing castable as a date
    }
    return false;
}
//converts javascript string datetime to datepicker datetime format
//e.g. '2014-08-20T15:30:00' to '08/20/2014 03:30 pm'
function jsDateTimeConvert(dt){
	if(dt === null)
		return '';
		
	var tt='am';
	var nextD = ['-','-','T',':',':'];
	//yr,m,d,hh,mm
	var dtArr = Array(nextD.length);
	var i=0;
	var idx=0;
	for(var j=0;j<nextD.length;j++){
		idx = dt.indexOf(nextD[j],i);
		dtArr[j] = dt.substring(i,idx);
		i = idx+1;
	}
	dtArr[3] = parseInt(dtArr[3]);
    if(dtArr[3] === 0){
        dtArr[3] = 12;
    }
	if(dtArr[3] > 12){
		dtArr[3] = dtArr[3] - 12;
		tt = 'pm';
	}
	if(dtArr[3] < 10)
		dtArr[3] = '0'+dtArr[3];
			
	return ''.concat(dtArr[1],'/',dtArr[2],'/',dtArr[0],' ',dtArr[3],':',dtArr[4],' ',tt);
}
/*
    given an object parameter, returns a DOM link object <a> with the appropriate display class and link for viewing an SDF form
    edata should have properties:
        fexist - int that represents the boolean state of a form existing on SCAMP ePortal (e.g. 1 means it exists)
        fname - string that is the name of SDF form (e.g. 'CP_SDF2' for Chestpain scamp SDF 2)
        encurl (optional) - url to PDF version of SDF form (if SCAMP ePortal form doesn't exist)
        subid - id of inpatient encounter subunit
    example definition of an edata parameter
        var edata = {fexist:1,fname:'ISOLATED_SDF1_V1',apptid:10000003175};
*/
function createSDFurl(edata) {
    if (!edata || !edata.apptid)
        return '';

    var isPDF = false;//((edata.encurl) && edata.encurl.toLowerCase().indexOf('pdf') >= 0);
    //var webReady = (edata.fexist === 1);
    //all forms should be viewable
    var webReady = true;
    //base icon class for SDF link
    var eclass = 'eicon';
    var sdfURL;
    //create generate SDF url
    //note that an exception is made for any FOOD forms as they are not in database and wouldn't technically be considered 'webready'
    if (webReady || (edata && edata.fname && edata.fname.indexOf('FOOD_') !== -1)) {
        /*sdfURL = '/Forms/FSDF.aspx?SDF=' + edata.fname + '&APPT_ID=' + edata.apptid + '&VISIT_TYPE=' + edata.encd;
        if (edata.encd === 'I')
        sdfURL = sdfURL + '&SUBUNIT_ID=' + edata.subid;
        */
        webReady = true;
        sdfURL = '/patients/encounter/' + edata.apptid;
        if (edata.encd === 'I' && edata.subid)
            sdfURL = sdfURL + '?subunit=' + edata.subid;
    }
    else if (isPDF)
        sdfURL = edata.encurl;

    var iclass;
    var tooltip = '';
    var hasSDFlink = false;
    switch (edata.enstat) {
        case null:
        case '': //blank case, but form should be available
            //iclass = webReady ? ' iedit' : ' ilock';
            iclass = webReady ? ' iedit icon-clipboard' : ' ilock icon-lock-1';
            tooltip = webReady ? 'View/Edit SDF' : 'WebForm not available';
            break;
        case 'IN_PROGRESS_INCOMPLETE_SCANNED':
            iclass = ' ipacket icon-clipboard';
            tooltip = 'View scanned packet';
            break;
        case 'NOT_SCAMP_VISIT':
            iclass = ' iNA icon-cancel-outline';
            tooltip = 'Not a SCAMPs visit';
            sdfURL = false;
            break;
        case 'COMPLETED':
            iclass = webReady ? ' iedit icon-clipboard' : ' ilock icon-lock-1';
            tooltip = webReady ? 'Encounter Completed, open WebForm' : 'Encounter Completed, no WebForm';
            hasSDFlink = webReady;
            break;
        case 'IN_PROGRESS_NO_DATA':
            iclass = webReady ? ' iedit icon-clipboard' : (isPDF ? ' ipdf icon-doc' : '');
            tooltip = webReady ? 'View/Edit SDF' : 'WebForm not available, view PDF';
            hasSDFlink = webReady;
            break;
        default:
            iclass = webReady ? ' iedit icon-clipboard' : (isPDF ? ' ipdf icon-doc' : '');
            tooltip = webReady ? 'Encounter In Progress, open WebForm' : 'WebForm not available, view PDF';
            break;
    }
    
    if(edata.fname === null){
    	sdfURL = null;
    	iclass = null;
    }

    if (!sdfURL) {
        var ss = document.createElement('SPAN');
        ss.className = (iclass) ? (eclass + iclass) : eclass;
        return ss;
    }

    var a = document.createElement('A');
    a.href = sdfURL;
	//remove new tab
    //a.setAttribute('target', '_sdf');
    a.className = (iclass) ? (eclass + iclass) : eclass;
    if (tooltip)
        a.setAttribute('title', tooltip);
    return a;
}

//======== jquery pluggin to center element in client ========//
$.fn.centerInClient = function (options) {
    var opt = { forceAbsolute: false,
        container: window,    // selector of element to center in
        completeHandler: null,
        minX: 0,
        minY: 0
    };
    $.extend(opt, options);

    return this.each(function (i) {
        var el = $(this);
        var jWin = $(opt.container);
        var isWin = opt.container == window;
        // force to the top of document to ENSURE that 
        // document absolute positioning is available
        if (opt.forceAbsolute) {
            if (isWin)
                el.remove().appendTo("body");
            else
                el.remove().appendTo(jWin.get(0));
        }

        // have to make absolute
        el.css("position", "absolute");

        // height is off a bit so fudge it
        var heightFudge = isWin ? 2.0 : 1.8;

        var x = (isWin ? jWin.width() : jWin.outerWidth()) / 2 - el.outerWidth() / 2;
        var y = (isWin ? jWin.height() : jWin.outerHeight()) / heightFudge - el.outerHeight() / 2;

        el.css("left", x < opt.minX ? opt.minX + jWin.scrollLeft() : x + jWin.scrollLeft());
        el.css("top", y < opt.minY ? opt.minY + jWin.scrollTop() : y + jWin.scrollTop());

        // if specified make callback and pass element
        if (opt.completeHandler)
            opt.completeHandler(this);
    });
};

// querystring parameters
(function ($) {
    $.parameter = (function (e) {
        if (e == "") return {};
        var b = {};
        for (var i = 0; i < e.length; ++i) {
            var pm = e[i].split('=');
            if (pm.length != 2) continue;
            b[pm[0]] = decodeURIComponent(pm[1].replace(/\+/g, " "));
        }
        return b;
    })(window.location.search.substr(1).split('&'))
})($);

function qs(name) { 
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]'); 
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)', 'i'); 
    results = regex.exec(location.hash);
	if(!results)
		results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, ' ')); 
};

// zaphod - plug in to set focus on next visible field in a form...
$.fn.focusNextVisible = function () {
        return this.each(function () {
            var fields = $(this).parent('form:eq(0),body').find('button,input,textarea,select').not(':hidden');
            var index = fields.index(this);
            if (index > -1 && (index + 1) < fields.length) {
                fields.eq(index + 1).focus();
            }
            else { fields.first().focus(); }
            return false;
    });
};

//state manager for modals
(function (win) {
    //statemgr definition: currstate, states (state list array)
    win.StateMgr = function(obj) {
        if (typeof obj !== 'object')
            return null;
        if (!(this instanceof StateMgr))
            return new StateMgr(obj);

        this.PrevState = null;
        this.NextStates = [];

        if (obj.currstate)
            this.CurrState = obj.currstate;
        else
            this.CurrState = 'start';

        if (obj.states)
            this.StateList = obj.states;


        if (this.StateList && this.StateList[this.CurrState]) {
            //populate next states
            var cs = this.StateList[this.CurrState];
            if (cs.nextStates) {
                var sl = cs.nextStates;
                for (var i = 0; i < sl.length; i++)
                    this.NextStates.push(sl[i]);
            }
            //run action for starting state if applicable
            if (cs.action && typeof cs.action === 'function')
                cs.action.apply(this);
        }
    }
    StateMgr.prototype = {
        _clearNextStates: function () {
            if (this.NextStates && this.NextStates.length) {
                for (var i = 0; i < this.NextStates.length; i++) {
                    delete this.NextStates[i];
                }
				this.NextStates.length = 0;
            }
        },
        _setCurrentStates: function () {
            if (this.NextStates && this.NextStates.length === 0) {
                var s = this.CurrState;
                if (s && this.StateList && this.StateList[s] && this.StateList[s].nextStates) {
                    var l = this.StateList[s].nextStates;
                    for (var i = 0; i < l.length; i++)
                        this.NextStates.push(l[i]);
                }
            }
        },
        Transition: function (n) {
            if (typeof n !== 'string')
                return false;
			//verify that we have a statelist table, current state, and a table entry for current state
			var stateObj;
			//verify state
            if (this.StateList && this.CurrState && (stateObj = this.StateList[n])) {
				var o;
				//verify if valid next state
				if((o = this.NextStates) && typeof o.indexOf === 'function' && o.indexOf(n) !== -1){
					this.PrevState = this.CurrState;
					this.CurrState = n;
					this._clearNextStates();
					this._setCurrentStates();
					if (stateObj.action && typeof stateObj.action === 'function')
						stateObj.action.apply(this);
					return true;
				}
			}
			return false;
        }
    };
})(window);
//end modal state manager

//patient modal 2.0
var patAjax = {
    loadPatient: function (_mrn) {
        var frm = document.getElementById('frmPatient');

        if (typeof _mrn !== 'string') {
            if (typeof getPatMrn === 'function')
                _mrn = getPatMrn();
            else
                return;
        }

        var payload = 'mrn=' + _mrn;
        $('.ptOpen').attr('href', '/patient/' + _mrn);
        patModalMgr.Transition('wait');
        $.ajax({
            url: frm.action,
            type: 'GET',
            data: payload,
            complete: function (j, t) {
                $('body').css('cursor', '');
                var obj;
                var frm = document.getElementById('frmPatient');
                try {
                    obj = $.parseJSON(j.responseText);
                } catch (e) { }

                var msgout = $(frm).find('.frmmsg');

                if (j.status === 200 && obj.patient) {
                    //data massaging
                    if (!obj.patient.comment)
                        obj.patient.comment = '';
                    if (obj.patient.dob.length > 10)
                        obj.patient.dob = obj.patient.dob.substring(0, 10).replace(/-/g, '/');

                    if (typeof obj.patient.gender === 'string' && obj.patient.gender.length === 1)
                        $('[name=gender]').val(obj.patient.gender);

                    var fields = ['mrn', 'fname', 'lname', 'dob', 'comment'];
                    for (var i = 0; i < fields.length; i++) {
                        if (obj.patient[fields[i]] && frm[fields[i]])
                            $(frm[fields[i]]).val(obj.patient[fields[i]]);
                    }
                    patModalMgr.Transition('update');
                    $('.overlay').css('display', 'block');
                    $('.patientmanage').show().centerInClient();
                    return;
                }
                //otherwise, error
            }
        });
    },
    updatePatient: function () {
        var frm = document.getElementById('frmPatient');
        var payload = 'r=up&' + $(frm).serialize();
        patModalMgr.Transition('wait');
        $.ajax({
            url: frm.action,
            type: 'POST',
            data: payload,
            complete: function (j, t) {
                $('body').css('cursor', '');

                var msgout = $(frm).find('.frmmsg');

                var obj
                try {
                    obj = $.parseJSON(j.responseText);
                }
                catch (e) { }

                if (obj) {
                    if (obj.error === false) {
                        msgout.text(obj.msg);
                        if ($('.patientmanage').length > 0) {
                            var fields = ['mrn', 'fname', 'lname', 'comment'];
                            var vals = {};
                            for (var i = 0; i < fields.length; i++) {
                                vals[fields[i]] = $('.patientmanage [name=' + fields[i] + ']').val();
                            }
                            $('.patientheader .fullname').text(toTitleCase(vals.fname) + ' ' + toTitleCase(vals.lname));
                            if (typeof vals.comment === 'string' && vals.comment.length > 0) {
                                $('.patientheader .comments p').text(vals.comment);
                                $('i.icon-chat').removeClass('none');
                            }
                            else {
                                $('i.icon-chat').addClass('none');
                            }
                        }
                        //reset then close modal
						storehandler.patient.add($('.patientmanage [name=mrn]').val());
						
                        $('.overlay').css('display', 'none');
                        $('.patientmanage').hide();
                        return;
                        patModalMgr.Transition('reset');
                    }
                    else {
                        msgout.text('error: ' + obj.msg);
                    }
                    patModalMgr.Transition('update');
                }
                else {
                    msgout.text('error updating patient-- server error.');
                    patModalMgr.Transition('update');
                }
            }
        });
    },
    insertPatient: function () {
        var frm = document.getElementById('frmPatient');
        var payload = 'r=ip&' + $(frm).serialize();
        patModalMgr.Transition('wait');
        $.ajax({
            url: frm.action,
            type: 'POST',
            data: payload,
            complete: function (j, t) {
                $('body').css('cursor', '');

                var msgout = $(frm).find('.frmmsg');

                var obj
                try {
                    obj = $.parseJSON(j.responseText);
                }
                catch (e) { }

                if (obj) {
                    if (obj.error === false) {
                        msgout.text(obj.msg);
                        patModalMgr.Transition('update');
						storehandler.patient.add($(frm).find('[name=mrn]:enabled').val());
                    }
                    else {
                        msgout.text('error: ' + obj.msg);
                        patModalMgr.Transition('insert');
                    }
                }
                else {
                    msgout.text('error inserting patient-- server error.');
                    patModalMgr.Transition('insert');
                }
            }
        });
    },
    findPatient: function () {
        var frm = document.getElementById('frmPatient');
        var payload = $(frm).serialize();
        window._mrn = '???';
        for (var i = 0; i < frm.mrn.length; i++) {
            if (frm.mrn[i].value.length > 0)
                window._mrn = frm.mrn[i].value;
        }
        $('.ptOpen').attr('href', '/patient/' + _mrn);
        patModalMgr.Transition('wait');
        $.ajax({
            url: frm.action,
            type: 'GET',
            data: payload,
            complete: function (j, t) {
                $('body').css('cursor', '');

                var obj;
                var frm = document.getElementById('frmPatient');
                try {
                    obj = $.parseJSON(j.responseText);
                } catch (e) { }

                var msgout = $(frm).find('.frmmsg');

                if (obj.patient) {

                    if (obj.external === true) {
                        $(msgout).text('Patient \'' + _mrn + '\' found in external database. External record can be inserted.');
                        patModalMgr.Transition('insert');
                    }
                    else {
                        $(msgout).text('Patient \'' + _mrn + '\' already exists.');
                        patModalMgr.Transition('update');
                    }

                    //data massaging
                    if (!obj.patient.comment)
                        obj.patient.comment = '';
                    if (obj.patient.dob.length > 10)
                        obj.patient.dob = obj.patient.dob.substring(0, 10).replace(/-/g, '/');

                    var fields = ['mrn', 'fname', 'lname', 'dob', 'comment', 'gender'];
                    for (var i = 0; i < fields.length; i++) {
                        if (obj.patient[fields[i]] && frm[fields[i]])
                            $(frm[fields[i]]).val(obj.patient[fields[i]]);
                    }
                }
                //patient does not exist
                else if (obj.error === false) {
                    $(msgout).text(obj.msg);
                    patModalMgr.Transition('insert');
                }
                else if (obj.error === true) {
                    $(msgout).text('error finding patient: ' + obj.msg);
                    patModalMgr.Transition('find');
                }
                else {
                    $(msgout).text('Server error');
                    patModalMgr.Transition('find');
                }
                $(frm).find('input[type=hidden][name=mrn]').val(_mrn);
                _mrn = null;
            }
        });
    }
};
var patModalStates = {
    start: { nextStates: ['init'], action: function () { this.Transition('init'); } },
    init: { nextStates: ['find', 'load', 'reset'],
        action: function () {
            //bind all the controls
            var target = '#frmPatient';

            //handle enter, tab, esc
            $(target).find('[name=mrn]').keydown(function (e) {
                if (patModalMgr.CurrState !== 'find')
                    return;
                var val = $(this).val();
                if (!val || val.length <= 0)
                    return;
                var key;
                if ((key = e.keyCode) || (key = e.which)) {
                    var codes = [13, 9, 27];
                    if (codes.indexOf(key) !== -1)
                        patAjax.findPatient();
                }
            });

            $('.ptSearch').click(function () {
                if (patModalMgr.CurrState !== 'find')
                    return;
                patAjax.findPatient();
            });
            
            $('.ptFind').click(function(){
                if(patModalMgr.CurrState !== 'reset')
                patModalMgr.Transition('reset');
                patModalMgr.Transition('find');
                $('.overlay').css('display','block');
                $('.patientmanage').show().centerInClient();
            });

            $(target).find('.ptCreate').click(function () {
                patAjax.insertPatient();
            });
            $(target).find('.ptUpdate').click(function () {
                patAjax.updatePatient();
            });
            $('.ptView').click(function (e) {
	    	var _mrn = this.getAttribute('data-mrn');
                e.preventDefault();
		patAjax.loadPatient(_mrn);
            });

            $(target).find('.ptClose').click(function () {
                $('.overlay').css('display', 'none');
                $('.patientmanage').hide().centerInClient();
                patModalMgr.Transition('reset');
            });

            //reset condition
            this.Transition('reset');
        }
    },
    wait: { nextStates: ['create','insert', 'update', 'reset','find'], action: function () {
		$('body').css('cursor', 'wait');
        	$('#frmPatient [data-flag]').attr('disabled', 'disabled');
        	$('#frmPatient .enClose').removeAttr('disabled');
    	}
    },
    reset: { nextStates: ['update','find','load','reset'], action: function () { 
			$('#frmPatient .frmmsg').text(''); 
			$('#frmPatient .frmfields [data-flag]').attr('checked','').val(''); 
		}
	},
    load: { nextStates: ['wait', 'reset'], action: function () {}
    },
	find: { nextStates: ['wait', 'reset'], action: function () {
			var target = '#frmPatient';
			$(target).find('[data-flag]').attr('disabled', 'disabled').parent().hide();
			$(target).find('[data-flag~=F]').removeAttr('disabled').parent().show();
		}
	},
    insert: { nextStates: ['wait', 'reset'], action: function () {
			var target = '#frmPatient';
			$(target).find('[data-flag]').attr('disabled', 'disabled').parent().hide();
			$(target).find('[data-flag~=C]').removeAttr('disabled').parent().show();
		}
    },
    update: { nextStates: ['wait', 'reset'], action: function () {
        var target = '#frmPatient';
			$(target).find('[data-flag]').attr('disabled', 'disabled').parent().hide();
			$(target).find('[data-flag~=U]').removeAttr('disabled').parent().show();
		}
    }
};
//end patient
//============================================================//

//start Ternary Tree -- Used for autocompletion
//ternary tree
(function(){
	window.TTree = function(obj){
		if(typeof obj !== 'object')
			return null;
		if(!(this instanceof TTree))
			return new TTree(obj);
		
		this._root = null;
	}
	
	TTree.prototype = {
		add: function(str,i,node,prop){
			if(typeof str !== 'string')
				return;
			if(typeof node === 'undefined'){
				this.add(str,0,this,'_root');
				return;
			}
			else if(!node[prop]){
				node[prop] = {character:str.charAt(i),nameEnd:false};
			}
			
			if(str.charAt(i) < node[prop].character){
				this.add(str,i,node[prop],'Left');
			}
			else if(str.charAt(i) > node[prop].character){
				this.add(str,i,node[prop],'Right');
			}
			else{
				if(i + 1 == str.length){
					node[prop].nameEnd = true;
				}
				else{
					this.add(str,i+1,node[prop],'Center');
				}
			}
		},
		contains: function(str){
			if(typeof str !== 'string')
				return;
			var i = 0;
			var curr = this._root;
			while(typeof curr !== 'undefined' && curr !== null){
				var state = str.charAt(i) - curr.character;
				if(str.charAt(i) < curr.character){
					curr = curr.Left;
				}
				else if(str.charAt(i) > curr.character){
					curr = curr.Right;
				}
				else{
					i++;
					if(i === str.length){
						return curr.nameEnd;
					}
					curr = curr.Center;
				}
			}
			return false;
		},
		_rmatch: function(node,currstring,result){
			if(typeof node === 'undefined')
				return null;
			
			var str = currstring.concat(node.character);
			if(node.nameEnd === true)
				result.push(str);
				
			this._rmatch(node.Center,str,result);
			this._rmatch(node.Left,currstring,result);
			this._rmatch(node.Right,currstring,result);
			
		},
		matches: function(str){
			var i=0;
			var result = [];
			var curr = this._root;
			//find string's position in tree
			while(typeof curr !== 'undefined' && curr !== null){
				if(str.charAt(i) < curr.character){
					curr = curr.Left;
				}
				else if(str.charAt(i) > curr.character){
					curr = curr.Right;
				}
				else{
					i++;
					if(i === str.length){
						if(curr.nameEnd === true){
							result.push(str);
						}
						this._rmatch(curr.Center,str,result);
						return result;
					}
					curr = curr.Center;
				}
			}
			return result;
		}
	};
	window.TTree = TTree;
})();
//
//============================================================//
(function(){
	var st = {
		setObject: function(key,obj){
			if(typeof JSON === 'undefined' || typeof localStorage === 'undefined')
                		return;
			localStorage.setItem(key,JSON.stringify(obj));
		},
		getObject: function(key,defaultobj){
	 	        if(typeof JSON === 'undefined' || typeof localStorage === 'undefined')
                		return;
			var tmp;
			if ((tmp = localStorage.getItem(key)) == null){
				if(typeof defaultobj === 'object')
					return defaultobj;
				return null;
			}
			try{
				tmp = JSON.parse(tmp);
				return tmp;
			}catch(e){}
			return null;
		}
	};
	window.storehandler = st;

	//
	//============================================================//
	//Add helpers to deal with our known localstorage objects (encounter/patient).

	function addIfMissing(key, val)
	{
		if(typeof val != "string" && val.toString)
			val = val.toString();
		
		var currobj = storehandler.getObject(key, {i:-1,a:new Array(10)});
		if(currobj && currobj.a && currobj.a.indexOf(val) === -1)
		{
			currobj.i = ++currobj.i % currobj.a.length;
			currobj.a[currobj.i] = val;
			
			localStorage.setItem(key,JSON.stringify(currobj));
		}
}

	var en = {
		add: function(id){
			addIfMissing('encounter', id);
			if(typeof updateRecentEncounters === 'function') //This function will only exist if we're on the dashboard.
				updateRecentEncounters();
		}
	};
	
	var pt = {
		add: function(id){
			addIfMissing('patient', id);
			
			if(typeof updateRecentPatients === 'function') //This function will only exist if we're on the dashboard.
				updateRecentPatients();
		}
	};
	
	window.storehandler.encounter = en;
	window.storehandler.patient = pt;
})();

//
//============================================================//
//add startswith and endswith which are certainly handy in C#
if (typeof String.prototype.startsWith != 'function') {
  String.prototype.startsWith = function (str){
    return this.slice(0, str.length) == str;
  };
}
if (typeof String.prototype.endsWith != 'function') {
  String.prototype.endsWith = function (str){
    return this.slice(-str.length) == str;
  };
}

// ZAPHOD - come on javascript - number handling for goodness sake

function isInt(z) {
    return z === + z && z === (z | 0);
}
function isFloat(z) {
    return z === + z && z !== (z | 0);
}

//ZAPHOD quickie little jquery plugins to see if an element exists (2011.06)
$.fn.exists = function () { return $(this).length > 0; }
$.fn.notexists = function () { return $(this).length = 0; }

// set focus without scrolling, or, actually, scroll and scroll back
$.fn.noScrollFocus = function () {
    var x = window.scrollX;
    var y = window.scrollY;
    this.focus();
    window.scrollTo(x, y);
    // so we can chain...
    return this;
};

// ZAPHOD - auto complete replacement
// was originally a jquery project, highly modified for our purposes
function setAutoComplete()
{
    $("select.combobox").combobox();
}
$.widget("ui.combobox", {
    options: {
        strict: false,
    },
    _create: function () {
        var self = this;
        var select = this.element.hide(),
			selected = select.children(":selected"),
			value = selected.val() ? selected.text() : "";
        strict = this.options.strict;
        var input = $("<input />")
          .insertAfter(select)
          .val(value)
          .autocomplete({
              delay: 0,
              minLength: 0,
              source: function (request, response) {
                  var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                  response(select.children("option").map(function () {
                      var text = $(this).text();
                      if (this.value && (!request.term || matcher.test(text)))
                          return {
                              label: text.replace(
                                new RegExp(
                                  "(?![^&;]+;)(?!<[^<>]*)(" +
                                  $.ui.autocomplete.escapeRegex(request.term) +
                                  ")(?![^<>]*>)(?![^&;]+;)", "gi"),
                                "<strong>$1</strong>"),
                              value: text,
                              option: this
                          };
                  }));
              },
              select: function (event, ui) {
                  ui.item.option.selected = true;
                  self._trigger("selected", event, {
                      item: ui.item.option
                  });
              },
              autocomplete: function (value) {
                  this.element.val(value);
                  this.input.val(value);
              },
              change: function (event, ui) {
                  if (!ui.item) {
                      var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex($(this).val()) + "$", "i"),
                      valid = false;
                      select.children("option").each(function () {
                          if (this.value.match(matcher)) {
                              this.selected = valid = true;
                              return false;
                          }
                      });

                      if (!valid) {
                          // if strict is true, then unmatched values are not allowed
                          // likewise, if a generally instanciated combobox also has
                          // the class 'noadd' than adding items is prohibited
                          if (strict || select.hasClass("noadd")) {
                              // remove invalid value, as it didn't match anything
                              $(this).val("");
                              select.val("");
                              return false;
                          } else {
                              // This is a new item the user has typed.  We will accept 
                              // new items and auto add them
                              // it would be nice if the text was submitted by default
                              // but, alas, not the case.  So let's add it to the 
                              // list of options so it does, indeed, get submitted.
                              // first, we remove any previously added item... because Jenn said so, but it has no real function
                              select.children("option.dynamicadd").remove();

                              var newoption = new Option(this.value, this.value);
                              // add a class so we can remove it later, because Jenn.
                              $(newoption).addClass("dynamicadd");
                              $(newoption).attr("selected",true);
                              select.append(newoption);
                              $(this).addClass("newitem");
                              // select.select(newoption);
                              return true;
                          }   
                      }
                  }
                  else
                  {
                      $(this).removeClass("newitem");
                  }
              }
          })
          .addClass("ui-widget ui-widget-content ui-corner-left");

        input.data("ui-autocomplete")._renderItem = function (ul, item) {
            return $("<li></li>")
              .data("item.autocomplete", item)
              .append("<a>" + item.label + "</a>")
              .appendTo(ul);
        };

        $('<button type=\"button\">&nbsp;</button>')
        .attr("tabIndex", -1)
        .attr("title", "Show All Items")
        .insertAfter(input)
        .button({
            icons: {
                primary: "ui-icon-triangle-1-s"
            },
            text: false
        })
        .removeClass("ui-corner-all")
        .addClass("ui-corner-right ui-button-icon")
        .click(function () {
            // close if already visible
            if (input.autocomplete("widget").is(":visible")) {
                input.autocomplete("close");
                return;
            }
            // pass empty string as value to search for, displaying all results
            input.autocomplete("search", "");
            input.focus();
        });
    }
});
