/** make sure we have qunit **/
QUnit.test( "hello test", function( assert ) {
  assert.ok( 1 == "1", "Passed!" );
});

/** getDataSeriesFromTable test **/
QUnit.test( "Get Data series from table test", function dataTest1( assert ) {
		var data = getDataSeriesFromTable("#FormsCompleted", ".GrCategory", ".GrTotal"); 
		var expected = 10;
		actualResult = data.length;
		assert.propEqual(actualResult, expected);
	});
	
/** getDefaultData test - make sure works on test page table **/
QUnit.test( "Default Data Test", function( assert ) {
		assert.expect(3);
		var data = getDefaultData(); 
		var expectedLength = 10;
	
		assert.ok($.isArray(data.myrows));
		actualLengthResult = data.myrows.length;
		assert.propEqual(actualLengthResult, expectedLength);
		assert.deepEqual(data.myrows[0], {Count:"1",Date:"04-2000", Form:"Overt"})
	});
	
/** test default binned data behavior 
should get 2 bins of data back from collection of objects from getBinnedData()
**/
QUnit.test( "Default Binned Data Test", function( assert ) {
		var data = getBinnedData("Form"); 
		assert.ok(data.hasOwnProperty("Family"));
		assert.ok(data.hasOwnProperty("Overt"));
		
	});

	
/** Test to ensure understanding of jQuery extend() **/
QUnit.test( "MergeTest", function( assert ) {
	var expectedMergeResult = {
		orientation: 'v',
		marker: {
			color: "black",
			width: 1
		},
	  type: 'bar'
	};
	var mergObject = {orientation:"v", marker:{color:"black"}};
	actualResult = $().extend(true,barChartDef,mergObject);
	assert.propEqual (actualResult, expectedMergeResult);
	
	var object1 = {
		apple: 0,
		banana: { weight: 52, price: 100 },
		cherry: 97
	};
	var object2 = {
		banana: { price: 200 },
		durian: 100
	}; 
	expectedMergeResult = { 
		apple: 0,
		banana:{ price:200, weight: 52 }, 
		cherry:97,
		durian : 100
	};
	// Merge object2 into object1
	var actualResult = $.extend( true, object1, object2 );
	assert.propEqual (actualResult, expectedMergeResult,"deep $.extend() works as expected");
});

QUnit.test( "ValidateGraphOptions test", function( assert ) {
	var testGraphOptions = {};
	var retVal = validateGraphOptions(testGraphOptions);
	assert.deepEqual(retVal, graphOptionsDefault, "Returned expected default object");
	
	testGraphOptions = {chartLocation:"pieChart"};
	retVal = validateGraphOptions(testGraphOptions);
	assert.deepEqual(retVal, testGraphOptions, "Returned validated object with only chartLocation");
	
	
});