﻿//search handler
(function (win) {
    var params = ['searchtype', 'mrn', 'lname', 'fname', 'pid', 'dob', 'gender', 'scamp_id', 'vtype', 'sflag', 'disp_id', 'har', 'sdate', 'edate', 'prov_id', 'location', 'exactsearch', 'localsearch'];
	var _weChangedHash = false;
	
	var hashChangeHandler = function (evt) { 
		if(_weChangedHash)
		{
			_weChangedHash = false;
			return;
		}

		SearchHandler.resetSearch();
		SearchHandler.readSearchQueryParam();
		SearchHandler.search();
	};
	
	var shdlr = {
        resetSearch: function () {
            $('form')[0].reset();
			$('[name=vtype]').change();
			$('[name=searchtype]:checked').click();
			$('form .error').removeClass('error');
			$('[data-name]').each(function () { var targ = this.getAttribute('data-name'); $('[name="' + targ + '"]').removeAttr('value'); });
        },
        readSearchQueryParam: function () {
            var hasParam = false;
            for (var i = 0, l = params.length; i < l; i++) {
                var val = qs(params[i]);
				
				if(params[i] == 'localsearch' && val == '0')
				{
					//localsearch is backwards from everything else.
					var filtered = $('[name=localsearch]');
					if(filtered.is(':checked'))
						filtered.click();
					
					return;
				}
				
                if (typeof val === 'string' && val.length > 0) {
                    hasParam = true;
					
					var formElement = $('[name=' + params[i] + ']');
					
					if(formElement.attr('type') == 'radio' || formElement.attr('type') == 'checkbox')
					{
						var filtered = formElement.filter('[value=' + val +']');
						if(!filtered.is(':checked'))
							filtered.click();
					} else 
						formElement.val(val).change();
                }
            }
            if (hasParam)
                $('[name=init]').val(0);
        },
		updateSearchParam: function () {
			var hash = '#?';
			var functionallyDifferent = false;
			
			for (var i = 0, l = params.length; i < l; i++) {
				var formElement = $('[name=' + params[i] + ']');
				var val;
				
				if(formElement.attr('type') == 'radio' || formElement.attr('type') == 'checkbox')
					val = formElement.filter(':checked').val();
				else
					val = formElement.val();
				
				if(params[i] == 'localsearch')
					if(!val && formElement.length > 0)
						val = '0';
					else
						val = '';
				
				if(val)
					hash += ((hash.length > 2) ? '&' : '') + params[i] + '=' + encodeURIComponent(val);
				
				if(qs(params[i]) != (val) ? val : '')
					functionallyDifferent = true;
			}
			
			if(functionallyDifferent)
			{
				_weChangedHash = true;
				
				if(hash !== '#?')
					location.hash = hash;
				else
					location.hash = '';
			}
			
			var href = '/search' + hash.replace('#', '');
			$('#permalink').prop('href', href);
		},
		search: function ()
		{
			//columns that are for encounter search only, defined by the ordinality in searchData datatable column definition
			var encColumns = [6, 7, 8, 9, 10, 11, 12];
			var searchType = $('input[name="searchtype"]:checked').val();

			$('body').css('cursor', 'progress');
			//get form elements
			var ss = $(this.form).serialize();
			tbl.api().ajax.reload();

			//searchtype 1 is encounters, 0 is patient search
			var setRestcol = (searchType === '1');
			tbl.api().columns(encColumns).visible(setRestcol);
			
			SearchHandler.updateSearchParam();
		}
    };
    win.SearchHandler = shdlr;
	
	$(function() { $(window).on('hashchange',  hashChangeHandler); });
})(window);

var ddlData = { prov: [], loc: [], scamps:[]};
var tbl;

$(document).ready(function () {
    $('.searchpanel form').keypress(function (e) { if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) $('.searchpanel form input[type=submit]').trigger('click'); });

    $('.searchpanel form input[type=submit]').click(function (e) {
        e.preventDefault();
        SearchHandler.search();
    });

    //$('.rollup').click(function (e) {
    //    $(this).next().slideToggle(200);
    //    //▲▼▲
    //    $(this).text($(this).text() === '\u25B2' ? '\u25BC' : '\u25B2');
    //});

    $('select[name=vtype]').change(function () {
        currval = $(this).val();
        var target = $('input[data-name=location]');

        var lut = { 'I': 4, 'O': 3, 'E': 5 };

        $(target).val('');
        $('input[type=hidden][name=' + target.attr('data-name') + ']').val('');

        if (currval === '') {
            $(target).attr('disabled', 'disabled');
            return;
        }

        $(target).attr('data-cat', lut[currval]).removeAttr('disabled')
		    .autocomplete('option', 'source', DOMBindHelper.getDLdataSrc(target));

    });
	
	$("#searchtype1").on("click", function (e) {
        $(".encounterfields").show();
        $(".locfields").show();
        $(".patfields").removeClass("bottomborder");
    });
    $("#searchtype2").on("click", function (e) {
        $(".encounterfields").hide();
        $(".locfields").hide();
        $(".patfields").addClass("bottomborder");
    });
	
	//Required because Chrome/FireFox will remember whether it was checked or not when using back/forward from other pages.
	if($("#permalink_ID").is(':checked'))
		$('#permalink').show();
	else
		$('#permalink').hide();

	$("#permalink_ID").on("click", function (e) {
		$('#permalink').toggle(100);
	});
	
	$.ajax({ async: false, type: 'POST', url: '/search.ajax', data: 'opt=0,1,2,3,4,5',
        complete: function (j, t) {
            if (j.status === 200) {
                var obj = $.parseJSON(j.responseText);
                if (obj) {
                    for (var i = 0; i < obj.length; i++) {
                        switch (obj[i].c) {
                            case 0:
                                ddlData.scamps.push(obj[i]);
                                break;
                            case 1:
                            case 2:
                                ddlData.prov.push(obj[i]);
                                break;
                            case 3:
                            case 4:
                            case 5:
                                ddlData.loc.push(obj[i]);
                                break;
                            default:
                                break;
                        }
                    }
                    ddlData.scamps.sort(function (a, b) { return a.label < b.label ? -1 : 1; });
                }
            }
			//bind options to input controls
			$('.sourcedOpts').each(DOMBindHelper.initSelect);
			$('.autocomplete').each(DOMBindHelper.initAutoComplete);
			$('#clearsearch').click(SearchHandler.resetSearch);
			
			SearchHandler.readSearchQueryParam();
        }
    });

    if($("#searchtype2:checked").length == 1) {
        $(".encounterfields").hide();
        $(".locfields").hide();
        $(".patfields").addClass("bottomborder");
    };

    tbl = $('#searchData').dataTable({
        ajax: {
            url: '/search.ajax',
            type: 'POST',
            //initial search should return a provider's encounters...
            data: function () { return $('.searchpanel form').serialize(); },
            complete: function () {
                $('body').css('cursor', 'default');
                $('input[name="init"]').val('0');
            }
        },
        stateSave: true,
        // patient columns: [0,2,3,4,5,6]
        // encounter columns: ALL
        // xor: [1,7,8,9,10,11,12]
        columns: [
            { data: null, title: '', defaultContent: '&nbsp;', orderable: false, className: 'nowrap toolicons' },
            { data: 'mrn', title: 'MRN', defaultContent: '&nbsp;' },
            { data: 'lname', title: 'Last', defaultContent: '&nbsp;' },
            { data: 'fname', title: 'First', defaultContent: '&nbsp;' },
            { data: 'dob', title: 'DOB', defaultContent: '&nbsp;', className: 'nowrap' },
            { data: 'gender', title: 'Gender', defaultContent: '&nbsp;', className: 'center' },
            { data: 'prov', title: 'Provider', defaultContent: '&nbsp;' },
            { data: 'date', title: 'Date', defaultContent: '&nbsp;', className: 'nowrap' },
            { data: 'location', title: 'Location', defaultContent: '&nbsp;' },
            { data: 'visit', title: 'Visit', defaultContent: '&nbsp;' },
            { data: 'scamp', title: 'SCAMP', defaultContent: '&nbsp;', className: 'center' },
            { data: 'encounter', title: 'Form', defaultContent: '&nbsp;', className: 'max' },
            { data: 'status', title: 'Status', defaultContent: '&nbsp;', className: 'max' }
        ],
        order: [[2, 'asc']],
        oLanguage: {
            sSearch: 'Filter:'
        },
        lengthMenu: [[25, 50, 100, -1], [25, 50, 100, 'All']],
        createdRow: function (row, data, index) {
            $(row.children[0]).empty();
            if (data.pdata) {
                var a = document.createElement('A');

                a.className = data.pdata + ' icon-user-3';
                a.setAttribute('title', 'View Patient');
                a.setAttribute('href', '/patient/' + data.mrn);
                //remove new tab
                //a.setAttribute('target', '_pat');
                $(row.children[0]).append(a);
            }

            if (data.edata) {
                var a = createSDFurl(data.edata);
                $(row.children[0]).append(a);
            }
            var ioff = 0;
            //if (data.edata && data.edata.apptid)
            //ioff;

            if (data.lname)
                $(row.cells[2 + ioff]).text(toTitleCase(data.lname));
            if (data.fname)
                $(row.cells[3 + ioff]).text(toTitleCase(data.fname));
            if (data.dob)
                $(row.children[4 + ioff]).text(data.dob.substring(0, data.dob.indexOf('T')));
            if (data.date)
                $(row.children[7 + ioff]).text(data.date.substring(0, 16).replace('T', ' '));
        }
    });

    //handles table click
    tbl.on('click', function (e) {
        if (!e.target)
            return;
		
        var patele = $(e.target.parentElement).find('a.icon-user-3');
        var href;

        if (patele.length <= 0 || !(href = patele.attr('href')))
            return;

        if (patele.hasClass('icon-patExt')) {
            e.preventDefault();
            var idx = href.lastIndexOf('/');
            if (idx === -1)
                return;
            if (++idx >= href.length)
                return;
            href = href.substring(idx);
            patModalMgr.Transition('reset');
            patModalMgr.Transition('find');
            $('#frmPatient input[name=mrn]').val(href);
            patAjax.findPatient();
            $('.overlay').css('display', 'block');
            $('.patientmanage').show().centerInClient();
        }
        else if(e.target.tagName.toUpperCase() != 'A') {
            window.location = href;
        }
    });

	SearchHandler.updateSearchParam();
	
    patModalMgr = StateMgr({ states: patModalStates });
});