﻿//============ modal control methods ===============


(function ($) {





    $.modal.defaults = {
        appendTo: 'body',
        setfocus: true,

        //container
        modalID: '',    // no id by default
        modalClass: 'modal',
        draggable: 'true',
        containerCSS: {},
        resize: true,
        center: true,
        zIndex: 1000,

        //content
        contentID: '',  // no id by default
        contentClass: 'contents',
        contentCSS: {},

        // overlay
        overlayopacity: 50,
        overlayID: 'simplemodal-overlay',
        overlayCSS: {},

        // actions
        close: true,  // render the close button
        closeHTML: '<div class="box"><i class="icon-cancel-1 modalClose"></i></div>',
        escClose: true,
        onOpen: null,
        onShow: null,
        onClose: null
    };

});



function Modal(contents, title, buttons) {
    this.title = title;

    switch(contents)
    {
        // allow literal naming
        case 'literal':

            break;
        default:
            // get the form contents from the server
            $.get("modal.ajax?what=" + contents, function (data) {
                var ra = $.parseJSON(data);
                this.contents = ra.markup;
                this.modal = ra.modal;
            });
            break;
    }

    // set up the buttons...





};
